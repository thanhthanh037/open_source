`include "parameter.v"
module state_machine (clk, rst, wrq1, wrq2, wrq3, wrq4, wrq5, wrq6, wrq7, wrq8, wrq9, wrq10, wrq11, wrq12, wrq13, wrq14, wrq15, 
                      wchannel1, wchannel2, wchannel3, wchannel4, wchannel5, wchannel6, wchannel7, wchannel8, wchannel9, wchannel10, wchannel11, wchannel12, wchannel13, wchannel14, wchannel15,
					  wgr1, wgr2, wgr3, wgr4, wgr5, wgr6, wgr7, wgr8, wgr9, wgr10, wgr11, wgr12, wgr13, wgr14, wgr15, we, wdataout);
 
  input clk, rst;
  input wrq1, wrq2, wrq3, wrq4, wrq5, wrq6, wrq7, wrq8, wrq9, wrq10, wrq11, wrq12, wrq13, wrq14, wrq15;
  input [data_width-1:0] wchannel1, wchannel2, wchannel3, wchannel4, wchannel5, wchannel6, wchannel7, wchannel8, wchannel9, wchannel10, wchannel11, wchannel12, wchannel13, wchannel14, wchannel15;
  output reg wgr1, wgr2, wgr3, wgr4, wgr5, wgr6, wgr7, wgr8, wgr9, wgr10, wgr11, wgr12, wgr13, wgr14, wgr15, we;
  output reg [data_width-1:0] wdataout;
  
  reg [3:0] state;
  
  always@ (posedge clk)
  if (rst) 
    state <= 0;
  else
    case (state)
	   0: 
		  begin
		    wgr1 <= 0;
		    wgr2 <= 0;		
		    wgr3 <= 0;
		    wgr4 <= 0;	
		    wgr5 <= 0;
		    wgr6 <= 0;		
		    wgr7 <= 0;
		    wgr8 <= 0;	
		    wgr9 <= 0;
		    wgr10 <= 0;		
		    wgr11 <= 0;
		    wgr12 <= 0;	
		    wgr13 <= 0;
		    wgr14 <= 0;		
		    wgr15 <= 0;
          we <= 0;			 
        if (wrq1)
		    state <= 1;
		  else if (wrq2)
		    state <= 2;
		  else if (wrq3)
		    state <= 3;
		  else if (wrq4)
		    state <= 4;
		  else if (wrq5)
		    state <= 5;
		  else if (wrq6)
		    state <= 6;
		  else if (wrq7)
		    state <= 7;
		  else if (wrq8)
		    state <= 8;
		  else if (wrq9)
		    state <= 9;
		  else if (wrq10)
		    state <= 10;
		  else if (wrq11)
		    state <= 11;
		  else if (wrq12)
		    state <= 12;
		  else if (wrq13)
		    state <= 13;
		  else if (wrq14)
		    state <= 14;
		  else if (wrq15)
		    state <= 15;	
        end 			 
      1:
		begin
			 wgr1 <= 1;
			 we <= 1;
             wdataout <= wchannel1;
			 if (wchannel1[data_width-2])
			   state <= 0;
        end	
      2:
		begin
			 wgr2 <= 1;
			 we <= 1;
             wdataout <= wchannel2;
			 if (wchannel2[data_width-2])
			   state <= 0;
        end
      3:
		begin
			 wgr3 <= 1;
			 we <= 1;
             wdataout <= wchannel3;
			 if (wchannel3[data_width-2])
			   state <= 0;
        end
      4:
		begin
			 wgr4 <= 1;
			 we <= 1;
             wdataout <= wchannel4;
			 if (wchannel4[data_width-2])
			   state <= 0;
        end
      5:
		  begin
			 wgr5 <= 1;
			 we <= 1;
             wdataout <= wchannel5;
			 if (wchannel5[data_width-2])
			   state <= 0;
        end
      6:
	    begin
			 wgr6 <= 1;
			 we <= 1;
             wdataout <= wchannel6;
			 if (wchannel6[data_width-2])
			   state <= 0;
        end
      7:
		begin
			 wgr7 <= 1;
			 we <= 1;
             wdataout <= wchannel7;
			 if (wchannel7[data_width-2])
			   state <= 0;
        end
      8:
		begin
			 wgr8 <= 1;
			 we <= 1;
             wdataout <= wchannel8;
			 if (wchannel8[data_width-2])
			   state <= 0;
        end
      9:
		begin
			 wgr9 <= 1;
			 we <= 1;
             wdataout <= wchannel9;
			 if (wchannel9[data_width-2])
			   state <= 0;
        end	
      10:
		begin
			 wgr10 <= 1;
			 we <= 1;
             wdataout <= wchannel10;
			 if (wchannel10[data_width-2])
			   state <= 0;
        end
      11:
		begin
			 wgr11 <= 1;
			 we <= 1;
             wdataout <= wchannel11;
			 if (wchannel11[data_width-2])
			   state <= 0;
        end
      12:
		begin
			 wgr12 <= 1;
			 we <= 1;
             wdataout <= wchannel12;
			 if (wchannel12[data_width-2])
			   state <= 0;
        end
      13:
		begin
			 wgr13 <= 1;
			 we <= 1;
             wdataout <= wchannel13;
			 if (wchannel13[data_width-2])
			   state <= 0;
        end
      14:
		begin
			 wgr14 <= 1;
			 we <= 1;
             wdataout <= wchannel14;
			 if (wchannel14[data_width-2])
			   state <= 0;
        end
      15:
		begin
			 wgr15 <= 1;
			 we <= 1;
             wdataout <= wchannel15;
			 if (wchannel15[data_width-2])
			   state <= 0;
        end
    endcase
	 
endmodule
