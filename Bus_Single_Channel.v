//----------------------------------------------------------------------------------------------------------------------
// File         : Bus_Single_Channel.v
// Author       : Thanh Bui
// Organization : The University of Adelaide
// Created      : May 2015
// Platform     : VHDL 1993
// Simulators   : Modelsim
// Synthesizers : ISE 14.7
// Targets      : Virtex-7 VC707
// Description  : Single channel bus customized for FFT interconnection: Centralized arbiter and decoder, fixed priority
//----------------------------------------------------------------------------------------------------------------------
`include "parameter.v"
module Bus_Single_Channel (clk, rst, wrq0, wrq1, wrq2, wrq3, wrq4, wrq5, wrq6, wrq7, wrq8, wrq9, wrq10, wrq11, wrq12, wrq13, wrq14, wrq15, wrq16,
                           waddr0, waddr1, waddr2, waddr3, waddr4, waddr5, waddr6, waddr7, waddr8, waddr9, waddr10, waddr11, waddr12, waddr13, waddr14, waddr15, waddr16,
                           wdata_in0, wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16,				  
                           wgr0, wgr1, wgr2, wgr3, wgr4, wgr5, wgr6, wgr7, wgr8, wgr9, wgr10, wgr11, wgr12, wgr13, wgr14, wgr15, wgr16,
				           we0, we1, we2, we3, we4, we5, we6, we7, we8, we9, we10, we11, we12, we13, we14, we15, we16,
                           wdata_out);
    
  input clk, rst, wrq0, wrq1, wrq2, wrq3, wrq4, wrq5, wrq6, wrq7, wrq8, wrq9, wrq10, wrq11, wrq12, wrq13, wrq14, wrq15, wrq16;
  input [3:0] waddr0, waddr1, waddr2, waddr3, waddr4, waddr5, waddr6, waddr7, waddr8, waddr9, waddr10, waddr11, waddr12, waddr13, waddr14, waddr15, waddr16;
  input [data_width-1:0] wdata_in0, wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16;
  output reg wgr0, wgr1, wgr2, wgr3, wgr4, wgr5, wgr6, wgr7, wgr8, wgr9, wgr10, wgr11, wgr12, wgr13, wgr14, wgr15, wgr16;
  output reg we0, we1, we2, we3, we4, we5, we6, we7, we8, we9, we10, we11, we12, we13, we14, we15, we16;
  output reg [data_width-1:0] wdata_out;
  
  reg [4:0] state;
	 
  always@ (posedge clk)
  if (rst)
	   state <= 0;
  else
    case (state)
	  0: 
		begin
		  wgr0 <= 0;
		  wgr1 <= 0;
		  wgr2 <= 0;		
		  wgr3 <= 0;
		  wgr4 <= 0;	
		  wgr5 <= 0;
		  wgr6 <= 0;		
		  wgr7 <= 0;
		  wgr8 <= 0;	
		  wgr9 <= 0;
		  wgr10 <= 0;		
		  wgr11 <= 0;
		  wgr12 <= 0;	
		  wgr13 <= 0;
		  wgr14 <= 0;		
		  wgr15 <= 0;
	      wgr16 <= 0;
		  we0 <= 0;
		  we1 <= 0;
		  we2 <= 0;		
		  we3 <= 0;
		  we4 <= 0;	
		  we5 <= 0;
		  we6 <= 0;		
		  we7 <= 0;
		  we8 <= 0;	
		  we9 <= 0;
		  we10 <= 0;		
		  we11 <= 0;
		  we12 <= 0;	
		  we13 <= 0;
		  we14 <= 0;		
		  we15 <= 0;
	      we16 <= 0;			 
		  if (wrq0)
		    state <= 1;
		  else if (wrq1)
		    state <= 2;
		  else if (wrq2)
		    state <= 3;
		  else if (wrq3)
		    state <= 4;
		  else if (wrq4)
		    state <= 5;
		  else if (wrq5)
		    state <= 6;
		  else if (wrq6)
		    state <= 7;
		  else if (wrq7)
		    state <= 8;
		  else if (wrq8)
		    state <= 9;
		  else if (wrq9)
		    state <= 10;
		  else if (wrq10)
		    state <= 11;
		  else if (wrq11)
		    state <= 12;
		  else if (wrq12)
		    state <= 13;
		  else if (wrq13)
		    state <= 14;
		  else if (wrq14)
		    state <= 15;
		  else if (wrq15)
		    state <= 16;	 
		  else if (wrq16)
		    state <= 17;
		end
      1:
		begin
		  wgr0 <= 1;
		  case (waddr0)	
		    4'b0000: we1 <= 1; 	 
			4'b0001: we2 <= 1;
			4'b0010: we3 <= 1;		
			4'b0011: we4 <= 1;
			4'b0100: we5 <= 1;
			4'b0101: we6 <= 1;
			4'b0110: we7 <= 1;		
			4'b0111: we8 <= 1;
			4'b1000: we9 <= 1;
			4'b1001: we10 <= 1;
			4'b1010: we11 <= 1;		
			4'b1011: we12 <= 1;
			4'b1100: we13 <= 1;
			4'b1101: we14 <= 1;
			4'b1110: we15 <= 1;		
			4'b1111: we16 <= 1;
		  endcase
          wdata_out <= wdata_in0;
		  if (!wrq0)
			state <= 0;
        end		  
      2:
		begin
		  wgr1 <= 1;
		  case (waddr1)	
			4'b0000: we0 <= 1; 	 
			4'b0001: we2 <= 1;
			4'b0010: we3 <= 1;		
			4'b0011: we4 <= 1;
			4'b0100: we5 <= 1;
			4'b0101: we6 <= 1;
			4'b0110: we7 <= 1;		
			4'b0111: we8 <= 1;
			4'b1000: we9 <= 1;
			4'b1001: we10 <= 1;
			4'b1010: we11 <= 1;		
			4'b1011: we12 <= 1;
			4'b1100: we13 <= 1;
			4'b1101: we14 <= 1;
			4'b1110: we15 <= 1;		
			4'b1111: we16 <= 1;
		  endcase
          wdata_out <= wdata_in1;
		  if (!wrq1)
			state <= 0;
        end	
      3:
		begin
		  wgr2 <= 1;
		  case (waddr2)	
			4'b0000: we0 <= 1; 	 
			4'b0001: we1 <= 1;
			4'b0010: we3 <= 1;		
			4'b0011: we4 <= 1;
			4'b0100: we5 <= 1;
			4'b0101: we6 <= 1;
			4'b0110: we7 <= 1;		
			4'b0111: we8 <= 1;
			4'b1000: we9 <= 1;
			4'b1001: we10 <= 1;
			4'b1010: we11 <= 1;		
			4'b1011: we12 <= 1;
			4'b1100: we13 <= 1;
			4'b1101: we14 <= 1;
			4'b1110: we15 <= 1;		
			4'b1111: we16 <= 1;
		  endcase
          wdata_out <= wdata_in2;
		  if (!wrq2)
			state <= 0;
        end
      4:
		begin
		  wgr3 <= 1;
		  case (waddr3)	
			4'b0000: we0 <= 1; 	 
			4'b0001: we1 <= 1;
			4'b0010: we2 <= 1;		
			4'b0011: we4 <= 1;
			4'b0100: we5 <= 1;
			4'b0101: we6 <= 1;
			4'b0110: we7 <= 1;		
			4'b0111: we8 <= 1;
			4'b1000: we9 <= 1;
			4'b1001: we10 <= 1;
			4'b1010: we11 <= 1;		
			4'b1011: we12 <= 1;
			4'b1100: we13 <= 1;
			4'b1101: we14 <= 1;
			4'b1110: we15 <= 1;		
			4'b1111: we16 <= 1;
		  endcase
          wdata_out <= wdata_in3;
		  if (!wrq3)
			state <= 0;
        end
      5:
		begin
		  wgr4 <= 1;
		  case (waddr4)	
			4'b0000: we0 <= 1; 	 
			4'b0001: we1 <= 1;
			4'b0010: we2 <= 1;		
			4'b0011: we3 <= 1;
			4'b0100: we5 <= 1;
			4'b0101: we6 <= 1;
			4'b0110: we7 <= 1;		
			4'b0111: we8 <= 1;
			4'b1000: we9 <= 1;
			4'b1001: we10 <= 1;
			4'b1010: we11 <= 1;		
			4'b1011: we12 <= 1;
			4'b1100: we13 <= 1;
			4'b1101: we14 <= 1;
			4'b1110: we15 <= 1;		
			4'b1111: we16 <= 1;
		  endcase
          wdata_out <= wdata_in4;
		  if (!wrq4)
			state <= 0;
        end
      6:
		begin
		  wgr5 <= 1;
		  case (waddr5)	
			4'b0000: we0 <= 1; 	 
			4'b0001: we1 <= 1;
			4'b0010: we2 <= 1;		
			4'b0011: we3 <= 1;
			4'b0100: we4 <= 1;
			4'b0101: we6 <= 1;
			4'b0110: we7 <= 1;		
			4'b0111: we8 <= 1;
			4'b1000: we9 <= 1;
			4'b1001: we10 <= 1;
			4'b1010: we11 <= 1;		
			4'b1011: we12 <= 1;
			4'b1100: we13 <= 1;
			4'b1101: we14 <= 1;
			4'b1110: we15 <= 1;		
			4'b1111: we16 <= 1;
		  endcase
          wdata_out <= wdata_in5;
		  if (!wrq5)
			state <= 0;
        end
      7:
		begin
		  wgr6 <= 1;
		  case (waddr6)	
			4'b0000: we0 <= 1; 	 
			4'b0001: we1 <= 1;
			4'b0010: we2 <= 1;		
			4'b0011: we3 <= 1;
			4'b0100: we4 <= 1;
			4'b0101: we5 <= 1;
			4'b0110: we7 <= 1;		
			4'b0111: we8 <= 1;
			4'b1000: we9 <= 1;
			4'b1001: we10 <= 1;
			4'b1010: we11 <= 1;		
			4'b1011: we12 <= 1;
			4'b1100: we13 <= 1;
			4'b1101: we14 <= 1;
			4'b1110: we15 <= 1;		
			4'b1111: we16 <= 1;
		  endcase
          wdata_out <= wdata_in6;
		  if (!wrq6)
			state <= 0;
        end
      8:
		begin
		  wgr7 <= 1;
		  case (waddr7)	
			4'b0000: we0 <= 1; 	 
			4'b0001: we1 <= 1;
		    4'b0010: we2 <= 1;		
			4'b0011: we3 <= 1;
			4'b0100: we4 <= 1;
			4'b0101: we5 <= 1;
			4'b0110: we6 <= 1;		
			4'b0111: we8 <= 1;
			4'b1000: we9 <= 1;
			4'b1001: we10 <= 1;
			4'b1010: we11 <= 1;		
			4'b1011: we12 <= 1;
			4'b1100: we13 <= 1;
			4'b1101: we14 <= 1;
			4'b1110: we15 <= 1;		
			4'b1111: we16 <= 1;
		  endcase
          wdata_out <= wdata_in7;
		  if (!wrq7)
			state <= 0;
        end
      9:
		begin
		  wgr8 <= 1;
		  case (waddr8)	
			4'b0000: we0 <= 1; 	 
			4'b0001: we1 <= 1;
			4'b0010: we2 <= 1;		
			4'b0011: we3 <= 1;
			4'b0100: we4 <= 1;
			4'b0101: we5 <= 1;
			4'b0110: we6 <= 1;		
			4'b0111: we7 <= 1;
			4'b1000: we9 <= 1;
			4'b1001: we10 <= 1;
			4'b1010: we11 <= 1;		
			4'b1011: we12 <= 1;
			4'b1100: we13 <= 1;
			4'b1101: we14 <= 1;
			4'b1110: we15 <= 1;		
			4'b1111: we16 <= 1;
		  endcase
          wdata_out <= wdata_in8;
		  if (!wrq8)
			state <= 0;
        end
      10:
		begin
		  wgr9 <= 1;
		  case (waddr9)	
			4'b0000: we0 <= 1; 	 
			4'b0001: we1 <= 1;
			4'b0010: we2 <= 1;		
			4'b0011: we3 <= 1;
			4'b0100: we4 <= 1;
			4'b0101: we5 <= 1;
			4'b0110: we6 <= 1;		
			4'b0111: we7 <= 1;
			4'b1000: we8 <= 1;
			4'b1001: we10 <= 1;
			4'b1010: we11 <= 1;		
			4'b1011: we12 <= 1;
			4'b1100: we13 <= 1;
			4'b1101: we14 <= 1;
			4'b1110: we15 <= 1;		
			4'b1111: we16 <= 1;
		  endcase
          wdata_out <= wdata_in9;
		  if (!wrq9)
			state <= 0;
        end	
      11:
		begin
		  wgr10 <= 1;
		  case (waddr10)	
			4'b0000: we0 <= 1; 	 
			4'b0001: we1 <= 1;
			4'b0010: we2 <= 1;		
			4'b0011: we3 <= 1;
			4'b0100: we4 <= 1;
			4'b0101: we5 <= 1;
			4'b0110: we6 <= 1;		
			4'b0111: we7 <= 1;
			4'b1000: we8 <= 1;
			4'b1001: we9 <= 1;
			4'b1010: we11 <= 1;		
			4'b1011: we12 <= 1;
			4'b1100: we13 <= 1;
			4'b1101: we14 <= 1;
			4'b1110: we15 <= 1;		
			4'b1111: we16 <= 1;
		  endcase
          wdata_out <= wdata_in10;
		  if (!wrq10)
			state <= 0;
        end
      12:
		begin
		  wgr11 <= 1;
		  case (waddr11)	
			4'b0000: we0 <= 1; 	 
			4'b0001: we1 <= 1;
			4'b0010: we2 <= 1;		
			4'b0011: we3 <= 1;
			4'b0100: we4 <= 1;
			4'b0101: we5 <= 1;
			4'b0110: we6 <= 1;		
			4'b0111: we7 <= 1;
			4'b1000: we8 <= 1;
			4'b1001: we9 <= 1;
			4'b1010: we10 <= 1;		
			4'b1011: we12 <= 1;
			4'b1100: we13 <= 1;
			4'b1101: we14 <= 1;
			4'b1110: we15 <= 1;		
			4'b1111: we16 <= 1;
		  endcase
          wdata_out <= wdata_in11;
		  if (!wrq11)
			state <= 0;
        end
      13:
		begin
		  wgr12 <= 1;
		  case (waddr12)	
			4'b0000: we0 <= 1; 	 
			4'b0001: we1 <= 1;
			4'b0010: we2 <= 1;		
			4'b0011: we3 <= 1;
			4'b0100: we4 <= 1;
			4'b0101: we5 <= 1;
			4'b0110: we6 <= 1;		
			4'b0111: we7 <= 1;
			4'b1000: we8 <= 1;
			4'b1001: we9 <= 1;
			4'b1010: we10 <= 1;		
			4'b1011: we11 <= 1;
			4'b1100: we13 <= 1;
			4'b1101: we14 <= 1;
			4'b1110: we15 <= 1;		
			4'b1111: we16 <= 1;
		  endcase
          wdata_out <= wdata_in12;
		  if (!wrq12)
			state <= 0;
        end
      14:
		begin
		  wgr13 <= 1;
		  case (waddr13)	
			4'b0000: we0 <= 1; 	 
			4'b0001: we1 <= 1;
			4'b0010: we2 <= 1;		
			4'b0011: we3 <= 1;
			4'b0100: we4 <= 1;
			4'b0101: we5 <= 1;
			4'b0110: we6 <= 1;		
			4'b0111: we7 <= 1;
			4'b1000: we8 <= 1;
			4'b1001: we9 <= 1;
			4'b1010: we10 <= 1;		
			4'b1011: we11 <= 1;
			4'b1100: we12 <= 1;
			4'b1101: we14 <= 1;
			4'b1110: we15 <= 1;		
			4'b1111: we16 <= 1;
		  endcase
          wdata_out <= wdata_in13;
		  if (!wrq13)
			state <= 0;
        end
      15:
		begin
		  wgr14 <= 1;
		  case (waddr14)	
			4'b0000: we0 <= 1; 	 
			4'b0001: we1 <= 1;
			4'b0010: we2 <= 1;		
			4'b0011: we3 <= 1;
			4'b0100: we4 <= 1;
			4'b0101: we5 <= 1;
			4'b0110: we6 <= 1;		
			4'b0111: we7 <= 1;
			4'b1000: we8 <= 1;
			4'b1001: we9 <= 1;
			4'b1010: we10 <= 1;		
			4'b1011: we11 <= 1;
			4'b1100: we12 <= 1;
			4'b1101: we13 <= 1;
			4'b1110: we15 <= 1;		
			4'b1111: we16 <= 1;
		  endcase
          wdata_out <= wdata_in14;
		  if (!wrq14)
			state <= 0;
        end
      16:
		begin
		  wgr15 <= 1;
		  case (waddr15)	
			4'b0000: we0 <= 1; 	 
			4'b0001: we1 <= 1;
			4'b0010: we2 <= 1;		
			4'b0011: we3 <= 1;
			4'b0100: we4 <= 1;
			4'b0101: we5 <= 1;
			4'b0110: we6 <= 1;		
			4'b0111: we7 <= 1;
			4'b1000: we8 <= 1;
			4'b1001: we9 <= 1;
			4'b1010: we10 <= 1;		
			4'b1011: we11 <= 1;
			4'b1100: we12 <= 1;
			4'b1101: we13 <= 1;
			4'b1110: we14 <= 1;		
			4'b1111: we16 <= 1;
		  endcase
          wdata_out <= wdata_in15;
		  if (!wrq15)
			state <= 0;
        end
      17:
		begin
		  wgr16 <= 1;
		  case (waddr16)	
			4'b0000: we0 <= 1; 	 
			4'b0001: we1 <= 1;
			4'b0010: we2 <= 1;		
			4'b0011: we3 <= 1;
			4'b0100: we4 <= 1;
			4'b0101: we5 <= 1;
			4'b0110: we6 <= 1;		
			4'b0111: we7 <= 1;
			4'b1000: we8 <= 1;
			4'b1001: we9 <= 1;
			4'b1010: we10 <= 1;		
			4'b1011: we11 <= 1;
			4'b1100: we12 <= 1;
			4'b1101: we13 <= 1;
			4'b1110: we14 <= 1;		
			4'b1111: we15 <= 1;
		  endcase
          wdata_out <= wdata_in16;
		  if (!wrq16)
			state <= 0;
        end		  
    endcase

endmodule