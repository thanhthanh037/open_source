`include "parameter.v"
module NoC_interface_1x4 (clk, rst, x_router, y_router,
						  isend, iaccept, iflit, 
                          done_hidden, done_output, rdata,
						  osend, oaccept, oflit, 
						  w_start, we_i, we_w, re, waddr, wdata);                       

  input clk, rst;  
  input [2:0] x_router, y_router; 
  input isend, iaccept;
  input [flit_data_width-1:0] iflit;
  input done_hidden, done_output;
  input [11:0]  rdata; 
  output reg osend, oaccept;
  output reg [flit_data_width-1:0] oflit;
  output reg  w_start, we_i, we_w, re;  
  output reg [5:0] waddr;  
  output reg [flit_data_width-3:0] wdata;
  
  reg [1:0] r_state;
  reg [2:0] t_state;
  reg [6:0] count_t;

  //receiver
  always@ (posedge clk)
  begin
    if (rst)
      r_state <= 0;
    else
      case (r_state)
        0:
          begin
            oaccept <= 0;
            we_i <= 0;
            we_w <= 0;
			w_start <= 1;
            if (isend)
              begin
                oaccept <= 1;
				r_state <= 1;
			  end
		  end
		1:
		  begin
		    oaccept <= 0;
            if (iflit[flit_data_width-1: flit_data_width-2] == 2'b10)
              begin
                if (iflit[flit_data_width-3])
			      r_state <= 2;
			    else 
				  r_state <= 3;
              end
          end       
        2:
		  begin
            if (!iflit[flit_data_width-1])
			  begin
				w_start <= 0;
                we_i <= 1;
			  end
		    if (iflit[flit_data_width-2])
			  begin
  			    r_state <= 0;              
              end
		  end
        3:
		  begin
            if (!iflit[flit_data_width-1])
              we_w <= 1;
		    if (iflit[flit_data_width-2])
			  begin
   			    r_state <= 0;              
              end
		  end 
        default: r_state <= 0;
      endcase
	wdata <= iflit[flit_data_width-3:0];
    if (iflit[flit_data_width-1])
      case (iflit[flit_data_width-4:flit_data_width-9])
        0: waddr <= 0;		
        1: waddr <= 0;
        2: waddr <= 1;
        3: waddr <= 2;
        4: waddr <= 3;
        5: waddr <= 4;
        6: waddr <= 5;
        7: waddr <= 6;
        8: waddr <= 7;
        9: waddr <= 8;
        10: waddr <= 9;
        11: waddr <= 10;
        12: waddr <= 11;
        13: waddr <= 12;
        14: waddr <= 13;
        15: waddr <= 14;
        16: waddr <= 15;
        17: waddr <= 16;
        18: waddr <= 17;
        19: waddr <= 18;
        20: waddr <= 19;		  
        21: waddr <= 20;
        22: waddr <= 21;
        23: waddr <= 22;
        24: waddr <= 23;
        25: waddr <= 24;
        26: waddr <= 25;
        27: waddr <= 26;
        28: waddr <= 27;
        29: waddr <= 28;
        30: waddr <= 29;	
        31: waddr <= 30;
        32: waddr <= 31;
        33: waddr <= 32;
        34: waddr <= 33;
        35: waddr <= 34;
        36: waddr <= 35;
        37: waddr <= 36;
        38: waddr <= 37;
        39: waddr <= 38;
        40: waddr <= 39;	
        41: waddr <= 40;
        42: waddr <= 41;
        43: waddr <= 42;
        44: waddr <= 43;
        45: waddr <= 44;
        46: waddr <= 45;
        47: waddr <= 46;
        48: waddr <= 47;
        49: waddr <= 48;
        50: waddr <= 49;	
        51: waddr <= 50;
        52: waddr <= 51;
        53: waddr <= 52;
        54: waddr <= 53;
        55: waddr <= 54;
        56: waddr <= 55;
        57: waddr <= 56;
        58: waddr <= 57;
        59: waddr <= 58;
        60: waddr <= 59;	
        61: waddr <= 60;
        62: waddr <= 61;
        63: waddr <= 62;
        default: waddr <= 0;
      endcase	 
  end
  
  //transmiter
  always@ (posedge clk)
  begin
    if (rst)
      t_state <= 0;
    else
      case (t_state)
        0:
          begin
            re <= 0;
            if (done_hidden)
              begin
                osend <= 1;
                oflit <= {3'b101, x_router, y_router, 9'b0};
                t_state <= 1;
              end
			else if (done_output)
              begin
                osend <= 1;
                oflit <= {3'b100, x_router, y_router, 9'b0};
                t_state <= 2;
              end				  
          end
        1:
          begin            
            if (iaccept)
			  begin
				osend <= 0;              
                count_t <= 0;
				re <= 1;
				t_state <= 3;
              end
          end
        3:
          begin
            count_t <= count_t + 1'b1;
			if (count_t != 0)
			  oflit <= {2'b00, 4'b0, rdata};
			if (count_t == op_per_neuron_hidden - 1'b1)
			  re <= 0;
            if (count_t == op_per_neuron_hidden)
              begin
                oflit <= {2'b01, 4'b0, rdata};
                t_state <= 0;
              end              
          end
        2:
          begin            
            if (iaccept)
              begin
				osend <= 0;
                count_t <= 0;
				re <= 1;
				t_state <= 4;
              end
          end	 
        4:
          begin
            count_t <= count_t + 1'b1;
			if (count_t != 0)
			  oflit <= {2'b00, 4'b0, rdata};
			if (count_t == op_per_neuron_hidden -1'b1)
			  re <= 0;				
            if (count_t == op_per_neuron_output)
              begin
                oflit <= {2'b01, 4'b0, rdata};
                t_state <= 0;
              end            
          end
        default: t_state <= 0;
      endcase
  end  
         
endmodule