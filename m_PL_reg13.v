module m_PL_reg13 (clk, sign12, exp12, mult_result3, zero_no2, sign13, exp13, mult_result4, zero_no3); 
  
  input   clk;
  input   sign12;
  input   [11:0] exp12;
  input   [105:0] mult_result3;
  input   [6:0] zero_no2;
  output reg sign13;
  output reg [11:0] exp13;
  output reg [105:0] mult_result4;
  output reg [6:0] zero_no3;
    
  always@ (posedge clk)
    begin
      sign13 <= sign12;
      exp13 <= exp12;
      mult_result4 <= mult_result3;
      zero_no3 <= zero_no2;
    end

endmodule
