`include "parameter.v"
module PE_1x8(clk, rst, x_router, y_router, isend, iaccept, iflit, osend, oaccept, oflit);

  input clk, rst;  
  input [2:0] x_router, y_router;  
  input isend, iaccept;
  input [flit_data_width-1:0] iflit;
  output osend, oaccept;
  output [flit_data_width-1:0] oflit;
  
  wire w_start, re_i, re_w, we_w, re_o, re_co, we_o, we_oi, rst_acc, output_start;
  wire [5:0] waddr;
  wire [flit_data_width-3:0] idata, input_values;
  wire [flit_data_width-3:0] weights, weights_4;  
  wire [11:0] result, odata; 
  
  NoC_interface_1x8 NoC_interface (clk, rst, x_router, y_router,
								   isend, iaccept, iflit, 
								   done_hidden, done_output, odata,
								   osend, oaccept, oflit, 
								   w_start, we_i, we_w, 
								   re_o, waddr, idata);
										 
  control_8 control (clk, rst, we_i, output_start, re_i, re_w, re_co, we_o, we_oi, rst_acc, done_hidden, done_output);

  RAM_i_8 RAM_i (clk, rst, re_i, we_i || we_oi, w_start || output_start, output_start ? x_router*8+y_router: waddr, we_oi ? {4'b0, odata, 112'b0} : idata, output_start, input_values);
 
  RAM_w_8 RAM_w (clk, rst, re_w, we_w, idata, weights);
 
  RAM_o RAM_o (clk, rst, re_o || re_co, we_o, result, odata);
  
  neuron_8x neuron (clk, rst, rst_acc, input_values[15:0], input_values[31:16], input_values[47:32], input_values[63:48], input_values[79:64], input_values[95:80], input_values[111:96], input_values[127:112], weights[15:0], weights[31:16], weights[47:32], weights[63:48], weights[79:64], weights[95:80], weights[111:96], weights[127:112], result);

endmodule