module m_rounding (in, out);
  
  input [104:0] in;
  output [51:0] out;
  reg [51:0] out;

  always@ (*)
  begin
    if (in[53] == 1'b1 || in[52:0] == 53'h1fffffffffffff)
      out = {in[104:54], 1'b1};
    else
      out = in[104:53];
  end

endmodule
      