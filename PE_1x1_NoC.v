`include "parameter.v"
module PE_1x1(clk, rst, x_router, y_router, isend, iaccept, iflit, osend, oaccept, oflit);

  input clk, rst; 
  input [2:0] x_router, y_router; 
  input [flit_data_width-1:0] iflit;
  output osend, oaccept;
  output [flit_data_width-1:0] oflit;
  
  wire w_start, re_i, re_w, we_w, re_o, we_o, rst_acc, output_start;
  wire [5:0] waddr;
  wire [15:0] idata, input_values, weights;  
  wire [11:0] result, odata; 
  
  NoC_interface_1x1 NoC_interface (clk, rst, x_router, y_router,
								   isend, iaccept, iflit, 
								   done_hidden, done_output, odata,
								   osend, oaccept, oflit, 
								   w_start, we_i, we_w, 
								   re_o, waddr, idata);
										 
  control_1 control (clk, rst, we_i, re_i, re_w, we_o, rst_acc, done_hidden, done_output);
  
  RAM_i RAM_i (clk, rst, re_i, we_i, w_start, waddr, idata, output_start, input_values);
  
  RAM_w RAM_w (clk, rst, re_w, we_w, idata, weights);
  
  RAM_o RAM_o (clk, rst, re_o, we_o, result, odata);
  
  neuron_1x neuron (clk, rst, rst_acc, input_values, weights, result);

  
endmodule