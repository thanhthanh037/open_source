module a_PL_reg10 (clk, sign4, exp_nor10, man_sum_nor2, sign5, exp_nor11, man_sum_nor3);
  
  input clk, sign4;
  input [10:0] exp_nor10;
  input [52:0] man_sum_nor2;
  output reg sign5;
  output reg [10:0] exp_nor11;
  output reg [52:0] man_sum_nor3;
  
  always@ (posedge clk)
  begin
    sign5 <= sign4;
    exp_nor11 <= exp_nor10;
    man_sum_nor3 <= man_sum_nor2;
  end

endmodule