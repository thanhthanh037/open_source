module m_PL_reg12 (clk, sign11, exp11, mult_result2, zero5_no1, zero6_no1, zero7_no1, zero8_no1, zero_no, sign12, exp12, mult_result3, zero5_no2, zero6_no2, zero7_no2, zero8_no2, zero_no1); 
  
  input   clk;
  input   sign11;
  input   [11:0] exp11;
  input   [105:0] mult_result2;
  input   [4:0] zero5_no1, zero6_no1, zero7_no1, zero8_no1;
  input   [6:0] zero_no;
  output reg sign12;
  output reg [11:0] exp12;
  output reg [105:0] mult_result3;
  output reg [4:0] zero5_no2, zero6_no2, zero7_no2, zero8_no2;
  output reg [6:0] zero_no1;
    
  always@ (posedge clk)
    begin
      sign12 <= sign11;
      exp12 <= exp11;
      mult_result3 <= mult_result2;
      zero5_no2 <= zero5_no1;
      zero6_no2 <= zero6_no1;
      zero7_no2 <= zero7_no1;
      zero8_no2 <= zero8_no1;
      zero_no1 <= zero_no;
    end

endmodule
