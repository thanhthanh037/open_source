//-----------------------------------------------------------------------------------------------------
// File         : mult.v
// Author       : Thanh Bui
// Organization : The University of Adelaide
// Created      : March 2015
// Platform     : VHDL 1993
// Simulators   : Modelsim
// Synthesizers : ISE 14.7
// Targets      : Virtex-7 VC707
// Description  : IEEE 754 compliant double-precision floating-point multiplier with 17 pipeline stages
//-----------------------------------------------------------------------------------------------------
module mult (clk, a, b, result);
  
  input clk;
  input [63:0] a, b;
  output [63:0] result;

  wire  sign, sign0, sign1, sign2, sign3, sign4, sign5, sign6, sign7, sign8, sign9, sign10, sign11, sign12, sign13, sign14, sign15, sign16;
  wire  [11:0] exp, exp0, exp1, exp2, exp3, exp4, exp5, exp6, exp7, exp8, exp9, exp10, exp11, exp12, exp13, exp_nor, exp_nor1;
  wire  [10:0] exp_nor2, exp_nor3, exp_nor4;
  wire  [52:0] man_a, man_b, a0, b0, a1, b1, a2, b2, a3, a4;
  wire  [47:0] a5, a6;
  wire  [23:0] a7, a8;
  wire  [35:0] b3, b4;
  wire  [18:0] b5, b6;
  wire  [1:0] b7, b8;
  wire  [25:0] a0b3, a0b3_1, a1b3, a1b3_1;
  wire  [40:0] a0b2, a0b2_1, a1b1, a1b1_1, a0b1, a0b1_1, a1b2, a1b2_1, a1b0, a1b0_1, a0b0, a0b0_1;
  wire  [21:0] a2b1, a2b1_1, a2b0, a2b0_1, a2b2, a2b2_1;
  wire  [6:0]  a2b3, a2b3_1;
  wire  [104:0] sum1, sum1_1, sum1_2;
  wire  [105:0] sum2, sum2_1, sum2_2, sum3, sum3_1, sum3_2, sum4, sum4_1, sum4_2;
  wire  [105:0] mult_result, mult_result1, mult_result2, mult_result3, mult_result4;
  wire  [4:0] zero1_no, zero1_no1, zero2_no, zero2_no1, zero3_no, zero3_no1, zero4_no, zero4_no1, zero5_no, zero5_no1, zero5_no2, zero6_no, zero6_no1, zero6_no2, zero7_no, zero7_no1, zero7_no2, zero8_no, zero8_no1, zero8_no2;
  wire  [6:0] zero_no, zero_no1, zero_no2, zero_no3;
  wire  zero_no_msb1;
  wire  [104:0] normalise1_result, normalise1_result1, normalise2_result, normalise2_result1;
  wire  [51:0] rounding_result, rounding_result1; 
	
  assign sign = a[63] ^ b[63];
  assign exp = a[62:52] + b[62:52] - 11'h3ff;
  assign man_a = a[62:0] ? {1'b1, a[51:0]} : {1'b0, a[51:0]};
  assign man_b = b[62:0] ? {1'b1, b[51:0]} : {1'b0, b[51:0]};
  //
  m_PL_reg0 m_PL_reg0 (clk, sign, exp, man_a, man_b, sign0, exp0, a0, b0);
  //
  assign a2b2 = a0[52:48] * b0[50:34];
  assign a1b1 = a0[47:24] * b0[33:17];
  assign a0b0 = a0[23:0] * b0[16:0];
  assign a2b1 = a0[52:48] * b0[33:17]; 
  assign a1b0 = a0[47:24] * b0[16:0];
  //
  m_PL_reg1 m_PL_reg1 (clk, sign0, exp0, a0, b0, a2b2, a1b1, a0b0, a2b1, a1b0, sign1, exp1, a1, b1, a2b2_1, a1b1_1, a0b0_1, a2b1_1, a1b0_1); 
  //
  assign sum1 = {{1'b0, a2b2_1, a1b1_1, a0b0_1[40:24]} + {a2b1_1, a1b0_1}, a0b0_1[23:0]};
  //
  m_PL_reg2 m_PL_reg2 (clk, sign1, exp1, a1, b1, sum1, sign2, exp2, a2, b2, sum1_1);
  //  
  assign a2b0 = a2[52:48] * b2[16:0]; 
  //
  m_PL_reg3 m_PL_reg3 (clk, sign2, exp2, a2, b2[52:17], a2b0[21:0], sum1_1, sign3, exp3, a3, b3, a2b0_1, sum1_2);
  //
  assign sum2 = {a2b0_1 + {1'b0, sum1_2[104:48]}, sum1_2[47:0]};
  //
  m_PL_reg4 m_PL_reg4 (clk, sign3, exp3, a3, b3, sum2, sign4, exp4, a4, b4, sum2_1);
  //
  assign a2b3 = a4[52:48] * b4[35:34];
  assign a1b2 = a4[47:24] * b4[33:17];
  assign a0b1 = a4[23:0] * b4[16:0]; 
  //
  m_PL_reg5 m_PL_reg5 (clk, sign4, exp4, a4[47:0], b4[35:17], a2b3, a1b2, a0b1, sum2_1, sign5, exp5, a5, b5, a2b3_1, a1b2_1, a0b1_1, sum2_2);
  //
  assign sum3 = {{a2b3_1, a1b2_1, a0b1_1} + sum2_2[105:17], sum2_2[16:0]};
  //
  m_PL_reg6 m_PL_reg6 (clk, sign5, exp5, a5, b5, sum3, sign6, exp6, a6, b6, sum3_1);
  //
  assign a1b3 = a6[47:24] * b6[18:17];
  assign a0b2 = a6[23:0] * b6[16:0];
  //
  m_PL_reg7 m_PL_reg7 (clk, sign6, exp6, a6[23:0], b6[18:17], a1b3, a0b2, sum3_1, sign7, exp7, a7, b7, a1b3_1, a0b2_1, sum3_2);
  // 
  assign sum4 = {{a1b3_1, a0b2_1} + sum3_2[105:34], sum3_2[33:0]};
  //
  m_PL_reg8 m_PL_reg8 (clk, sign7, exp7, a7, b7, sum4, sign8, exp8, a8, b8, sum4_1);
  //
  assign a0b3 = a8 * b8;
  //
  m_PL_reg9 m_PL_reg9 (clk, sign8, exp8, a0b3, sum4_1, sign9, exp9, a0b3_1, sum4_2);
  //
  assign mult_result = {a0b3_1 + sum4_2[105:51], sum4_2[50:0]};
  //
  m_PL_reg10 m_PL_reg10 (clk, sign9, exp9, mult_result, sign10, exp10, mult_result1);
  //
  m_leading_zero m_leading_zero1 (mult_result1[103:91], zero1_no);
  m_leading_zero m_leading_zero2 (mult_result1[90:78], zero2_no);
  m_leading_zero m_leading_zero3 (mult_result1[77:65], zero3_no);
  m_leading_zero m_leading_zero4 (mult_result1[64:52], zero4_no);
  m_leading_zero m_leading_zero5 (mult_result1[51:39], zero5_no);
  m_leading_zero m_leading_zero6 (mult_result1[38:26], zero6_no);
  m_leading_zero m_leading_zero7 (mult_result1[25:13], zero7_no);
  m_leading_zero m_leading_zero8 (mult_result1[12:0], zero8_no);
  //
  m_PL_reg11 m_PL_reg11 (clk, sign10, exp10, mult_result1, zero1_no, zero2_no, zero3_no, zero4_no, zero5_no, zero6_no, zero7_no, zero8_no, sign11, exp11, mult_result2, zero1_no1, zero2_no1, zero3_no1, zero4_no1, zero5_no1, zero6_no1, zero7_no1, zero8_no1);
  //
  m_leading_zero_cal1 m_leading_zero_cal1 (mult_result2[105:104], zero1_no1, zero2_no1, zero3_no1, zero4_no1, zero_no);
  //
  m_PL_reg12 m_PL_reg12 (clk, sign11, exp11, mult_result2, zero5_no1, zero6_no1, zero7_no1, zero8_no1, zero_no, sign12, exp12, mult_result3, zero5_no2, zero6_no2, zero7_no2, zero8_no2, zero_no1); 
  //
  m_leading_zero_cal2 m_leading_zero_cal2 (zero5_no2, zero6_no2, zero7_no2, zero8_no2, zero_no1, zero_no2);
  //
  m_PL_reg13 m_PL_reg13 (clk, sign12, exp12, mult_result3, zero_no2, sign13, exp13, mult_result4, zero_no3); 
  //
  assign normalise1_result = mult_result4[104:0] << zero_no3[5:0];
  assign exp_nor = (mult_result4 == 0) ? 12'b0 : (exp13 - zero_no3 + 1'b1);
  //
  m_PL_reg14 m_PL_reg14 (clk, sign13, exp_nor, normalise1_result, zero_no3[6], sign14, exp_nor1, normalise1_result1, zero_no_msb1);
  //
  assign normalise2_result = zero_no_msb1 ? normalise1_result1 << 64 : normalise1_result1;
  assign exp_nor2 = exp_nor1[11] ? 11'h7ff : exp_nor1[10:0];
  //
  m_PL_reg15 m_PL_reg15 (clk, sign14, exp_nor2, normalise2_result, sign15, exp_nor3, normalise2_result1);
  //
  m_rounding m_rounding (normalise2_result1, rounding_result);
  //
  m_PL_reg16 m_PL_reg16 (clk, sign15, exp_nor3, rounding_result, sign16, exp_nor4, rounding_result1);
  //
  assign result = {sign16, exp_nor4, rounding_result1};
  
endmodule    