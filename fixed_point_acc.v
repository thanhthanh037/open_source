//////////////////////////////////////////////////////////////////////////////////
// Company: 	UoA
// Engineer: 	Thanh Bui
// 
// Create Date:    12:53:43 03/02/2017 
// Design Name: 
// Module Name:    fixed_point_acc 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 1-input 16-bit fixed-point accumulator
//
//////////////////////////////////////////////////////////////////////////////////
module fixed_point_acc(clk, rst, rst_acc, a, result);

  input clk, rst, rst_acc;
  input [15:0] a;
  output [15:0] result;

  reg [15:0] a1, result1;
  reg [15:0] temp;

  always@ (posedge clk)
  begin
    if (rst||rst_acc)
      begin
        temp <= 0;
        a1 <= 0;
        result1 <= 0;
      end	   
	else
	  begin
        temp <= a + result;
        a1 <= a;
        result1 <= result;
	  end
  end
  
  assign result = (a1[15] & result1[15] & !temp[15]) ? 16'h8000 : (!a1[15] & !result1[15] & temp[15]) ? 16'h7fff : temp;
  
endmodule
