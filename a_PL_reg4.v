module a_PL_reg4 (clk, exp_nor2, sign_a2, sign_b2, man_a_nor2, man_b_nor2, exp_nor3, sign_a3, sign_b3, man_a_nor3, man_b_nor3);

  input clk;
  input sign_a2, sign_b2;
  input [10:0] exp_nor2;
  input [52:0] man_a_nor2, man_b_nor2;
  output reg sign_a3, sign_b3;
  output reg [10:0] exp_nor3;
  output reg [52:0] man_a_nor3, man_b_nor3;

  always@ (posedge clk)
  begin
     sign_a3 <= sign_a2;
     sign_b3 <= sign_b2;
     exp_nor3 <= exp_nor2;
     man_a_nor3 <= man_a_nor2;
     man_b_nor3 <= man_b_nor2;
  end

endmodule
