module sigmoid(clk, in, out);
  
  input clk;
  input [7:0] in;
  output reg [11:0] out;
	 
  reg [11:0] out_temp;
  wire [11:0] out_temp_negative;
  
  always@ (*)
  begin
    case (in[6:0])
	   0: out_temp = 12'b100000000000;	     
	   1: out_temp = 12'b100001100110;	
	   2: out_temp = 12'b100011001100;	     
	   3: out_temp = 12'b100011001100;	  
	   4: out_temp = 12'b100100110001;	     
	   5: out_temp = 12'b100110010100;	
	   6: out_temp = 12'b100110010100;	     
	   7: out_temp = 12'b100111110110;	
	   8: out_temp = 12'b100111110110;	     
	   9: out_temp = 12'b101001010101;	
	   10: out_temp = 12'b101010110001;	     
	   11: out_temp = 12'b101010110001;	  
	   12: out_temp = 12'b101100001010;	     
	   13: out_temp = 12'b101101100000;	
	   14: out_temp = 12'b101101100000;	     
	   15: out_temp = 12'b101110110010;	  
	   16: out_temp = 12'b101110110010;	     
	   17: out_temp = 12'b110000000001;	
	   18: out_temp = 12'b110001001100;	     
	   19: out_temp = 12'b110001001100;	  
	   20: out_temp = 12'b110010010011;	     
	   21: out_temp = 12'b110011010110;	
	   22: out_temp = 12'b110011010110;	     
	   23: out_temp = 12'b110100010101;	
	   24: out_temp = 12'b110100010101;	     
	   25: out_temp = 12'b110101010000;	
	   26: out_temp = 12'b110110000111;	     
	   27: out_temp = 12'b110110000111;	  
	   28: out_temp = 12'b110110111011;	     
	   29: out_temp = 12'b110111101011;	
	   30: out_temp = 12'b110111101011;	     
	   31: out_temp = 12'b111000011000;		
	   32: out_temp = 12'b111000011000;	     
	   33: out_temp = 12'b111001000001;	
	   34: out_temp = 12'b111001100111;	     
	   35: out_temp = 12'b111001100111;	  
	   36: out_temp = 12'b111010001011;	     
	   37: out_temp = 12'b111010101011;	
	   38: out_temp = 12'b111010101011;	     
	   39: out_temp = 12'b111011001001;	
	   40: out_temp = 12'b111011001001;	     
	   41: out_temp = 12'b111011100101;	
	   42: out_temp = 12'b111011111110;	     
	   43: out_temp = 12'b111011111110;	  
	   44: out_temp = 12'b111100010101;	     
	   45: out_temp = 12'b111100101010;	
	   46: out_temp = 12'b111100101010;	     
	   47: out_temp = 12'b111100111110;	  
	   48: out_temp = 12'b111100111110;	     
	   49: out_temp = 12'b111101001111;	
	   50: out_temp = 12'b111101100000;	     
	   51: out_temp = 12'b111101100000;	  
	   52: out_temp = 12'b111101101110;	     
	   53: out_temp = 12'b111101111100;	
	   54: out_temp = 12'b111101111100;	     
	   55: out_temp = 12'b111110001000;	
	   56: out_temp = 12'b111110001000;	     
	   57: out_temp = 12'b111110010011;	
	   58: out_temp = 12'b111110011101;	     
	   59: out_temp = 12'b111110011101;	  
	   60: out_temp = 12'b111110100110;	     
	   61: out_temp = 12'b111110101111;	
	   62: out_temp = 12'b111110101111;	     
	   63: out_temp = 12'b111110110110;			
	   64: out_temp = 12'b111110110110;	     
	   65: out_temp = 12'b111110111101;	
	   66: out_temp = 12'b111111000011;	     
	   67: out_temp = 12'b111111000011;	  
	   68: out_temp = 12'b111111001001;	     
	   69: out_temp = 12'b111111001110;	
	   70: out_temp = 12'b111111001110;	     
	   71: out_temp = 12'b111111010011;	
	   72: out_temp = 12'b111111010011;	     
	   73: out_temp = 12'b111111010111;	
	   74: out_temp = 12'b111111011011;	     
	   75: out_temp = 12'b111111011011;	  
	   76: out_temp = 12'b111111011111;	     
	   77: out_temp = 12'b111111100010;	
	   78: out_temp = 12'b111111100010;	     
	   79: out_temp = 12'b111111100101;	  
	   80: out_temp = 12'b111111100101;	     
	   81: out_temp = 12'b111111100111;	
	   82: out_temp = 12'b111111101010;	     
	   83: out_temp = 12'b111111101010;	  
	   84: out_temp = 12'b111111101100;	     
	   85: out_temp = 12'b111111101110;	
	   86: out_temp = 12'b111111101110;	     
	   87: out_temp = 12'b111111101111;	
	   88: out_temp = 12'b111111101111;	     
	   89: out_temp = 12'b111111110001;	
	   90: out_temp = 12'b111111110010;	     
	   91: out_temp = 12'b111111110010;	  
	   92: out_temp = 12'b111111110100;	     
	   93: out_temp = 12'b111111110101;	
	   94: out_temp = 12'b111111110101;	     
	   95: out_temp = 12'b111111110110;		
	   96: out_temp = 12'b111111110110;	     
	   97: out_temp = 12'b111111110111;	
	   98: out_temp = 12'b111111111000;	     
	   99: out_temp = 12'b111111111000;	  
	   100: out_temp = 12'b111111111000;	     
	   101: out_temp = 12'b111111111001;	
	   102: out_temp = 12'b111111111001;	     
	   103: out_temp = 12'b111111111010;	
	   104: out_temp = 12'b111111111010;	     
	   105: out_temp = 12'b111111111010;	
	   106: out_temp = 12'b111111111011;	     
	   107: out_temp = 12'b111111111011;	  
	   108: out_temp = 12'b111111111011;	     
	   109: out_temp = 12'b111111111100;	
	   110: out_temp = 12'b111111111100;	     
	   111: out_temp = 12'b111111111100;	  
	   112: out_temp = 12'b111111111100;	     
	   113: out_temp = 12'b111111111101;	
	   114: out_temp = 12'b111111111101;	     
	   115: out_temp = 12'b111111111101;	
	   116: out_temp = 12'b111111111101;		
	   117: out_temp = 12'b111111111101;	     
	   118: out_temp = 12'b111111111101;	
	   default: out_temp = 12'b111111111110;
    endcase
  end	 
  
  assign out_temp_negative = ~out_temp + 1'b1;
  
  always@(posedge clk)
  begin
    if (in[7])
	   out <= out_temp_negative;
	 else
	   out <= out_temp;  	
  end		
		
endmodule