`include "parameter.v"
module in (clk, rst, port_dir, isend, ack, iflit, oaccept, dir, iflit_buffer);
  
  input clk, rst, isend, ack;
  input [1:0] port_dir;
  input [flit_data_width-1:0] iflit;
  output reg oaccept;
  output reg [2:0] dir;
  output reg [flit_data_width-1:0] iflit_buffer;

  reg [2:0] state;

  always@ (posedge clk)
  begin
    if (rst)
      state <= 0;
    else
      case (state)
        0:
          begin
            oaccept <= 0;
            dir <= 3'b111; 
            if (isend)
              state <= 1;
          end
        1:
          begin
            if (iflit[1:0] == port_dir)
              dir <= 3'b100;
            else
              case(iflit[1:0])
                2'b00: dir <= N;
                2'b01: dir <= S;
                2'b10: dir <= E;
                2'b11: dir <= W;
              endcase
            iflit_buffer <= {iflit[flit_data_width-1:flit_data_width-30], iflit[flit_data_width-31:0] >> 2};
            state <= 2;
          end
        2:
          if (ack)
            begin
              dir <= 3'b111;
              oaccept <= 1;
              state <= 3; 
            end
        3:
          if (iflit[flit_data_width-2])
            state <= 0;
          else if (!iflit[flit_data_width-1])
            begin
              iflit_buffer <= iflit;
              state <= 4;
            end
        4:
          begin
            iflit_buffer <= iflit;
            if (iflit[flit_data_width-2])
              state <= 0;
          end         
        default: state <= 0;
      endcase
  end

endmodule