`include "parameter.v"
module PE_4x8(clk, rst, source_node, wgr, broadcast, we, idata, wrq, waddr, odata);
  
  input clk, rst, wgr, broadcast, we;
  input [5:0] source_node;
  input [data_width-1:0] idata;
  output wrq;
  output waddr;
  output [data_width-1:0] odata;
  
  wire w_start, re_i, we_i, re_w, we_w_1, we_w_2, we_w_3, we_w_4, re_o, we_o, rst_acc, done_hidden, done_output;
  wire [5:0] waddr_i;
  wire [data_width-1:0] wdata;
  wire [data_width-1:0] input_values;
  wire [data_width-1:0] weights_1, weights_2, weights_3, weights_4;  
  wire [11:0] result_1, result_2, result_3, result_4, odata_1, odata_2, odata_3, odata_4; 
  
  Bus_interface_4x8 Bus_interface (clk, rst, source_node,
								   wgr, broadcast, we, idata, 
								   done_hidden, done_output, odata_1, odata_2, odata_3, odata_4,		    
								   w_start, we_i, we_w_1, we_w_2, we_w_3, we_w_4, 
								   re_o, waddr_i, wdata,
								   wrq, waddr, odata);
								   
  control_8 control (clk, rst, we_i, re_i, re_w, we_o, rst_acc, done_hidden, done_output);
  
  RAM_i_8 RAM_i (clk, rst, re_i, we_i, w_start, waddr_i, wdata, input_values);
  
  RAM_w_8 RAM_w_1 (clk, rst, re_w, we_w_1, wdata, weights_1);
  RAM_w_8 RAM_w_2 (clk, rst, re_w, we_w_2, wdata, weights_2);
  RAM_w_8 RAM_w_3 (clk, rst, re_w, we_w_3, wdata, weights_3);
  RAM_w_8 RAM_w_4 (clk, rst, re_w, we_w_4, wdata, weights_4);   
  
  RAM_o RAM_o_1 (clk, rst, re_o, we_o, result_1, odata_1);
  RAM_o RAM_o_2 (clk, rst, re_o, we_o, result_2, odata_2);
  RAM_o RAM_o_3 (clk, rst, re_o, we_o, result_3, odata_3);
  RAM_o RAM_o_4 (clk, rst, re_o, we_o, result_4, odata_4);  
  
  neuron_8x neuron_1 (clk, rst, rst_acc, input_values[15:0], input_values[31:16], input_values[47:32], input_values[63:48], input_values[79:64], input_values[95:80], input_values[111:96], input_values[127:112], weights_1[15:0], weights_1[31:16], weights_1[47:32], weights_1[63:48], weights_1[79:64], weights_1[95:80], weights_1[111:96], weights_1[127:112], result_1);
  neuron_8x neuron_2 (clk, rst, rst_acc, input_values[15:0], input_values[31:16], input_values[47:32], input_values[63:48], input_values[79:64], input_values[95:80], input_values[111:96], input_values[127:112], weights_2[15:0], weights_2[31:16], weights_2[47:32], weights_2[63:48], weights_2[79:64], weights_2[95:80], weights_2[111:96], weights_2[127:112], result_2);
  neuron_8x neuron_3 (clk, rst, rst_acc, input_values[15:0], input_values[31:16], input_values[47:32], input_values[63:48], input_values[79:64], input_values[95:80], input_values[111:96], input_values[127:112], weights_3[15:0], weights_3[31:16], weights_3[47:32], weights_3[63:48], weights_3[79:64], weights_3[95:80], weights_3[111:96], weights_3[127:112], result_3);
  neuron_8x neuron_4 (clk, rst, rst_acc, input_values[15:0], input_values[31:16], input_values[47:32], input_values[63:48], input_values[79:64], input_values[95:80], input_values[111:96], input_values[127:112], weights_4[15:0], weights_4[31:16], weights_4[47:32], weights_4[63:48], weights_4[79:64], weights_4[95:80], weights_4[111:96], weights_4[127:112], result_4);
  
endmodule