module m_PL_reg7 (clk, sign6, exp6, a6, b6, a1b3, a0b2, sum3_1, sign7, exp7, a7, b7, a1b3_1, a0b2_1, sum3_2); 
  
  input   clk;
  input   sign6;
  input   [11:0] exp6;
  input   [23:0] a6;
  input   [1:0] b6;
  input   [25:0] a1b3;
  input   [40:0] a0b2;
  input   [105:0] sum3_1;
  output reg sign7;
  output reg [11:0] exp7;
  output reg [23:0] a7;
  output reg  [1:0] b7;
  output reg [25:0] a1b3_1;
  output reg [40:0] a0b2_1;
  output reg [105:0] sum3_2;
    
  always@ (posedge clk)
    begin
      sign7 <= sign6;
      exp7 <= exp6;
      a7 <= a6;
      b7 <= b6;
      a1b3_1 <= a1b3;
      a0b2_1 <= a0b2;
      sum3_2 <= sum3_1;    
    end

endmodule






