module in (clk, rst, isend, available, eop, oaccept, wr_en);
  
  input clk, rst, isend, available, eop;
  output reg oaccept, wr_en;
  
  reg [1:0] state;

  always@ (posedge clk)
  begin
    if (rst)
      state <= 2'b00;
    else
      case (state)
        2'b00: 
          if (isend && available)
            begin
      	      oaccept <= 1'b1;
              state <= 2'b01;
            end
        2'b01:
          begin
            state <= 2'b10;
            oaccept <= 1'b0;
          end            
        2'b10:
          begin
            wr_en <= 1'b1;
            state <= 2'b11; 
          end
        2'b11:
		  begin
            if (eop)
              begin
                wr_en <= 1'b0;
                state <= 2'b00;	 
              end
		  end
        default: state <= 2'b00;
      endcase
  end

endmodule