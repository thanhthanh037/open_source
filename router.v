//------------------------------------------------------------------------------------------------------------
// File         : router.v
// Author       : Thanh Bui
// Organization : The University of Adelaide
<<<<<<< HEAD
// Created      : March 2017
=======
// Created      : May 2015
>>>>>>> 77342d8364d52c748f28fc230881e779ffa0e508
// Platform     : VHDL 1993
// Simulators   : Modelsim
// Synthesizers : ISE 14.7
// Targets      : Virtex-7 VC707
<<<<<<< HEAD
// Description  : Network on chip router: Wormhole Switching, Broadcast and X-Y Routing
//------------------------------------------------------------------------------------------------------------
`include "parameters.v"
module router (clk, rst, router_type, x_router, y_router, 
               isend, isend_n, isend_s, isend_e, isend_w,
=======
// Description  : Network on chip router: Five ports of 2 x 130 bits, Circuit Switching, Deterministic Routing
//------------------------------------------------------------------------------------------------------------
`include "parameter.v"
module router (clk, rst, isend, isend_n, isend_s, isend_e, isend_w,
>>>>>>> 77342d8364d52c748f28fc230881e779ffa0e508
               iaccept, iaccept_n, iaccept_s, iaccept_e, iaccept_w, 
               iflit, iflit_n, iflit_s, iflit_e, iflit_w, 
               osend, osend_n, osend_s, osend_e, osend_w, 
               oaccept, oaccept_n, oaccept_s, oaccept_e, oaccept_w, 
               oflit, oflit_n, oflit_s, oflit_e, oflit_w);
  
  input clk, rst;
<<<<<<< HEAD
  input [3:0] router_type;
  input [2:0] x_router, y_router;
=======
>>>>>>> 77342d8364d52c748f28fc230881e779ffa0e508
  input isend, isend_n, isend_s, isend_e, isend_w, iaccept, iaccept_n, iaccept_s, iaccept_e, iaccept_w;
  input [flit_data_width-1:0] iflit, iflit_n, iflit_s, iflit_e, iflit_w;
  output osend, osend_n, osend_s, osend_e, osend_w, oaccept, oaccept_n, oaccept_s, oaccept_e, oaccept_w;
  output [flit_data_width-1:0] oflit, oflit_n, oflit_s, oflit_e, oflit_w;
  
<<<<<<< HEAD
  wire rd_en, rd_en_n, rd_en_s, rd_en_e, rd_en_w;
  wire wr_en, wr_en_n, wr_en_s, wr_en_e, wr_en_w;
  wire re_np, re_pn, re_ps, re_pe, re_pw;
  wire re_sp, re_sn, re_ns, re_ne, re_nw;
  wire re_ep, re_en, re_es, re_se, re_sw;
  wire re_wp, re_wn, re_ws, re_we, re_ew;

  wire [flit_data_width-1:0] rd_flit, rd_flit_n, rd_flit_s, rd_flit_e, rd_flit_w;
  wire available, available_n, available_s, available_e, available_w;
  wire [4:0] dir, dir_n, dir_s, dir_e, dir_w;
  wire [4:0] dir_amd, dir_n_amd, dir_s_amd, dir_e_amd, dir_w_amd;  
  wire [1:0] sel, sel_n, sel_s, sel_e, sel_w; 
  wire [1:0] read_times, read_times_n, read_times_s, read_times_e, read_times_w;
  wire new_packet, new_packet_n, new_packet_s, new_packet_e, new_packet_w;
  
  in in_PE (clk, rst, isend, available, iflit[flit_data_width-2], oaccept, wr_en);
  in in_N (clk, rst, isend_n, available_n, iflit_n[flit_data_width-2], oaccept_n, wr_en_n);
  in in_S (clk, rst, isend_s, available_s, iflit_s[flit_data_width-2], oaccept_s, wr_en_s);
  in in_E (clk, rst, isend_e, available_e, iflit_e[flit_data_width-2], oaccept_e, wr_en_e);
  in in_W (clk, rst, isend_w, available_w, iflit_w[flit_data_width-2], oaccept_w, wr_en_w);
  
  in_buffer in_buffer_PE (clk, rst, read_times, rd_en, wr_en, iflit, rd_flit, available, new_packet);
  in_buffer in_buffer_N (clk, rst, read_times_n, rd_en_n, wr_en_n, iflit_n, rd_flit_n, available_n, new_packet_n);
  in_buffer in_buffer_S (clk, rst, read_times_s, rd_en_s, wr_en_s, iflit_s, rd_flit_s, available_s, new_packet_s);
  in_buffer in_buffer_E (clk, rst, read_times_e, rd_en_e, wr_en_e, iflit_e, rd_flit_e, available_e, new_packet_e);
  in_buffer in_buffer_W (clk, rst, read_times_w, rd_en_w, wr_en_w, iflit_w, rd_flit_w, available_w, new_packet_w);
  
  route_cal route_cal_PE (clk, rst, router_type, x_router, y_router, new_packet, rd_flit[flit_data_width-3], rd_flit[flit_data_width-4:flit_data_width-6], rd_flit[flit_data_width-7:flit_data_width-9], rd_flit[flit_data_width-10:flit_data_width-12], rd_flit[flit_data_width-13:flit_data_width-15], dir, new_dir);
  route_cal route_cal_N (clk, rst, router_type, x_router, y_router, new_packet_n, rd_flit_n[flit_data_width-3], rd_flit_n[flit_data_width-4:flit_data_width-6], rd_flit_n[flit_data_width-7:flit_data_width-9], rd_flit_n[flit_data_width-10:flit_data_width-12], rd_flit_n[flit_data_width-13:flit_data_width-15], dir_n, new_dir_n);
  route_cal route_cal_S (clk, rst, router_type, x_router, y_router, new_packet_s, rd_flit_s[flit_data_width-3], rd_flit_s[flit_data_width-4:flit_data_width-6], rd_flit_s[flit_data_width-7:flit_data_width-9], rd_flit_s[flit_data_width-10:flit_data_width-12], rd_flit_s[flit_data_width-13:flit_data_width-15], dir_s, new_dir_s);
  route_cal route_cal_E (clk, rst, router_type, x_router, y_router, new_packet_e, rd_flit_e[flit_data_width-3], rd_flit_e[flit_data_width-4:flit_data_width-6], rd_flit_e[flit_data_width-7:flit_data_width-9], rd_flit_e[flit_data_width-10:flit_data_width-12], rd_flit_e[flit_data_width-13:flit_data_width-15], dir_e, new_dir_e);
  route_cal route_cal_W (clk, rst, router_type, x_router, y_router, new_packet_w, rd_flit_w[flit_data_width-3], rd_flit_w[flit_data_width-4:flit_data_width-6], rd_flit_w[flit_data_width-7:flit_data_width-9], rd_flit_w[flit_data_width-10:flit_data_width-12], rd_flit_w[flit_data_width-13:flit_data_width-15], dir_w, new_dir_w);
  
  arbiter_PE arbiter_PE (clk, rst, iaccept, oflit[flit_data_width-2], dir_n_amd, dir_e_amd, dir_s_amd, dir_w_amd, osend, sel, re_pn, re_pe, re_ps, re_pw);
  arbiter_N arbiter_N (clk, rst, iaccept_n, oflit_n[flit_data_width-2], dir_e_amd, dir_s_amd, dir_w_amd, dir_amd, osend_n, sel_n, re_ne, re_ns, re_nw, re_np);
  arbiter_S arbiter_S (clk, rst, iaccept_s, oflit_s[flit_data_width-2], dir_w_amd, dir_amd, dir_n_amd, dir_e_amd, osend_s, sel_s, re_sw, re_sp, re_sn, re_se);
  arbiter_E arbiter_E (clk, rst, iaccept_e, oflit_e[flit_data_width-2], dir_s_amd, dir_w_amd, dir_amd, dir_n_amd, osend_e, sel_e, re_es, re_ew, re_ep, re_en);
  arbiter_W arbiter_W (clk, rst, iaccept_w, oflit_w[flit_data_width-2], dir_amd, dir_n_amd, dir_e_amd, dir_s_amd, osend_w, sel_w, re_wp, re_wn, re_we, re_ws);
  
  crossbar crossbar_PE (rd_flit_n, rd_flit_e, rd_flit_s, rd_flit_w, sel, oflit);
  crossbar crossbar_N (rd_flit_e, rd_flit_s, rd_flit_w, rd_flit, sel_n, oflit_n);
  crossbar crossbar_S (rd_flit_w, rd_flit, rd_flit_n, rd_flit_e, sel_s, oflit_s);
  crossbar crossbar_E (rd_flit_s, rd_flit_w, rd_flit, rd_flit_n, sel_e, oflit_e);
  crossbar crossbar_W (rd_flit, rd_flit_n, rd_flit_e, rd_flit_s, sel_w, oflit_w);
  
  assign rd_en = re_np || re_sp || re_ep || re_wp;
  assign rd_en_n = re_pn || re_sn || re_en || re_wn;
  assign rd_en_s = re_ps || re_ns || re_es || re_ws;
  assign rd_en_e = re_pe || re_ne || re_se || re_we;
  assign rd_en_w = re_pw || re_nw || re_sw || re_ew;  
  
  read_times read_times_PE (dir_amd, read_times);
  read_times read_times_N (dir_n_amd, read_times_n);
  read_times read_times_S (dir_s_amd, read_times_s);
  read_times read_times_E (dir_e_amd, read_times_e);
  read_times read_times_W (dir_w_amd, read_times_w);
  
  dir_amended dir_amended (rst, clk, dir, dir_n, dir_s, dir_e, dir_w,
                           new_dir, new_dir_n, new_dir_s, new_dir_e, new_dir_w, 
                           rd_flit[flit_data_width-2], rd_flit_n[flit_data_width-2], rd_flit_s[flit_data_width-2], rd_flit_e[flit_data_width-2], rd_flit_w[flit_data_width-2],
						   re_pn, re_ps, re_pe, re_pw,
						   re_np, re_ns, re_ne, re_nw,
						   re_sp, re_sn, re_se, re_sw,
						   re_ep, re_en, re_es, re_ew,
						   re_wp, re_wn, re_ws, re_we,
						   dir_amd, dir_n_amd, dir_s_amd, dir_e_amd, dir_w_amd);
  
 
=======
  wire ack, ack_n, ack_s, ack_e, ack_w;
  wire ack1, ack_n1, ack_s1, ack_e1, ack_w1;
  wire ack2, ack_n2, ack_s2, ack_e2, ack_w2;
  wire ack3, ack_n3, ack_s3, ack_e3, ack_w3;
  wire ack4, ack_n4, ack_s4, ack_e4, ack_w4;
  wire [flit_data_width-1:0] iflit_buffer, iflit_buffer_n, iflit_buffer_s, iflit_buffer_e, iflit_buffer_w;
  wire [2:0] dir, dir_n, dir_s, dir_e, dir_w;
  wire [1:0] sel, sel_n, sel_s, sel_e, sel_w; 
  
  in_pe in_pe (clk, rst, isend, ack, iflit, oaccept, dir, iflit_buffer);
  in in_n (clk, rst, 2'b00, isend_n, ack_n, iflit_n, oaccept_n, dir_n, iflit_buffer_n);
  in in_s (clk, rst, 2'b01, isend_s, ack_s, iflit_s, oaccept_s, dir_s, iflit_buffer_s);
  in in_e (clk, rst, 2'b10, isend_e, ack_e, iflit_e, oaccept_e, dir_e, iflit_buffer_e);
  in in_w (clk, rst, 2'b11, isend_w, ack_w, iflit_w, oaccept_w, dir_w, iflit_buffer_w);
  
  arbiter arbiter_PE (clk, rst, 3'b100, iaccept, oflit[flit_data_width-2], dir_n, dir_e, dir_s, dir_w, osend, sel, ack_n1, ack_e1, ack_s1, ack_w1);
  arbiter arbiter_N (clk, rst, 3'b000, iaccept_n, oflit_n[flit_data_width-2], dir_e, dir_s, dir_w, dir, osend_n, sel_n, ack_e2, ack_s2, ack_w2, ack1);
  arbiter arbiter_S (clk, rst, 3'b001, iaccept_s, oflit_s[flit_data_width-2], dir_w, dir, dir_n, dir_e, osend_s, sel_s, ack_w3, ack2, ack_n2, ack_e3);
  arbiter arbiter_E (clk, rst, 3'b010, iaccept_e, oflit_e[flit_data_width-2], dir_s, dir_w, dir, dir_n, osend_e, sel_e, ack_s3, ack_w4, ack3, ack_n3);
  arbiter arbiter_W (clk, rst, 3'b011, iaccept_w, oflit_w[flit_data_width-2], dir, dir_n, dir_e, dir_s, osend_w, sel_w, ack4, ack_n4, ack_e4, ack_s4);
  
  assign ack = ack1 || ack2 || ack3 || ack4;
  assign ack_n = ack_n1 || ack_n2 || ack_n3 || ack_n4;
  assign ack_s = ack_s1 || ack_s2 || ack_s3 || ack_s4;
  assign ack_e = ack_e1 || ack_e2 || ack_e3 || ack_e4;
  assign ack_w = ack_w1 || ack_w2 || ack_w3 || ack_w4;
  
  crossbar crossbar_PE (iflit_buffer_n, iflit_buffer_e, iflit_buffer_s, iflit_buffer_w, sel, oflit);
  crossbar crossbar_N (iflit_buffer_e, iflit_buffer_s, iflit_buffer_w, iflit_buffer, sel_n, oflit_n);
  crossbar crossbar_S (iflit_buffer_w, iflit_buffer, iflit_buffer_n, iflit_buffer_e, sel_s, oflit_s);
  crossbar crossbar_E (iflit_buffer_s, iflit_buffer_w, iflit_buffer, iflit_buffer_n, sel_e, oflit_e);
  crossbar crossbar_W (iflit_buffer, iflit_buffer_n, iflit_buffer_e, iflit_buffer_s, sel_w, oflit_w);
  
>>>>>>> 77342d8364d52c748f28fc230881e779ffa0e508
endmodule
  
