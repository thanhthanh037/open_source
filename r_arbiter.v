module arbiter (clk, rst, port_dir, iaccept, eop, dir1, dir2, dir3, dir4, osend, sel, ack1, ack2, ack3, ack4); 
                
  input clk, rst, iaccept, eop;
  input [2:0] port_dir, dir1, dir2, dir3, dir4;
  output reg osend, ack1, ack2, ack3, ack4;
  output reg [1:0] sel;
  
  reg [4:0] state;
  
  always@ (posedge clk)
  begin
    if (rst)
      state <= 0;
    else
      case (state)
        0:
          begin
            ack1 <= 0;
            ack2 <= 0;
            ack3 <= 0;
            ack4 <= 0;
            if ((dir1 == port_dir) && (dir2 != port_dir) && (dir3 != port_dir) && (dir4 != port_dir))
              begin
                osend <= 1'b1;
                state <= 1;
              end
            if ((dir1 != port_dir) && (dir2 == port_dir) && (dir3 != port_dir) && (dir4 != port_dir))
              begin
                osend <= 1'b1;
                state <= 2;
              end
            if ((dir1 != port_dir) && (dir2 != port_dir) && (dir3 == port_dir) && (dir4 != port_dir))
              begin
                osend <= 1'b1;
                state <= 3;
              end
            if ((dir1 != port_dir) && (dir2 != port_dir) && (dir3 != port_dir) && (dir4 == port_dir))
              begin
                osend <= 1'b1;
                state <= 4;
              end
            if ((dir1 == port_dir) && (dir2 == port_dir) && (dir3 != port_dir) && (dir4 != port_dir))
              begin
                osend <= 1'b1;
                state <= 5;
              end
            if ((dir1 == port_dir) && (dir2 != port_dir) && (dir3 == port_dir) && (dir4 != port_dir))
              begin
                osend <= 1'b1;
                state <= 6;
              end
            if ((dir1 == port_dir) && (dir2 != port_dir) && (dir3 != port_dir) && (dir4 == port_dir))
              begin
                osend <= 1'b1;
                state <= 7;
              end
            if ((dir1 != port_dir) && (dir2 == port_dir) && (dir3 == port_dir) && (dir4 != port_dir))
              begin
                osend <= 1'b1;
                state <= 8;
              end
            if ((dir1 != port_dir) && (dir2 == port_dir) && (dir3 != port_dir) && (dir4 == port_dir))
              begin
                osend <= 1'b1;
                state <= 9;
              end
            if ((dir1 != port_dir) && (dir2 != port_dir) && (dir3 == port_dir) && (dir4 == port_dir))
              begin
                osend <= 1'b1;
                state <= 10;
              end
            if ((dir1 == port_dir) && (dir2 == port_dir) && (dir3 == port_dir) && (dir4 != port_dir))
              begin
                osend <= 1'b1;
                state <= 11;
              end
            if ((dir1 == port_dir) && (dir2 == port_dir) && (dir3 != port_dir) && (dir4 == port_dir))
              begin
                osend <= 1'b1;
                state <= 12;
              end
            if ((dir1 == port_dir) && (dir2 != port_dir) && (dir3 == port_dir) && (dir4 == port_dir))
              begin
                osend <= 1'b1;
                state <= 13;
              end
            if ((dir1 != port_dir) && (dir2 == port_dir) && (dir3 == port_dir) && (dir4 == port_dir))
              begin
                osend <= 1'b1;
                state <= 14;
              end
            if ((dir1 == port_dir) && (dir2 == port_dir) && (dir3 == port_dir) && (dir4 == port_dir))
              begin
                osend <= 1'b1;
                state <= 15;
              end
          end
        1:
          begin                                  
            sel <= 2'b00;
            osend <= 0;
            if (iaccept)
              begin
                ack1 <= 1;
                state <= 16;
              end
          end
        2:
          begin    
            sel <= 2'b01;
            osend <= 0;
            if (iaccept)
              begin
                ack2 <= 1;
                state <= 16;
              end
          end
        3:
          begin    
            sel <= 2'b10;
            osend <= 0;
            if (iaccept)
              begin
                ack3 <= 1;
                state <= 16;
              end
          end
        4:
          begin    
            sel <= 2'b11;
            osend <= 0;
            if (iaccept)
              begin
                ack4 <= 1;
                state <= 16;
              end
          end
        5:
          begin                                  
            sel <= 2'b00;
            osend <= 0;
            if (iaccept)
              begin
                ack1 <= 1;
                state <= 17;
              end
          end
        6:
          begin    
            sel <= 2'b00;
            osend <= 0;
            if (iaccept)
              begin
                ack1 <= 1;
                state <= 18;
              end
          end
        7:
          begin    
            sel <= 2'b00;
            osend <= 0;
             if (iaccept)
              begin
                ack1 <= 1;
                state <= 19;
              end
            end
        8:
          begin    
            sel <= 2'b01;
            osend <= 0;
            if (iaccept)
              begin
                ack2 <= 1;
                state <= 20;
              end
          end
        9:
          begin                                  
            sel <= 2'b01;
            osend <= 0;
            if (iaccept)
              begin
                ack2 <= 1;
                state <= 21;
              end
          end
        10:
          begin    
            sel <= 2'b10;
            osend <= 0;
            if (iaccept)
              begin
                ack3 <= 1;
                state <= 22;
              end
          end
        11:
          begin    
            sel <= 2'b00;
            osend <= 0;
            if (iaccept)
              begin
                ack4 <= 1;
                state <= 23;
              end
          end
        12:
          begin    
            sel <= 2'b00;
            osend <= 0;
            if (iaccept)
              begin
                ack1 <= 1;
                state <= 24;
              end
          end
        13:
          begin                                  
            sel <= 2'b00;
            osend <= 0;
            if (iaccept)
              begin
                ack1 <= 1;
                state <= 25;
              end
          end
        14:
          begin    
            sel <= 2'b01;
            osend <= 0;
            if (iaccept)
              begin
                ack2 <= 1;
                state <= 26;
              end
          end
        15:
          begin    
            sel <= 2'b00;
            osend <= 0;
            if (iaccept)
              begin
                ack1 <= 1;
                state <= 27;
              end
          end       
        16:
          if (eop)
            begin              
              ack1 <= 1'b0;
              ack2 <= 1'b0;
              ack3 <= 1'b0;
              ack4 <= 1'b0;
              state <= 0;        
            end                                   
        17:
          if (eop)
            begin              
              ack1 <= 1'b0;
              osend <= 1;
              state <= 2;        
            end
        18:
          if (eop)
            begin              
              ack1 <= 1'b0;
              osend <= 1;
              state <= 3;        
            end
        19:
          if (eop)
            begin              
              ack1 <= 1'b0;
              osend <= 1;
              state <= 4;        
            end
        20:
          if (eop)
            begin              
              ack2 <= 1'b0;
              osend <= 1;
              state <= 3;        
            end
        21:
          if (eop)
            begin              
              ack2 <= 1'b0;
              osend <= 1;
              state <= 4;        
            end
        22:
          if (eop)
            begin              
              ack3 <= 1'b0;
              osend <= 1;
              state <= 4;        
            end
        23:
          if (eop)
            begin              
              ack1 <= 1'b0;
              osend <= 1;
              state <= 8;        
            end
        24:
          if (eop)
            begin              
              ack1 <= 1'b0;
              osend <= 1;
              state <= 9;        
            end
        25:
          if (eop)
            begin              
              ack1 <= 1'b0;
              osend <= 1;
              state <= 10;        
            end
        26:
          if (eop)
            begin              
              ack2 <= 1'b0;
              osend <= 1;
              state <= 10;        
            end
        27:
          if (eop)
            begin              
              ack1 <= 1'b0;
              osend <= 1;
              state <= 14;        
            end
        default: state <= 0;            
      endcase
  end
   
endmodule