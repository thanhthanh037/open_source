`include "parameter.v"
module Bus_interface_1x1 (clk, rst, source_node,
						  wgr, broadcast, we, idata, 
                          done_hidden, done_output, rdata,		    
						  w_start, we_i, we_w,
						  re_o, waddr_i, wdata,
						  wrq, waddr_o, odata);   
								
  input clk, rst;  
  input [5:0] source_node; 
  input wgr, broadcast, we;
  input [data_width-1:0] idata;
  input done_hidden, done_output;
  input [11:0] rdata; 
  output reg [data_width-1:0] wdata;
  output reg w_start, we_i, we_w, re_o, wrq;  
  output reg [5:0] waddr_i;
  output reg waddr_o;
  output reg [data_width-1:0] odata; 
  
  reg  r_state;
  reg [2:0] t_state;
  reg [6:0] count_t;
  
  //receiver
  always@ (posedge clk)
  begin
    if (rst)
	  begin
        r_state <= 0;
	  end
    else
      case (r_state)
        0:
          begin
            we_i <= 0;
            we_w <= 0;
			w_start <= 1;
            if (we)
              begin
                if (broadcast)
				  begin
					w_start <= 0;
					we_i <= 1;
				  end
				else 
  				  we_w <= 1;
				r_state <= 1;
              end
          end       
        1:
		    begin			   
            if (!we)
			  begin
                we_i <= 0;
				we_w <= 0;
  				r_state <= 0;              
  			  end
			 end
        default: r_state <= 0;
      endcase
	wdata <= idata;
    if (source_node == 0)
	  waddr_i <= 0;
	else
	  waddr_i <= source_node - 1'b1;
		 
  end
  
  //transmiter
  always@ (posedge clk)
  begin
    if (rst)
      t_state <= 0;
    else
      case (t_state)
        0:
          begin
            re_o <= 0;		
            if (done_hidden)
              begin
                wrq <= 1;
                t_state <= 1;
              end
		    else if (done_output)
              begin
                wrq <= 1;
                t_state <= 2;
              end				  
          end
        1:
          begin            
            if (wgr)
			  begin       
                count_t <= 0;
				re_o <= 1;
				t_state <= 3;
              end
          end
        3:
          begin
            count_t <= count_t + 1'b1;
            odata <= {4'b0, rdata};
				if (count_t == op_per_neuron_hidden -1'b1)
				  begin
 		            re_o <= 0;	
				    wrq <= 0; 						 
                    count_t <= 0;
					t_state <= 0;
                  end              
          end   
        2:
          begin            
            if (wgr)
			  begin       
                count_t <= 0;
				re_o <= 1;
				t_state <= 4;
              end
          end
        4:
          begin
            count_t <= count_t + 1'b1;
            odata <= {4'b0, rdata};
				if (count_t == op_per_neuron_output -1'b1)
				  begin
 		            re_o <= 0;	
				    wrq <= 0; 						 
                    count_t <= 0;
					t_state <= 0;
                  end              
          end 			 
        default: t_state <= 0;
      endcase
  end  
  
endmodule