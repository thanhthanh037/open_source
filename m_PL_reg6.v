module m_PL_reg6 (clk, sign5, exp5, a5, b5, sum3, sign6, exp6, a6, b6, sum3_1); 
  
  input   clk;
  input   sign5;
  input   [11:0] exp5;
  input   [47:0] a5;
  input   [18:0] b5;
  input   [105:0] sum3;
  output reg sign6;
  output reg [11:0] exp6;
  output reg [47:0] a6;
  output reg [18:0] b6;
  output reg [105:0] sum3_1;
    
  always@ (posedge clk)
    begin
      sign6 <= sign5;
      exp6 <= exp5;
      a6 <= a5;
      b6 <= b5;
      sum3_1 <= sum3; 
    end

endmodule





