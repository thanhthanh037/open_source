`include "parameter.v"
module NoC_interface_8x8 (clk, rst, x_router, y_router,
						  isend, iaccept, iflit, 
                          done_hidden, done_output, rdata_1, rdata_2, rdata_3, rdata_4, rdata_5, rdata_6, rdata_7, rdata_8,
						  osend, oaccept, oflit, 
						  w_start, we_i, we_w_1, we_w_2, we_w_3, we_w_4, we_w_5, we_w_6, we_w_7, we_w_8,
						  re_o, waddr, wdata);                       



  input clk, rst;  
  input [2:0] x_router, y_router; 
  input isend, iaccept;
  input [flit_data_width-1:0] iflit;
  input done_hidden, done_output;
  input [11:0] rdata_1, rdata_2, rdata_3, rdata_4, rdata_5, rdata_6, rdata_7, rdata_8; 
  output reg osend, oaccept;
  output reg [flit_data_width-1:0] oflit;
  output reg  w_start, we_i, we_w_1, we_w_2, we_w_3, we_w_4, we_w_5, we_w_6, we_w_7, we_w_8, re_o;  
  output reg [5:0] waddr;  
  output reg [flit_data_width-3:0] wdata;
  
  reg [1:0] r_state;
  reg [2:0] t_state;
  reg [2:0] count_t;
  reg [2:0] count_r;

  //receiver
  always@ (posedge clk)
  begin
    if (rst)
	  begin
        r_state <= 0;
		count_r <= 0;
	  end
    else
      case (r_state)
        0:
          begin
            oaccept <= 0;
            we_i <= 0;
            we_w_1 <= 0;
			we_w_2 <= 0;
			we_w_3 <= 0;
			we_w_4 <= 0;
            we_w_5 <= 0;
			we_w_6 <= 0;
			we_w_7 <= 0;
			we_w_8 <= 0;				
			w_start <= 0;
            if (isend)
              begin
				w_start <= 1;
                oaccept <= 1;
				r_state <= 1;
			  end
		    end
		1:
		  begin
		    oaccept <= 0;
            if (iflit[flit_data_width-1: flit_data_width-2] == 2'b10)
              begin
                if (iflit[flit_data_width-3])
				  r_state <= 2;
			    else 
				  r_state <= 3;
              end
          end       
        2:
		  begin
            if (!iflit[flit_data_width-1])
			  begin
				w_start <= 0;
                we_i <= 1;
			  end
			if (iflit[flit_data_width-2])
			  begin
 			    r_state <= 0;              
              end
		  end
        3:
  	      begin
            if (!iflit[flit_data_width-1])
			  case(count_r)
                0: we_w_1 <= 1;
				1: we_w_2 <= 1;
				2: we_w_3 <= 1;
				3: we_w_4 <= 1;
                4: we_w_5 <= 1;
				5: we_w_6 <= 1;
				6: we_w_7 <= 1;
				7: we_w_8 <= 1;					 
			  endcase
			if (iflit[flit_data_width-2])
			  begin
				if (count_r == 7)
				  count_r <= 0;
				else
				  count_r <= count_r + 1'b1;
				  r_state <= 0;              
              end
		  end 
        default: r_state <= 0;
      endcase
	wdata <= iflit[flit_data_width-3:0];
    if (iflit[flit_data_width-1])
      case (iflit[flit_data_width-4:flit_data_width-9])
        0: waddr <= 0; 		
        1: waddr <= 0;
        2: waddr <= 1;
        3: waddr <= 2;
        4: waddr <= 3;     
        8: waddr <= 4;
        9: waddr <= 5;
        10: waddr <= 6;
        11: waddr <= 7;
        16: waddr <= 8;
        17: waddr <= 9;
        18: waddr <= 10;
        19: waddr <= 11;
        24: waddr <= 12;
        25: waddr <= 13;
        26: waddr <= 14;
        27: waddr <= 15;
        32: waddr <= 16;
        33: waddr <= 17;
        34: waddr <= 18;
        35: waddr <= 19;
        40: waddr <= 20;	
        41: waddr <= 21;
        42: waddr <= 22;
        43: waddr <= 23;
        48: waddr <= 24;
        49: waddr <= 25;
        50: waddr <= 26;	
        51: waddr <= 27;
        56: waddr <= 28;
        57: waddr <= 29;
        58: waddr <= 30;
        59: waddr <= 31;
        default: waddr <= 63;
      endcase	 
  end
  
  //transmiter
  always@ (posedge clk)
  begin
    if (rst)
      t_state <= 0;
    else
      case (t_state)
        0:
          begin
            re_o <= 0;
            if (done_hidden)
              begin
                osend <= 1;
                oflit <= {3'b101, x_router, y_router, 121'b0};
                t_state <= 1;
              end
			else if (done_output)
              begin
                osend <= 1;
                oflit <= {3'b100, x_router, y_router, 121'b0};
                t_state <= 2;
              end				  
          end
        1:
          begin            
            if (iaccept)
			  begin
				osend <= 0;              
                count_t <= 0;
				re_o <= 1;
				t_state <= 3;
              end
          end
        3:
          begin
            count_t <= count_t + 1'b1;
			if (count_t != 0)
			  oflit <= {2'b00, 4'b0, rdata_1, 4'b0, rdata_2, 4'b0, rdata_3, 4'b0, rdata_4, 4'b0, rdata_5, 4'b0, rdata_6, 4'b0, rdata_7, 4'b0, rdata_8};//1 redundant cycle due to synchronous read
		    if (count_t == op_per_neuron_hidden -1'b1)
			  begin
 		        re_o <= 0;
              end					 
            if (count_t == op_per_neuron_hidden)
              begin 
                oflit <= {2'b01, 4'b0, rdata_1, 4'b0, rdata_2, 4'b0, rdata_3, 4'b0, rdata_4, 4'b0, rdata_5, 4'b0, rdata_6, 4'b0, rdata_7, 4'b0, rdata_8};
				t_state <= 0;
              end              
          end
        2:
          begin            
            if (iaccept)
              begin
				osend <= 0;
                count_t <= 0;
				re_o <= 1;
				t_state <= 4;
              end
          end	 
        4:
          begin
            count_t <= count_t + 1'b1;
			if (count_t != 0)
			  oflit <= {2'b00, 4'b0, rdata_1, 4'b0, rdata_2, 4'b0, rdata_3, 4'b0, rdata_4, 4'b0, rdata_5, 4'b0, rdata_6, 4'b0, rdata_7, 4'b0, rdata_8};//1 redundant cycle due to synchronous read
			if (count_t == op_per_neuron_output -1'b1)
			  begin
 		        re_o <= 0;
              end					 
            if (count_t == op_per_neuron_output)
              begin 
                oflit <= {2'b01, 4'b0, rdata_1, 4'b0, rdata_2, 4'b0, rdata_3, 4'b0, rdata_4, 4'b0, rdata_5, 4'b0, rdata_6, 4'b0, rdata_7, 4'b0, rdata_8};
				t_state <= 0;
              end              
          end
        default: t_state <= 0;
      endcase
  end  
         
endmodule