module m_PL_reg9 (clk, sign8, exp8, a0b3, sum4_1, sign9, exp9, a0b3_1, sum4_2);
  
  input   clk;
  input   sign8;
  input   [11:0] exp8;
  input   [25:0] a0b3;
  input   [105:0] sum4_1;
  output reg sign9;
  output reg [11:0] exp9;
  output reg [25:0] a0b3_1;  
  output reg [105:0] sum4_2;
    
  always@ (posedge clk)
    begin
      sign9 <= sign8;
      exp9 <= exp8;
      a0b3_1 <= a0b3;
      sum4_2 <= sum4_1;      
    end

endmodule




