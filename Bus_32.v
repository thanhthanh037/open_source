`include "parameters.v"
module Bus_32 (clk, rst, wrq0, wrq1, wrq2, wrq3, wrq4, wrq5, wrq6, wrq7, wrq8, wrq9, 
               wrq10, wrq11, wrq12, wrq13, wrq14, wrq15, wrq16, wrq17, wrq18, wrq19,
			   wrq20, wrq21, wrq22, wrq23, wrq24, wrq25, wrq26, wrq27, wrq28, wrq29, 
			   wrq30, wrq31, 				  
               waddr0, waddr1, waddr2, waddr3, waddr4, waddr5, waddr6, waddr7, waddr8, waddr9, 
               waddr10, waddr11, waddr12, waddr13, waddr14, waddr15, waddr16, waddr17, waddr18, waddr19,
			   waddr20, waddr21, waddr22, waddr23, waddr24, waddr25, waddr26, waddr27, waddr28, waddr29, 
			   waddr30, waddr31,
               wdata_in0, wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, 
               wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, wdata_in17, wdata_in18, wdata_in19,
			   wdata_in20, wdata_in21, wdata_in22, wdata_in23, wdata_in24, wdata_in25, wdata_in26, wdata_in27, wdata_in28, wdata_in29, 
			   wdata_in30, wdata_in31,				  
               wgr0, wgr1, wgr2, wgr3, wgr4, wgr5, wgr6, wgr7, wgr8, wgr9, 
               wgr10, wgr11, wgr12, wgr13, wgr14, wgr15, wgr16, wgr17, wgr18, wgr19,
			   wgr20, wgr21, wgr22, wgr23, wgr24, wgr25, wgr26, wgr27, wgr28, wgr29, 
			   wgr30, wgr31,			  
			   we0, we1, we2, we3, we4, we5, we6, we7, we8, we9, 
               we10, we11, we12, we13, we14, we15, we16, we17, we18, we19,
			   we20, we21, we22, we23, we24, we25, we26, we27, we28, we29, 
			   we30, we31,
			   broadcast,
			   source_node,
               wdata_out);
  
  input clk, rst;
  input wrq0, wrq1, wrq2, wrq3, wrq4, wrq5, wrq6, wrq7, wrq8, wrq9;
  input wrq10, wrq11, wrq12, wrq13, wrq14, wrq15, wrq16, wrq17, wrq18, wrq19;
  input wrq20, wrq21, wrq22, wrq23, wrq24, wrq25, wrq26, wrq27, wrq28, wrq29; 
  input wrq30, wrq31;
  input [4:0] waddr0;
  input waddr1, waddr2, waddr3, waddr4, waddr5, waddr6, waddr7, waddr8, waddr9;
  input waddr10, waddr11, waddr12, waddr13, waddr14, waddr15, waddr16, waddr17, waddr18, waddr19;
  input waddr20, waddr21, waddr22, waddr23, waddr24, waddr25, waddr26, waddr27, waddr28, waddr29; 
  input waddr30, waddr31;
  input [data_width-1:0] wdata_in0;
  input [data_width-1:0] wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9;
  input [data_width-1:0] wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, wdata_in17, wdata_in18, wdata_in19;
  input [data_width-1:0] wdata_in20, wdata_in21, wdata_in22, wdata_in23, wdata_in24, wdata_in25, wdata_in26, wdata_in27, wdata_in28, wdata_in29; 
  input [data_width-1:0] wdata_in30, wdata_in31;
				  
  output reg wgr0, wgr1, wgr2, wgr3, wgr4, wgr5, wgr6, wgr7, wgr8, wgr9;
  output reg wgr10, wgr11, wgr12, wgr13, wgr14, wgr15, wgr16, wgr17, wgr18, wgr19;
  output reg wgr20, wgr21, wgr22, wgr23, wgr24, wgr25, wgr26, wgr27, wgr28, wgr29; 
  output reg wgr30, wgr31;
  output reg we0, we1, we2, we3, we4, we5, we6, we7, we8, we9;
  output reg we10, we11, we12, we13, we14, we15, we16, we17, we18, we19;
  output reg we20, we21, we22, we23, we24, we25, we26, we27, we28, we29; 
  output reg we30, we31;  
  output reg broadcast;
  output reg [4:0] source_node;
  output reg [data_width-1:0] wdata_out;
  
  reg [5:0] state;
  reg count;
  
  always@ (posedge clk)
  if (rst) 
    state <= 0;
  else
    case (state)
	   0: 
		  begin
		    wgr0 <= 0;
		    wgr1 <= 0;
		    wgr2 <= 0;		
		    wgr3 <= 0;
		    wgr4 <= 0;	
		    wgr5 <= 0;
		    wgr6 <= 0;		
		    wgr7 <= 0;
		    wgr8 <= 0;	
		    wgr9 <= 0;
		    wgr10 <= 0;		
		    wgr11 <= 0;
		    wgr12 <= 0;	
		    wgr13 <= 0;
		    wgr14 <= 0;		
		    wgr15 <= 0;
			wgr16 <= 0;
		    wgr17 <= 0;
		    wgr18 <= 0;	
		    wgr19 <= 0;	
		    wgr20 <= 0;
		    wgr21 <= 0;
		    wgr22 <= 0;		
		    wgr23 <= 0;
		    wgr24 <= 0;	
		    wgr25 <= 0;
		    wgr26 <= 0;		
		    wgr27 <= 0;
		    wgr28 <= 0;	
		    wgr29 <= 0;
		    wgr30 <= 0;
		    wgr31 <= 0;   			 
		    we0 <= 0;
		    we1 <= 0;
		    we2 <= 0;		
		    we3 <= 0;
		    we4 <= 0;	
		    we5 <= 0;
		    we6 <= 0;		
		    we7 <= 0;
		    we8 <= 0;	
		    we9 <= 0;
		    we10 <= 0;		
		    we11 <= 0;
		    we12 <= 0;	
		    we13 <= 0;
		    we14 <= 0;		
		    we15 <= 0;
			 we16 <= 0;
		    we17 <= 0;
		    we18 <= 0;	
		    we19 <= 0;	
		    we20 <= 0;
		    we21 <= 0;
		    we22 <= 0;		
		    we23 <= 0;
		    we24 <= 0;	
		    we25 <= 0;
		    we26 <= 0;		
		    we27 <= 0;
		    we28 <= 0;	
		    we29 <= 0;
		    we30 <= 0;
		    we31 <= 0;	    			 
			count <= 0;
            broadcast <= 0;			 
			  if (wrq0)
			    begin
				   wgr0 <= 1;
				   state <= 1;
				 end
			  else if (wrq1)
			    begin
				   wgr1 <= 1;
				   state <= 2;
				 end				 
			  else if (wrq2)
			    begin
				   wgr2 <= 1;
				   state <= 3;
				 end
			  else if (wrq3)
			    begin
				   wgr3 <= 1;
				   state <= 4;
				 end
			  else if (wrq4)
			    begin
				   wgr4 <= 1;
				   state <= 5;
				 end
			  else if (wrq5)
			    begin
				   wgr5 <= 1;
				   state <= 6;
				 end
			  else if (wrq6)
			    begin
				   wgr6 <= 1;
				   state <= 7;
				 end
			  else if (wrq7)
			    begin
				   wgr7 <= 1;
				   state <= 8;
				 end
			  else if (wrq8)
			    begin
				   wgr8 <= 1;
				   state <= 9;
				 end
			  else if (wrq9)
			    begin
				   wgr9 <= 1;
				   state <= 10;
				 end
			  else if (wrq10)
			    begin
				   wgr10 <= 1;
				   state <= 11;
				 end
			  else if (wrq11)
			    begin
				   wgr11 <= 1;
				   state <= 12;
				 end
			  else if (wrq12)
			    begin
				   wgr12 <= 1;
				   state <= 13;
				 end
			  else if (wrq13)
			    begin
				   wgr13 <= 1;
				   state <= 14;
				 end
			  else if (wrq14)
			    begin
				   wgr14 <= 1;
				   state <= 15;
				 end
			  else if (wrq15)
			    begin
				   wgr15 <= 1;
				   state <= 16;
				 end
			  else if (wrq16)
			    begin
				   wgr16 <= 1;
				   state <= 17;
				 end
			  else if (wrq17)
			    begin
				   wgr17 <= 1;
				   state <= 18;
				 end
			  else if (wrq18)
			    begin
				   wgr18 <= 1;
				   state <= 19;
				 end
			  else if (wrq19)
			    begin
				   wgr19 <= 1;
				   state <= 20;
				 end
			  else if (wrq20)
			    begin
				   wgr20 <= 1;
				   state <= 21;
				 end
			  else if (wrq21)
			    begin
				   wgr21 <= 1;
				   state <= 22;
				 end				 
			  else if (wrq22)
			    begin
				   wgr22 <= 1;
				   state <= 23;
				 end
			  else if (wrq23)
			    begin
				   wgr23 <= 1;
				   state <= 24;
				 end
			  else if (wrq24)
			    begin
				   wgr24 <= 1;
				   state <= 25;
				 end
			  else if (wrq25)
			    begin
				   wgr25 <= 1;
				   state <= 26;
				 end
			  else if (wrq26)
			    begin
				   wgr26 <= 1;
				   state <=27;
				 end
			  else if (wrq27)
			    begin
				   wgr27 <= 1;
				   state <= 28;
				 end
			  else if (wrq28)
			    begin
				   wgr28 <= 1;
				   state <= 29;
				 end
			  else if (wrq29)
			    begin
				   wgr29 <= 1;
				   state <= 30;
				 end	
			  else if (wrq30)
			    begin
				   wgr30 <= 1;
				   state <= 31;
				 end
			  else if (wrq31)
			    begin
				   wgr31 <= 1;
				   state <= 32;
				 end
        end			 
      1:
		  begin
			 count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr0)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;		 	
						 broadcast <= 1;					 
					  end
					1: we1 <= 1;
					2: we2 <= 1;	
					3: we3 <= 1;
					4: we4 <= 1;	
					5: we5 <= 1;
					6: we6 <= 1;	
					7: we7 <= 1;
					8: we8 <= 1;	
					9: we9 <= 1;
					10: we10 <= 1;	
					11: we11 <= 1;
					12: we12 <= 1;	
					13: we13 <= 1;
					14: we14 <= 1;	
					15: we15 <= 1;
					16: we16 <= 1;	
					17: we17 <= 1;
					18: we18 <= 1;	
					19: we19 <= 1;
					20: we20 <= 1;	
					21: we21 <= 1;
					22: we22 <= 1;	
					23: we23 <= 1;
					24: we24 <= 1;	
					25: we25 <= 1;
					26: we26 <= 1;	
					27: we27 <= 1;
					28: we28 <= 1;	
					29: we29 <= 1;
					30: we30 <= 1;	
					31: we31 <= 1;			
				  endcase
				end
			 source_node <= 0;
			 if (!wrq0)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end	
      2:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr1)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 1;
			 if (!wrq1)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      3:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr2)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 2;
			 if (!wrq2)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      4:
        begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr3)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 3;
			 if (!wrq3)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      5:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr4)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 4;
			 if (!wrq4)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      6:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr5)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 5;
			 if (!wrq5)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      7:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr6)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 6;
			 if (!wrq6)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      8:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr7)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 7;
			 if (!wrq7)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      9:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr8)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 8;
			 if (!wrq8)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      10:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr9)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 9;
			 if (!wrq9)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      11:
		  begin
			 count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr10)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;					 
					  end
					1: we0 <= 1;		
				  endcase
				end
			 source_node <= 10;
			 if (!wrq10)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  state <= 0;
				end
        end	
      12:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr11)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 11;
			 if (!wrq11)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      13:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr12)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 12;
			 if (!wrq12)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      14:
        begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr13)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 13;
			 if (!wrq13)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      15:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr14)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 14;
			 if (!wrq14)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;	  
				  state <= 0;
				end
        end
      16:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr15)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 15;
			 if (!wrq15)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      17:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr16)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 16;
			 if (!wrq16)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      18:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr17)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 17;
			 if (!wrq17)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      19:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr18)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 18;
			 if (!wrq18)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      20:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr19)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 19;
			 if (!wrq19)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end	
      21:
		  begin
			 count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr20)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;					 
					  end
					1: we0 <= 1;
				  endcase
				end
			 source_node <= 20;
			 if (!wrq20)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end	
      22:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr21)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 21;
			 if (!wrq21)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      23:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr22)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 22;
			 if (!wrq22)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      24:
        begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr23)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 23;
			 if (!wrq23)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      25:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr24)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 24;
			 if (!wrq24)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      26:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr25)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 25;
			 if (!wrq25)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      27:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr26)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 26;
			 if (!wrq26)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      28:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr27)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 27;
			 if (!wrq27)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      29:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr28)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;	 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 28;
			 if (!wrq28)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
      30:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr29)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 29;
			 if (!wrq29)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end	
      31:
		  begin
			 count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr30)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;	
						 broadcast <= 1;					 
					  end
					1: we0 <= 1;
				  endcase
				end
			 source_node <= 30;
			 if (!wrq30)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end	
      32:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr31)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 31;
			 if (!wrq31)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  state <= 0;
				end
        end
    endcase
 
  always@ (posedge clk)
  begin
    case (state)
	   1: wdata_out = wdata_in0;
	   2: wdata_out = wdata_in1;
	   3: wdata_out = wdata_in2;
	   4: wdata_out = wdata_in3;	
	   5: wdata_out = wdata_in4;
	   6: wdata_out = wdata_in5;	
	   7: wdata_out = wdata_in6;
	   8: wdata_out = wdata_in7;	
	   9: wdata_out = wdata_in8;
	   10: wdata_out = wdata_in9;	
	   11: wdata_out = wdata_in10;
	   12: wdata_out = wdata_in11;	
	   13: wdata_out = wdata_in12;
	   14: wdata_out = wdata_in13;	
	   15: wdata_out = wdata_in14;
	   16: wdata_out = wdata_in15;	
	   17: wdata_out = wdata_in16;
	   18: wdata_out = wdata_in17;	
	   19: wdata_out = wdata_in18;
	   20: wdata_out = wdata_in19;	
	   21: wdata_out = wdata_in20;
	   22: wdata_out = wdata_in21;	
	   23: wdata_out = wdata_in22;
	   24: wdata_out = wdata_in23;	
	   25: wdata_out = wdata_in24;
	   26: wdata_out = wdata_in25;	
	   27: wdata_out = wdata_in26;
	   28: wdata_out = wdata_in27;	
	   29: wdata_out = wdata_in28;
	   30: wdata_out = wdata_in29;
	   31: wdata_out = wdata_in30;
	   32: wdata_out = wdata_in31;	
	   default: wdata_out = 0;
    endcase
  end	 
		
endmodule