module m_PL_reg4 (clk, sign3, exp3, a3, b3, sum2, sign4, exp4, a4, b4, sum2_1); 
  
  input   clk;
  input   sign3;
  input   [11:0] exp3;
  input   [52:0] a3;
  input   [35:0] b3;
  input   [105:0] sum2;
  output reg sign4;
  output reg [11:0] exp4;
  output reg [52:0] a4;
  output reg [35:0] b4;
  output reg [105:0] sum2_1;
    
  always@ (posedge clk)
    begin
      sign4 <= sign3;
      exp4 <= exp3;
      a4 <= a3;
      b4 <= b3;
      sum2_1 <= sum2;      
    end

endmodule



