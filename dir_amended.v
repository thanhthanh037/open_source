`include "parameters.v"
module dir_amended (rst, clk, dir, dir_n, dir_s, dir_e, dir_w,
                    send_rq, send_rq_n, send_rq_s, send_rq_e, send_rq_w, 
                    eop, eop_n, eop_s, eop_e, eop_w,
						  re_pn, re_ps, re_pe, re_pw,
						  re_np, re_ns, re_ne, re_nw,
						  re_sp, re_sn, re_se, re_sw,
						  re_ep, re_en, re_es, re_ew,
						  re_wp, re_wn, re_ws, re_we,
						  dir_amended, dir_n_amended, dir_s_amended, dir_e_amended, dir_w_amended);
 
  input clk, rst; 
  input [4:0] dir, dir_n, dir_s, dir_e, dir_w;
  input send_rq, send_rq_n, send_rq_s, send_rq_e, send_rq_w;
  input eop, eop_n, eop_s, eop_e, eop_w;
  input re_pn, re_ps, re_pe, re_pw;
  input re_np, re_ns, re_ne, re_nw;
  input re_sp, re_sn, re_se, re_sw;
  input re_ep, re_en, re_es, re_ew;
  input re_wp, re_wn, re_ws, re_we;
  output reg [4:0] dir_amended, dir_n_amended, dir_s_amended, dir_e_amended, dir_w_amended;  
  
  always@ (posedge clk)  
  if (rst)
    dir_amended <= NA;
  else 
    case(dir_amended)
      NSEW:
	    if (re_np && eop)
		  dir_amended <= SEW;
	  NSE:
	    if (re_np && eop)
		  dir_amended <= SE;	
	  NSW:
	    if (re_sp && eop)
		  dir_amended <= NW;		
	  NEW:
	    if (re_wp && eop)
		  dir_amended <= NE;			 
	  SEW:
        if (re_ep && eop)
		  dir_amended <= SW;
      NS:
        if (re_sp && eop)
		  dir_amended <= N;
      NE:
        if (re_np && eop)
		  dir_amended <= E;
      NW:
        if (re_wp && eop)
		  dir_amended <= N;			 
      SE:
        if (re_ep && eop)
		  dir_amended <= S;	
      SW:
        if (re_sp && eop)
		    dir_amended <= W;	
      EW:
        if (re_ep && eop)
		  dir_amended <= W;			 
      N:
        if (re_np && eop)
          dir_amended <= NA; 
      S:
        if (re_sp && eop)
          dir_amended <= NA;
      E:
        if (re_ep && eop)
          dir_amended <= NA;
      W:
        if (re_wp && eop)
          dir_amended <= NA;	
      NA: 
		  if (send_rq)
          dir_amended <= dir;			 
	   default: dir_amended <= dir; 
    endcase

  always@ (posedge clk)  
  if (rst)
    dir_n_amended <= NA;
  else 
    case(dir_n_amended)
	  SEP:
        if (re_en && eop_n)
		  dir_n_amended <= SP;
	  SWP:
        if (re_sn && eop_n)
		  dir_n_amended <= WP;	
	  SEW:
        if (re_en && eop_n)
		  dir_n_amended <= SW;
      SE:
        if (re_en && eop_n)
		  dir_n_amended <= S;	
      SW:
        if (re_sn && eop_n)
		  dir_n_amended <= W;	
      EW:
        if (re_en && eop_n)
		  dir_n_amended <= W;	
      SP:
        if (re_pn && eop_n)
		  dir_n_amended <= S;				 
      EP:
        if (re_pn && eop_n)
		  dir_n_amended <= E;	
      WP:
        if (re_pn && eop_n)
		  dir_n_amended <= W;	
      P:
        if (re_pn && eop_n)
          dir_n_amended <= NA; 
      S:
        if (re_sn && eop_n)
          dir_n_amended <= NA;
      E:
        if (re_en && eop_n)
          dir_n_amended <= NA;
      W:
        if (re_wn && eop_n)
          dir_n_amended <= NA;	
      NA: 
		if (send_rq_n)
          dir_n_amended <= dir_n;			 
	  default: dir_n_amended <= dir_n; 
    endcase
	 
  always@ (posedge clk)  
  if (rst)
    dir_s_amended <= NA;
  else 
    case(dir_s_amended)
	  NEP:
        if (re_ns && eop_s)
		  dir_s_amended <= EP;
	  NWP:
        if (re_ws && eop_s)
		  dir_s_amended <= NP;
	  NEW:
	    if (re_ws && eop_s)
		  dir_s_amended <= NE;			 
      NE:
        if (re_ns && eop_s)
		  dir_s_amended <= E;
      NW:
        if (re_ws && eop_s)
		  dir_s_amended <= N;			 
      EW:
        if (re_es && eop_s)
		  dir_s_amended <= W;			 
      NP:
        if (re_ps && eop_s)
		  dir_s_amended <= N;
      EP:
        if (re_ps && eop_s)
		  dir_s_amended <= E;	
      WP:
        if (re_ps && eop_s)
		  dir_s_amended <= W;				 
      P:
        if (re_ps && eop_s)
          dir_s_amended <= NA; 
      N:
        if (re_ns && eop_s)
          dir_s_amended <= NA;
      E:
        if (re_es && eop_s)
          dir_s_amended <= NA;
      W:
        if (re_ws && eop_s)
          dir_s_amended <= NA;
      NA: 
		if (send_rq_s)
          dir_s_amended <= dir_s;		  
	  default: dir_s_amended <= dir_s; 
    endcase

  always@ (posedge clk)  
  if (rst)
    dir_e_amended <= NA;
  else 
    case(dir_e_amended)
	  NWP:
        if (re_we && eop_e)
		  dir_e_amended <= NP;  
	  SWP:
        if (re_se && eop_e)
		  dir_e_amended <= WP;
	  NSW:
	    if (re_se && eop_e)
		  dir_e_amended <= NW;		
      NS:
        if (re_se && eop_e)
		  dir_e_amended <= N;
      NW:
        if (re_we && eop_e)
		  dir_e_amended <= N;			 
      SW:
        if (re_se && eop_e)
		  dir_e_amended <= W;				 
      NP:
        if (re_pe && eop_e)
		  dir_e_amended <= N;
      SP:
        if (re_pe && eop_e)
		  dir_e_amended <= S;			 
      WP:
        if (re_pe && eop_e)
		  dir_e_amended <= W;		 
      P:
        if (re_pe && eop_e)
          dir_e_amended <= NA; 
      N:
        if (re_ne && eop_e)
          dir_e_amended <= NA;
      S:
        if (re_se && eop_e)
          dir_e_amended <= NA;
      W:
        if (re_we && eop_e)
          dir_e_amended <= NA;	
      NA: 
		  if (send_rq_e)
          dir_e_amended <= dir_e;			 
	  default: dir_e_amended <= dir_e; 
    endcase

  always@ (posedge clk)  
  if (rst)
    dir_w_amended <= NA;
  else 
    case(dir_w_amended)
	  NEP:
        if (re_nw && eop_w)
		  dir_w_amended <= EP;
	  SEP:
        if (re_ew && eop_w)
		  dir_w_amended <= SP;
	  NSE:
	    if (re_nw && eop_w)
		  dir_w_amended <= SE;	
      NS:
        if (re_sw && eop_w)
		  dir_w_amended <= N;
      NE:
        if (re_nw && eop_w)
		  dir_w_amended <= E;
      NP:
        if (re_pw && eop_w)
		  dir_w_amended <= N;	
      SE:
        if (re_ew && eop_w)
		  dir_w_amended <= S;				 
      SP:
        if (re_pw && eop_w)
		  dir_w_amended <= S;	
      EP:
        if (re_pw && eop_w)
		  dir_w_amended <= E;	
      P:
        if (re_pw && eop_w)
          dir_w_amended <= NA; 
      N:
        if (re_nw && eop_w)
          dir_w_amended <= NA;
      S:
        if (re_sw && eop_w)
          dir_w_amended <= NA;
      E:
        if (re_ew && eop_w)
          dir_w_amended <= NA;
      NA: 
		if (send_rq_w)
          dir_w_amended <= dir_w;			 
	  default: dir_w_amended <= dir_w; 
    endcase
	 
endmodule
