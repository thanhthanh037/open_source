//------------------------------------------------------------------------------------------------------
// File         : pe_Bus.v
// Author       : Thanh Bui
// Organization : The University of Adelaide
// Created      : May 2015
// Platform     : VHDL 1993
// Simulators   : Modelsim
// Synthesizers : ISE 14.7
// Targets      : Virtex-7 VC707
// Description  : IEEE 754 compliant double-precision floating-point FFT processing element
//                Compatible with Circuit switching NoC-based communication
//                On chip memory of 62 x 36Kb block RAMs 
//                Maximum FFT size of 2^13 points
//------------------------------------------------------------------------------------------------------
`include "parameter.v"
module pe_Bus (clk, rst, ireq, irep, idata, oreq, oaddr, odata);
  
  input clk, rst, ireq, irep;
  input [bus_data_width-1:0] idata;
  output oreq;
  output [3:0] oaddr;
  output [bus_data_width-1:0] odata;

  wire we_a_n, we_b_n, we_w, re_a_n, re_b_n, start;
  wire re_a_c, re_b_c, re_1st, we_a_c, we_b_c, send_pe, send_pc, a_or_b;
  wire we_a, we_b, we2_a, we2_b, re_a, re_b, re_w;
  wire [63:0] ar_wdata, ai_wdata, br_wdata, bi_wdata, wr_wdata, wi_wdata;
  wire [63:0] ar_rdata, ai_rdata, br_rdata, bi_rdata, wr_rdata, wi_rdata, wr, wi;
  wire [63:0] idata1_ar, idata1_ai, idata1_br, idata1_bi, idata2_ar, idata2_ai, idata2_br, idata2_bi;
  wire [63:0] mult1_result, mult2_result, mult3_result, mult4_result, bw_real, bw_img, sum_real, sum_img, dif_real, dif_img;  
  wire [11:0] op_per_pe, w_per_pe;
  wire [4:0] no_of_stage;
  wire [2:0] no_of_pe; 
  wire [1:0] dest;

  mult mult11 (clk, br_rdata, wr, mult1_result);
  mult mult21 (clk, bi_rdata, wi, mult2_result);
  mult mult31 (clk, br_rdata, wi, mult3_result);
  mult mult41 (clk, bi_rdata, wr, mult4_result);

  adder add11 (clk, mult1_result, {!mult2_result[63], mult2_result[62:0]}, bw_real);//sub
  adder add21 (clk, mult3_result, mult4_result, bw_img);
  adder add31 (clk, ar_rdata, bw_real, sum_real);
  adder add41 (clk, ai_rdata, bw_img, sum_img);
  adder add51 (clk, ar_rdata, {!bw_real[63], bw_real[62:0]}, dif_real);
  adder add61 (clk, ai_rdata, {!bw_img[63], bw_img[62:0]}, dif_img);  

  Bus_interface Bus_interface (clk, rst, ireq, irep, idata, send_pe, send_pc,
                      dest, a_or_b, sum_real, sum_img, dif_real, dif_img,
                      ar_rdata, ai_rdata, br_rdata, bi_rdata,
                      oreq, oaddr, odata, op_per_pe, no_of_stage, no_of_pe,
                      start, we_a_n, we_b_n, we_w, re_a_n, re_b_n,
                      ar_wdata, ai_wdata, br_wdata, bi_wdata, wr_wdata, wi_wdata); 

  control control (clk, rst, start, op_per_pe, no_of_stage, no_of_pe, 
                       re_a_c, re_b_c, re_1st, we_a_c, we_b_c, we2_a, we2_b, w_per_pe,  
                       send_pe, send_pc, dest, a_or_b); 

  RAM RAM_ar (clk, rst, re_a, we_a, we2_a, idata1_ar, idata2_ar, ar_rdata); 
  RAM RAM_ai (clk, rst, re_a, we_a, we2_a, idata1_ai, idata2_ai, ai_rdata); 
  RAM RAM_br (clk, rst, re_b, we_b, we2_b, idata1_br, idata2_br, br_rdata); 
  RAM RAM_bi (clk, rst, re_b, we_b, we2_b, idata1_bi, idata2_bi, bi_rdata); 
  RAM_w RAM_wr (clk, rst, re_w, we_w, op_per_pe, w_per_pe, wr_wdata, wr_rdata); 
  RAM_w RAM_wi (clk, rst, re_w, we_w, op_per_pe, w_per_pe, wi_wdata, wi_rdata); 

  assign re_a = re_a_n || re_a_c;
  assign re_b = re_b_n || re_b_c;
  assign re_w = re_1st ? 1'b0 : re_b_c;
  assign we_a = we_a_n || we_a_c;
  assign we_b = we_b_n || we_b_c;

  assign idata1_ar = we_a_n ? ar_wdata : (we_a_c || we2_a) ? sum_real : 64'b0;
  assign idata2_ar = we2_a ? dif_real : 64'b0;
  assign idata1_ai = we_a_n ? ai_wdata : (we_a_c || we2_a) ? sum_img : 64'b0; 
  assign idata2_ai = we2_a ? dif_img : 64'b0;   
  assign idata1_br = we_b_n ? br_wdata : we_b_c ? dif_real : we2_b ? sum_real : 64'b0;
  assign idata2_br = we2_b ? dif_real : 64'b0;
  assign idata1_bi = we_b_n ? bi_wdata : we_b_c ? dif_img : we2_b ? sum_img : 64'b0; 
  assign idata2_bi = we2_b ? dif_img : 64'b0;
  
  assign wr = re_1st ? 64'h3ff0000000000000 : wr_rdata;
  assign wi = re_1st ? 64'b0 : wi_rdata;
                                     
endmodule
