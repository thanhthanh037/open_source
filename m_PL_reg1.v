module m_PL_reg1 (clk, sign0, exp0, a0, b0, a2b2, a1b1, a0b0, a2b1, a1b0, sign1, exp1, a1, b1, a2b2_1, a1b1_1, a0b0_1, a2b1_1, a1b0_1);  
  
  input   clk;
  input   sign0;
  input   [11:0] exp0;
  input   [52:0] a0, b0;
  input   [21:0] a2b2, a2b1;
  input   [40:0] a1b1, a0b0, a1b0;
  output reg sign1;
  output reg [11:0] exp1;
  output reg [52:0] a1, b1;
  output reg [21:0] a2b2_1, a2b1_1;
  output reg [40:0] a1b1_1, a0b0_1, a1b0_1;
    
  always@ (posedge clk)
    begin
      sign1 <= sign0;
      exp1 <= exp0;
      a1 <= a0;
      b1 <= b0;
      a2b2_1 <= a2b2;
      a1b1_1 <= a1b1;
      a0b0_1 <= a0b0;
      a2b1_1 <= a2b1;
      a1b0_1 <= a1b0;
    end

endmodule