module m_PL_reg15 (clk, sign14, exp_nor2, normalise2_result, sign15, exp_nor3, normalise2_result1);

  input clk;
  input sign14;  
  input [10:0] exp_nor2;
  input [104:0] normalise2_result;
  output reg sign15;  
  output reg [10:0] exp_nor3;
  output reg [104:0] normalise2_result1;
  
  always@ (posedge clk)
    begin
      sign15 <= sign14;
      exp_nor3 <= exp_nor2;
      normalise2_result1 <= normalise2_result;
   end

endmodule 
