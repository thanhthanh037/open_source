module m_PL_reg0 (clk, sign, exp, man_a, man_b, sign0, exp0, a0, b0);
  
  input   clk;
  input   sign;
  input   [11:0] exp;
  input   [52:0] man_a, man_b;
  output reg  sign0;
  output reg [11:0] exp0;
  output reg [52:0] a0, b0;

  always@ (posedge clk)
    begin
      sign0 <= sign;
      exp0 <= exp;
      a0 <= man_a;
      b0 <= man_b;
    end

endmodule