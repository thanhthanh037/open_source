module m_PL_reg11 (clk, sign10, exp10, mult_result1, zero1_no, zero2_no, zero3_no, zero4_no, zero5_no, zero6_no, zero7_no, zero8_no, sign11, exp11, mult_result2, zero1_no1, zero2_no1, zero3_no1, zero4_no1, zero5_no1, zero6_no1, zero7_no1, zero8_no1); 
  
  input   clk;
  input   sign10;
  input   [11:0] exp10;
  input   [105:0] mult_result1;
  input   [4:0] zero1_no, zero2_no, zero3_no, zero4_no, zero5_no, zero6_no, zero7_no, zero8_no;
  output reg sign11;
  output reg [11:0] exp11;
  output reg [105:0] mult_result2;
  output reg [4:0] zero1_no1, zero2_no1, zero3_no1, zero4_no1, zero5_no1, zero6_no1, zero7_no1, zero8_no1;
    
  always@ (posedge clk)
    begin
      sign11 <= sign10;
      exp11 <= exp10;
      mult_result2 <= mult_result1;
      zero1_no1 <= zero1_no;
      zero2_no1 <= zero2_no;
      zero3_no1 <= zero3_no;
      zero4_no1 <= zero4_no;
      zero5_no1 <= zero5_no;
      zero6_no1 <= zero6_no;
      zero7_no1 <= zero7_no;
      zero8_no1 <= zero8_no;
    end

endmodule
