module a_adding (man_comp, signa, a, signb, b, result);
  
  input man_comp, signa, signb;
  input [52:0] a, b;
  output reg [53:0] result;//52-bit result for exact adding
  
  always@ (*)
  begin
    if (signa == signb)
      result = {1'b0, a} + {1'b0, b};
    else if (man_comp)
      result = {1'b0, a} - {1'b0, b};
    else
      result = {1'b0, b} - {1'b0, a};
  end
  
endmodule
        
        
     
