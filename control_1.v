///////////////////////////////////////////////////////////////
// Company: 	UoA
// Engineer: 	Thanh Bui 
// 
// Create Date:    11:39:19 03/01/2017 
// Design Name: 
// Module Name:    control_1
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "parameter.v"
module control_1(clk, rst, we_i, re_i, re_w, we_o, rst_acc, done_hidden, done_output);
 
  input clk, rst, we_i;
  output reg re_i, re_w, we_o, rst_acc, done_hidden, done_output;
  
  reg [1:0] state;
  reg [9:0] count, neuron;
  reg [3:0] result_no;

  always@ (posedge clk)
  begin
    if (rst)
	   begin
        re_i <= 0;
        re_w <= 0;
        we_o <= 0;
        rst_acc <= 1;
		done_hidden <= 0;
        done_output <= 0;
        result_no <= 0;		  
	    state <= 0;
	  end
	else
	   case (state)
		  0:
		    begin
			   re_i <= 0;
				re_w <= 0;
				we_o <= 0;
				rst_acc <= 1;
				done_output <= 0;
				result_no <= 0;
				if (we_i)
				  begin
				    count <= 0;
					neuron <= 0;
				    re_i <= 1;
				    re_w <= 1;					 
				    state <= 1;
				  end
			 end
		  1:
		    begin	
				we_o <= 0;				
			   count <= count + 1'b1;
				if (count == 2)
				  rst_acc <= 0;
                if (count == 783)
				  begin 
				    re_i <= 0;
				    re_w <= 0;
                  end				
				if (count == 787)
				  begin
				    rst_acc <= 1;
				    we_o <= 1;
				    neuron <= neuron + 1'b1;
				    count <= 0;
					if (neuron == op_per_neuron_hidden - 1'b1)
					  begin 
				        re_i <= 0;
				        re_w <= 0;
                        done_hidden <= 1;						  
					    state <= 2;
					  end
					else
					  begin
				        re_i <= 1;
				        re_w <= 1;
                      end						  
				  end
			 end
        2:
		    begin
			   we_o <= 0;
			   done_hidden <= 0;
               if (we_i)
				  begin
				    re_i <= 1;
				    re_w <= 1;					 
				    state <= 3;
				  end	
            end
        3:
          begin				
			   we_o <= 0;
				count <= count + 1'b1;
				if (count == 2)
				  rst_acc <= 0;				
				if (count == 511)
				  begin 
				    re_i <= 0;
				    re_w <= 0;
                  end	
				if (count == 515)				  
				  begin
				    rst_acc <= 1;
				    we_o <= 1;
					result_no <= result_no + 1'b1;
					count <= 0;
					if (result_no == op_per_neuron_output - 1'b1)
					  begin
						done_output <= 1;
                        state <= 0;
					  end
					else 
				      begin 
				        re_i <= 1;
				        re_w <= 1;
                      end						 
				  end
			 end
		default: state <= 0;
	  endcase
  end				
		  
endmodule