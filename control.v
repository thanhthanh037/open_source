
module control (clk, rst, start, op_per_pe, no_of_stage, no_of_pe, 
                re_a, re_b, re_1st, we_a, we_b, we2_a, we2_b, w_per_pe,  
                send_pe, send_pc, dest, a_or_b);

  input clk, rst, start;
  input [11:0] op_per_pe;
  input [4:0] no_of_stage;
  input [2:0] no_of_pe;
  output reg re_a, re_b, re_1st;
  output reg we_a, we_b;
  output reg we2_a, we2_b; 
  output reg [11:0] w_per_pe;
  output reg send_pe, send_pc;
  output reg [1:0] dest; 
  output reg a_or_b;
  
  reg [3:0] state;
  reg [4:0] current_stage, stage_change;
  reg [11:0] count;
  reg [10:0] count2;
  reg [10:0] change;  
  reg [12:0] op_per_pe_38;

  always@ (posedge clk)
  begin
    if (rst)
      state <= 0;
    else
      case (state)
        0: 
          begin
            re_a <= 0;
            re_b <= 0;
            re_1st <= 0;
            we_a <= 0;
            we2_a <= 0;
            we_b <= 0;
            we2_b <= 0;
            if (start)
              begin
                count <= 0;
                current_stage <= 0;
                re_b <= 1;
                re_1st <= 1;
                if (no_of_stage == 0)
                  state <= 1;
                else if (no_of_stage != no_of_pe)
                  state <= 2;
                else 
                  state <= 3;   
              end
          end
        1:
          begin
            count <= count + 1'b1;
            if (count == 0)
              send_pc <= 1;
            if (count == 26)
              re_a <= 1;
            if (count == 37)
              begin 
                we_a <= 1;                 
                we_b <= 1;
              end 
            if (count == 38)
              send_pc <= 0; 
            if (count == op_per_pe)
              re_b <= 0;
            if (count == op_per_pe + 27)
              re_a <= 0;
            if (count == op_per_pe_38)
              begin
                we_a <= 0;
                we_b <= 0;
                state <= 0;
              end              
          end 
        2: 
          begin
            count <= count + 1'b1;
            if (count == 26)
              re_a <= 1;
            if (count == 37)
              begin
                we2_a <= 1;
                count2 <= 0;
                state <= 4;
              end
            if (count == op_per_pe)
              re_b <= 0;
            if (count == op_per_pe + 27)
              re_a <= 0;              
          end
        4: 
          begin
            count <= count + 1'b1;            
            if (count == op_per_pe)
              re_b <= 0;
            if (count == op_per_pe + 27)
              re_a <= 0;              
            if (count2 == change)
              begin
                we2_a <= 0;
                we2_b <= 1;
                count2 <= 0;
                state <= 5;
              end
            else
              count2 <= count2 + 1'b1;
          end 
        5:
          begin
            if (count == op_per_pe)
              re_b <= 0;
            if (count == op_per_pe + 27)
              re_a <= 0;  
            if (count == op_per_pe_38)
              begin
                we2_a <= 0;
                we2_b <= 0;
                count <= 0;
                re_1st <= 0;
                re_b <= 1;
                current_stage <= current_stage + 1'b1;
                if ((no_of_pe != 0) && (current_stage == stage_change))
                  state <= 3;
                else
                  begin                           
                    count2 <= 0;
                    if (current_stage + 1'b1 == no_of_stage)
                      state <= 1;
                    else
                      state <= 2;
                  end
              end
            else
              begin
                count <= count + 1'b1; 
                if (count2 == change)
                  begin
                    we2_a <= 1;
                    we2_b <= 0;
                    count2 <= 0;
                    state <= 4;
                  end 
                else
                count2 <= count2 + 1'b1;
              end
          end  
        3:
          begin
            if (count == 0)
              send_pe <= 1;
            dest <= 0;
            a_or_b <= 0;
            count <= count + 1'b1;
            if (count == 26)
              re_a <= 1;
            if (count == 36)
              send_pe <= 0;
            if (count == 37)
              we_a <= 1;
            if (count == op_per_pe)
              re_b <= 0;
            if (count == op_per_pe + 27)
              re_a <= 0;              
            if (count == op_per_pe_38) 
              begin
                we_a <= 0;
                we_b <= 0; 
                count <= 0; 
                re_1st <= 0;
                re_b <= 1; 
                current_stage <= current_stage + 1'b1;
                if (no_of_pe == 1)           
                  state <= 1;
                else
                  state <= 6;                
              end
          end
        6:
          begin
            if (count == 0)
              send_pe <= 1;
            dest <= 1;
            a_or_b <= 0;
            count <= count + 1'b1;
            if (count == 26)
              re_a <= 1;
            if (count == 36)
              send_pe <= 0;
            if (count == 37)
              we_a <= 1;
            if (count == op_per_pe)
              re_b <= 0;
            if (count == op_per_pe + 27)
              re_a <= 0;              
            if (count == op_per_pe_38) 
              begin
                we_a <= 0;
                we_b <= 0; 
                count <= 0; 
                re_b <= 1; 
                current_stage <= current_stage + 1'b1;
                if (no_of_pe == 2)           
                  state <= 1;
                else
                  state <= 7;
              end
          end
        7:
          begin
            if (count == 0)  
              send_pe <= 1;
            dest <= 2;
            a_or_b <= 0;
            count <= count + 1'b1;
            if (count == 26)
              re_a <= 1;
            if (count == 36)
              send_pe <= 0;            
            if (count == 37)
              we_a <= 1;
            if (count == op_per_pe)
              re_b <= 0;
            if (count == op_per_pe + 27)
              re_a <= 0;              
            if (count == op_per_pe_38) 
              begin
                we_a <= 0;
                we_b <= 0; 
                count <= 0; 
                re_b <= 1; 
                current_stage <= current_stage + 1'b1;
                if (no_of_pe == 3)           
                  state <= 1;
                else
                  state <= 8;
              end
          end  
        8:
          begin
            if (count == 0)
              send_pe <= 1;
            dest <= 3;
            a_or_b <= 0;
            count <= count + 1'b1;
            if (count == 26)
              re_a <= 1;
            if (count == 36)
              send_pe <= 0;            
            if (count == 37)
              we_a <= 1;
            if (count == op_per_pe + 27)
              re_a <= 0;              
            if (count == op_per_pe)
              re_b <= 0;
            if (count == op_per_pe_38) 
              begin
                we_a <= 0;
                we_b <= 0; 
                count <= 0; 
                re_b <= 1; 
                current_stage <= current_stage + 1'b1;        
                state <= 1;
              end
          end
        default: state <= 0;                   
      endcase
  end
  
  always@ (posedge clk)
  begin
    op_per_pe_38 <= op_per_pe + 38;
    stage_change <= no_of_stage - no_of_pe - 1'b1;
    case (current_stage)              
      0: change <= 0;              
      1: change <= 1;       
      2: change <= 3;
      3: change <= 7;
      4: change <= 15;
      5: change <= 31;
      6: change <= 63;
      7: change <= 127;
      8: change <= 255;
      9: change <= 511;
      10: change <= 1023;
      11: change <= 2047;
      default: change <= 0;
    endcase
  end

  always@ (posedge clk)
  begin
    case (current_stage)
      0:w_per_pe <= 0;
      1:
        if (op_per_pe[0])
          w_per_pe <= 1;
        else
          w_per_pe <= op_per_pe;
      2:
        if (op_per_pe[1])
          w_per_pe <= 3;
        else
          w_per_pe <= op_per_pe;
      3:
        if (op_per_pe[2])
          w_per_pe <= 7;
        else
          w_per_pe <= op_per_pe;
      4:
        if (op_per_pe[3])
          w_per_pe <= 15;
        else
          w_per_pe <= op_per_pe;
      5:
        if (op_per_pe[4])
          w_per_pe <= 31;
        else
          w_per_pe <= op_per_pe;
      6:
        if (op_per_pe[5])
          w_per_pe <= 63;
        else
          w_per_pe <= op_per_pe;
      7:
        if (op_per_pe[6])
          w_per_pe <= 127;
        else
          w_per_pe <= op_per_pe;
      8:
        if (op_per_pe[7])
          w_per_pe <= 255;
        else
          w_per_pe <= op_per_pe;
      9:
        if (op_per_pe[8])
          w_per_pe <= 511;
        else
          w_per_pe <= op_per_pe;
      10:
        if (op_per_pe[9])
          w_per_pe <= 1023;
        else
          w_per_pe <= op_per_pe;
      11:
        if (op_per_pe[10])
          w_per_pe <= 2047;
        else
          w_per_pe <= op_per_pe;
      12:
        if (op_per_pe[11])
          w_per_pe <= 4095;
        else
         w_per_pe <= op_per_pe;
      default: w_per_pe <= 0;
    endcase  
  end

endmodule