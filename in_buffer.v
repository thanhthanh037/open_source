`include "parameters.v"
module in_buffer (clk, rst, rd_times, rd_en, wr_en, iflit, oflit, available, new_packet);
    
  parameter [3:0] address_width = 8;
  parameter [7:0] buffer_size = 64;//depend on NoC size
  parameter [7:0] flits_per_packet = 3;//depend on NoC and PE configurations
  
  input clk, rst;
  input [1:0] rd_times;
  input rd_en, wr_en;
  input [flit_data_width-1:0] iflit;
  output reg [flit_data_width-1:0] oflit;
  output available;
  output reg new_packet;
  
  reg [flit_data_width-1:0] buffer[buffer_size-1:0];
  reg [address_width-1:0] rd_ptr, wr_ptr, count;
  reg rd_done;

  //write pointer
  always@ (posedge clk)
  begin
    if (rst)
      wr_ptr <= 0;          
    else if (wr_en) 
      begin
        if (wr_ptr == buffer_size-1)
          wr_ptr <= 0;
        else
          wr_ptr <= wr_ptr + 1'b1;
      end
  end
  
  // read pointer  
  always@ (posedge clk)
  begin
    if (rst)
	   begin
        rd_ptr <= 0;
		  rd_done <= 1;
      end		  
    else if (rd_en && !oflit[flit_data_width-2])
      begin
		  rd_done <= 0;
        if (rd_ptr == buffer_size-1)
          rd_ptr <= 0;
        else        
          rd_ptr <= rd_ptr + 1'b1;
      end
	 else if (rd_en && oflit[flit_data_width-2])
	   begin		  
	     if (rd_times == 0)
 	       rd_done <= 1;			   
		  else if (rd_ptr == 0)
		    rd_ptr <= buffer_size - flits_per_packet;
		  else
		    rd_ptr <= rd_ptr - flits_per_packet;		  
	   end
  end
  
  //write operation
  always@ (posedge clk)
  begin
    if (wr_en)
      buffer[wr_ptr] <= iflit;
  end
  
  //read operation
  always@ (posedge clk)
  begin
    oflit <= buffer[rd_ptr];
  end

  //buffer status 
  always@ (posedge clk)
  begin
    if (rst)
      count <= 0;          
    else if (wr_en && !(rd_en && !oflit[flit_data_width-2] && (rd_times == 0) ) && (count != buffer_size-1))
      count <= count + 1'b1;
    else if (rd_en && !oflit[flit_data_width-2] && (rd_times == 0) && !wr_en && (count != 0))
      count <= count - 1'b1;  
	 new_packet <= (count != 0) && oflit[flit_data_width-1] && (rd_done); 	
  end
  
  assign available = (count <= buffer_size - flits_per_packet);

endmodule