module a_leading_zero_cal (man_sum, zero1_no, zero2_no, zero3_no, zero4_no, zero_no);
  
  input [1:0] man_sum;//actually just the first 2 bits of the real man_sum
  input [3:0] zero1_no, zero2_no, zero3_no, zero4_no;
  output reg [5:0] zero_no;
  
  always@ (*)
  begin
    if (man_sum[1])
      zero_no = 0;
    else if (man_sum[0])
      zero_no = 1;
    else if (zero1_no != 4'b1101)//13
      zero_no = zero1_no + 2;
    else if (zero2_no != 4'b1101)
      zero_no = zero2_no + 6'b001111;//15
    else if (zero3_no != 4'b1101)
      zero_no = zero3_no + 6'b011100;//28
    else if (zero4_no != 4'b1101)
      zero_no = zero4_no + 6'b101001;//41    
    else
      zero_no = 6'b110110;//54
  end

endmodule
  
