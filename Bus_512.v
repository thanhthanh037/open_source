`include "parameters.v"
module Bus_512 (clk, rst, 
                wrq,				  
                waddr0, waddr,
                wdata_in0, wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, 
                wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, wdata_in17, wdata_in18, wdata_in19,
			    wdata_in20, wdata_in21, wdata_in22, wdata_in23, wdata_in24, wdata_in25, wdata_in26, wdata_in27, wdata_in28, wdata_in29, 
				wdata_in30, wdata_in31, wdata_in32, wdata_in33, wdata_in34, wdata_in35, wdata_in36, wdata_in37, wdata_in38, wdata_in39, 
				wdata_in40, wdata_in41, wdata_in42, wdata_in43, wdata_in44, wdata_in45, wdata_in46, wdata_in47, wdata_in48, wdata_in49,
				wdata_in50, wdata_in51, wdata_in52, wdata_in53, wdata_in54, wdata_in55, wdata_in56, wdata_in57, wdata_in58, wdata_in59, 
                wdata_in60, wdata_in61, wdata_in62, wdata_in63, wdata_in64, wdata_in65, wdata_in66, wdata_in67, wdata_in68, wdata_in69,
				wdata_in70, wdata_in71, wdata_in72, wdata_in73, wdata_in74, wdata_in75, wdata_in76, wdata_in77, wdata_in78, wdata_in79, 
				wdata_in80, wdata_in81, wdata_in82, wdata_in83, wdata_in84, wdata_in85, wdata_in86, wdata_in87, wdata_in88, wdata_in89, 
				wdata_in90, wdata_in91, wdata_in92, wdata_in93, wdata_in94, wdata_in95, wdata_in96, wdata_in97, wdata_in98, wdata_in99,
				wdata_in100, wdata_in101, wdata_in102, wdata_in103, wdata_in104, wdata_in105, wdata_in106, wdata_in107, wdata_in108, wdata_in109, 
                wdata_in110, wdata_in111, wdata_in112, wdata_in113, wdata_in114, wdata_in115, wdata_in116, wdata_in117, wdata_in118, wdata_in119,
				wdata_in120, wdata_in121, wdata_in122, wdata_in123, wdata_in124, wdata_in125, wdata_in126, wdata_in127, wdata_in128, wdata_in129,				  
				wdata_in130, wdata_in131, wdata_in132, wdata_in133, wdata_in134, wdata_in135, wdata_in136, wdata_in137, wdata_in138, wdata_in139, 
				wdata_in140, wdata_in141, wdata_in142, wdata_in143, wdata_in144, wdata_in145, wdata_in146, wdata_in147, wdata_in148, wdata_in149,
				wdata_in150, wdata_in151, wdata_in152, wdata_in153, wdata_in154, wdata_in155, wdata_in156, wdata_in157, wdata_in158, wdata_in159, 
                wdata_in160, wdata_in161, wdata_in162, wdata_in163, wdata_in164, wdata_in165, wdata_in166, wdata_in167, wdata_in168, wdata_in169,
				wdata_in170, wdata_in171, wdata_in172, wdata_in173, wdata_in174, wdata_in175, wdata_in176, wdata_in177, wdata_in178, wdata_in179, 
				wdata_in180, wdata_in181, wdata_in182, wdata_in183, wdata_in184, wdata_in185, wdata_in186, wdata_in187, wdata_in188, wdata_in189, 
				wdata_in190, wdata_in191, wdata_in192, wdata_in193, wdata_in194, wdata_in195, wdata_in196, wdata_in197, wdata_in198, wdata_in199,
				wdata_in200, wdata_in201, wdata_in202, wdata_in203, wdata_in204, wdata_in205, wdata_in206, wdata_in207, wdata_in208, wdata_in209, 
                wdata_in210, wdata_in211, wdata_in212, wdata_in213, wdata_in214, wdata_in215, wdata_in216, wdata_in217, wdata_in218, wdata_in219,
				wdata_in220, wdata_in221, wdata_in222, wdata_in223, wdata_in224, wdata_in225, wdata_in226, wdata_in227, wdata_in228, wdata_in229,				  
				wdata_in230, wdata_in231, wdata_in232, wdata_in233, wdata_in234, wdata_in235, wdata_in236, wdata_in237, wdata_in238, wdata_in239, 
				wdata_in240, wdata_in241, wdata_in242, wdata_in243, wdata_in244, wdata_in245, wdata_in246, wdata_in247, wdata_in248, wdata_in249,
				wdata_in250, wdata_in251, wdata_in252, wdata_in253, wdata_in254, wdata_in255, wdata_in256, wdata_in257, wdata_in258, wdata_in259, 
                wdata_in260, wdata_in261, wdata_in262, wdata_in263, wdata_in264, wdata_in265, wdata_in266, wdata_in267, wdata_in268, wdata_in269,
				wdata_in270, wdata_in271, wdata_in272, wdata_in273, wdata_in274, wdata_in275, wdata_in276, wdata_in277, wdata_in278, wdata_in279, 
				wdata_in280, wdata_in281, wdata_in282, wdata_in283, wdata_in284, wdata_in285, wdata_in286, wdata_in287, wdata_in288, wdata_in289, 
				wdata_in290, wdata_in291, wdata_in292, wdata_in293, wdata_in294, wdata_in295, wdata_in296, wdata_in297, wdata_in298, wdata_in299, 
				wdata_in300, wdata_in301, wdata_in302, wdata_in303, wdata_in304, wdata_in305, wdata_in306, wdata_in307, wdata_in308, wdata_in309, 
                wdata_in310, wdata_in311, wdata_in312, wdata_in313, wdata_in314, wdata_in315, wdata_in316, wdata_in317, wdata_in318, wdata_in319,
				wdata_in320, wdata_in321, wdata_in322, wdata_in323, wdata_in324, wdata_in325, wdata_in326, wdata_in327, wdata_in328, wdata_in329,				  
				wdata_in330, wdata_in331, wdata_in332, wdata_in333, wdata_in334, wdata_in335, wdata_in336, wdata_in337, wdata_in338, wdata_in339, 
				wdata_in340, wdata_in341, wdata_in342, wdata_in343, wdata_in344, wdata_in345, wdata_in346, wdata_in347, wdata_in348, wdata_in349,
				wdata_in350, wdata_in351, wdata_in352, wdata_in353, wdata_in354, wdata_in355, wdata_in356, wdata_in357, wdata_in358, wdata_in359, 
                wdata_in360, wdata_in361, wdata_in362, wdata_in363, wdata_in364, wdata_in365, wdata_in366, wdata_in367, wdata_in368, wdata_in369,
				wdata_in370, wdata_in371, wdata_in372, wdata_in373, wdata_in374, wdata_in375, wdata_in376, wdata_in377, wdata_in378, wdata_in379, 
				wdata_in380, wdata_in381, wdata_in382, wdata_in383, wdata_in384, wdata_in385, wdata_in386, wdata_in387, wdata_in388, wdata_in389, 
				wdata_in390, wdata_in391, wdata_in392, wdata_in393, wdata_in394, wdata_in395, wdata_in396, wdata_in397, wdata_in398, wdata_in399,
				wdata_in400, wdata_in401, wdata_in402, wdata_in403, wdata_in404, wdata_in405, wdata_in406, wdata_in407, wdata_in408, wdata_in409, 
                wdata_in410, wdata_in411, wdata_in412, wdata_in413, wdata_in414, wdata_in415, wdata_in416, wdata_in417, wdata_in418, wdata_in419,
				wdata_in420, wdata_in421, wdata_in422, wdata_in423, wdata_in424, wdata_in425, wdata_in426, wdata_in427, wdata_in428, wdata_in429,				  
				wdata_in430, wdata_in431, wdata_in432, wdata_in433, wdata_in434, wdata_in435, wdata_in436, wdata_in437, wdata_in438, wdata_in439, 
				wdata_in440, wdata_in441, wdata_in442, wdata_in443, wdata_in444, wdata_in445, wdata_in446, wdata_in447, wdata_in448, wdata_in449,
				wdata_in450, wdata_in451, wdata_in452, wdata_in453, wdata_in454, wdata_in455, wdata_in456, wdata_in457, wdata_in458, wdata_in459, 
                wdata_in460, wdata_in461, wdata_in462, wdata_in463, wdata_in464, wdata_in465, wdata_in466, wdata_in467, wdata_in468, wdata_in469,
				wdata_in470, wdata_in471, wdata_in472, wdata_in473, wdata_in474, wdata_in475, wdata_in476, wdata_in477, wdata_in478, wdata_in479, 
				wdata_in480, wdata_in481, wdata_in482, wdata_in483, wdata_in484, wdata_in485, wdata_in486, wdata_in487, wdata_in488, wdata_in489, 
				wdata_in490, wdata_in491, wdata_in492, wdata_in493, wdata_in494, wdata_in495, wdata_in496, wdata_in497, wdata_in498, wdata_in499, 	
				wdata_in500, wdata_in501, wdata_in502, wdata_in503, wdata_in504, wdata_in505, wdata_in506, wdata_in507, wdata_in508, wdata_in509, 
                wdata_in510, wdata_in511,			  
				wgr,				  
				we,
				broadcast,
				source_node,
                wdata_out);  

  input clk, rst;
  input [511:0] wrq;	
  input [8:0] waddr0;	
  input [510:0] waddr;
  input [data_width-1:0] wdata_in0, wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9; 
  input [data_width-1:0] wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, wdata_in17, wdata_in18, wdata_in19;
  input [data_width-1:0] wdata_in20, wdata_in21, wdata_in22, wdata_in23, wdata_in24, wdata_in25, wdata_in26, wdata_in27, wdata_in28, wdata_in29; 
  input [data_width-1:0] wdata_in30, wdata_in31, wdata_in32, wdata_in33, wdata_in34, wdata_in35, wdata_in36, wdata_in37, wdata_in38, wdata_in39; 
  input [data_width-1:0] wdata_in40, wdata_in41, wdata_in42, wdata_in43, wdata_in44, wdata_in45, wdata_in46, wdata_in47, wdata_in48, wdata_in49;
  input [data_width-1:0] wdata_in50, wdata_in51, wdata_in52, wdata_in53, wdata_in54, wdata_in55, wdata_in56, wdata_in57, wdata_in58, wdata_in59; 
  input [data_width-1:0] wdata_in60, wdata_in61, wdata_in62, wdata_in63, wdata_in64, wdata_in65, wdata_in66, wdata_in67, wdata_in68, wdata_in69;
  input [data_width-1:0] wdata_in70, wdata_in71, wdata_in72, wdata_in73, wdata_in74, wdata_in75, wdata_in76, wdata_in77, wdata_in78, wdata_in79; 
  input [data_width-1:0] wdata_in80, wdata_in81, wdata_in82, wdata_in83, wdata_in84, wdata_in85, wdata_in86, wdata_in87, wdata_in88, wdata_in89; 
  input [data_width-1:0] wdata_in90, wdata_in91, wdata_in92, wdata_in93, wdata_in94, wdata_in95, wdata_in96, wdata_in97, wdata_in98, wdata_in99;
  input [data_width-1:0] wdata_in100, wdata_in101, wdata_in102, wdata_in103, wdata_in104, wdata_in105, wdata_in106, wdata_in107, wdata_in108, wdata_in109; 
  input [data_width-1:0] wdata_in110, wdata_in111, wdata_in112, wdata_in113, wdata_in114, wdata_in115, wdata_in116, wdata_in117, wdata_in118, wdata_in119;
  input [data_width-1:0] wdata_in120, wdata_in121, wdata_in122, wdata_in123, wdata_in124, wdata_in125, wdata_in126, wdata_in127, wdata_in128, wdata_in129;	
  input [data_width-1:0] wdata_in130, wdata_in131, wdata_in132, wdata_in133, wdata_in134, wdata_in135, wdata_in136, wdata_in137, wdata_in138, wdata_in139; 
  input [data_width-1:0] wdata_in140, wdata_in141, wdata_in142, wdata_in143, wdata_in144, wdata_in145, wdata_in146, wdata_in147, wdata_in148, wdata_in149;
  input [data_width-1:0] wdata_in150, wdata_in151, wdata_in152, wdata_in153, wdata_in154, wdata_in155, wdata_in156, wdata_in157, wdata_in158, wdata_in159; 
  input [data_width-1:0] wdata_in160, wdata_in161, wdata_in162, wdata_in163, wdata_in164, wdata_in165, wdata_in166, wdata_in167, wdata_in168, wdata_in169;
  input [data_width-1:0] wdata_in170, wdata_in171, wdata_in172, wdata_in173, wdata_in174, wdata_in175, wdata_in176, wdata_in177, wdata_in178, wdata_in179; 
  input [data_width-1:0] wdata_in180, wdata_in181, wdata_in182, wdata_in183, wdata_in184, wdata_in185, wdata_in186, wdata_in187, wdata_in188, wdata_in189; 
  input [data_width-1:0] wdata_in190, wdata_in191, wdata_in192, wdata_in193, wdata_in194, wdata_in195, wdata_in196, wdata_in197, wdata_in198, wdata_in199;
  input [data_width-1:0] wdata_in200, wdata_in201, wdata_in202, wdata_in203, wdata_in204, wdata_in205, wdata_in206, wdata_in207, wdata_in208, wdata_in209; 
  input [data_width-1:0] wdata_in210, wdata_in211, wdata_in212, wdata_in213, wdata_in214, wdata_in215, wdata_in216, wdata_in217, wdata_in218, wdata_in219;
  input [data_width-1:0] wdata_in220, wdata_in221, wdata_in222, wdata_in223, wdata_in224, wdata_in225, wdata_in226, wdata_in227, wdata_in228, wdata_in229;				  
  input [data_width-1:0] wdata_in230, wdata_in231, wdata_in232, wdata_in233, wdata_in234, wdata_in235, wdata_in236, wdata_in237, wdata_in238, wdata_in239; 
  input [data_width-1:0] wdata_in240, wdata_in241, wdata_in242, wdata_in243, wdata_in244, wdata_in245, wdata_in246, wdata_in247, wdata_in248, wdata_in249;
  input [data_width-1:0] wdata_in250, wdata_in251, wdata_in252, wdata_in253, wdata_in254, wdata_in255, wdata_in256, wdata_in257, wdata_in258, wdata_in259; 
  input [data_width-1:0] wdata_in260, wdata_in261, wdata_in262, wdata_in263, wdata_in264, wdata_in265, wdata_in266, wdata_in267, wdata_in268, wdata_in269;
  input [data_width-1:0] wdata_in270, wdata_in271, wdata_in272, wdata_in273, wdata_in274, wdata_in275, wdata_in276, wdata_in277, wdata_in278, wdata_in279; 
  input [data_width-1:0] wdata_in280, wdata_in281, wdata_in282, wdata_in283, wdata_in284, wdata_in285, wdata_in286, wdata_in287, wdata_in288, wdata_in289; 
  input [data_width-1:0] wdata_in290, wdata_in291, wdata_in292, wdata_in293, wdata_in294, wdata_in295, wdata_in296, wdata_in297, wdata_in298, wdata_in299;
  input [data_width-1:0] wdata_in300, wdata_in301, wdata_in302, wdata_in303, wdata_in304, wdata_in305, wdata_in306, wdata_in307, wdata_in308, wdata_in309; 
  input [data_width-1:0] wdata_in310, wdata_in311, wdata_in312, wdata_in313, wdata_in314, wdata_in315, wdata_in316, wdata_in317, wdata_in318, wdata_in319;
  input [data_width-1:0] wdata_in320, wdata_in321, wdata_in322, wdata_in323, wdata_in324, wdata_in325, wdata_in326, wdata_in327, wdata_in328, wdata_in329;	
  input [data_width-1:0] wdata_in330, wdata_in331, wdata_in332, wdata_in333, wdata_in334, wdata_in335, wdata_in336, wdata_in337, wdata_in338, wdata_in339; 
  input [data_width-1:0] wdata_in340, wdata_in341, wdata_in342, wdata_in343, wdata_in344, wdata_in345, wdata_in346, wdata_in347, wdata_in348, wdata_in349;
  input [data_width-1:0] wdata_in350, wdata_in351, wdata_in352, wdata_in353, wdata_in354, wdata_in355, wdata_in356, wdata_in357, wdata_in358, wdata_in359; 
  input [data_width-1:0] wdata_in360, wdata_in361, wdata_in362, wdata_in363, wdata_in364, wdata_in365, wdata_in366, wdata_in367, wdata_in368, wdata_in369;
  input [data_width-1:0] wdata_in370, wdata_in371, wdata_in372, wdata_in373, wdata_in374, wdata_in375, wdata_in376, wdata_in377, wdata_in378, wdata_in379; 
  input [data_width-1:0] wdata_in380, wdata_in381, wdata_in382, wdata_in383, wdata_in384, wdata_in385, wdata_in386, wdata_in387, wdata_in388, wdata_in389; 
  input [data_width-1:0] wdata_in390, wdata_in391, wdata_in392, wdata_in393, wdata_in394, wdata_in395, wdata_in396, wdata_in397, wdata_in398, wdata_in399;
  input [data_width-1:0] wdata_in400, wdata_in401, wdata_in402, wdata_in403, wdata_in404, wdata_in405, wdata_in406, wdata_in407, wdata_in408, wdata_in409; 
  input [data_width-1:0] wdata_in410, wdata_in411, wdata_in412, wdata_in413, wdata_in414, wdata_in415, wdata_in416, wdata_in417, wdata_in418, wdata_in419;
  input [data_width-1:0] wdata_in420, wdata_in421, wdata_in422, wdata_in423, wdata_in424, wdata_in425, wdata_in426, wdata_in427, wdata_in428, wdata_in429;				  
  input [data_width-1:0] wdata_in430, wdata_in431, wdata_in432, wdata_in433, wdata_in434, wdata_in435, wdata_in436, wdata_in437, wdata_in438, wdata_in439; 
  input [data_width-1:0] wdata_in440, wdata_in441, wdata_in442, wdata_in443, wdata_in444, wdata_in445, wdata_in446, wdata_in447, wdata_in448, wdata_in449;
  input [data_width-1:0] wdata_in450, wdata_in451, wdata_in452, wdata_in453, wdata_in454, wdata_in455, wdata_in456, wdata_in457, wdata_in458, wdata_in459; 
  input [data_width-1:0] wdata_in460, wdata_in461, wdata_in462, wdata_in463, wdata_in464, wdata_in465, wdata_in466, wdata_in467, wdata_in468, wdata_in469;
  input [data_width-1:0] wdata_in470, wdata_in471, wdata_in472, wdata_in473, wdata_in474, wdata_in475, wdata_in476, wdata_in477, wdata_in478, wdata_in479; 
  input [data_width-1:0] wdata_in480, wdata_in481, wdata_in482, wdata_in483, wdata_in484, wdata_in485, wdata_in486, wdata_in487, wdata_in488, wdata_in489; 
  input [data_width-1:0] wdata_in490, wdata_in491, wdata_in492, wdata_in493, wdata_in494, wdata_in495, wdata_in496, wdata_in497, wdata_in498, wdata_in499; 
  input [data_width-1:0] wdata_in500, wdata_in501, wdata_in502, wdata_in503, wdata_in504, wdata_in505, wdata_in506, wdata_in507, wdata_in508, wdata_in509; 
  input [data_width-1:0] wdata_in510, wdata_in511;  
  output reg [511:0] wgr;  
  output reg [511:0] we;	 
  output reg broadcast;
  output reg [8:0] source_node;
  output reg [data_width-1:0] wdata_out;
  
  reg [9:0] state;
  reg count;
  	  
  always@ (posedge clk)
  if (rst) 
    state <= 0;
  else
    case (state)
	   0: 
		  begin
		    wgr <= 0; 
		    we <= 0;		    			 
			count <= 0;
            broadcast <= 0;			 
			  if (wrq[0])
			    begin
				   wgr[0] <= 1;
				   state <= 1;
				 end
			  else if (wrq[1])
			    begin
				   wgr[1] <= 1;
				   state <= 2;
				 end				 
			  else if (wrq[2])
			    begin
				   wgr[2] <= 1;
				   state <= 3;
				 end
			  else if (wrq[3])
			    begin
				   wgr[3] <= 1;
				   state <= 4;
				 end
			  else if (wrq[4])
			    begin
				   wgr[4] <= 1;
				   state <= 5;
				 end
			  else if (wrq[5])
			    begin
				   wgr[5] <= 1;
				   state <= 6;
				 end
			  else if (wrq[6])
			    begin
				   wgr[6] <= 1;
				   state <= 7;
				 end
			  else if (wrq[7])
			    begin
				   wgr[7] <= 1;
				   state <= 8;
				 end
			  else if (wrq[8])
			    begin
				   wgr[8] <= 1;
				   state <= 9;
				 end
			  else if (wrq[9])
			    begin
				   wgr[9] <= 1;
				   state <= 10;
				 end
			  else if (wrq[10])
			    begin
				   wgr[10] <= 1;
				   state <= 11;
				 end
			  else if (wrq[11])
			    begin
				   wgr[11] <= 1;
				   state <= 12;
				 end
			  else if (wrq[12])
			    begin
				   wgr[12] <= 1;
				   state <= 13;
				 end
			  else if (wrq[13])
			    begin
				   wgr[13] <= 1;
				   state <= 14;
				 end
			  else if (wrq[14])
			    begin
				   wgr[14] <= 1;
				   state <= 15;
				 end
			  else if (wrq[15])
			    begin
				   wgr[15] <= 1;
				   state <= 16;
				 end
			  else if (wrq[16])
			    begin
				   wgr[16] <= 1;
				   state <= 17;
				 end
			  else if (wrq[17])
			    begin
				   wgr[17] <= 1;
				   state <= 18;
				 end
			  else if (wrq[18])
			    begin
				   wgr[18] <= 1;
				   state <= 19;
				 end
			  else if (wrq[19])
			    begin
				   wgr[19] <= 1;
				   state <= 20;
				 end
			  else if (wrq[20])
			    begin
				   wgr[20] <= 1;
				   state <= 21;
				 end
			  else if (wrq[21])
			    begin
				   wgr[21] <= 1;
				   state <= 22;
				 end				 
			  else if (wrq[22])
			    begin
				   wgr[22] <= 1;
				   state <= 23;
				 end
			  else if (wrq[23])
			    begin
				   wgr[23] <= 1;
				   state <= 24;
				 end
			  else if (wrq[24])
			    begin
				   wgr[24] <= 1;
				   state <= 25;
				 end
			  else if (wrq[25])
			    begin
				   wgr[25] <= 1;
				   state <= 26;
				 end
			  else if (wrq[26])
			    begin
				   wgr[26] <= 1;
				   state <=27;
				 end
			  else if (wrq[27])
			    begin
				   wgr[27] <= 1;
				   state <= 28;
				 end
			  else if (wrq[28])
			    begin
				   wgr[28] <= 1;
				   state <= 29;
				 end
			  else if (wrq[29])
			    begin
				   wgr[29] <= 1;
				   state <= 30;
				 end	
			  else if (wrq[30])
			    begin
				   wgr[30] <= 1;
				   state <= 31;
				 end
			  else if (wrq[31])
			    begin
				   wgr[31] <= 1;
				   state <= 32;
				 end				 
			  else if (wrq[32])
			    begin
				   wgr[32] <= 1;
				   state <= 33;
				 end
			  else if (wrq[33])
			    begin
				   wgr[33] <= 1;
				   state <= 34;
				 end
			  else if (wrq[34])
			    begin
				   wgr[34] <= 1;
				   state <= 35;
				 end
			  else if (wrq[35])
			    begin
				   wgr[35] <= 1;
				   state <= 36;
				 end
			  else if (wrq[36])
			    begin
				   wgr[36] <= 1;
				   state <= 37;
				 end
			  else if (wrq[37])
			    begin
				   wgr[37] <= 1;
				   state <= 38;
				 end
			  else if (wrq[38])
			    begin
				   wgr[38] <= 1;
				   state <= 39;
				 end
			  else if (wrq[39])
			    begin
				   wgr[39] <= 1;
				   state <= 40;
				 end	
			  else if (wrq[40])
			    begin
				   wgr[40] <= 1;
				   state <= 41;
				 end
			  else if (wrq[41])
			    begin
				   wgr[41] <= 1;
				   state <= 42;
				 end				 
			  else if (wrq[42])
			    begin
				   wgr[42] <= 1;
				   state <= 43;
				 end
			  else if (wrq[43])
			    begin
				   wgr[43] <= 1;
				   state <= 44;
				 end
			  else if (wrq[44])
			    begin
				   wgr[44] <= 1;
				   state <= 45;
				 end
			  else if (wrq[45])
			    begin
				   wgr[45] <= 1;
				   state <= 46;
				 end
			  else if (wrq[46])
			    begin
				   wgr[46] <= 1;
				   state <= 47;
				 end
			  else if (wrq[47])
			    begin
				   wgr[47] <= 1;
				   state <= 48;
				 end
			  else if (wrq[48])
			    begin
				   wgr[48] <= 1;
				   state <= 49;
				 end
			  else if (wrq[49])
			    begin
				   wgr[49] <= 1;
				   state <= 50;
				 end	
			  else if (wrq[50])
			    begin
				   wgr[50] <= 1;
				   state <= 51;
				 end
			  else if (wrq[51])
			    begin
				   wgr[51] <= 1;
				   state <= 52;
				 end				 
			  else if (wrq[52])
			    begin
				   wgr[52] <= 1;
				   state <= 53;
				 end
			  else if (wrq[53])
			    begin
				   wgr[53] <= 1;
				   state <= 54;
				 end
			  else if (wrq[54])
			    begin
				   wgr[54] <= 1;
				   state <= 55;
				 end
			  else if (wrq[55])
			    begin
				   wgr[55] <= 1;
				   state <= 56;
				 end
			  else if (wrq[56])
			    begin
				   wgr[56] <= 1;
				   state <= 57;
				 end
			  else if (wrq[57])
			    begin
				   wgr[57] <= 1;
				   state <= 58;
				 end
			  else if (wrq[58])
			    begin
				   wgr[58] <= 1;
				   state <= 59;
				 end
			  else if (wrq[59])
			    begin
				   wgr[59] <= 1;
				   state <= 60;
				 end	
			  else if (wrq[60])
			    begin
				   wgr[60] <= 1;
				   state <= 61;
				 end
			  else if (wrq[61])
			    begin
				   wgr[61] <= 1;
				   state <= 62;
				 end				 
			  else if (wrq[62])
			    begin
				   wgr[62] <= 1;
				   state <= 63;
				 end
			  else if (wrq[63])
			    begin
				   wgr[63] <= 1;
				   state <= 64;
				 end
			  else if (wrq[64])
			    begin
				   wgr[64] <= 1;
				   state <= 65;
				 end
			  else if (wrq[65])
			    begin
				   wgr[65] <= 1;
				   state <= 66;
				 end
			  else if (wrq[66])
			    begin
				   wgr[66] <= 1;
				   state <= 67;
				 end
			  else if (wrq[67])
			    begin
				   wgr[67] <= 1;
				   state <= 68;
				 end
			  else if (wrq[68])
			    begin
				   wgr[68] <= 1;
				   state <= 69;
				 end
			  else if (wrq[69])
			    begin
				   wgr[69] <= 1;
				   state <= 70;
				 end
			  else if (wrq[70])
			    begin
				   wgr[70] <= 1;
				   state <= 71;
				 end
			  else if (wrq[71])
			    begin
				   wgr[71] <= 1;
				   state <= 72;
				 end				 
			  else if (wrq[72])
			    begin
				   wgr[72] <= 1;
				   state <= 73;
				 end
			  else if (wrq[73])
			    begin
				   wgr[73] <= 1;
				   state <= 74;
				 end
			  else if (wrq[74])
			    begin
				   wgr[74] <= 1;
				   state <= 75;
				 end
			  else if (wrq[75])
			    begin
				   wgr[75] <= 1;
				   state <= 76;
				 end
			  else if (wrq[76])
			    begin
				   wgr[76] <= 1;
				   state <= 77;
				 end
			  else if (wrq[77])
			    begin
				   wgr[77] <= 1;
				   state <= 78;
				 end
			  else if (wrq[78])
			    begin
				   wgr[78] <= 1;
				   state <= 79;
				 end
			  else if (wrq[79])
			    begin
				   wgr[79] <= 1;
				   state <= 80;
				 end
			  else if (wrq[80])
			    begin
				   wgr[80] <= 1;
				   state <= 81;
				 end
			  else if (wrq[81])
			    begin
				   wgr[81] <= 1;
				   state <= 82;
				 end				 
			  else if (wrq[82])
			    begin
				   wgr[82] <= 1;
				   state <= 83;
				 end
			  else if (wrq[83])
			    begin
				   wgr[83] <= 1;
				   state <= 84;
				 end
			  else if (wrq[84])
			    begin
				   wgr[84] <= 1;
				   state <= 85;
				 end
			  else if (wrq[85])
			    begin
				   wgr[85] <= 1;
				   state <= 86;
				 end
			  else if (wrq[86])
			    begin
				   wgr[86] <= 1;
				   state <= 87;
				 end
			  else if (wrq[87])
			    begin
				   wgr[87] <= 1;
				   state <= 88;
				 end
			  else if (wrq[88])
			    begin
				   wgr[88] <= 1;
				   state <= 89;
				 end
			  else if (wrq[89])
			    begin
				   wgr[89] <= 1;
				   state <= 90;
				 end
			  else if (wrq[90])
			    begin
				   wgr[90] <= 1;
				   state <= 91;
				 end
			  else if (wrq[91])
			    begin
				   wgr[91] <= 1;
				   state <= 92;
				 end				 
			  else if (wrq[92])
			    begin
				   wgr[92] <= 1;
				   state <= 93;
				 end
			  else if (wrq[93])
			    begin
				   wgr[93] <= 1;
				   state <= 94;
				 end
			  else if (wrq[94])
			    begin
				   wgr[94] <= 1;
				   state <= 95;
				 end
			  else if (wrq[95])
			    begin
				   wgr[95] <= 1;
				   state <= 96;
				 end
			  else if (wrq[96])
			    begin
				   wgr[96] <= 1;
				   state <= 97;
				 end
			  else if (wrq[97])
			    begin
				   wgr[97] <= 1;
				   state <= 98;
				 end
			  else if (wrq[98])
			    begin
				   wgr[98] <= 1;
				   state <= 99;
				 end
			  else if (wrq[99])
			    begin
				   wgr[99] <= 1;
				   state <= 100;
				 end
			  else if (wrq[100])
			    begin
				   wgr[100] <= 1;
				   state <= 101;
				 end
			  else if (wrq[101])
			    begin
				   wgr[101] <= 1;
				   state <= 102;
				 end				 
			  else if (wrq[102])
			    begin
				   wgr[102] <= 1;
				   state <= 103;
				 end
			  else if (wrq[103])
			    begin
				   wgr[103] <= 1;
				   state <= 104;
				 end
			  else if (wrq[104])
			    begin
				   wgr[104] <= 1;
				   state <= 105;
				 end
			  else if (wrq[105])
			    begin
				   wgr[105] <= 1;
				   state <= 106;
				 end
			  else if (wrq[106])
			    begin
				   wgr[106] <= 1;
				   state <= 107;
				 end
			  else if (wrq[107])
			    begin
				   wgr[107] <= 1;
				   state <= 108;
				 end
			  else if (wrq[108])
			    begin
				   wgr[108] <= 1;
				   state <= 109;
				 end
			  else if (wrq[109])
			    begin
				   wgr[109] <= 1;
				   state <= 110;
				 end
			  else if (wrq[110])
			    begin
				   wgr[110] <= 1;
				   state <= 111;
				 end
			  else if (wrq[111])
			    begin
				   wgr[111] <= 1;
				   state <= 112;
				 end				 
			  else if (wrq[112])
			    begin
				   wgr[112] <= 1;
				   state <= 113;
				 end
			  else if (wrq[113])
			    begin
				   wgr[113] <= 1;
				   state <= 114;
				 end
			  else if (wrq[114])
			    begin
				   wgr[114] <= 1;
				   state <= 115;
				 end
			  else if (wrq[115])
			    begin
				   wgr[115] <= 1;
				   state <= 116;
				 end
			  else if (wrq[116])
			    begin
				   wgr[116] <= 1;
				   state <= 117;
				 end
			  else if (wrq[117])
			    begin
				   wgr[117] <= 1;
				   state <= 118;
				 end
			  else if (wrq[118])
			    begin
				   wgr[118] <= 1;
				   state <= 119;
				 end
			  else if (wrq[119])
			    begin
				   wgr[119] <= 1;
				   state <= 120;
				 end
			  else if (wrq[120])
			    begin
				   wgr[120] <= 1;
				   state <= 121;
				 end
			  else if (wrq[121])
			    begin
				   wgr[121] <= 1;
				   state <= 122;
				 end				 
			  else if (wrq[122])
			    begin
				   wgr[122] <= 1;
				   state <= 123;
				 end
			  else if (wrq[123])
			    begin
				   wgr[123] <= 1;
				   state <= 124;
				 end
			  else if (wrq[124])
			    begin
				   wgr[124] <= 1;
				   state <= 125;
				 end
			  else if (wrq[125])
			    begin
				   wgr[125] <= 1;
				   state <= 126;
				 end
			  else if (wrq[126])
			    begin
				   wgr[126] <= 1;
				   state <= 127;
				 end
			  else if (wrq[127])
			    begin
				   wgr[127] <= 1;
				   state <= 128;
				 end
			  else if (wrq[128])
			    begin
				   wgr[128] <= 1;
				   state <= 129;
				 end
			  else if (wrq[129])
			    begin
				   wgr[129] <= 1;
				   state <= 130;
				 end
			  else if (wrq[130])
			    begin
				   wgr[130] <= 1;
				   state <= 131;
				 end
			  else if (wrq[131])
			    begin
				   wgr[131] <= 1;
				   state <= 132;
				 end				 
			  else if (wrq[132])
			    begin
				   wgr[132] <= 1;
				   state <= 133;
				 end
			  else if (wrq[133])
			    begin
				   wgr[133] <= 1;
				   state <= 134;
				 end
			  else if (wrq[134])
			    begin
				   wgr[134] <= 1;
				   state <= 135;
				 end
			  else if (wrq[135])
			    begin
				   wgr[135] <= 1;
				   state <= 136;
				 end
			  else if (wrq[136])
			    begin
				   wgr[136] <= 1;
				   state <= 137;
				 end
			  else if (wrq[137])
			    begin
				   wgr[137] <= 1;
				   state <= 138;
				 end
			  else if (wrq[138])
			    begin
				   wgr[138] <= 1;
				   state <= 139;
				 end
			  else if (wrq[139])
			    begin
				   wgr[139] <= 1;
				   state <= 140;
				 end
			  else if (wrq[140])
			    begin
				   wgr[140] <= 1;
				   state <= 141;
				 end
			  else if (wrq[141])
			    begin
				   wgr[141] <= 1;
				   state <= 142;
				 end				 
			  else if (wrq[142])
			    begin
				   wgr[142] <= 1;
				   state <= 143;
				 end
			  else if (wrq[143])
			    begin
				   wgr[143] <= 1;
				   state <= 144;
				 end
			  else if (wrq[144])
			    begin
				   wgr[144] <= 1;
				   state <= 145;
				 end
			  else if (wrq[145])
			    begin
				   wgr[145] <= 1;
				   state <= 146;
				 end
			  else if (wrq[146])
			    begin
				   wgr[146] <= 1;
				   state <= 147;
				 end
			  else if (wrq[147])
			    begin
				   wgr[147] <= 1;
				   state <= 148;
				 end
			  else if (wrq[148])
			    begin
				   wgr[148] <= 1;
				   state <= 149;
				 end
			  else if (wrq[149])
			    begin
				   wgr[149] <= 1;
				   state <= 150;
				 end
			  else if (wrq[150])
			    begin
				   wgr[150] <= 1;
				   state <= 151;
				 end
			  else if (wrq[151])
			    begin
				   wgr[151] <= 1;
				   state <= 152;
				 end				 
			  else if (wrq[152])
			    begin
				   wgr[152] <= 1;
				   state <= 153;
				 end
			  else if (wrq[153])
			    begin
				   wgr[153] <= 1;
				   state <= 154;
				 end
			  else if (wrq[154])
			    begin
				   wgr[154] <= 1;
				   state <= 155;
				 end
			  else if (wrq[155])
			    begin
				   wgr[155] <= 1;
				   state <= 156;
				 end
			  else if (wrq[156])
			    begin
				   wgr[156] <= 1;
				   state <= 157;
				 end
			  else if (wrq[157])
			    begin
				   wgr[157] <= 1;
				   state <= 158;
				 end
			  else if (wrq[158])
			    begin
				   wgr[158] <= 1;
				   state <= 159;
				 end
			  else if (wrq[159])
			    begin
				   wgr[159] <= 1;
				   state <= 160;
				 end
			  else if (wrq[160])
			    begin
				   wgr[160] <= 1;
				   state <= 161;
				 end
			  else if (wrq[161])
			    begin
				   wgr[161] <= 1;
				   state <= 162;
				 end				 
			  else if (wrq[162])
			    begin
				   wgr[162] <= 1;
				   state <= 163;
				 end
			  else if (wrq[163])
			    begin
				   wgr[163] <= 1;
				   state <= 164;
				 end
			  else if (wrq[164])
			    begin
				   wgr[164] <= 1;
				   state <= 165;
				 end
			  else if (wrq[165])
			    begin
				   wgr[165] <= 1;
				   state <= 166;
				 end
			  else if (wrq[166])
			    begin
				   wgr[166] <= 1;
				   state <= 167;
				 end
			  else if (wrq[167])
			    begin
				   wgr[167] <= 1;
				   state <= 168;
				 end
			  else if (wrq[168])
			    begin
				   wgr[168] <= 1;
				   state <= 169;
				 end
			  else if (wrq[169])
			    begin
				   wgr[169] <= 1;
				   state <= 170;
				 end
			  else if (wrq[170])
			    begin
				   wgr[170] <= 1;
				   state <= 171;
				 end
			  else if (wrq[171])
			    begin
				   wgr[171] <= 1;
				   state <= 172;
				 end				 
			  else if (wrq[172])
			    begin
				   wgr[172] <= 1;
				   state <= 173;
				 end
			  else if (wrq[173])
			    begin
				   wgr[173] <= 1;
				   state <= 174;
				 end
			  else if (wrq[174])
			    begin
				   wgr[174] <= 1;
				   state <= 175;
				 end
			  else if (wrq[175])
			    begin
				   wgr[175] <= 1;
				   state <= 176;
				 end
			  else if (wrq[176])
			    begin
				   wgr[176] <= 1;
				   state <= 177;
				 end
			  else if (wrq[177])
			    begin
				   wgr[177] <= 1;
				   state <= 178;
				 end
			  else if (wrq[178])
			    begin
				   wgr[178] <= 1;
				   state <= 179;
				 end
			  else if (wrq[179])
			    begin
				   wgr[179] <= 1;
				   state <= 180;
				 end
			  else if (wrq[180])
			    begin
				   wgr[180] <= 1;
				   state <= 181;
				 end
			  else if (wrq[181])
			    begin
				   wgr[181] <= 1;
				   state <= 182;
				 end				 
			  else if (wrq[182])
			    begin
				   wgr[182] <= 1;
				   state <= 183;
				 end
			  else if (wrq[183])
			    begin
				   wgr[183] <= 1;
				   state <= 184;
				 end
			  else if (wrq[184])
			    begin
				   wgr[184] <= 1;
				   state <= 185;
				 end
			  else if (wrq[185])
			    begin
				   wgr[185] <= 1;
				   state <= 186;
				 end
			  else if (wrq[186])
			    begin
				   wgr[186] <= 1;
				   state <= 187;
				 end
			  else if (wrq[187])
			    begin
				   wgr[187] <= 1;
				   state <= 188;
				 end
			  else if (wrq[188])
			    begin
				   wgr[188] <= 1;
				   state <= 189;
				 end
			  else if (wrq[189])
			    begin
				   wgr[189] <= 1;
				   state <= 190;
				 end
			  else if (wrq[190])
			    begin
				   wgr[190] <= 1;
				   state <= 191;
				 end
			  else if (wrq[191])
			    begin
				   wgr[191] <= 1;
				   state <= 192;
				 end				 
			  else if (wrq[192])
			    begin
				   wgr[192] <= 1;
				   state <= 193;
				 end
			  else if (wrq[193])
			    begin
				   wgr[193] <= 1;
				   state <= 194;
				 end
			  else if (wrq[194])
			    begin
				   wgr[194] <= 1;
				   state <= 195;
				 end
			  else if (wrq[195])
			    begin
				   wgr[195] <= 1;
				   state <= 196;
				 end
			  else if (wrq[196])
			    begin
				   wgr[196] <= 1;
				   state <= 197;
				 end
			  else if (wrq[197])
			    begin
				   wgr[197] <= 1;
				   state <= 198;
				 end
			  else if (wrq[198])
			    begin
				   wgr[198] <= 1;
				   state <= 199;
				 end
			  else if (wrq[199])
			    begin
				   wgr[199] <= 1;
				   state <= 200;
				 end
			  else if (wrq[200])
			    begin
				   wgr[200] <= 1;
				   state <= 201;
				 end
			  else if (wrq[201])
			    begin
				   wgr[201] <= 1;
				   state <= 202;
				 end				 
			  else if (wrq[202])
			    begin
				   wgr[202] <= 1;
				   state <= 203;
				 end
			  else if (wrq[203])
			    begin
				   wgr[203] <= 1;
				   state <= 204;
				 end
			  else if (wrq[204])
			    begin
				   wgr[204] <= 1;
				   state <= 205;
				 end
			  else if (wrq[205])
			    begin
				   wgr[205] <= 1;
				   state <= 206;
				 end
			  else if (wrq[206])
			    begin
				   wgr[206] <= 1;
				   state <= 207;
				 end
			  else if (wrq[207])
			    begin
				   wgr[207] <= 1;
				   state <= 208;
				 end
			  else if (wrq[208])
			    begin
				   wgr[208] <= 1;
				   state <= 209;
				 end
			  else if (wrq[209])
			    begin
				   wgr[209] <= 1;
				   state <= 210;
				 end
			  else if (wrq[210])
			    begin
				   wgr[210] <= 1;
				   state <= 211;
				 end
			  else if (wrq[211])
			    begin
				   wgr[211] <= 1;
				   state <= 212;
				 end				 
			  else if (wrq[212])
			    begin
				   wgr[212] <= 1;
				   state <= 213;
				 end
			  else if (wrq[213])
			    begin
				   wgr[213] <= 1;
				   state <= 214;
				 end
			  else if (wrq[214])
			    begin
				   wgr[214] <= 1;
				   state <= 215;
				 end
			  else if (wrq[215])
			    begin
				   wgr[215] <= 1;
				   state <= 216;
				 end
			  else if (wrq[216])
			    begin
				   wgr[216] <= 1;
				   state <= 217;
				 end
			  else if (wrq[217])
			    begin
				   wgr[217] <= 1;
				   state <= 218;
				 end
			  else if (wrq[218])
			    begin
				   wgr[218] <= 1;
				   state <= 219;
				 end
			  else if (wrq[219])
			    begin
				   wgr[219] <= 1;
				   state <= 220;
				 end
			  else if (wrq[220])
			    begin
				   wgr[220] <= 1;
				   state <= 221;
				 end
			  else if (wrq[221])
			    begin
				   wgr[221] <= 1;
				   state <= 222;
				 end				 
			  else if (wrq[222])
			    begin
				   wgr[222] <= 1;
				   state <= 223;
				 end
			  else if (wrq[223])
			    begin
				   wgr[223] <= 1;
				   state <= 224;
				 end
			  else if (wrq[224])
			    begin
				   wgr[224] <= 1;
				   state <= 225;
				 end
			  else if (wrq[225])
			    begin
				   wgr[225] <= 1;
				   state <= 226;
				 end
			  else if (wrq[226])
			    begin
				   wgr[226] <= 1;
				   state <= 227;
				 end
			  else if (wrq[227])
			    begin
				   wgr[227] <= 1;
				   state <= 228;
				 end
			  else if (wrq[228])
			    begin
				   wgr[228] <= 1;
				   state <= 229;
				 end
			  else if (wrq[229])
			    begin
				   wgr[229] <= 1;
				   state <= 230;
				 end
			  else if (wrq[230])
			    begin
				   wgr[230] <= 1;
				   state <= 231;
				 end
			  else if (wrq[231])
			    begin
				   wgr[231] <= 1;
				   state <= 232;
				 end				 
			  else if (wrq[232])
			    begin
				   wgr[232] <= 1;
				   state <= 233;
				 end
			  else if (wrq[233])
			    begin
				   wgr[233] <= 1;
				   state <= 234;
				 end
			  else if (wrq[234])
			    begin
				   wgr[234] <= 1;
				   state <= 235;
				 end
			  else if (wrq[235])
			    begin
				   wgr[235] <= 1;
				   state <= 236;
				 end
			  else if (wrq[236])
			    begin
				   wgr[236] <= 1;
				   state <= 237;
				 end
			  else if (wrq[237])
			    begin
				   wgr[237] <= 1;
				   state <= 238;
				 end
			  else if (wrq[238])
			    begin
				   wgr[238] <= 1;
				   state <= 239;
				 end
			  else if (wrq[239])
			    begin
				   wgr[239] <= 1;
				   state <= 240;
				 end
			  else if (wrq[240])
			    begin
				   wgr[240] <= 1;
				   state <= 241;
				 end
			  else if (wrq[241])
			    begin
				   wgr[241] <= 1;
				   state <= 242;
				 end				 
			  else if (wrq[242])
			    begin
				   wgr[242] <= 1;
				   state <= 243;
				 end
			  else if (wrq[243])
			    begin
				   wgr[243] <= 1;
				   state <= 244;
				 end
			  else if (wrq[244])
			    begin
				   wgr[244] <= 1;
				   state <= 245;
				 end
			  else if (wrq[245])
			    begin
				   wgr[245] <= 1;
				   state <= 246;
				 end
			  else if (wrq[246])
			    begin
				   wgr[246] <= 1;
				   state <= 247;
				 end
			  else if (wrq[247])
			    begin
				   wgr[247] <= 1;
				   state <= 248;
				 end
			  else if (wrq[248])
			    begin
				   wgr[248] <= 1;
				   state <= 249;
				 end
			  else if (wrq[249])
			    begin
				   wgr[249] <= 1;
				   state <= 250;
				 end
			  else if (wrq[250])
			    begin
				   wgr[250] <= 1;
				   state <= 251;
				 end
			  else if (wrq[251])
			    begin
				   wgr[251] <= 1;
				   state <= 252;
				 end				 
			  else if (wrq[252])
			    begin
				   wgr[252] <= 1;
				   state <= 253;
				 end
			  else if (wrq[253])
			    begin
				   wgr[253] <= 1;
				   state <= 254;
				 end
			  else if (wrq[254])
			    begin
				   wgr[254] <= 1;
				   state <= 255;
				 end
			  else if (wrq[255])
			    begin
				   wgr[255] <= 1;
				   state <= 256;
				 end
			  else if (wrq[256])
			    begin
				   wgr[256] <= 1;
				   state <= 257;
				 end
			  else if (wrq[257])
			    begin
				   wgr[257] <= 1;
				   state <= 258;
				 end
			  else if (wrq[258])
			    begin
				   wgr[258] <= 1;
				   state <= 259;
				 end
			  else if (wrq[259])
			    begin
				   wgr[259] <= 1;
				   state <= 260;
				 end
			  else if (wrq[260])
			    begin
				   wgr[260] <= 1;
				   state <= 261;
				 end
			  else if (wrq[261])
			    begin
				   wgr[261] <= 1;
				   state <= 262;
				 end				 
			  else if (wrq[262])
			    begin
				   wgr[262] <= 1;
				   state <= 263;
				 end
			  else if (wrq[263])
			    begin
				   wgr[263] <= 1;
				   state <= 264;
				 end
			  else if (wrq[264])
			    begin
				   wgr[264] <= 1;
				   state <= 265;
				 end
			  else if (wrq[265])
			    begin
				   wgr[265] <= 1;
				   state <= 266;
				 end
			  else if (wrq[266])
			    begin
				   wgr[266] <= 1;
				   state <= 267;
				 end
			  else if (wrq[267])
			    begin
				   wgr[267] <= 1;
				   state <= 268;
				 end
			  else if (wrq[268])
			    begin
				   wgr[268] <= 1;
				   state <= 269;
				 end
			  else if (wrq[269])
			    begin
				   wgr[269] <= 1;
				   state <= 270;
				 end
			  else if (wrq[270])
			    begin
				   wgr[270] <= 1;
				   state <= 271;
				 end
			  else if (wrq[271])
			    begin
				   wgr[271] <= 1;
				   state <= 272;
				 end				 
			  else if (wrq[272])
			    begin
				   wgr[272] <= 1;
				   state <= 273;
				 end
			  else if (wrq[273])
			    begin
				   wgr[273] <= 1;
				   state <= 274;
				 end
			  else if (wrq[274])
			    begin
				   wgr[274] <= 1;
				   state <= 275;
				 end
			  else if (wrq[275])
			    begin
				   wgr[275] <= 1;
				   state <= 276;
				 end
			  else if (wrq[276])
			    begin
				   wgr[276] <= 1;
				   state <= 277;
				 end
			  else if (wrq[277])
			    begin
				   wgr[277] <= 1;
				   state <= 278;
				 end
			  else if (wrq[278])
			    begin
				   wgr[278] <= 1;
				   state <= 279;
				 end
			  else if (wrq[279])
			    begin
				   wgr[279] <= 1;
				   state <= 280;
				 end
			  else if (wrq[280])
			    begin
				   wgr[280] <= 1;
				   state <= 281;
				 end
			  else if (wrq[281])
			    begin
				   wgr[281] <= 1;
				   state <= 282;
				 end				 
			  else if (wrq[282])
			    begin
				   wgr[282] <= 1;
				   state <= 283;
				 end
			  else if (wrq[283])
			    begin
				   wgr[283] <= 1;
				   state <= 284;
				 end
			  else if (wrq[284])
			    begin
				   wgr[284] <= 1;
				   state <= 285;
				 end
			  else if (wrq[285])
			    begin
				   wgr[285] <= 1;
				   state <= 286;
				 end
			  else if (wrq[286])
			    begin
				   wgr[286] <= 1;
				   state <= 287;
				 end
			  else if (wrq[287])
			    begin
				   wgr[287] <= 1;
				   state <= 288;
				 end
			  else if (wrq[288])
			    begin
				   wgr[288] <= 1;
				   state <= 289;
				 end
			  else if (wrq[289])
			    begin
				   wgr[289] <= 1;
				   state <= 290;
				 end
			  else if (wrq[290])
			    begin
				   wgr[290] <= 1;
				   state <= 291;
				 end
			  else if (wrq[291])
			    begin
				   wgr[291] <= 1;
				   state <= 292;
				 end				 
			  else if (wrq[292])
			    begin
				   wgr[292] <= 1;
				   state <= 293;
				 end
			  else if (wrq[293])
			    begin
				   wgr[293] <= 1;
				   state <= 294;
				 end
			  else if (wrq[294])
			    begin
				   wgr[294] <= 1;
				   state <= 295;
				 end
			  else if (wrq[295])
			    begin
				   wgr[295] <= 1;
				   state <= 296;
				 end
			  else if (wrq[296])
			    begin
				   wgr[296] <= 1;
				   state <= 297;
				 end
			  else if (wrq[297])
			    begin
				   wgr[297] <= 1;
				   state <= 298;
				 end
			  else if (wrq[298])
			    begin
				   wgr[298] <= 1;
				   state <= 299;
				 end
			  else if (wrq[299])
			    begin
				   wgr[299] <= 1;
				   state <= 300;
				 end	
			  else if (wrq[300])
			    begin
				   wgr[300] <= 1;
				   state <= 301;
				 end
			  else if (wrq[301])
			    begin
				   wgr[301] <= 1;
				   state <= 302;
				 end				 
			  else if (wrq[302])
			    begin
				   wgr[302] <= 1;
				   state <= 303;
				 end
			  else if (wrq[303])
			    begin
				   wgr[303] <= 1;
				   state <= 304;
				 end
			  else if (wrq[304])
			    begin
				   wgr[304] <= 1;
				   state <= 305;
				 end
			  else if (wrq[305])
			    begin
				   wgr[305] <= 1;
				   state <= 306;
				 end
			  else if (wrq[306])
			    begin
				   wgr[306] <= 1;
				   state <= 307;
				 end
			  else if (wrq[307])
			    begin
				   wgr[307] <= 1;
				   state <= 308;
				 end
			  else if (wrq[308])
			    begin
				   wgr[308] <= 1;
				   state <= 309;
				 end
			  else if (wrq[309])
			    begin
				   wgr[309] <= 1;
				   state <= 310;
				 end
			  else if (wrq[310])
			    begin
				   wgr[310] <= 1;
				   state <= 311;
				 end
			  else if (wrq[311])
			    begin
				   wgr[311] <= 1;
				   state <= 312;
				 end				 
			  else if (wrq[312])
			    begin
				   wgr[312] <= 1;
				   state <= 313;
				 end
			  else if (wrq[313])
			    begin
				   wgr[313] <= 1;
				   state <= 314;
				 end
			  else if (wrq[314])
			    begin
				   wgr[314] <= 1;
				   state <= 315;
				 end
			  else if (wrq[315])
			    begin
				   wgr[315] <= 1;
				   state <= 316;
				 end
			  else if (wrq[316])
			    begin
				   wgr[316] <= 1;
				   state <= 317;
				 end
			  else if (wrq[317])
			    begin
				   wgr[317] <= 1;
				   state <= 318;
				 end
			  else if (wrq[318])
			    begin
				   wgr[318] <= 1;
				   state <= 319;
				 end
			  else if (wrq[319])
			    begin
				   wgr[319] <= 1;
				   state <= 320;
				 end
			  else if (wrq[320])
			    begin
				   wgr[320] <= 1;
				   state <= 321;
				 end
			  else if (wrq[321])
			    begin
				   wgr[321] <= 1;
				   state <= 322;
				 end				 
			  else if (wrq[322])
			    begin
				   wgr[322] <= 1;
				   state <= 323;
				 end
			  else if (wrq[323])
			    begin
				   wgr[323] <= 1;
				   state <= 324;
				 end
			  else if (wrq[324])
			    begin
				   wgr[324] <= 1;
				   state <= 325;
				 end
			  else if (wrq[325])
			    begin
				   wgr[325] <= 1;
				   state <= 326;
				 end
			  else if (wrq[326])
			    begin
				   wgr[326] <= 1;
				   state <= 327;
				 end
			  else if (wrq[327])
			    begin
				   wgr[327] <= 1;
				   state <= 328;
				 end
			  else if (wrq[328])
			    begin
				   wgr[328] <= 1;
				   state <= 329;
				 end
			  else if (wrq[329])
			    begin
				   wgr[329] <= 1;
				   state <= 330;
				 end
			  else if (wrq[330])
			    begin
				   wgr[330] <= 1;
				   state <= 331;
				 end
			  else if (wrq[331])
			    begin
				   wgr[331] <= 1;
				   state <= 332;
				 end				 
			  else if (wrq[332])
			    begin
				   wgr[332] <= 1;
				   state <= 333;
				 end
			  else if (wrq[333])
			    begin
				   wgr[333] <= 1;
				   state <= 334;
				 end
			  else if (wrq[334])
			    begin
				   wgr[334] <= 1;
				   state <= 335;
				 end
			  else if (wrq[335])
			    begin
				   wgr[335] <= 1;
				   state <= 336;
				 end
			  else if (wrq[336])
			    begin
				   wgr[336] <= 1;
				   state <= 337;
				 end
			  else if (wrq[337])
			    begin
				   wgr[337] <= 1;
				   state <= 338;
				 end
			  else if (wrq[338])
			    begin
				   wgr[338] <= 1;
				   state <= 339;
				 end
			  else if (wrq[339])
			    begin
				   wgr[339] <= 1;
				   state <= 340;
				 end
			  else if (wrq[340])
			    begin
				   wgr[340] <= 1;
				   state <= 341;
				 end
			  else if (wrq[341])
			    begin
				   wgr[341] <= 1;
				   state <= 342;
				 end				 
			  else if (wrq[342])
			    begin
				   wgr[342] <= 1;
				   state <= 343;
				 end
			  else if (wrq[343])
			    begin
				   wgr[343] <= 1;
				   state <= 344;
				 end
			  else if (wrq[344])
			    begin
				   wgr[344] <= 1;
				   state <= 345;
				 end
			  else if (wrq[345])
			    begin
				   wgr[345] <= 1;
				   state <= 346;
				 end
			  else if (wrq[346])
			    begin
				   wgr[346] <= 1;
				   state <= 347;
				 end
			  else if (wrq[347])
			    begin
				   wgr[347] <= 1;
				   state <= 348;
				 end
			  else if (wrq[348])
			    begin
				   wgr[348] <= 1;
				   state <= 349;
				 end
			  else if (wrq[349])
			    begin
				   wgr[349] <= 1;
				   state <= 350;
				 end
			  else if (wrq[350])
			    begin
				   wgr[350] <= 1;
				   state <= 351;
				 end
			  else if (wrq[351])
			    begin
				   wgr[351] <= 1;
				   state <= 352;
				 end				 
			  else if (wrq[352])
			    begin
				   wgr[352] <= 1;
				   state <= 353;
				 end
			  else if (wrq[353])
			    begin
				   wgr[353] <= 1;
				   state <= 354;
				 end
			  else if (wrq[354])
			    begin
				   wgr[354] <= 1;
				   state <= 355;
				 end
			  else if (wrq[355])
			    begin
				   wgr[355] <= 1;
				   state <= 356;
				 end
			  else if (wrq[356])
			    begin
				   wgr[356] <= 1;
				   state <= 357;
				 end
			  else if (wrq[357])
			    begin
				   wgr[357] <= 1;
				   state <= 358;
				 end
			  else if (wrq[358])
			    begin
				   wgr[358] <= 1;
				   state <= 359;
				 end
			  else if (wrq[359])
			    begin
				   wgr[359] <= 1;
				   state <= 360;
				 end
			  else if (wrq[360])
			    begin
				   wgr[360] <= 1;
				   state <= 361;
				 end
			  else if (wrq[361])
			    begin
				   wgr[361] <= 1;
				   state <= 362;
				 end				 
			  else if (wrq[362])
			    begin
				   wgr[362] <= 1;
				   state <= 363;
				 end
			  else if (wrq[363])
			    begin
				   wgr[363] <= 1;
				   state <= 364;
				 end
			  else if (wrq[364])
			    begin
				   wgr[364] <= 1;
				   state <= 365;
				 end
			  else if (wrq[365])
			    begin
				   wgr[365] <= 1;
				   state <= 366;
				 end
			  else if (wrq[366])
			    begin
				   wgr[366] <= 1;
				   state <= 367;
				 end
			  else if (wrq[367])
			    begin
				   wgr[367] <= 1;
				   state <= 368;
				 end
			  else if (wrq[368])
			    begin
				   wgr[368] <= 1;
				   state <= 369;
				 end
			  else if (wrq[369])
			    begin
				   wgr[369] <= 1;
				   state <= 370;
				 end
			  else if (wrq[370])
			    begin
				   wgr[370] <= 1;
				   state <= 371;
				 end
			  else if (wrq[371])
			    begin
				   wgr[371] <= 1;
				   state <= 372;
				 end				 
			  else if (wrq[372])
			    begin
				   wgr[372] <= 1;
				   state <= 373;
				 end
			  else if (wrq[373])
			    begin
				   wgr[373] <= 1;
				   state <= 374;
				 end
			  else if (wrq[374])
			    begin
				   wgr[374] <= 1;
				   state <= 375;
				 end
			  else if (wrq[375])
			    begin
				   wgr[375] <= 1;
				   state <= 376;
				 end
			  else if (wrq[376])
			    begin
				   wgr[376] <= 1;
				   state <= 377;
				 end
			  else if (wrq[377])
			    begin
				   wgr[377] <= 1;
				   state <= 378;
				 end
			  else if (wrq[378])
			    begin
				   wgr[378] <= 1;
				   state <= 379;
				 end
			  else if (wrq[379])
			    begin
				   wgr[379] <= 1;
				   state <= 380;
				 end
			  else if (wrq[380])
			    begin
				   wgr[380] <= 1;
				   state <= 381;
				 end
			  else if (wrq[381])
			    begin
				   wgr[381] <= 1;
				   state <= 382;
				 end				 
			  else if (wrq[382])
			    begin
				   wgr[382] <= 1;
				   state <= 383;
				 end
			  else if (wrq[383])
			    begin
				   wgr[383] <= 1;
				   state <= 384;
				 end
			  else if (wrq[384])
			    begin
				   wgr[384] <= 1;
				   state <= 385;
				 end
			  else if (wrq[385])
			    begin
				   wgr[385] <= 1;
				   state <= 386;
				 end
			  else if (wrq[386])
			    begin
				   wgr[386] <= 1;
				   state <= 387;
				 end
			  else if (wrq[387])
			    begin
				   wgr[387] <= 1;
				   state <= 388;
				 end
			  else if (wrq[388])
			    begin
				   wgr[388] <= 1;
				   state <= 389;
				 end
			  else if (wrq[389])
			    begin
				   wgr[389] <= 1;
				   state <= 390;
				 end
			  else if (wrq[390])
			    begin
				   wgr[390] <= 1;
				   state <= 391;
				 end
			  else if (wrq[391])
			    begin
				   wgr[391] <= 1;
				   state <= 392;
				 end				 
			  else if (wrq[392])
			    begin
				   wgr[392] <= 1;
				   state <= 393;
				 end
			  else if (wrq[393])
			    begin
				   wgr[393] <= 1;
				   state <= 394;
				 end
			  else if (wrq[394])
			    begin
				   wgr[394] <= 1;
				   state <= 395;
				 end
			  else if (wrq[395])
			    begin
				   wgr[395] <= 1;
				   state <= 396;
				 end
			  else if (wrq[396])
			    begin
				   wgr[396] <= 1;
				   state <= 397;
				 end
			  else if (wrq[397])
			    begin
				   wgr[397] <= 1;
				   state <= 398;
				 end
			  else if (wrq[398])
			    begin
				   wgr[398] <= 1;
				   state <= 399;
				 end
			  else if (wrq[399])
			    begin
				   wgr[399] <= 1;
				   state <= 400;
				 end	
			  else if (wrq[400])
			    begin
				   wgr[400] <= 1;
				   state <= 401;
				 end
			  else if (wrq[401])
			    begin
				   wgr[401] <= 1;
				   state <= 402;
				 end				 
			  else if (wrq[402])
			    begin
				   wgr[402] <= 1;
				   state <= 403;
				 end
			  else if (wrq[403])
			    begin
				   wgr[403] <= 1;
				   state <= 404;
				 end
			  else if (wrq[404])
			    begin
				   wgr[404] <= 1;
				   state <= 405;
				 end
			  else if (wrq[405])
			    begin
				   wgr[405] <= 1;
				   state <= 406;
				 end
			  else if (wrq[406])
			    begin
				   wgr[406] <= 1;
				   state <= 407;
				 end
			  else if (wrq[407])
			    begin
				   wgr[407] <= 1;
				   state <= 408;
				 end
			  else if (wrq[408])
			    begin
				   wgr[408] <= 1;
				   state <= 409;
				 end
			  else if (wrq[409])
			    begin
				   wgr[409] <= 1;
				   state <= 410;
				 end
			  else if (wrq[410])
			    begin
				   wgr[410] <= 1;
				   state <= 411;
				 end
			  else if (wrq[411])
			    begin
				   wgr[411] <= 1;
				   state <= 412;
				 end				 
			  else if (wrq[412])
			    begin
				   wgr[412] <= 1;
				   state <= 413;
				 end
			  else if (wrq[413])
			    begin
				   wgr[413] <= 1;
				   state <= 414;
				 end
			  else if (wrq[414])
			    begin
				   wgr[414] <= 1;
				   state <= 415;
				 end
			  else if (wrq[415])
			    begin
				   wgr[415] <= 1;
				   state <= 416;
				 end
			  else if (wrq[416])
			    begin
				   wgr[416] <= 1;
				   state <= 417;
				 end
			  else if (wrq[417])
			    begin
				   wgr[417] <= 1;
				   state <= 418;
				 end
			  else if (wrq[418])
			    begin
				   wgr[418] <= 1;
				   state <= 419;
				 end
			  else if (wrq[419])
			    begin
				   wgr[419] <= 1;
				   state <= 420;
				 end
			  else if (wrq[420])
			    begin
				   wgr[420] <= 1;
				   state <= 421;
				 end
			  else if (wrq[421])
			    begin
				   wgr[421] <= 1;
				   state <= 422;
				 end				 
			  else if (wrq[422])
			    begin
				   wgr[422] <= 1;
				   state <= 423;
				 end
			  else if (wrq[423])
			    begin
				   wgr[423] <= 1;
				   state <= 424;
				 end
			  else if (wrq[424])
			    begin
				   wgr[424] <= 1;
				   state <= 425;
				 end
			  else if (wrq[425])
			    begin
				   wgr[425] <= 1;
				   state <= 426;
				 end
			  else if (wrq[426])
			    begin
				   wgr[426] <= 1;
				   state <= 427;
				 end
			  else if (wrq[427])
			    begin
				   wgr[427] <= 1;
				   state <= 428;
				 end
			  else if (wrq[428])
			    begin
				   wgr[428] <= 1;
				   state <= 429;
				 end
			  else if (wrq[429])
			    begin
				   wgr[429] <= 1;
				   state <= 430;
				 end
			  else if (wrq[430])
			    begin
				   wgr[430] <= 1;
				   state <= 431;
				 end
			  else if (wrq[431])
			    begin
				   wgr[431] <= 1;
				   state <= 432;
				 end				 
			  else if (wrq[432])
			    begin
				   wgr[432] <= 1;
				   state <= 433;
				 end
			  else if (wrq[433])
			    begin
				   wgr[433] <= 1;
				   state <= 434;
				 end
			  else if (wrq[434])
			    begin
				   wgr[434] <= 1;
				   state <= 435;
				 end
			  else if (wrq[435])
			    begin
				   wgr[435] <= 1;
				   state <= 436;
				 end
			  else if (wrq[436])
			    begin
				   wgr[436] <= 1;
				   state <= 437;
				 end
			  else if (wrq[437])
			    begin
				   wgr[437] <= 1;
				   state <= 438;
				 end
			  else if (wrq[438])
			    begin
				   wgr[438] <= 1;
				   state <= 439;
				 end
			  else if (wrq[439])
			    begin
				   wgr[439] <= 1;
				   state <= 440;
				 end
			  else if (wrq[440])
			    begin
				   wgr[440] <= 1;
				   state <= 441;
				 end
			  else if (wrq[441])
			    begin
				   wgr[441] <= 1;
				   state <= 442;
				 end				 
			  else if (wrq[442])
			    begin
				   wgr[442] <= 1;
				   state <= 443;
				 end
			  else if (wrq[443])
			    begin
				   wgr[443] <= 1;
				   state <= 444;
				 end
			  else if (wrq[444])
			    begin
				   wgr[444] <= 1;
				   state <= 445;
				 end
			  else if (wrq[445])
			    begin
				   wgr[445] <= 1;
				   state <= 446;
				 end
			  else if (wrq[446])
			    begin
				   wgr[446] <= 1;
				   state <= 447;
				 end
			  else if (wrq[447])
			    begin
				   wgr[447] <= 1;
				   state <= 448;
				 end
			  else if (wrq[448])
			    begin
				   wgr[448] <= 1;
				   state <= 449;
				 end
			  else if (wrq[449])
			    begin
				   wgr[449] <= 1;
				   state <= 450;
				 end
			  else if (wrq[450])
			    begin
				   wgr[450] <= 1;
				   state <= 451;
				 end
			  else if (wrq[451])
			    begin
				   wgr[451] <= 1;
				   state <= 452;
				 end				 
			  else if (wrq[452])
			    begin
				   wgr[452] <= 1;
				   state <= 453;
				 end
			  else if (wrq[453])
			    begin
				   wgr[453] <= 1;
				   state <= 454;
				 end
			  else if (wrq[454])
			    begin
				   wgr[454] <= 1;
				   state <= 455;
				 end
			  else if (wrq[455])
			    begin
				   wgr[455] <= 1;
				   state <= 456;
				 end
			  else if (wrq[456])
			    begin
				   wgr[456] <= 1;
				   state <= 457;
				 end
			  else if (wrq[457])
			    begin
				   wgr[457] <= 1;
				   state <= 458;
				 end
			  else if (wrq[458])
			    begin
				   wgr[458] <= 1;
				   state <= 459;
				 end
			  else if (wrq[459])
			    begin
				   wgr[459] <= 1;
				   state <= 460;
				 end
			  else if (wrq[460])
			    begin
				   wgr[460] <= 1;
				   state <= 461;
				 end
			  else if (wrq[461])
			    begin
				   wgr[461] <= 1;
				   state <= 462;
				 end				 
			  else if (wrq[462])
			    begin
				   wgr[462] <= 1;
				   state <= 463;
				 end
			  else if (wrq[463])
			    begin
				   wgr[463] <= 1;
				   state <= 464;
				 end
			  else if (wrq[464])
			    begin
				   wgr[464] <= 1;
				   state <= 465;
				 end
			  else if (wrq[465])
			    begin
				   wgr[465] <= 1;
				   state <= 466;
				 end
			  else if (wrq[466])
			    begin
				   wgr[466] <= 1;
				   state <= 467;
				 end
			  else if (wrq[467])
			    begin
				   wgr[467] <= 1;
				   state <= 468;
				 end
			  else if (wrq[468])
			    begin
				   wgr[468] <= 1;
				   state <= 469;
				 end
			  else if (wrq[469])
			    begin
				   wgr[469] <= 1;
				   state <= 470;
				 end
			  else if (wrq[470])
			    begin
				   wgr[470] <= 1;
				   state <= 471;
				 end
			  else if (wrq[471])
			    begin
				   wgr[471] <= 1;
				   state <= 472;
				 end				 
			  else if (wrq[472])
			    begin
				   wgr[472] <= 1;
				   state <= 473;
				 end
			  else if (wrq[473])
			    begin
				   wgr[473] <= 1;
				   state <= 474;
				 end
			  else if (wrq[474])
			    begin
				   wgr[474] <= 1;
				   state <= 475;
				 end
			  else if (wrq[475])
			    begin
				   wgr[475] <= 1;
				   state <= 476;
				 end
			  else if (wrq[476])
			    begin
				   wgr[476] <= 1;
				   state <= 477;
				 end
			  else if (wrq[477])
			    begin
				   wgr[477] <= 1;
				   state <= 478;
				 end
			  else if (wrq[478])
			    begin
				   wgr[478] <= 1;
				   state <= 479;
				 end
			  else if (wrq[479])
			    begin
				   wgr[479] <= 1;
				   state <= 480;
				 end
			  else if (wrq[480])
			    begin
				   wgr[480] <= 1;
				   state <= 481;
				 end
			  else if (wrq[481])
			    begin
				   wgr[481] <= 1;
				   state <= 482;
				 end				 
			  else if (wrq[482])
			    begin
				   wgr[482] <= 1;
				   state <= 483;
				 end
			  else if (wrq[483])
			    begin
				   wgr[483] <= 1;
				   state <= 484;
				 end
			  else if (wrq[484])
			    begin
				   wgr[484] <= 1;
				   state <= 485;
				 end
			  else if (wrq[485])
			    begin
				   wgr[485] <= 1;
				   state <= 486;
				 end
			  else if (wrq[486])
			    begin
				   wgr[486] <= 1;
				   state <= 487;
				 end
			  else if (wrq[487])
			    begin
				   wgr[487] <= 1;
				   state <= 488;
				 end
			  else if (wrq[488])
			    begin
				   wgr[488] <= 1;
				   state <= 489;
				 end
			  else if (wrq[489])
			    begin
				   wgr[489] <= 1;
				   state <= 490;
				 end
			  else if (wrq[490])
			    begin
				   wgr[490] <= 1;
				   state <= 491;
				 end
			  else if (wrq[491])
			    begin
				   wgr[491] <= 1;
				   state <= 492;
				 end				 
			  else if (wrq[492])
			    begin
				   wgr[492] <= 1;
				   state <= 493;
				 end
			  else if (wrq[493])
			    begin
				   wgr[493] <= 1;
				   state <= 494;
				 end
			  else if (wrq[494])
			    begin
				   wgr[494] <= 1;
				   state <= 495;
				 end
			  else if (wrq[495])
			    begin
				   wgr[495] <= 1;
				   state <= 496;
				 end
			  else if (wrq[496])
			    begin
				   wgr[496] <= 1;
				   state <= 497;
				 end
			  else if (wrq[497])
			    begin
				   wgr[497] <= 1;
				   state <= 498;
				 end
			  else if (wrq[498])
			    begin
				   wgr[498] <= 1;
				   state <= 499;
				 end
			  else if (wrq[499])
			    begin
				   wgr[499] <= 1;
				   state <= 200;
				 end	
			  else if (wrq[500])
			    begin
				   wgr[500] <= 1;
				   state <= 501;
				 end
			  else if (wrq[501])
			    begin
				   wgr[501] <= 1;
				   state <= 502;
				 end				 
			  else if (wrq[502])
			    begin
				   wgr[502] <= 1;
				   state <= 503;
				 end
			  else if (wrq[503])
			    begin
				   wgr[503] <= 1;
				   state <= 504;
				 end
			  else if (wrq[504])
			    begin
				   wgr[504] <= 1;
				   state <= 505;
				 end
			  else if (wrq[505])
			    begin
				   wgr[505] <= 1;
				   state <= 506;
				 end
			  else if (wrq[506])
			    begin
				   wgr[506] <= 1;
				   state <= 507;
				 end
			  else if (wrq[507])
			    begin
				   wgr[507] <= 1;
				   state <= 508;
				 end
			  else if (wrq[508])
			    begin
				   wgr[508] <= 1;
				   state <= 509;
				 end
			  else if (wrq[509])
			    begin
				   wgr[509] <= 1;
				   state <= 150;
				 end
			  else if (wrq[510])
			    begin
				   wgr[510] <= 1;
				   state <= 511;
				 end	
			  else if (wrq[511])
			    begin
				   wgr[511] <= 1;
				   state <= 512;
				 end					 
        end			 
      1:
		  begin
			 count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr0)
					0: 
					  begin
						 we <= 512'hffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe;
						 broadcast <= 1;					 
					  end
			      default: we[waddr0] <= 1;
				  endcase
				end
			 source_node <= 0;
			 if (!wrq[0])
			   begin
				  we <= 0;
				  state <= 0;
				end
        end	
      default:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr[state-2])
					0: 
					  begin
						 we <= 512'hfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe;
						 broadcast <= 1;						 
					  end
					1: we[0] <= 1;
				  endcase
            end				  
			 source_node <= state-1;
			 if (!wrq[state-1])
			   begin
				  we <= 0;
				  state <= 0;
				end
        end
    endcase
 
  always@ (posedge clk)
  begin
    case (state)
	   1: wdata_out = wdata_in0;
	   2: wdata_out = wdata_in1;
	   3: wdata_out = wdata_in2;
	   4: wdata_out = wdata_in3;	
	   5: wdata_out = wdata_in4;
	   6: wdata_out = wdata_in5;	
	   7: wdata_out = wdata_in6;
	   8: wdata_out = wdata_in7;	
	   9: wdata_out = wdata_in8;
	   10: wdata_out = wdata_in9;	
	   11: wdata_out = wdata_in10;
	   12: wdata_out = wdata_in11;	
	   13: wdata_out = wdata_in12;
	   14: wdata_out = wdata_in13;	
	   15: wdata_out = wdata_in14;
	   16: wdata_out = wdata_in15;	
	   17: wdata_out = wdata_in16;
	   18: wdata_out = wdata_in17;	
	   19: wdata_out = wdata_in18;
	   20: wdata_out = wdata_in19;	
	   21: wdata_out = wdata_in20;
	   22: wdata_out = wdata_in21;	
	   23: wdata_out = wdata_in22;
	   24: wdata_out = wdata_in23;	
	   25: wdata_out = wdata_in24;
	   26: wdata_out = wdata_in25;	
	   27: wdata_out = wdata_in26;
	   28: wdata_out = wdata_in27;	
	   29: wdata_out = wdata_in28;
	   30: wdata_out = wdata_in29;
	   31: wdata_out = wdata_in30;
	   32: wdata_out = wdata_in31;	
	   33: wdata_out = wdata_in32;
	   34: wdata_out = wdata_in33;	
	   35: wdata_out = wdata_in34;
	   36: wdata_out = wdata_in35;	
	   37: wdata_out = wdata_in36;
	   38: wdata_out = wdata_in37;	
	   39: wdata_out = wdata_in38;
	   40: wdata_out = wdata_in39;		
	   41: wdata_out = wdata_in40;
	   42: wdata_out = wdata_in41;	
	   43: wdata_out = wdata_in42;
	   44: wdata_out = wdata_in43;	
	   45: wdata_out = wdata_in44;
	   46: wdata_out = wdata_in45;	
	   47: wdata_out = wdata_in46;
	   48: wdata_out = wdata_in47;	
	   49: wdata_out = wdata_in48;
	   50: wdata_out = wdata_in49;	
	   50: wdata_out = wdata_in49;	
	   51: wdata_out = wdata_in50;
	   52: wdata_out = wdata_in51;	
	   53: wdata_out = wdata_in52;
	   54: wdata_out = wdata_in53;	
	   55: wdata_out = wdata_in54;
	   56: wdata_out = wdata_in55;	
	   57: wdata_out = wdata_in56;
	   58: wdata_out = wdata_in57;	
	   59: wdata_out = wdata_in58;
	   60: wdata_out = wdata_in59;	
	   61: wdata_out = wdata_in60;
	   62: wdata_out = wdata_in61;	
	   63: wdata_out = wdata_in62;
	   64: wdata_out = wdata_in63;	
	   65: wdata_out = wdata_in64;
	   66: wdata_out = wdata_in65;	
	   67: wdata_out = wdata_in66;
	   68: wdata_out = wdata_in67;	
	   69: wdata_out = wdata_in68;
	   70: wdata_out = wdata_in69;	
	   71: wdata_out = wdata_in70;
	   72: wdata_out = wdata_in71;	
	   73: wdata_out = wdata_in72;
	   74: wdata_out = wdata_in73;	
	   75: wdata_out = wdata_in74;
	   76: wdata_out = wdata_in75;	
	   77: wdata_out = wdata_in76;
	   78: wdata_out = wdata_in77;	
	   79: wdata_out = wdata_in78;	
	   80: wdata_out = wdata_in79;	
	   81: wdata_out = wdata_in80;
	   82: wdata_out = wdata_in81;	
	   83: wdata_out = wdata_in82;
	   84: wdata_out = wdata_in83;	
	   85: wdata_out = wdata_in84;
	   86: wdata_out = wdata_in85;	
	   87: wdata_out = wdata_in86;
	   88: wdata_out = wdata_in87;	
	   89: wdata_out = wdata_in88;	
	   90: wdata_out = wdata_in89;	
	   91: wdata_out = wdata_in90;
	   92: wdata_out = wdata_in91;	
	   93: wdata_out = wdata_in92;
	   94: wdata_out = wdata_in93;	
	   95: wdata_out = wdata_in94;
	   96: wdata_out = wdata_in95;	
	   97: wdata_out = wdata_in96;
	   98: wdata_out = wdata_in97;	
	   99: wdata_out = wdata_in98;
	   100: wdata_out = wdata_in99;	
	   101: wdata_out = wdata_in100;
	   102: wdata_out = wdata_in101;	
	   103: wdata_out = wdata_in102;
	   104: wdata_out = wdata_in103;	
	   105: wdata_out = wdata_in104;
	   106: wdata_out = wdata_in105;	
	   107: wdata_out = wdata_in106;
	   108: wdata_out = wdata_in107;	
	   109: wdata_out = wdata_in108;	
	   110: wdata_out = wdata_in109;	
	   111: wdata_out = wdata_in110;
	   112: wdata_out = wdata_in111;	
	   113: wdata_out = wdata_in112;
	   114: wdata_out = wdata_in113;	
	   115: wdata_out = wdata_in114;
	   116: wdata_out = wdata_in115;	
	   117: wdata_out = wdata_in116;
	   118: wdata_out = wdata_in117;	
	   119: wdata_out = wdata_in118;
	   120: wdata_out = wdata_in119;	
	   121: wdata_out = wdata_in120;
	   122: wdata_out = wdata_in121;	
	   123: wdata_out = wdata_in122;
	   124: wdata_out = wdata_in123;	
	   125: wdata_out = wdata_in124;
	   126: wdata_out = wdata_in125;	
	   127: wdata_out = wdata_in126;
	   128: wdata_out = wdata_in127;		
	   129: wdata_out = wdata_in128;
	   130: wdata_out = wdata_in129;	
	   131: wdata_out = wdata_in130;
	   132: wdata_out = wdata_in131;	
	   133: wdata_out = wdata_in132;
	   134: wdata_out = wdata_in133;	
	   135: wdata_out = wdata_in134;
	   136: wdata_out = wdata_in135;	
	   137: wdata_out = wdata_in136;
	   138: wdata_out = wdata_in137;		
	   139: wdata_out = wdata_in138;
	   140: wdata_out = wdata_in139;	
	   141: wdata_out = wdata_in140;
	   142: wdata_out = wdata_in141;	
	   143: wdata_out = wdata_in142;
	   144: wdata_out = wdata_in143;	
	   145: wdata_out = wdata_in144;
	   146: wdata_out = wdata_in145;	
	   147: wdata_out = wdata_in146;
	   148: wdata_out = wdata_in147;		
	   149: wdata_out = wdata_in148;
	   150: wdata_out = wdata_in149;	
	   151: wdata_out = wdata_in150;
	   152: wdata_out = wdata_in151;	
	   153: wdata_out = wdata_in152;
	   154: wdata_out = wdata_in153;	
	   155: wdata_out = wdata_in154;
	   156: wdata_out = wdata_in155;	
	   157: wdata_out = wdata_in156;
	   158: wdata_out = wdata_in157;		
	   159: wdata_out = wdata_in158;
	   160: wdata_out = wdata_in159;	
	   161: wdata_out = wdata_in160;
	   162: wdata_out = wdata_in161;	
	   163: wdata_out = wdata_in162;
	   164: wdata_out = wdata_in163;	
	   165: wdata_out = wdata_in164;
	   166: wdata_out = wdata_in165;	
	   167: wdata_out = wdata_in166;
	   168: wdata_out = wdata_in167;		
	   169: wdata_out = wdata_in168;
	   170: wdata_out = wdata_in169;	
	   171: wdata_out = wdata_in170;
	   172: wdata_out = wdata_in171;	
	   173: wdata_out = wdata_in172;
	   174: wdata_out = wdata_in173;	
	   175: wdata_out = wdata_in174;
	   176: wdata_out = wdata_in175;	
	   177: wdata_out = wdata_in176;
	   178: wdata_out = wdata_in177;		
	   179: wdata_out = wdata_in178;
	   180: wdata_out = wdata_in179;	
	   181: wdata_out = wdata_in180;
	   182: wdata_out = wdata_in181;	
	   183: wdata_out = wdata_in182;
	   184: wdata_out = wdata_in183;	
	   185: wdata_out = wdata_in184;
	   186: wdata_out = wdata_in185;	
	   187: wdata_out = wdata_in186;
	   188: wdata_out = wdata_in187;		
	   189: wdata_out = wdata_in188;
	   190: wdata_out = wdata_in189;	
	   191: wdata_out = wdata_in190;
	   192: wdata_out = wdata_in191;	
	   193: wdata_out = wdata_in192;
	   194: wdata_out = wdata_in193;	
	   195: wdata_out = wdata_in194;
	   196: wdata_out = wdata_in195;	
	   197: wdata_out = wdata_in196;
	   198: wdata_out = wdata_in197;		
	   199: wdata_out = wdata_in198;
	   200: wdata_out = wdata_in199;	
	   201: wdata_out = wdata_in200;
	   202: wdata_out = wdata_in201;	
	   203: wdata_out = wdata_in202;
	   204: wdata_out = wdata_in203;	
	   205: wdata_out = wdata_in204;
	   206: wdata_out = wdata_in205;	
	   207: wdata_out = wdata_in206;
	   208: wdata_out = wdata_in207;		
	   209: wdata_out = wdata_in208;
	   210: wdata_out = wdata_in209;	
	   211: wdata_out = wdata_in210;
	   212: wdata_out = wdata_in211;	
	   213: wdata_out = wdata_in212;
	   214: wdata_out = wdata_in213;	
	   215: wdata_out = wdata_in214;
	   216: wdata_out = wdata_in215;	
	   217: wdata_out = wdata_in216;
	   218: wdata_out = wdata_in217;		
	   219: wdata_out = wdata_in218;
	   220: wdata_out = wdata_in219;	
	   221: wdata_out = wdata_in220;
	   222: wdata_out = wdata_in221;	
	   223: wdata_out = wdata_in222;
	   224: wdata_out = wdata_in223;	
	   225: wdata_out = wdata_in224;
	   226: wdata_out = wdata_in225;	
	   227: wdata_out = wdata_in226;
	   228: wdata_out = wdata_in227;		
	   229: wdata_out = wdata_in228;
	   230: wdata_out = wdata_in229;	
	   231: wdata_out = wdata_in230;
	   232: wdata_out = wdata_in231;	
	   233: wdata_out = wdata_in232;
	   234: wdata_out = wdata_in233;	
	   235: wdata_out = wdata_in234;
	   236: wdata_out = wdata_in235;	
	   237: wdata_out = wdata_in236;
	   238: wdata_out = wdata_in237;		
	   239: wdata_out = wdata_in238;
	   240: wdata_out = wdata_in239;	
	   241: wdata_out = wdata_in240;
	   242: wdata_out = wdata_in241;	
	   243: wdata_out = wdata_in242;
	   244: wdata_out = wdata_in243;	
	   245: wdata_out = wdata_in244;
	   246: wdata_out = wdata_in245;	
	   247: wdata_out = wdata_in246;
	   248: wdata_out = wdata_in247;		
	   249: wdata_out = wdata_in248;
	   250: wdata_out = wdata_in249;	
	   251: wdata_out = wdata_in250;
	   252: wdata_out = wdata_in251;	
	   253: wdata_out = wdata_in252;
	   254: wdata_out = wdata_in253;	
	   255: wdata_out = wdata_in254;
	   256: wdata_out = wdata_in255;	
	   257: wdata_out = wdata_in256;
	   258: wdata_out = wdata_in257;	
	   259: wdata_out = wdata_in258;
	   260: wdata_out = wdata_in259;	
	   261: wdata_out = wdata_in260;
	   262: wdata_out = wdata_in261;	
	   263: wdata_out = wdata_in262;
	   264: wdata_out = wdata_in263;	
	   265: wdata_out = wdata_in264;
	   266: wdata_out = wdata_in265;	
	   267: wdata_out = wdata_in266;
	   268: wdata_out = wdata_in267;	
	   269: wdata_out = wdata_in268;
	   270: wdata_out = wdata_in269;	
	   271: wdata_out = wdata_in270;
	   272: wdata_out = wdata_in271;	
	   273: wdata_out = wdata_in272;
	   274: wdata_out = wdata_in273;	
	   275: wdata_out = wdata_in274;
	   276: wdata_out = wdata_in275;	
	   277: wdata_out = wdata_in276;
	   278: wdata_out = wdata_in277;	
	   279: wdata_out = wdata_in278;	
	   280: wdata_out = wdata_in279;	
	   281: wdata_out = wdata_in280;
	   282: wdata_out = wdata_in281;	
	   283: wdata_out = wdata_in282;
	   284: wdata_out = wdata_in283;	
	   285: wdata_out = wdata_in284;
	   286: wdata_out = wdata_in285;	
	   287: wdata_out = wdata_in286;
	   288: wdata_out = wdata_in287;	
	   289: wdata_out = wdata_in288;	
	   290: wdata_out = wdata_in289;	
	   291: wdata_out = wdata_in290;
	   292: wdata_out = wdata_in291;	
	   293: wdata_out = wdata_in292;
	   294: wdata_out = wdata_in293;	
	   295: wdata_out = wdata_in294;
	   296: wdata_out = wdata_in295;	
	   297: wdata_out = wdata_in296;
	   298: wdata_out = wdata_in297;	
	   299: wdata_out = wdata_in298;
	   300: wdata_out = wdata_in299;	
	   301: wdata_out = wdata_in300;
	   302: wdata_out = wdata_in301;
	   303: wdata_out = wdata_in302;
	   304: wdata_out = wdata_in303;	
	   305: wdata_out = wdata_in304;
	   306: wdata_out = wdata_in305;	
	   307: wdata_out = wdata_in306;
	   308: wdata_out = wdata_in307;	
	   309: wdata_out = wdata_in308;
	   310: wdata_out = wdata_in309;	
	   311: wdata_out = wdata_in310;
	   312: wdata_out = wdata_in311;	
	   313: wdata_out = wdata_in312;
	   314: wdata_out = wdata_in313;	
	   315: wdata_out = wdata_in314;
	   316: wdata_out = wdata_in315;	
	   317: wdata_out = wdata_in316;
	   318: wdata_out = wdata_in317;	
	   319: wdata_out = wdata_in318;
	   320: wdata_out = wdata_in319;	
	   321: wdata_out = wdata_in320;
	   322: wdata_out = wdata_in321;	
	   323: wdata_out = wdata_in322;
	   324: wdata_out = wdata_in323;	
	   325: wdata_out = wdata_in324;
	   326: wdata_out = wdata_in325;	
	   327: wdata_out = wdata_in326;
	   328: wdata_out = wdata_in327;	
	   329: wdata_out = wdata_in328;
	   330: wdata_out = wdata_in329;
	   331: wdata_out = wdata_in330;
	   332: wdata_out = wdata_in331;	
	   333: wdata_out = wdata_in332;
	   334: wdata_out = wdata_in333;	
	   335: wdata_out = wdata_in334;
	   336: wdata_out = wdata_in335;	
	   337: wdata_out = wdata_in336;
	   338: wdata_out = wdata_in337;	
	   339: wdata_out = wdata_in338;
	   340: wdata_out = wdata_in339;		
	   341: wdata_out = wdata_in340;
	   342: wdata_out = wdata_in341;	
	   343: wdata_out = wdata_in342;
	   344: wdata_out = wdata_in343;	
	   345: wdata_out = wdata_in344;
	   346: wdata_out = wdata_in345;	
	   347: wdata_out = wdata_in346;
	   348: wdata_out = wdata_in347;	
	   349: wdata_out = wdata_in348;
	   350: wdata_out = wdata_in349;	
	   350: wdata_out = wdata_in349;	
	   351: wdata_out = wdata_in350;
	   352: wdata_out = wdata_in351;	
	   353: wdata_out = wdata_in352;
	   354: wdata_out = wdata_in353;	
	   355: wdata_out = wdata_in354;
	   356: wdata_out = wdata_in355;	
	   357: wdata_out = wdata_in356;
	   358: wdata_out = wdata_in357;	
	   359: wdata_out = wdata_in358;
	   360: wdata_out = wdata_in359;	
	   361: wdata_out = wdata_in360;
	   362: wdata_out = wdata_in361;	
	   363: wdata_out = wdata_in362;
	   364: wdata_out = wdata_in363;	
	   365: wdata_out = wdata_in364;
	   366: wdata_out = wdata_in365;	
	   367: wdata_out = wdata_in366;
	   368: wdata_out = wdata_in367;	
	   369: wdata_out = wdata_in368;
	   370: wdata_out = wdata_in369;	
	   371: wdata_out = wdata_in370;
	   372: wdata_out = wdata_in371;	
	   373: wdata_out = wdata_in372;
	   374: wdata_out = wdata_in373;	
	   375: wdata_out = wdata_in374;
	   376: wdata_out = wdata_in375;	
	   377: wdata_out = wdata_in376;
	   378: wdata_out = wdata_in377;	
	   379: wdata_out = wdata_in378;	
	   380: wdata_out = wdata_in379;	
	   381: wdata_out = wdata_in380;
	   382: wdata_out = wdata_in381;	
	   383: wdata_out = wdata_in382;
	   384: wdata_out = wdata_in383;	
	   385: wdata_out = wdata_in384;
	   386: wdata_out = wdata_in385;	
	   387: wdata_out = wdata_in386;
	   388: wdata_out = wdata_in387;	
	   389: wdata_out = wdata_in388;	
	   390: wdata_out = wdata_in389;	
	   391: wdata_out = wdata_in390;
	   392: wdata_out = wdata_in391;	
	   393: wdata_out = wdata_in392;
	   394: wdata_out = wdata_in393;	
	   395: wdata_out = wdata_in394;
	   396: wdata_out = wdata_in395;	
	   397: wdata_out = wdata_in396;
	   398: wdata_out = wdata_in397;	
	   399: wdata_out = wdata_in398;
	   400: wdata_out = wdata_in399;	
	   401: wdata_out = wdata_in400;
	   402: wdata_out = wdata_in401;
	   403: wdata_out = wdata_in402;
	   404: wdata_out = wdata_in403;	
	   405: wdata_out = wdata_in404;
	   406: wdata_out = wdata_in405;	
	   407: wdata_out = wdata_in406;
	   408: wdata_out = wdata_in407;	
	   409: wdata_out = wdata_in408;
	   410: wdata_out = wdata_in409;	
	   411: wdata_out = wdata_in410;
	   412: wdata_out = wdata_in411;	
	   413: wdata_out = wdata_in412;
	   414: wdata_out = wdata_in413;	
	   415: wdata_out = wdata_in414;
	   416: wdata_out = wdata_in415;	
	   417: wdata_out = wdata_in416;
	   418: wdata_out = wdata_in417;	
	   419: wdata_out = wdata_in418;
	   420: wdata_out = wdata_in419;	
	   421: wdata_out = wdata_in420;
	   422: wdata_out = wdata_in421;	
	   423: wdata_out = wdata_in422;
	   424: wdata_out = wdata_in423;	
	   425: wdata_out = wdata_in424;
	   426: wdata_out = wdata_in425;	
	   427: wdata_out = wdata_in426;
	   428: wdata_out = wdata_in427;	
	   429: wdata_out = wdata_in428;
	   430: wdata_out = wdata_in429;
	   431: wdata_out = wdata_in430;
	   432: wdata_out = wdata_in431;	
	   433: wdata_out = wdata_in432;
	   434: wdata_out = wdata_in433;	
	   435: wdata_out = wdata_in434;
	   436: wdata_out = wdata_in435;	
	   437: wdata_out = wdata_in436;
	   438: wdata_out = wdata_in437;	
	   439: wdata_out = wdata_in438;
	   440: wdata_out = wdata_in439;		
	   441: wdata_out = wdata_in440;
	   442: wdata_out = wdata_in441;	
	   443: wdata_out = wdata_in442;
	   444: wdata_out = wdata_in443;	
	   445: wdata_out = wdata_in444;
	   446: wdata_out = wdata_in445;	
	   447: wdata_out = wdata_in446;
	   448: wdata_out = wdata_in447;	
	   449: wdata_out = wdata_in448;
	   450: wdata_out = wdata_in449;	
	   450: wdata_out = wdata_in449;	
	   451: wdata_out = wdata_in450;
	   452: wdata_out = wdata_in451;	
	   453: wdata_out = wdata_in452;
	   454: wdata_out = wdata_in453;	
	   455: wdata_out = wdata_in454;
	   456: wdata_out = wdata_in455;	
	   457: wdata_out = wdata_in456;
	   458: wdata_out = wdata_in457;	
	   459: wdata_out = wdata_in458;
	   460: wdata_out = wdata_in459;	
	   461: wdata_out = wdata_in460;
	   462: wdata_out = wdata_in461;	
	   463: wdata_out = wdata_in462;
	   464: wdata_out = wdata_in463;	
	   465: wdata_out = wdata_in464;
	   466: wdata_out = wdata_in465;	
	   467: wdata_out = wdata_in466;
	   468: wdata_out = wdata_in467;	
	   469: wdata_out = wdata_in468;
	   470: wdata_out = wdata_in469;	
	   471: wdata_out = wdata_in470;
	   472: wdata_out = wdata_in471;	
	   473: wdata_out = wdata_in472;
	   474: wdata_out = wdata_in473;	
	   475: wdata_out = wdata_in474;
	   476: wdata_out = wdata_in475;	
	   477: wdata_out = wdata_in476;
	   478: wdata_out = wdata_in477;	
	   479: wdata_out = wdata_in478;	
	   480: wdata_out = wdata_in479;	
	   481: wdata_out = wdata_in480;
	   482: wdata_out = wdata_in481;	
	   483: wdata_out = wdata_in482;
	   484: wdata_out = wdata_in483;	
	   485: wdata_out = wdata_in484;
	   486: wdata_out = wdata_in485;	
	   487: wdata_out = wdata_in486;
	   488: wdata_out = wdata_in487;	
	   489: wdata_out = wdata_in488;	
	   490: wdata_out = wdata_in489;	
	   491: wdata_out = wdata_in490;
	   492: wdata_out = wdata_in491;	
	   493: wdata_out = wdata_in492;
	   494: wdata_out = wdata_in493;	
	   495: wdata_out = wdata_in494;
	   496: wdata_out = wdata_in495;	
	   497: wdata_out = wdata_in496;
	   498: wdata_out = wdata_in497;	
	   499: wdata_out = wdata_in498;
	   500: wdata_out = wdata_in499;	
	   500: wdata_out = wdata_in499;	
	   501: wdata_out = wdata_in500;
	   502: wdata_out = wdata_in501;
	   503: wdata_out = wdata_in502;
	   504: wdata_out = wdata_in503;	
	   505: wdata_out = wdata_in504;
	   506: wdata_out = wdata_in505;	
	   507: wdata_out = wdata_in506;
	   508: wdata_out = wdata_in507;	
	   509: wdata_out = wdata_in508;
	   510: wdata_out = wdata_in509;	
	   511: wdata_out = wdata_in510;
	   512: wdata_out = wdata_in511;			
	   default: wdata_out = 0;
    endcase
  end	 
		
endmodule