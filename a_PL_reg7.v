module a_PL_reg7 (clk, sign1, exp_nor5, man_sum1, zero1_no, zero2_no, zero3_no, zero4_no, sign2, exp_nor6, man_sum2, zero1_no1, zero2_no1, zero3_no1, zero4_no1);
  
  input clk, sign1;
  input [10:0] exp_nor5;
  input [53:0] man_sum1;
  input [3:0] zero1_no, zero2_no, zero3_no, zero4_no;
  output reg sign2;
  output reg [10:0] exp_nor6;
  output reg [53:0] man_sum2;
  output reg [3:0] zero1_no1, zero2_no1, zero3_no1, zero4_no1;
  
  always@ (posedge clk)
  begin
    sign2 <= sign1;
    exp_nor6 <= exp_nor5;
    man_sum2 <= man_sum1;
    zero1_no1 <= zero1_no;
    zero2_no1 <= zero2_no;
    zero3_no1 <= zero3_no;
    zero4_no1 <= zero4_no;
  end

endmodule

