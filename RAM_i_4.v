module RAM_i_4(clk, rst, rd_en, wr_en, w_start, waddr, idata, odata);
    
  parameter [6:0] data_width = 64;
  parameter [3:0] address_width = 10;
  parameter [10:0] RAM_size = 196;
  
  input clk, rst;
  input rd_en, wr_en, w_start;
  input [5:0] waddr;
  input [data_width-1:0] idata;
  output reg [data_width-1:0] odata;
  
  reg [data_width-1:0] RAM[RAM_size-1:0];
  reg [address_width-1:0] rd_ptr, wr_ptr;

    //write pointer
  always@ (posedge clk)
  begin
    if (rst)
      wr_ptr <= 0; 
    else if (w_start)
	   wr_ptr <= waddr * op_per_neuron_hidden;		
    else if (wr_en) 
      begin
        if (wr_ptr == RAM_size-1)
          wr_ptr <= 0;
        else
          wr_ptr <= wr_ptr + 1'b1;
      end
  end
  
   //write operation
  always@ (posedge clk)
  begin
    if (wr_en)
      RAM[wr_ptr] <= idata;
  end
  
  // read pointer  
  always@ (posedge clk)
  begin
    if (rst)
      rd_ptr <= 0;       
    else if (rd_en)
      begin
        if (rd_ptr == RAM_size-1)
          rd_ptr <= 0;		  
        else    
          rd_ptr <= rd_ptr + 1'b1;
      end
  end  

  //read operation
  always@ (posedge clk)
  begin
    odata <= RAM[rd_ptr]; 
  end

endmodule