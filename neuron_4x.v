//////////////////////////////////////////////////////////////////////////////////
// Company: 	UoA
// Engineer: 	Thanh Bui 
// 
// Create Date:    16:21:32 02/27/2017 
// Design Name: 
// Module Name:    neuron_4x 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module neuron_4x(clk, rst, rst_acc, idata1, idata2, idata3, idata4, weights1, weights2, weights3, weights4, result);

  input clk, rst, rst_acc;  
  input [15:0] idata1, idata2, idata3, idata4, weights1, weights2, weights3, weights4;
  output [11:0] result;
  

  reg [15:0] mult_result_2c1, mult_result_2c2, mult_result_2c3, mult_result_2c4, acc_result_sm;
  wire [15:0] mult_result1, mult_result2, mult_result3, mult_result4, acc_result;
  wire [14:0] mult_temp1, mult_temp2, mult_temp3, mult_temp4, acc_temp;  
  
  fixed_point_mult mult1 (clk, idata1, weights1, mult_result1);
  fixed_point_mult mult2 (clk, idata2, weights2, mult_result2); 
  fixed_point_mult mult3 (clk, idata3, weights3, mult_result3);
  fixed_point_mult mult4 (clk, idata4, weights4, mult_result4);   
  
  fixed_point_acc_4 acc_4 (clk, rst, rst_acc, mult_result_2c1, mult_result_2c2, mult_result_2c3, mult_result_2c4, acc_result);
  
  sigmoid sigmoid (clk, acc_result_sm[15:8], result);
  
  assign mult_temp1 = ~mult_result1[14:0] + 1'b1;
  assign mult_temp2 = ~mult_result2[14:0] + 1'b1;  
  assign mult_temp3 = ~mult_result3[14:0] + 1'b1;
  assign mult_temp4 = ~mult_result4[14:0] + 1'b1;   
  
  assign acc_temp = ~acc_result[14:0] + 1'b1;
  
  always@ (posedge clk)
  begin
    mult_result_2c1 <= mult_result1[15] ? {1'b1,mult_temp1} : mult_result1; 
    mult_result_2c2 <= mult_result2[15] ? {1'b1,mult_temp2} : mult_result2;
    mult_result_2c3 <= mult_result3[15] ? {1'b1,mult_temp3} : mult_result3;
    mult_result_2c4 <= mult_result4[15] ? {1'b1,mult_temp4} : mult_result4;  
	acc_result_sm = (acc_result == 16'h8000) ? 16'hffff : acc_result[15] ? {1'b1,acc_temp} : acc_result;
  end
  
endmodule