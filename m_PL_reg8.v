module m_PL_reg8 (clk, sign7, exp7, a7, b7, sum4, sign8, exp8, a8, b8, sum4_1); 
  
  input   clk;
  input   sign7;
  input   [11:0] exp7;
  input   [23:0] a7;
  input   [1:0] b7;
  input   [105:0] sum4;
  output reg sign8;
  output reg [11:0] exp8;
  output reg [23:0] a8;
  output reg [1:0] b8;
  output reg [105:0] sum4_1;

  always@ (posedge clk)
    begin
      sign8 <= sign7;
      exp8 <= exp7;
      a8 <= a7;
      b8 <= b7;
      sum4_1 <= sum4;
    end

endmodule

