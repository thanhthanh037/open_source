module m_leading_zero (in, zero_no);
  
  input [12:0] in;
  output reg [4:0] zero_no;

  always@ (*)
  begin
    if (in[12])
      zero_no = 0;
    else if (in[11])
      zero_no = 1;
    else if (in[10])
      zero_no = 2;
    else if (in[9])
      zero_no = 3;
    else if (in[8])
      zero_no = 4;
    else if (in[7])
      zero_no = 5;
    else if (in[6])
      zero_no = 6;
    else if (in[5])
      zero_no = 7;
    else if (in[4])
      zero_no = 8;
    else if (in[3])
      zero_no = 9;      
    else if (in[2])
      zero_no = 10;
    else if (in[1])
      zero_no = 11;
    else if (in[0])
      zero_no = 12;
    else
      zero_no = 5'b10000;//redundant msb bit but necessary for optimizing speed
  end

endmodule