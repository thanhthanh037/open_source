`include "parameter.v"
module PE_1x4(clk, rst, x_router, y_router, isend, iaccept, iflit, osend, oaccept, oflit);

  input clk, rst; 
  input [2:0] x_router, y_router; 
  input isend, iaccept;
  input [flit_data_width-1:0] iflit;
  output osend, oaccept;
  output [flit_data_width-1:0] oflit;
  
  wire w_start, re_i, re_w, we_w, re_o, we_o, rst_acc;
  wire [5:0] waddr;
  wire [flit_data_width-3:0] idata, input_values;
  wire [flit_data_width-3:0] weights;  
  wire [11:0] result, odata; 
  
  NoC_interface_1x4 NoC_interface (clk, rst, x_router, y_router,
								   isend, iaccept, iflit, 
								   done_hidden, done_output, odata,
								   osend, oaccept, oflit, 
								   w_start, we_i, we_w, 
								   re_o, waddr, idata);
								   
  control_4 control (clk, rst, we_i, re_i, re_w, we_o, rst_acc, done_hidden, done_output);
  
  RAM_i_4 RAM_i (clk, rst, re_i, we_i, w_start, waddr, idata, input_values);
  
  RAM_w_4 RAM_w (clk, rst, re_w, we_w, idata, weights);
   
  
  RAM_o RAM_o (clk, rst, re_o, we_o, result, odata);
 
  
  neuron_4x neuron_1 (clk, rst, rst_acc, input_values[15:0], input_values[31:16], input_values[47:32], input_values[63:48], weights[15:0], weights[31:16], weights[47:32], weights[63:48], result);

  
endmodule