`include "parameter.v"
module PE_8x4(clk, rst, source_node, wgr, broadcast, we, idata, wrq, waddr, odata);

  input clk, rst, wgr, broadcast, we;
  input [5:0] source_node;
  input [data_width-1:0] idata;
  output wrq;
  output waddr;
  output [data_width-1:0] odata;

  wire w_start, re_i, we_i, re_w, we_w_1, we_w_2, we_w_3, we_w_4, we_w_5, we_w_6, we_w_7, we_w_8, re_o_1, re_o_2, we_o, rst_acc, done_hidden, done_output;
  wire [5:0] waddr_i;
  wire [data_width-1:0] wdata;
  wire [data_width-1:0] input_values;
  wire [data_width-1:0] weights_1, weights_2, weights_3, weights_4, weights_5, weights_6, weights_7, weights_8;  
  wire [11:0] result_1, result_2, result_3, result_4, result_5, result_6, result_7, result_8, odata_1, odata_2, odata_3, odata_4, odata_5, odata_6, odata_7, odata_8;
  
			 
  Bus_interface_8x4 Bus_interface (clk, rst, source_node,
								   wgr, broadcast, we, idata, 
								   done_hidden, done_output, odata_1, odata_2, odata_3, odata_4, odata_5, odata_6, odata_7, odata_8,		    
								   w_start, we_i, we_w_1, we_w_2, we_w_3, we_w_4, we_w_5, we_w_6, we_w_7, we_w_8, 
								   re_o_1, re_o_2, waddr_i, wdata,
								   wrq, waddr, odata);
											  
  control_4 control (clk, rst, we_i, re_i, re_w, we_o, rst_acc, done_hidden, done_output);
  
  RAM_i_4 RAM_i (clk, rst, re_i, we_i, w_start, waddr_i, wdata, input_values);
  
  RAM_w_4 RAM_w_1 (clk, rst, re_w, we_w_1, wdata, weights_1);
  RAM_w_4 RAM_w_2 (clk, rst, re_w, we_w_2, wdata, weights_2);
  RAM_w_4 RAM_w_3 (clk, rst, re_w, we_w_3, wdata, weights_3);
  RAM_w_4 RAM_w_4 (clk, rst, re_w, we_w_4, wdata, weights_4); 
  RAM_w_4 RAM_w_5 (clk, rst, re_w, we_w_5, wdata, weights_5);
  RAM_w_4 RAM_w_6 (clk, rst, re_w, we_w_6, wdata, weights_6);
  RAM_w_4 RAM_w_7 (clk, rst, re_w, we_w_7, wdata, weights_7);
  RAM_w_4 RAM_w_8 (clk, rst, re_w, we_w_8, wdata, weights_8);  
  
  RAM_o RAM_o_1 (clk, rst, re_o_1, we_o, result_1, odata_1);
  RAM_o RAM_o_2 (clk, rst, re_o_1, we_o, result_2, odata_2);
  RAM_o RAM_o_3 (clk, rst, re_o_1, we_o, result_3, odata_3);
  RAM_o RAM_o_4 (clk, rst, re_o_1, we_o, result_4, odata_4);  
  RAM_o RAM_o_5 (clk, rst, re_o_2, we_o, result_5, odata_5);
  RAM_o RAM_o_6 (clk, rst, re_o_2, we_o, result_6, odata_6);
  RAM_o RAM_o_7 (clk, rst, re_o_2, we_o, result_7, odata_7);
  RAM_o RAM_o_8 (clk, rst, re_o_2, we_o, result_8, odata_8); 
  
  neuron_4x neuron_1 (clk, rst, rst_acc, input_values[15:0], input_values[31:16], input_values[47:32], input_values[63:48], weights_1[15:0], weights_1[31:16], weights_1[47:32], weights_1[63:48], result_1);
  neuron_4x neuron_2 (clk, rst, rst_acc, input_values[15:0], input_values[31:16], input_values[47:32], input_values[63:48], weights_2[15:0], weights_2[31:16], weights_2[47:32], weights_2[63:48], result_2);
  neuron_4x neuron_3 (clk, rst, rst_acc, input_values[15:0], input_values[31:16], input_values[47:32], input_values[63:48], weights_3[15:0], weights_3[31:16], weights_3[47:32], weights_3[63:48], result_3);
  neuron_4x neuron_4 (clk, rst, rst_acc, input_values[15:0], input_values[31:16], input_values[47:32], input_values[63:48], weights_4[15:0], weights_4[31:16], weights_4[47:32], weights_4[63:48], result_4);
  neuron_4x neuron_5 (clk, rst, rst_acc, input_values[15:0], input_values[31:16], input_values[47:32], input_values[63:48], weights_5[15:0], weights_5[31:16], weights_5[47:32], weights_5[63:48], result_5);
  neuron_4x neuron_6 (clk, rst, rst_acc, input_values[15:0], input_values[31:16], input_values[47:32], input_values[63:48], weights_6[15:0], weights_6[31:16], weights_6[47:32], weights_6[63:48], result_6);
  neuron_4x neuron_7 (clk, rst, rst_acc, input_values[15:0], input_values[31:16], input_values[47:32], input_values[63:48], weights_7[15:0], weights_7[31:16], weights_7[47:32], weights_7[63:48], result_7);
  neuron_4x neuron_8 (clk, rst, rst_acc, input_values[15:0], input_values[31:16], input_values[47:32], input_values[63:48], weights_8[15:0], weights_8[31:16], weights_8[47:32], weights_8[63:48], result_8);
  
endmodule