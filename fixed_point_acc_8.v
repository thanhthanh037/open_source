//////////////////////////////////////////////////////////////////////////////////
// Company: 	UoA
// Engineer: 	Thanh Bui
// 
// Create Date:    12:24:03 06/15/2017 
// Design Name: 
// Module Name:    fixed_point_acc_8 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 8-input 16-bit fixed-point accumulator
//
//////////////////////////////////////////////////////////////////////////////////
module fixed_point_acc_8(clk, rst, rst_acc, in1, in2, in3, in4, in5, in6, in7, in8, result);

  input clk, rst, rst_acc;
  input [15:0] in1, in2, in3, in4, in5, in6, in7, in8;
  output [15:0] result;

  reg [15:0] r1, r2, a;
  reg [15:0] a1, result1;
  reg [15:0] temp;  
  
  always@ (posedge clk)
  begin
    if (rst||rst_acc)
      begin
        temp <= 0;
        a1 <= 0;
        result1 <= 0;
      end	   
	else
	  begin
        temp <= a + result;
        a1 <= a;
        result1 <= result;
	  end
	r1 <= in1 + in2 + in3 + in4;
    r2 <= in5 + in6 + in7 + in8;	
	a <= r1 + r2;
  end
  
 
  assign result = (a1[15] & result1[15] & !temp[15]) ? 16'h8000 : (!a1[15] & !result1[15] & temp[15]) ? 16'h7fff : temp;
  
endmodule
