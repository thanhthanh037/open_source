`include "parameters.v"
module abiter_N (clk, rst, iaccept, eop, 
                 dir1, dir2, dir3, dir4, osend, sel, 
                 rd_en1, rd_en2, rd_en3, rd_en4);
                
  input clk, rst, iaccept, eop;
  input [4:0] dir1, dir2, dir3, dir4;
  output reg osend;
  output reg [1:0] sel;
  output reg rd_en1, rd_en2, rd_en3, rd_en4;
  
  reg [4:0] state;
  
  always@ (posedge clk)
  begin
    if (rst)
      begin
        state <= 0;
        rd_en1 <= 1'b0;
        rd_en2 <= 1'b0;
        rd_en3 <= 1'b0;
        rd_en4 <= 1'b0;
      end
    else
      case (state)
        0:
          begin
          if ((dir1 == N || dir1 == NE || dir1 == NSE || dir1 == NEP || dir1 == NSEW) && (dir2 != N && dir2 != NE && dir2 != NSE && dir2 != NEP && dir2 != NSEW) && (dir3 != N && dir3 != NE && dir3 != NSE && dir3 != NEP && dir3 != NSEW) && (dir4 != N && dir4 != NE && dir4 != NSE && dir4 != NEP && dir4 != NSEW))
            begin
              osend <= 1'b1;
              state <= 1;
            end
          if ((dir1 != N && dir1 != NE && dir1 != NSE && dir1 != NEP && dir1 != NSEW) && (dir2 == N || dir2 == NE || dir2 == NSE || dir2 == NEP || dir2 == NSEW) && (dir3 != N && dir3 != NE && dir3 != NSE && dir3 != NEP && dir3 != NSEW) && (dir4 != N && dir4 != NE && dir4 != NSE && dir4 != NEP && dir4 != NSEW))
            begin
              osend <= 1'b1;
              state <= 2;
            end
          if ((dir1 != N && dir1 != NE && dir1 != NSE && dir1 != NEP && dir1 != NSEW) && (dir2 != N && dir2 != NE && dir2 != NSE && dir2 != NEP && dir2 != NSEW) && (dir3 == N || dir3 == NE || dir3 == NSE || dir3 == NEP || dir3 == NSEW) && (dir4 != N && dir4 != NE && dir4 != NSE && dir4 != NEP && dir4 != NSEW))
            begin
              osend <= 1'b1;
              state <= 3;
            end
          if ((dir1 != N && dir1 != NE && dir1 != NSE && dir1 != NEP && dir1 != NSEW) && (dir2 != N && dir2 != NE && dir2 != NSE && dir2 != NEP && dir2 != NSEW) && (dir3 != N && dir3 != NE && dir3 != NSE && dir3 != NEP && dir3 != NSEW) && (dir4 == N || dir4 == NE || dir4 == NSE || dir4 == NEP || dir4 == NSEW))
            begin
              osend <= 1'b1;
              state <= 4;
            end
          if ((dir1 == N || dir1 == NE || dir1 == NSE || dir1 == NEP || dir1 == NSEW) && (dir2 == N || dir2 == NE || dir2 == NSE || dir2 == NEP || dir2 == NSEW) && (dir3 != N && dir3 != NE && dir3 != NSE && dir3 != NEP && dir3 != NSEW) && (dir4 != N && dir4 != NE && dir4 != NSE && dir4 != NEP && dir4 != NSEW))
            begin
              osend <= 1'b1;
              state <= 5;
            end
          if ((dir1 == N || dir1 == NE || dir1 == NSE || dir1 == NEP || dir1 == NSEW) && (dir2 != N && dir2 != NE && dir2 != NSE && dir2 != NEP && dir2 != NSEW) && (dir3 == N || dir3 == NE || dir3 == NSE || dir3 == NEP || dir3 == NSEW) && (dir4 != N && dir4 != NE && dir4 != NSE && dir4 != NEP && dir4 != NSEW))
            begin
              osend <= 1'b1;
              state <= 6;
            end
          if ((dir1 == N || dir1 == NE || dir1 == NSE || dir1 == NEP || dir1 == NSEW) && (dir2 != N && dir2 != NE && dir2 != NSE && dir2 != NEP && dir2 != NSEW) && (dir3 != N && dir3 != NE && dir3 != NSE && dir3 != NEP && dir3 != NSEW) && (dir4 == N || dir4 == NE || dir4 == NSE || dir4 == NEP || dir4 == NSEW))
            begin
              osend <= 1'b1;
              state <= 7;
            end
          if ((dir1 != N && dir1 != NE && dir1 != NSE && dir1 != NEP && dir1 != NSEW) && (dir2 == N || dir2 == NE || dir2 == NSE || dir2 == NEP || dir2 == NSEW)&& (dir3 == N || dir3 == NE || dir3 == NSE || dir3 == NEP || dir3 == NSEW) && (dir4 != N && dir4 != NE && dir4 != NSE && dir4 != NEP && dir4 != NSEW))
            begin
              osend <= 1'b1;
              state <= 8;
            end
          if ((dir1 != N && dir1 != NE && dir1 != NSE && dir1 != NEP && dir1 != NSEW) && (dir2 == N || dir2 == NE || dir2 == NSE || dir2 == NEP || dir2 == NSEW)&& (dir3 != N && dir3 != NE && dir3 != NSE && dir3 != NEP && dir3 != NSEW) && (dir4 == N || dir4 == NE || dir4 == NSE || dir4 == NEP || dir4 == NSEW))
            begin
              osend <= 1'b1;
              state <= 9;
            end
          if ((dir1 != N && dir1 != NE && dir1 != NSE && dir1 != NEP && dir1 != NSEW) && (dir2 != N && dir2 != NE && dir2 != NSE && dir2 != NEP && dir2 != NSEW) && (dir3 == N || dir3 == NE || dir3 == NSE || dir3 == NEP || dir3 == NSEW) && (dir4 == N || dir4 == NE || dir4 == NSE || dir4 == NEP || dir4 == NSEW))
            begin
              osend <= 1'b1;
              state <= 10;
            end
          if ((dir1 == N || dir1 == NE || dir1 == NSE || dir1 == NEP || dir1 == NSEW) && (dir2 == N || dir2 == NE || dir2 == NSE || dir2 == NEP || dir2 == NSEW)&& (dir3 == N || dir3 == NE || dir3 == NSE || dir3 == NEP || dir3 == NSEW) && (dir4 != N && dir4 != NE && dir4 != NSE && dir4 != NEP && dir4 != NSEW))
            begin
              osend <= 1'b1;
              state <= 11;
            end
          if ((dir1 == N || dir1 == NE || dir1 == NSE || dir1 == NEP || dir1 == NSEW) && (dir2 == N || dir2 == NE || dir2 == NSE || dir2 == NEP || dir2 == NSEW)&& (dir3 != N && dir3 != NE && dir3 != NSE && dir3 != NEP && dir3 != NSEW) && (dir4 == N || dir4 == NE || dir4 == NSE || dir4 == NEP || dir4 == NSEW))
            begin
              osend <= 1'b1;
              state <= 12;
            end
          if ((dir1 == N || dir1 == NE || dir1 == NSE || dir1 == NEP || dir1 == NSEW) && (dir2 != N && dir2 != NE && dir2 != NSE && dir2 != NEP && dir2 != NSEW) && (dir3 == N || dir3 == NE || dir3 == NSE || dir3 == NEP || dir3 == NSEW) && (dir4 == N || dir4 == NE || dir4 == NSE || dir4 == NEP || dir4 == NSEW))
            begin
              osend <= 1'b1;
              state <= 13;
            end
          if ((dir1 != N && dir1 != NE && dir1 != NSE && dir1 != NEP && dir1 != NSEW) && (dir2 == N || dir2 == NE || dir2 == NSE || dir2 == NEP || dir2 == NSEW)&& (dir3 == N || dir3 == NE || dir3 == NSE || dir3 == NEP || dir3 == NSEW) && (dir4 == N || dir4 == NE || dir4 == NSE || dir4 == NEP || dir4 == NSEW))
            begin
              osend <= 1'b1;
              state <= 14;
            end
          if ((dir1 == N || dir1 == NE || dir1 == NSE || dir1 == NEP || dir1 == NSEW) && (dir2 == N || dir2 == NE || dir2 == NSE || dir2 == NEP || dir2 == NSEW)&& (dir3 == N || dir3 == NE || dir3 == NSE || dir3 == NEP || dir3 == NSEW) && (dir4 == N || dir4 == NE || dir4 == NSE || dir4 == NEP || dir4 == NSEW))
            begin
              osend <= 1'b1;
              state <= 15;
            end
          end
        1:
          if (iaccept)
            begin                                  
              sel <= 2'b00;
              rd_en1 <= 1'b1;
              osend <= 0;
              state <= 16;
            end
        2:
          if (iaccept)
            begin    
              sel <= 2'b01;
              rd_en2 <= 1'b1;
              osend <= 0;
              state <= 16;
            end
        3:
          if (iaccept)
            begin    
              sel <= 2'b10;
              rd_en3 <= 1'b1;
              osend <= 0;
              state <= 16;
            end
        4:
          if (iaccept)
            begin    
              sel <= 2'b11;
              rd_en4 <= 1'b1;
              osend <= 0;
              state <= 16;
            end
        5:
          if (iaccept)
            begin                                  
              sel <= 2'b00;
              rd_en1 <= 1'b1;
              osend <= 0;
              state <= 17;
            end
        6:
          if (iaccept)
            begin    
              sel <= 2'b00;
              rd_en1 <= 1'b1;
              osend <= 0;
              state <= 18;
            end
        7:
          if (iaccept)
            begin    
              sel <= 2'b00;
              rd_en1 <= 1'b1;
              osend <= 0;
              state <= 19;
            end
        8:
          if (iaccept)
            begin    
              sel <= 2'b01;
              rd_en2 <= 1'b1;
              osend <= 0;
              state <= 20;
            end
        9:
          if (iaccept)
            begin                                  
              sel <= 2'b01;
              rd_en2 <= 1'b1;
              osend <= 0;
              state <= 21;
            end
        10:
          if (iaccept)
            begin    
              sel <= 2'b10;
              rd_en3 <= 1'b1;
              osend <= 0;
              state <= 22;
            end
        11:
          if (iaccept)
            begin    
              sel <= 2'b00;
              rd_en1 <= 1'b1;
              osend <= 0;
              state <= 23;
            end
        12:
          if (iaccept)
            begin    
              sel <= 2'b00;
              rd_en1 <= 1'b1;
              osend <= 0;
              state <= 24;
            end
        13:
          if (iaccept)
            begin                                  
              sel <= 2'b00;
              rd_en1 <= 1'b1;
              osend <= 0;
              state <= 25;
            end
        14:
          if (iaccept)
            begin    
              sel <= 2'b01;
              rd_en2 <= 1'b1;
              osend <= 0;
              state <= 26;
            end
        15:
          if (iaccept)
            begin    
              sel <= 2'b00;
              rd_en1 <= 1'b1;
              osend <= 0;
              state <= 27;
            end
        16:
          if (eop)
            begin              
              rd_en1 <= 1'b0;
              rd_en2 <= 1'b0;
              rd_en3 <= 1'b0;
              rd_en4 <= 1'b0;
              state <= 0;        
            end
        17:
          if (eop)
            begin              
              rd_en1 <= 1'b0;
              osend <= 1;
              state <= 2;        
            end
        18:
          if (eop)
            begin              
              rd_en1 <= 1'b0;
              osend <= 1;
              state <= 3;        
            end
        19:
          if (eop)
            begin              
              rd_en1 <= 1'b0;
              osend <= 1;
              state <= 4;        
            end
        20:
          if (eop)
            begin              
              rd_en2 <= 1'b0;
              osend <= 1;
              state <= 3;        
            end
        21:
          if (eop)
            begin              
              rd_en2 <= 1'b0;
              osend <= 1;
              state <= 4;        
            end
        22:
          if (eop)
            begin              
              rd_en3 <= 1'b0;
              osend <= 1;
              state <= 4;        
            end
        23:
          if (eop)
            begin              
              rd_en1 <= 1'b0;
              osend <= 1;
              state <= 8;        
            end
        24:
          if (eop)
            begin              
              rd_en1 <= 1'b0;
              osend <= 1;
              state <= 9;        
            end
        25:
          if (eop)
            begin              
              rd_en1 <= 1'b0;
              osend <= 1;
              state <= 10;        
            end
        26:
          if (eop)
            begin              
              rd_en2 <= 1'b0;
              osend <= 1;
              state <= 10;        
            end
        27:
          if (eop)
            begin              
              rd_en1 <= 1'b0;
              osend <= 1;
              state <= 14;        
            end
        default: state <= 0;            
      endcase
  end
      
endmodule