module a_PL_reg9 (clk, sign3, exp_nor8, man_sum_nor, zero_no, sign4, exp_nor9, man_sum_nor1, zero_no_part);

  input clk, sign3;
  input [11:0] exp_nor8;
  input [52:0] man_sum_nor;
  input [1:0] zero_no;//zero_no here is just the first 2 bits of the real zero_no
  output reg sign4;
  output reg [11:0] exp_nor9;
  output reg [52:0] man_sum_nor1;
  output reg [1:0] zero_no_part;
  
  always@ (posedge clk)
  begin
    sign4 <= sign3;
    exp_nor9 <= exp_nor8;
    man_sum_nor1 <= man_sum_nor;
    zero_no_part <= zero_no;
  end
  
endmodule