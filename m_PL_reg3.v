module m_PL_reg3 (clk, sign2, exp2, a2, b2, a2b0, sum1_1, sign3, exp3, a3, b3, a2b0_1, sum1_2); 
  
  input   clk;
  input   sign2;
  input   [11:0] exp2;
  input   [52:0] a2;
  input   [35:0] b2;
  input   [21:0] a2b0;
  input   [104:0] sum1_1;
  output reg sign3;
  output reg [11:0] exp3;
  output reg [52:0] a3;
  output reg [35:0] b3;
  output reg [21:0] a2b0_1;
  output reg [104:0] sum1_2;
    
  always@ (posedge clk)
    begin
      sign3 <= sign2;
      exp3 <= exp2;
      a3 <= a2;
      b3 <= b2;
      a2b0_1 <= a2b0;
      sum1_2 <= sum1_1;
    end

endmodule
