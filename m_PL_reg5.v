module m_PL_reg5 (clk, sign4, exp4, a4, b4, a2b3, a1b2, a0b1, sum2_1, sign5, exp5, a5, b5, a2b3_1, a1b2_1, a0b1_1, sum2_2);
  
  input   clk;
  input   sign4;
  input   [11:0] exp4;
  input   [47:0] a4;
  input   [18:0] b4;
  input   [6:0] a2b3;
  input   [40:0] a1b2, a0b1;
  input   [105:0] sum2_1;
  output reg sign5;
  output reg [11:0] exp5;
  output reg [47:0] a5;
  output reg [18:0] b5;
  output reg [6:0] a2b3_1;
  output reg [40:0] a1b2_1, a0b1_1;
  output reg [105:0] sum2_2;
    
  always@ (posedge clk)
    begin
      sign5 <= sign4;
      exp5 <= exp4;
      a5 <= a4;
      b5 <= b4;
      a2b3_1 <= a2b3;
      a1b2_1 <= a1b2;
      a0b1_1 <= a0b1;
      sum2_2 <= sum2_1;      
    end

endmodule




