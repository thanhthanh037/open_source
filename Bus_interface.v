`include "parameter.v"
module Bus_interface (clk, rst, ireq, irep, idata, send_pe, send_pc,
                      dest, a_or_b, sum_real, sum_img, dif_real, dif_img,
                      ar_rdata, ai_rdata, br_rdata, bi_rdata,
                      oreq, oaddr, odata, op_per_pe, no_of_stage, no_of_pe,
                      start, we_a, we_b, we_w, re_a, re_b,
                      ar_wdata, ai_wdata, br_wdata, bi_wdata, wr_wdata, wi_wdata); 
							 
  input clk, rst, ireq, irep; 
  input [bus_data_width-1:0] idata; 
  input send_pe, send_pc, a_or_b;
  input [63:0] sum_real, sum_img, dif_real, dif_img, ar_rdata, ai_rdata, br_rdata, bi_rdata;
  input [1:0] dest; 
  output reg oreq;
  output reg [3:0] oaddr;  
  output reg [bus_data_width-1:0] odata;
  output reg [11:0] op_per_pe;
  output reg [4:0] no_of_stage;
  output reg [2:0] no_of_pe;
  output reg start, we_a, we_b, we_w, re_a, re_b;  
  output reg [63:0] ar_wdata, ai_wdata, br_wdata, bi_wdata, wr_wdata, wi_wdata;
  
  reg [2:0] r_state;
  reg [2:0] t_state;
  reg [11:0] count_r, count_t;

  //receiver
  always@ (posedge clk)
  begin
    if (rst)
      r_state <= 0;
    else
      case (r_state)
        0:
          begin
            start <= 0; 
            count_r <= 0;
            we_a = 0;
            we_b = 0;
            we_w = 0;
            if (ireq)
              begin
                r_state <= 1;
              end
          end       
        1:
          begin
            case (idata[bus_data_width-1:bus_data_width-2])
              2'b11:
                begin
                  op_per_pe <= idata[bus_data_width-3:bus_data_width-14];
                  no_of_stage <= idata[bus_data_width-15:bus_data_width-19];
                  no_of_pe <= idata[bus_data_width-20:bus_data_width-22];
                  r_state <= 0;
                end
              2'b00:
                begin
                  we_a = 1;
                  ar_wdata = idata[bus_data_width-3:64];
                  ai_wdata = idata[63:0];        
                  r_state <= 2; 
                end	 
              2'b01: 
                begin
                  we_b = 1;
                  br_wdata = idata[bus_data_width-3:64];
                  bi_wdata = idata[63:0];  
                  count_r <= count_r + 1'b1;         
                  if (count_r == op_per_pe)
                    begin
                      start <= 1;
                      r_state <= 0;
                    end
                  else
                    r_state <= 3;
                  end				  
              2'b10: 
                begin
                  we_w = 1;
                  wr_wdata = idata[bus_data_width-3:64];
                  wi_wdata = idata[63:0];              
                  r_state <= 4; 
                end				  
             endcase
          end
        2:
          begin
            ar_wdata = idata[bus_data_width-3:64];
            ai_wdata = idata[63:0];
            if (idata[bus_data_width-1])
              r_state <= 0;
          end 
        3:
          begin
            br_wdata = idata[bus_data_width-3:64];
            bi_wdata = idata[63:0];
            count_r <= count_r + 1'b1;
            if (idata[bus_data_width-1])
              begin
                if (count_r == op_per_pe)
                  start <= 1;
                r_state <= 0;
              end
          end 
        4:
          begin
            wr_wdata = idata[bus_data_width-3:64];
            wi_wdata = idata[63:0];
            if (idata[bus_data_width-1])
              r_state <= 0;
          end 
        default: r_state <= 0;
      endcase
  end
  
  //transmiter
  always@ (posedge clk)
  begin
    if (rst)
      t_state <= 0;
    else
      case (t_state)
        0:
          begin
            re_a <= 0;
            re_b <= 0;
            if (send_pe)
              begin
                oreq <= 1;
                case (dest)
                  0: oaddr <= dest1;
                  1: oaddr <= dest2;
                  2: oaddr <= dest3;
                  3: oaddr <= dest4;
                  default: oaddr = 0;
                endcase
                t_state <= 1;
              end
            else if (send_pc)
              begin
                oreq <= 1;
                oaddr <= 0;
                t_state <= 2;
              end
          end
        1:
          begin
            oreq <= 0;
            if (irep && !send_pe)
              begin
                count_t <= 0;
                t_state <= 3;
              end
          end
        2:
          begin
            oreq <= 0;
            if (irep && !send_pc)
              begin
                count_t <= 0;
                re_a <= 1;
                t_state <= 4;
              end
          end
        3:
          begin
            count_t <= count_t + 1'b1;
            if (count_t == op_per_pe)
              begin
                odata <= {1'b0, a_or_b, dif_real, dif_img};
                t_state <= 0;
              end
            else
              odata <= {1'b0, a_or_b, dif_real, dif_img};
          end
        4:
          begin
            count_t <= count_t + 1'b1;
            odata <= {2'b00, ar_rdata, ai_rdata};
            if (count_t == op_per_pe)
              begin
                count_t <= 0;
                re_a <= 0;
                re_b <= 1;
                t_state <= 5;
              end             
          end  
        5:
          begin
            count_t <= count_t + 1'b1;
            if (count_t == 0)
              odata <= {2'b00, ar_rdata, ai_rdata};
            else
              odata <= {2'b00, br_rdata, bi_rdata};            
            if (count_t == op_per_pe)
              begin                
                count_t <= 0;
                re_b <= 0;
                t_state <= 6;
              end
          end 
        6:
          begin
            odata <= {2'b00, br_rdata, bi_rdata};   
            t_state <= 0;
          end    
        default: t_state <= 0;
      endcase
  end  
         
endmodule