module a_normalise (zero_no, man_sum, man_sum_nor);
  
  input [1:0] zero_no;// just the first 2 bits of the real zero_no
  input [52:0] man_sum;
  output reg [52:0] man_sum_nor;
  
  always@ (*)
  begin
    case(zero_no)
      2'b00: man_sum_nor <= man_sum;
      2'b01: man_sum_nor <= man_sum << 16;
      2'b10: man_sum_nor <= man_sum << 32;
      2'b11: man_sum_nor <= man_sum << 48;
      default: man_sum_nor <= man_sum;
    endcase
  end

endmodule