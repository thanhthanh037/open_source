module m_PL_reg16 (clk, sign15, exp_nor3, rounding_result, sign16, exp_nor4, rounding_result1);
  
  input clk;
  input sign15;  
  input [10:0] exp_nor3;
  input [51:0] rounding_result;
  output reg sign16;  
  output reg [10:0] exp_nor4;
  output reg [51:0] rounding_result1;
  
  always@ (posedge clk)
    begin
      sign16 <= sign15;
      exp_nor4 <= exp_nor3;
      rounding_result1 <= rounding_result;
   end

endmodule 