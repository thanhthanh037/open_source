`include "parameters.v"
module Bus_256 (clk, rst, 
                wrq,				  
                waddr0, waddr,
                wdata_in0, wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, 
                wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, wdata_in17, wdata_in18, wdata_in19,
				wdata_in20, wdata_in21, wdata_in22, wdata_in23, wdata_in24, wdata_in25, wdata_in26, wdata_in27, wdata_in28, wdata_in29, 
				wdata_in30, wdata_in31, wdata_in32, wdata_in33, wdata_in34, wdata_in35, wdata_in36, wdata_in37, wdata_in38, wdata_in39, 
				wdata_in40, wdata_in41, wdata_in42, wdata_in43, wdata_in44, wdata_in45, wdata_in46, wdata_in47, wdata_in48, wdata_in49,
				wdata_in50, wdata_in51, wdata_in52, wdata_in53, wdata_in54, wdata_in55, wdata_in56, wdata_in57, wdata_in58, wdata_in59, 
                wdata_in60, wdata_in61, wdata_in62, wdata_in63, wdata_in64, wdata_in65, wdata_in66, wdata_in67, wdata_in68, wdata_in69,
				wdata_in70, wdata_in71, wdata_in72, wdata_in73, wdata_in74, wdata_in75, wdata_in76, wdata_in77, wdata_in78, wdata_in79, 
				wdata_in80, wdata_in81, wdata_in82, wdata_in83, wdata_in84, wdata_in85, wdata_in86, wdata_in87, wdata_in88, wdata_in89, 
				wdata_in90, wdata_in91, wdata_in92, wdata_in93, wdata_in94, wdata_in95, wdata_in96, wdata_in97, wdata_in98, wdata_in99,
				wdata_in100, wdata_in101, wdata_in102, wdata_in103, wdata_in104, wdata_in105, wdata_in106, wdata_in107, wdata_in108, wdata_in109, 
                wdata_in110, wdata_in111, wdata_in112, wdata_in113, wdata_in114, wdata_in115, wdata_in116, wdata_in117, wdata_in118, wdata_in119,
				wdata_in120, wdata_in121, wdata_in122, wdata_in123, wdata_in124, wdata_in125, wdata_in126, wdata_in127, wdata_in128, wdata_in129,				  
				wdata_in130, wdata_in131, wdata_in132, wdata_in133, wdata_in134, wdata_in135, wdata_in136, wdata_in137, wdata_in138, wdata_in139, 
				wdata_in140, wdata_in141, wdata_in142, wdata_in143, wdata_in144, wdata_in145, wdata_in146, wdata_in147, wdata_in148, wdata_in149,
				wdata_in150, wdata_in151, wdata_in152, wdata_in153, wdata_in154, wdata_in155, wdata_in156, wdata_in157, wdata_in158, wdata_in159, 
                wdata_in160, wdata_in161, wdata_in162, wdata_in163, wdata_in164, wdata_in165, wdata_in166, wdata_in167, wdata_in168, wdata_in169,
				wdata_in170, wdata_in171, wdata_in172, wdata_in173, wdata_in174, wdata_in175, wdata_in176, wdata_in177, wdata_in178, wdata_in179, 
				wdata_in180, wdata_in181, wdata_in182, wdata_in183, wdata_in184, wdata_in185, wdata_in186, wdata_in187, wdata_in188, wdata_in189, 
				wdata_in190, wdata_in191, wdata_in192, wdata_in193, wdata_in194, wdata_in195, wdata_in196, wdata_in197, wdata_in198, wdata_in199,
				wdata_in200, wdata_in201, wdata_in202, wdata_in203, wdata_in204, wdata_in205, wdata_in206, wdata_in207, wdata_in208, wdata_in209, 
                wdata_in210, wdata_in211, wdata_in212, wdata_in213, wdata_in214, wdata_in215, wdata_in216, wdata_in217, wdata_in218, wdata_in219,
		        wdata_in220, wdata_in221, wdata_in222, wdata_in223, wdata_in224, wdata_in225, wdata_in226, wdata_in227, wdata_in228, wdata_in229,				  
				wdata_in230, wdata_in231, wdata_in232, wdata_in233, wdata_in234, wdata_in235, wdata_in236, wdata_in237, wdata_in238, wdata_in239, 
				wdata_in240, wdata_in241, wdata_in242, wdata_in243, wdata_in244, wdata_in245, wdata_in246, wdata_in247, wdata_in248, wdata_in249,
				wdata_in250, wdata_in251, wdata_in252, wdata_in253, wdata_in254, wdata_in255,  				               
				wgr,				  
				we,
				broadcast,
				source_node,
                wdata_out);
  
  input clk, rst;
  input [255:0] wrq;	
  input [7:0] waddr0;	
  input [254:0] waddr;
  input [data_width-1:0] wdata_in0, wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9; 
  input [data_width-1:0] wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, wdata_in17, wdata_in18, wdata_in19;
  input [data_width-1:0] wdata_in20, wdata_in21, wdata_in22, wdata_in23, wdata_in24, wdata_in25, wdata_in26, wdata_in27, wdata_in28, wdata_in29; 
  input [data_width-1:0] wdata_in30, wdata_in31, wdata_in32, wdata_in33, wdata_in34, wdata_in35, wdata_in36, wdata_in37, wdata_in38, wdata_in39; 
  input [data_width-1:0] wdata_in40, wdata_in41, wdata_in42, wdata_in43, wdata_in44, wdata_in45, wdata_in46, wdata_in47, wdata_in48, wdata_in49;
  input [data_width-1:0] wdata_in50, wdata_in51, wdata_in52, wdata_in53, wdata_in54, wdata_in55, wdata_in56, wdata_in57, wdata_in58, wdata_in59; 
  input [data_width-1:0] wdata_in60, wdata_in61, wdata_in62, wdata_in63, wdata_in64, wdata_in65, wdata_in66, wdata_in67, wdata_in68, wdata_in69;
  input [data_width-1:0] wdata_in70, wdata_in71, wdata_in72, wdata_in73, wdata_in74, wdata_in75, wdata_in76, wdata_in77, wdata_in78, wdata_in79; 
  input [data_width-1:0] wdata_in80, wdata_in81, wdata_in82, wdata_in83, wdata_in84, wdata_in85, wdata_in86, wdata_in87, wdata_in88, wdata_in89; 
  input [data_width-1:0] wdata_in90, wdata_in91, wdata_in92, wdata_in93, wdata_in94, wdata_in95, wdata_in96, wdata_in97, wdata_in98, wdata_in99;
  input [data_width-1:0] wdata_in100, wdata_in101, wdata_in102, wdata_in103, wdata_in104, wdata_in105, wdata_in106, wdata_in107, wdata_in108, wdata_in109; 
  input [data_width-1:0] wdata_in110, wdata_in111, wdata_in112, wdata_in113, wdata_in114, wdata_in115, wdata_in116, wdata_in117, wdata_in118, wdata_in119;
  input [data_width-1:0] wdata_in120, wdata_in121, wdata_in122, wdata_in123, wdata_in124, wdata_in125, wdata_in126, wdata_in127, wdata_in128, wdata_in129;	
  input [data_width-1:0] wdata_in130, wdata_in131, wdata_in132, wdata_in133, wdata_in134, wdata_in135, wdata_in136, wdata_in137, wdata_in138, wdata_in139; 
  input [data_width-1:0] wdata_in140, wdata_in141, wdata_in142, wdata_in143, wdata_in144, wdata_in145, wdata_in146, wdata_in147, wdata_in148, wdata_in149;
  input [data_width-1:0] wdata_in150, wdata_in151, wdata_in152, wdata_in153, wdata_in154, wdata_in155, wdata_in156, wdata_in157, wdata_in158, wdata_in159; 
  input [data_width-1:0] wdata_in160, wdata_in161, wdata_in162, wdata_in163, wdata_in164, wdata_in165, wdata_in166, wdata_in167, wdata_in168, wdata_in169;
  input [data_width-1:0] wdata_in170, wdata_in171, wdata_in172, wdata_in173, wdata_in174, wdata_in175, wdata_in176, wdata_in177, wdata_in178, wdata_in179; 
  input [data_width-1:0] wdata_in180, wdata_in181, wdata_in182, wdata_in183, wdata_in184, wdata_in185, wdata_in186, wdata_in187, wdata_in188, wdata_in189; 
  input [data_width-1:0] wdata_in190, wdata_in191, wdata_in192, wdata_in193, wdata_in194, wdata_in195, wdata_in196, wdata_in197, wdata_in198, wdata_in199;
  input [data_width-1:0] wdata_in200, wdata_in201, wdata_in202, wdata_in203, wdata_in204, wdata_in205, wdata_in206, wdata_in207, wdata_in208, wdata_in209; 
  input [data_width-1:0] wdata_in210, wdata_in211, wdata_in212, wdata_in213, wdata_in214, wdata_in215, wdata_in216, wdata_in217, wdata_in218, wdata_in219;
  input [data_width-1:0] wdata_in220, wdata_in221, wdata_in222, wdata_in223, wdata_in224, wdata_in225, wdata_in226, wdata_in227, wdata_in228, wdata_in229;				  
  input [data_width-1:0] wdata_in230, wdata_in231, wdata_in232, wdata_in233, wdata_in234, wdata_in235, wdata_in236, wdata_in237, wdata_in238, wdata_in239; 
  input [data_width-1:0] wdata_in240, wdata_in241, wdata_in242, wdata_in243, wdata_in244, wdata_in245, wdata_in246, wdata_in247, wdata_in248, wdata_in249;
  input [data_width-1:0] wdata_in250, wdata_in251, wdata_in252, wdata_in253, wdata_in254, wdata_in255;				  
  output reg [255:0] wgr;  
  output reg [255:0] we;	 
  output reg broadcast;
  output reg [7:0] source_node;
  output reg [data_width-1:0] wdata_out;
  
  reg [8:0] state;
  reg count;
  	  
  always@ (posedge clk)
  if (rst) 
    state <= 0;
  else
    case (state)
	   0: 
		  begin
		    wgr <= 0; 
		    we <= 0;		    			 
			count <= 0;
            broadcast <= 0;			 
			  if (wrq[0])
			    begin
				   wgr[0] <= 1;
				   state <= 1;
				 end
			  else if (wrq[1])
			    begin
				   wgr[1] <= 1;
				   state <= 2;
				 end				 
			  else if (wrq[2])
			    begin
				   wgr[2] <= 1;
				   state <= 3;
				 end
			  else if (wrq[3])
			    begin
				   wgr[3] <= 1;
				   state <= 4;
				 end
			  else if (wrq[4])
			    begin
				   wgr[4] <= 1;
				   state <= 5;
				 end
			  else if (wrq[5])
			    begin
				   wgr[5] <= 1;
				   state <= 6;
				 end
			  else if (wrq[6])
			    begin
				   wgr[6] <= 1;
				   state <= 7;
				 end
			  else if (wrq[7])
			    begin
				   wgr[7] <= 1;
				   state <= 8;
				 end
			  else if (wrq[8])
			    begin
				   wgr[8] <= 1;
				   state <= 9;
				 end
			  else if (wrq[9])
			    begin
				   wgr[9] <= 1;
				   state <= 10;
				 end
			  else if (wrq[10])
			    begin
				   wgr[10] <= 1;
				   state <= 11;
				 end
			  else if (wrq[11])
			    begin
				   wgr[11] <= 1;
				   state <= 12;
				 end
			  else if (wrq[12])
			    begin
				   wgr[12] <= 1;
				   state <= 13;
				 end
			  else if (wrq[13])
			    begin
				   wgr[13] <= 1;
				   state <= 14;
				 end
			  else if (wrq[14])
			    begin
				   wgr[14] <= 1;
				   state <= 15;
				 end
			  else if (wrq[15])
			    begin
				   wgr[15] <= 1;
				   state <= 16;
				 end
			  else if (wrq[16])
			    begin
				   wgr[16] <= 1;
				   state <= 17;
				 end
			  else if (wrq[17])
			    begin
				   wgr[17] <= 1;
				   state <= 18;
				 end
			  else if (wrq[18])
			    begin
				   wgr[18] <= 1;
				   state <= 19;
				 end
			  else if (wrq[19])
			    begin
				   wgr[19] <= 1;
				   state <= 20;
				 end
			  else if (wrq[20])
			    begin
				   wgr[20] <= 1;
				   state <= 21;
				 end
			  else if (wrq[21])
			    begin
				   wgr[21] <= 1;
				   state <= 22;
				 end				 
			  else if (wrq[22])
			    begin
				   wgr[22] <= 1;
				   state <= 23;
				 end
			  else if (wrq[23])
			    begin
				   wgr[23] <= 1;
				   state <= 24;
				 end
			  else if (wrq[24])
			    begin
				   wgr[24] <= 1;
				   state <= 25;
				 end
			  else if (wrq[25])
			    begin
				   wgr[25] <= 1;
				   state <= 26;
				 end
			  else if (wrq[26])
			    begin
				   wgr[26] <= 1;
				   state <=27;
				 end
			  else if (wrq[27])
			    begin
				   wgr[27] <= 1;
				   state <= 28;
				 end
			  else if (wrq[28])
			    begin
				   wgr[28] <= 1;
				   state <= 29;
				 end
			  else if (wrq[29])
			    begin
				   wgr[29] <= 1;
				   state <= 30;
				 end	
			  else if (wrq[30])
			    begin
				   wgr[30] <= 1;
				   state <= 31;
				 end
			  else if (wrq[31])
			    begin
				   wgr[31] <= 1;
				   state <= 32;
				 end				 
			  else if (wrq[32])
			    begin
				   wgr[32] <= 1;
				   state <= 33;
				 end
			  else if (wrq[33])
			    begin
				   wgr[33] <= 1;
				   state <= 34;
				 end
			  else if (wrq[34])
			    begin
				   wgr[34] <= 1;
				   state <= 35;
				 end
			  else if (wrq[35])
			    begin
				   wgr[35] <= 1;
				   state <= 36;
				 end
			  else if (wrq[36])
			    begin
				   wgr[36] <= 1;
				   state <= 37;
				 end
			  else if (wrq[37])
			    begin
				   wgr[37] <= 1;
				   state <= 38;
				 end
			  else if (wrq[38])
			    begin
				   wgr[38] <= 1;
				   state <= 39;
				 end
			  else if (wrq[39])
			    begin
				   wgr[39] <= 1;
				   state <= 40;
				 end	
			  else if (wrq[40])
			    begin
				   wgr[40] <= 1;
				   state <= 41;
				 end
			  else if (wrq[41])
			    begin
				   wgr[41] <= 1;
				   state <= 42;
				 end				 
			  else if (wrq[42])
			    begin
				   wgr[42] <= 1;
				   state <= 43;
				 end
			  else if (wrq[43])
			    begin
				   wgr[43] <= 1;
				   state <= 44;
				 end
			  else if (wrq[44])
			    begin
				   wgr[44] <= 1;
				   state <= 45;
				 end
			  else if (wrq[45])
			    begin
				   wgr[45] <= 1;
				   state <= 46;
				 end
			  else if (wrq[46])
			    begin
				   wgr[46] <= 1;
				   state <= 47;
				 end
			  else if (wrq[47])
			    begin
				   wgr[47] <= 1;
				   state <= 48;
				 end
			  else if (wrq[48])
			    begin
				   wgr[48] <= 1;
				   state <= 49;
				 end
			  else if (wrq[49])
			    begin
				   wgr[49] <= 1;
				   state <= 50;
				 end	
			  else if (wrq[50])
			    begin
				   wgr[50] <= 1;
				   state <= 51;
				 end
			  else if (wrq[51])
			    begin
				   wgr[51] <= 1;
				   state <= 52;
				 end				 
			  else if (wrq[52])
			    begin
				   wgr[52] <= 1;
				   state <= 53;
				 end
			  else if (wrq[53])
			    begin
				   wgr[53] <= 1;
				   state <= 54;
				 end
			  else if (wrq[54])
			    begin
				   wgr[54] <= 1;
				   state <= 55;
				 end
			  else if (wrq[55])
			    begin
				   wgr[55] <= 1;
				   state <= 56;
				 end
			  else if (wrq[56])
			    begin
				   wgr[56] <= 1;
				   state <= 57;
				 end
			  else if (wrq[57])
			    begin
				   wgr[57] <= 1;
				   state <= 58;
				 end
			  else if (wrq[58])
			    begin
				   wgr[58] <= 1;
				   state <= 59;
				 end
			  else if (wrq[59])
			    begin
				   wgr[59] <= 1;
				   state <= 60;
				 end	
			  else if (wrq[60])
			    begin
				   wgr[60] <= 1;
				   state <= 61;
				 end
			  else if (wrq[61])
			    begin
				   wgr[61] <= 1;
				   state <= 62;
				 end				 
			  else if (wrq[62])
			    begin
				   wgr[62] <= 1;
				   state <= 63;
				 end
			  else if (wrq[63])
			    begin
				   wgr[63] <= 1;
				   state <= 64;
				 end
			  else if (wrq[64])
			    begin
				   wgr[64] <= 1;
				   state <= 65;
				 end
			  else if (wrq[65])
			    begin
				   wgr[65] <= 1;
				   state <= 66;
				 end
			  else if (wrq[66])
			    begin
				   wgr[66] <= 1;
				   state <= 67;
				 end
			  else if (wrq[67])
			    begin
				   wgr[67] <= 1;
				   state <= 68;
				 end
			  else if (wrq[68])
			    begin
				   wgr[68] <= 1;
				   state <= 69;
				 end
			  else if (wrq[69])
			    begin
				   wgr[69] <= 1;
				   state <= 70;
				 end
			  else if (wrq[70])
			    begin
				   wgr[70] <= 1;
				   state <= 71;
				 end
			  else if (wrq[71])
			    begin
				   wgr[71] <= 1;
				   state <= 72;
				 end				 
			  else if (wrq[72])
			    begin
				   wgr[72] <= 1;
				   state <= 73;
				 end
			  else if (wrq[73])
			    begin
				   wgr[73] <= 1;
				   state <= 74;
				 end
			  else if (wrq[74])
			    begin
				   wgr[74] <= 1;
				   state <= 75;
				 end
			  else if (wrq[75])
			    begin
				   wgr[75] <= 1;
				   state <= 76;
				 end
			  else if (wrq[76])
			    begin
				   wgr[76] <= 1;
				   state <= 77;
				 end
			  else if (wrq[77])
			    begin
				   wgr[77] <= 1;
				   state <= 78;
				 end
			  else if (wrq[78])
			    begin
				   wgr[78] <= 1;
				   state <= 79;
				 end
			  else if (wrq[79])
			    begin
				   wgr[79] <= 1;
				   state <= 80;
				 end
			  else if (wrq[80])
			    begin
				   wgr[80] <= 1;
				   state <= 81;
				 end
			  else if (wrq[81])
			    begin
				   wgr[81] <= 1;
				   state <= 82;
				 end				 
			  else if (wrq[82])
			    begin
				   wgr[82] <= 1;
				   state <= 83;
				 end
			  else if (wrq[83])
			    begin
				   wgr[83] <= 1;
				   state <= 84;
				 end
			  else if (wrq[84])
			    begin
				   wgr[84] <= 1;
				   state <= 85;
				 end
			  else if (wrq[85])
			    begin
				   wgr[85] <= 1;
				   state <= 86;
				 end
			  else if (wrq[86])
			    begin
				   wgr[86] <= 1;
				   state <= 87;
				 end
			  else if (wrq[87])
			    begin
				   wgr[87] <= 1;
				   state <= 88;
				 end
			  else if (wrq[88])
			    begin
				   wgr[88] <= 1;
				   state <= 89;
				 end
			  else if (wrq[89])
			    begin
				   wgr[89] <= 1;
				   state <= 90;
				 end
			  else if (wrq[90])
			    begin
				   wgr[90] <= 1;
				   state <= 91;
				 end
			  else if (wrq[91])
			    begin
				   wgr[91] <= 1;
				   state <= 92;
				 end				 
			  else if (wrq[92])
			    begin
				   wgr[92] <= 1;
				   state <= 93;
				 end
			  else if (wrq[93])
			    begin
				   wgr[93] <= 1;
				   state <= 94;
				 end
			  else if (wrq[94])
			    begin
				   wgr[94] <= 1;
				   state <= 95;
				 end
			  else if (wrq[95])
			    begin
				   wgr[95] <= 1;
				   state <= 96;
				 end
			  else if (wrq[96])
			    begin
				   wgr[96] <= 1;
				   state <= 97;
				 end
			  else if (wrq[97])
			    begin
				   wgr[97] <= 1;
				   state <= 98;
				 end
			  else if (wrq[98])
			    begin
				   wgr[98] <= 1;
				   state <= 99;
				 end
			  else if (wrq[99])
			    begin
				   wgr[99] <= 1;
				   state <= 100;
				 end
			  else if (wrq[100])
			    begin
				   wgr[100] <= 1;
				   state <= 101;
				 end
			  else if (wrq[101])
			    begin
				   wgr[101] <= 1;
				   state <= 102;
				 end				 
			  else if (wrq[102])
			    begin
				   wgr[102] <= 1;
				   state <= 103;
				 end
			  else if (wrq[103])
			    begin
				   wgr[103] <= 1;
				   state <= 104;
				 end
			  else if (wrq[104])
			    begin
				   wgr[104] <= 1;
				   state <= 105;
				 end
			  else if (wrq[105])
			    begin
				   wgr[105] <= 1;
				   state <= 106;
				 end
			  else if (wrq[106])
			    begin
				   wgr[106] <= 1;
				   state <= 107;
				 end
			  else if (wrq[107])
			    begin
				   wgr[107] <= 1;
				   state <= 108;
				 end
			  else if (wrq[108])
			    begin
				   wgr[108] <= 1;
				   state <= 109;
				 end
			  else if (wrq[109])
			    begin
				   wgr[109] <= 1;
				   state <= 110;
				 end
			  else if (wrq[110])
			    begin
				   wgr[110] <= 1;
				   state <= 111;
				 end
			  else if (wrq[111])
			    begin
				   wgr[111] <= 1;
				   state <= 112;
				 end				 
			  else if (wrq[112])
			    begin
				   wgr[112] <= 1;
				   state <= 113;
				 end
			  else if (wrq[113])
			    begin
				   wgr[113] <= 1;
				   state <= 114;
				 end
			  else if (wrq[114])
			    begin
				   wgr[114] <= 1;
				   state <= 115;
				 end
			  else if (wrq[115])
			    begin
				   wgr[115] <= 1;
				   state <= 116;
				 end
			  else if (wrq[116])
			    begin
				   wgr[116] <= 1;
				   state <= 117;
				 end
			  else if (wrq[117])
			    begin
				   wgr[117] <= 1;
				   state <= 118;
				 end
			  else if (wrq[118])
			    begin
				   wgr[118] <= 1;
				   state <= 119;
				 end
			  else if (wrq[119])
			    begin
				   wgr[119] <= 1;
				   state <= 120;
				 end
			  else if (wrq[120])
			    begin
				   wgr[120] <= 1;
				   state <= 121;
				 end
			  else if (wrq[121])
			    begin
				   wgr[121] <= 1;
				   state <= 122;
				 end				 
			  else if (wrq[122])
			    begin
				   wgr[122] <= 1;
				   state <= 123;
				 end
			  else if (wrq[123])
			    begin
				   wgr[123] <= 1;
				   state <= 124;
				 end
			  else if (wrq[124])
			    begin
				   wgr[124] <= 1;
				   state <= 125;
				 end
			  else if (wrq[125])
			    begin
				   wgr[125] <= 1;
				   state <= 126;
				 end
			  else if (wrq[126])
			    begin
				   wgr[126] <= 1;
				   state <= 127;
				 end
			  else if (wrq[127])
			    begin
				   wgr[127] <= 1;
				   state <= 128;
				 end
			  else if (wrq[128])
			    begin
				   wgr[128] <= 1;
				   state <= 129;
				 end
			  else if (wrq[129])
			    begin
				   wgr[129] <= 1;
				   state <= 130;
				 end
			  else if (wrq[130])
			    begin
				   wgr[130] <= 1;
				   state <= 131;
				 end
			  else if (wrq[131])
			    begin
				   wgr[131] <= 1;
				   state <= 132;
				 end				 
			  else if (wrq[132])
			    begin
				   wgr[132] <= 1;
				   state <= 133;
				 end
			  else if (wrq[133])
			    begin
				   wgr[133] <= 1;
				   state <= 134;
				 end
			  else if (wrq[134])
			    begin
				   wgr[134] <= 1;
				   state <= 135;
				 end
			  else if (wrq[135])
			    begin
				   wgr[135] <= 1;
				   state <= 136;
				 end
			  else if (wrq[136])
			    begin
				   wgr[136] <= 1;
				   state <= 137;
				 end
			  else if (wrq[137])
			    begin
				   wgr[137] <= 1;
				   state <= 138;
				 end
			  else if (wrq[138])
			    begin
				   wgr[138] <= 1;
				   state <= 139;
				 end
			  else if (wrq[139])
			    begin
				   wgr[139] <= 1;
				   state <= 140;
				 end
			  else if (wrq[140])
			    begin
				   wgr[140] <= 1;
				   state <= 141;
				 end
			  else if (wrq[141])
			    begin
				   wgr[141] <= 1;
				   state <= 142;
				 end				 
			  else if (wrq[142])
			    begin
				   wgr[142] <= 1;
				   state <= 143;
				 end
			  else if (wrq[143])
			    begin
				   wgr[143] <= 1;
				   state <= 144;
				 end
			  else if (wrq[144])
			    begin
				   wgr[144] <= 1;
				   state <= 145;
				 end
			  else if (wrq[145])
			    begin
				   wgr[145] <= 1;
				   state <= 146;
				 end
			  else if (wrq[146])
			    begin
				   wgr[146] <= 1;
				   state <= 147;
				 end
			  else if (wrq[147])
			    begin
				   wgr[147] <= 1;
				   state <= 148;
				 end
			  else if (wrq[148])
			    begin
				   wgr[148] <= 1;
				   state <= 149;
				 end
			  else if (wrq[149])
			    begin
				   wgr[149] <= 1;
				   state <= 150;
				 end
			  else if (wrq[150])
			    begin
				   wgr[150] <= 1;
				   state <= 151;
				 end
			  else if (wrq[151])
			    begin
				   wgr[151] <= 1;
				   state <= 152;
				 end				 
			  else if (wrq[152])
			    begin
				   wgr[152] <= 1;
				   state <= 153;
				 end
			  else if (wrq[153])
			    begin
				   wgr[153] <= 1;
				   state <= 154;
				 end
			  else if (wrq[154])
			    begin
				   wgr[154] <= 1;
				   state <= 155;
				 end
			  else if (wrq[155])
			    begin
				   wgr[155] <= 1;
				   state <= 156;
				 end
			  else if (wrq[156])
			    begin
				   wgr[156] <= 1;
				   state <= 157;
				 end
			  else if (wrq[157])
			    begin
				   wgr[157] <= 1;
				   state <= 158;
				 end
			  else if (wrq[158])
			    begin
				   wgr[158] <= 1;
				   state <= 159;
				 end
			  else if (wrq[159])
			    begin
				   wgr[159] <= 1;
				   state <= 160;
				 end
			  else if (wrq[160])
			    begin
				   wgr[160] <= 1;
				   state <= 161;
				 end
			  else if (wrq[161])
			    begin
				   wgr[161] <= 1;
				   state <= 162;
				 end				 
			  else if (wrq[162])
			    begin
				   wgr[162] <= 1;
				   state <= 163;
				 end
			  else if (wrq[163])
			    begin
				   wgr[163] <= 1;
				   state <= 164;
				 end
			  else if (wrq[164])
			    begin
				   wgr[164] <= 1;
				   state <= 165;
				 end
			  else if (wrq[165])
			    begin
				   wgr[165] <= 1;
				   state <= 166;
				 end
			  else if (wrq[166])
			    begin
				   wgr[166] <= 1;
				   state <= 167;
				 end
			  else if (wrq[167])
			    begin
				   wgr[167] <= 1;
				   state <= 168;
				 end
			  else if (wrq[168])
			    begin
				   wgr[168] <= 1;
				   state <= 169;
				 end
			  else if (wrq[169])
			    begin
				   wgr[169] <= 1;
				   state <= 170;
				 end
			  else if (wrq[170])
			    begin
				   wgr[170] <= 1;
				   state <= 171;
				 end
			  else if (wrq[171])
			    begin
				   wgr[171] <= 1;
				   state <= 172;
				 end				 
			  else if (wrq[172])
			    begin
				   wgr[172] <= 1;
				   state <= 173;
				 end
			  else if (wrq[173])
			    begin
				   wgr[173] <= 1;
				   state <= 174;
				 end
			  else if (wrq[174])
			    begin
				   wgr[174] <= 1;
				   state <= 175;
				 end
			  else if (wrq[175])
			    begin
				   wgr[175] <= 1;
				   state <= 176;
				 end
			  else if (wrq[176])
			    begin
				   wgr[176] <= 1;
				   state <= 177;
				 end
			  else if (wrq[177])
			    begin
				   wgr[177] <= 1;
				   state <= 178;
				 end
			  else if (wrq[178])
			    begin
				   wgr[178] <= 1;
				   state <= 179;
				 end
			  else if (wrq[179])
			    begin
				   wgr[179] <= 1;
				   state <= 180;
				 end
			  else if (wrq[180])
			    begin
				   wgr[180] <= 1;
				   state <= 181;
				 end
			  else if (wrq[181])
			    begin
				   wgr[181] <= 1;
				   state <= 182;
				 end				 
			  else if (wrq[182])
			    begin
				   wgr[182] <= 1;
				   state <= 183;
				 end
			  else if (wrq[183])
			    begin
				   wgr[183] <= 1;
				   state <= 184;
				 end
			  else if (wrq[184])
			    begin
				   wgr[184] <= 1;
				   state <= 185;
				 end
			  else if (wrq[185])
			    begin
				   wgr[185] <= 1;
				   state <= 186;
				 end
			  else if (wrq[186])
			    begin
				   wgr[186] <= 1;
				   state <= 187;
				 end
			  else if (wrq[187])
			    begin
				   wgr[187] <= 1;
				   state <= 188;
				 end
			  else if (wrq[188])
			    begin
				   wgr[188] <= 1;
				   state <= 189;
				 end
			  else if (wrq[189])
			    begin
				   wgr[189] <= 1;
				   state <= 190;
				 end
			  else if (wrq[190])
			    begin
				   wgr[190] <= 1;
				   state <= 191;
				 end
			  else if (wrq[191])
			    begin
				   wgr[191] <= 1;
				   state <= 192;
				 end				 
			  else if (wrq[192])
			    begin
				   wgr[192] <= 1;
				   state <= 193;
				 end
			  else if (wrq[193])
			    begin
				   wgr[193] <= 1;
				   state <= 194;
				 end
			  else if (wrq[194])
			    begin
				   wgr[194] <= 1;
				   state <= 195;
				 end
			  else if (wrq[195])
			    begin
				   wgr[195] <= 1;
				   state <= 196;
				 end
			  else if (wrq[196])
			    begin
				   wgr[196] <= 1;
				   state <= 197;
				 end
			  else if (wrq[197])
			    begin
				   wgr[197] <= 1;
				   state <= 198;
				 end
			  else if (wrq[198])
			    begin
				   wgr[198] <= 1;
				   state <= 199;
				 end
			  else if (wrq[199])
			    begin
				   wgr[199] <= 1;
				   state <= 200;
				 end
			  else if (wrq[200])
			    begin
				   wgr[200] <= 1;
				   state <= 201;
				 end
			  else if (wrq[201])
			    begin
				   wgr[201] <= 1;
				   state <= 202;
				 end				 
			  else if (wrq[202])
			    begin
				   wgr[202] <= 1;
				   state <= 203;
				 end
			  else if (wrq[203])
			    begin
				   wgr[203] <= 1;
				   state <= 204;
				 end
			  else if (wrq[204])
			    begin
				   wgr[204] <= 1;
				   state <= 205;
				 end
			  else if (wrq[205])
			    begin
				   wgr[205] <= 1;
				   state <= 206;
				 end
			  else if (wrq[206])
			    begin
				   wgr[206] <= 1;
				   state <= 207;
				 end
			  else if (wrq[207])
			    begin
				   wgr[207] <= 1;
				   state <= 208;
				 end
			  else if (wrq[208])
			    begin
				   wgr[208] <= 1;
				   state <= 209;
				 end
			  else if (wrq[209])
			    begin
				   wgr[209] <= 1;
				   state <= 210;
				 end
			  else if (wrq[210])
			    begin
				   wgr[210] <= 1;
				   state <= 211;
				 end
			  else if (wrq[211])
			    begin
				   wgr[211] <= 1;
				   state <= 212;
				 end				 
			  else if (wrq[212])
			    begin
				   wgr[212] <= 1;
				   state <= 213;
				 end
			  else if (wrq[213])
			    begin
				   wgr[213] <= 1;
				   state <= 214;
				 end
			  else if (wrq[214])
			    begin
				   wgr[214] <= 1;
				   state <= 215;
				 end
			  else if (wrq[215])
			    begin
				   wgr[215] <= 1;
				   state <= 216;
				 end
			  else if (wrq[216])
			    begin
				   wgr[216] <= 1;
				   state <= 217;
				 end
			  else if (wrq[217])
			    begin
				   wgr[217] <= 1;
				   state <= 218;
				 end
			  else if (wrq[218])
			    begin
				   wgr[218] <= 1;
				   state <= 219;
				 end
			  else if (wrq[219])
			    begin
				   wgr[219] <= 1;
				   state <= 220;
				 end
			  else if (wrq[220])
			    begin
				   wgr[220] <= 1;
				   state <= 221;
				 end
			  else if (wrq[221])
			    begin
				   wgr[221] <= 1;
				   state <= 222;
				 end				 
			  else if (wrq[222])
			    begin
				   wgr[222] <= 1;
				   state <= 223;
				 end
			  else if (wrq[223])
			    begin
				   wgr[223] <= 1;
				   state <= 224;
				 end
			  else if (wrq[224])
			    begin
				   wgr[224] <= 1;
				   state <= 225;
				 end
			  else if (wrq[225])
			    begin
				   wgr[225] <= 1;
				   state <= 226;
				 end
			  else if (wrq[226])
			    begin
				   wgr[226] <= 1;
				   state <= 227;
				 end
			  else if (wrq[227])
			    begin
				   wgr[227] <= 1;
				   state <= 228;
				 end
			  else if (wrq[228])
			    begin
				   wgr[228] <= 1;
				   state <= 229;
				 end
			  else if (wrq[229])
			    begin
				   wgr[229] <= 1;
				   state <= 230;
				 end
			  else if (wrq[230])
			    begin
				   wgr[230] <= 1;
				   state <= 231;
				 end
			  else if (wrq[231])
			    begin
				   wgr[231] <= 1;
				   state <= 232;
				 end				 
			  else if (wrq[232])
			    begin
				   wgr[232] <= 1;
				   state <= 233;
				 end
			  else if (wrq[233])
			    begin
				   wgr[233] <= 1;
				   state <= 234;
				 end
			  else if (wrq[234])
			    begin
				   wgr[234] <= 1;
				   state <= 235;
				 end
			  else if (wrq[235])
			    begin
				   wgr[235] <= 1;
				   state <= 236;
				 end
			  else if (wrq[236])
			    begin
				   wgr[236] <= 1;
				   state <= 237;
				 end
			  else if (wrq[237])
			    begin
				   wgr[237] <= 1;
				   state <= 238;
				 end
			  else if (wrq[238])
			    begin
				   wgr[238] <= 1;
				   state <= 239;
				 end
			  else if (wrq[239])
			    begin
				   wgr[239] <= 1;
				   state <= 240;
				 end
			  else if (wrq[240])
			    begin
				   wgr[240] <= 1;
				   state <= 241;
				 end
			  else if (wrq[241])
			    begin
				   wgr[241] <= 1;
				   state <= 242;
				 end				 
			  else if (wrq[242])
			    begin
				   wgr[242] <= 1;
				   state <= 243;
				 end
			  else if (wrq[243])
			    begin
				   wgr[243] <= 1;
				   state <= 244;
				 end
			  else if (wrq[244])
			    begin
				   wgr[244] <= 1;
				   state <= 245;
				 end
			  else if (wrq[245])
			    begin
				   wgr[245] <= 1;
				   state <= 246;
				 end
			  else if (wrq[246])
			    begin
				   wgr[246] <= 1;
				   state <= 247;
				 end
			  else if (wrq[247])
			    begin
				   wgr[247] <= 1;
				   state <= 248;
				 end
			  else if (wrq[248])
			    begin
				   wgr[248] <= 1;
				   state <= 249;
				 end
			  else if (wrq[249])
			    begin
				   wgr[249] <= 1;
				   state <= 250;
				 end
			  else if (wrq[250])
			    begin
				   wgr[250] <= 1;
				   state <= 251;
				 end
			  else if (wrq[251])
			    begin
				   wgr[251] <= 1;
				   state <= 252;
				 end				 
			  else if (wrq[252])
			    begin
				   wgr[252] <= 1;
				   state <= 253;
				 end
			  else if (wrq[253])
			    begin
				   wgr[253] <= 1;
				   state <= 254;
				 end
			  else if (wrq[254])
			    begin
				   wgr[254] <= 1;
				   state <= 255;
				 end
			  else if (wrq[255])
			    begin
				   wgr[255] <= 1;
				   state <= 256;
				 end				 
        end			 
      1:
		  begin
			 count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr0)
					0: 
					  begin
						 we <= 256'hfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe;
						 broadcast <= 1;					 
					  end
			      default: we[waddr0] <= 1;
				  endcase
				end
			 source_node <= 0;
			 if (!wrq[0])
			   begin
				  we <= 0;
				  state <= 0;
				end
        end	
      default:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr[state-2])
					0: 
					  begin
						 we <= 256'hfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe;
						 broadcast <= 1;						 
					  end
					1: we[0] <= 1;
				  endcase
            end				  
			 source_node <= state-1;
			 if (!wrq[state-1])
			   begin
				  we <= 0;
				  state <= 0;
				end
        end
    endcase
 
  always@ (posedge clk)
  begin
    case (state)
	   1: wdata_out = wdata_in0;
	   2: wdata_out = wdata_in1;
	   3: wdata_out = wdata_in2;
	   4: wdata_out = wdata_in3;	
	   5: wdata_out = wdata_in4;
	   6: wdata_out = wdata_in5;	
	   7: wdata_out = wdata_in6;
	   8: wdata_out = wdata_in7;	
	   9: wdata_out = wdata_in8;
	   10: wdata_out = wdata_in9;	
	   11: wdata_out = wdata_in10;
	   12: wdata_out = wdata_in11;	
	   13: wdata_out = wdata_in12;
	   14: wdata_out = wdata_in13;	
	   15: wdata_out = wdata_in14;
	   16: wdata_out = wdata_in15;	
	   17: wdata_out = wdata_in16;
	   18: wdata_out = wdata_in17;	
	   19: wdata_out = wdata_in18;
	   20: wdata_out = wdata_in19;	
	   21: wdata_out = wdata_in20;
	   22: wdata_out = wdata_in21;	
	   23: wdata_out = wdata_in22;
	   24: wdata_out = wdata_in23;	
	   25: wdata_out = wdata_in24;
	   26: wdata_out = wdata_in25;	
	   27: wdata_out = wdata_in26;
	   28: wdata_out = wdata_in27;	
	   29: wdata_out = wdata_in28;
	   30: wdata_out = wdata_in29;
	   31: wdata_out = wdata_in30;
	   32: wdata_out = wdata_in31;	
	   33: wdata_out = wdata_in32;
	   34: wdata_out = wdata_in33;	
	   35: wdata_out = wdata_in34;
	   36: wdata_out = wdata_in35;	
	   37: wdata_out = wdata_in36;
	   38: wdata_out = wdata_in37;	
	   39: wdata_out = wdata_in38;
	   40: wdata_out = wdata_in39;		
	   41: wdata_out = wdata_in40;
	   42: wdata_out = wdata_in41;	
	   43: wdata_out = wdata_in42;
	   44: wdata_out = wdata_in43;	
	   45: wdata_out = wdata_in44;
	   46: wdata_out = wdata_in45;	
	   47: wdata_out = wdata_in46;
	   48: wdata_out = wdata_in47;	
	   49: wdata_out = wdata_in48;
	   50: wdata_out = wdata_in49;	
	   50: wdata_out = wdata_in49;	
	   51: wdata_out = wdata_in50;
	   52: wdata_out = wdata_in51;	
	   53: wdata_out = wdata_in52;
	   54: wdata_out = wdata_in53;	
	   55: wdata_out = wdata_in54;
	   56: wdata_out = wdata_in55;	
	   57: wdata_out = wdata_in56;
	   58: wdata_out = wdata_in57;	
	   59: wdata_out = wdata_in58;
	   60: wdata_out = wdata_in59;	
	   61: wdata_out = wdata_in60;
	   62: wdata_out = wdata_in61;	
	   63: wdata_out = wdata_in62;
	   64: wdata_out = wdata_in63;	
	   65: wdata_out = wdata_in64;
	   66: wdata_out = wdata_in65;	
	   67: wdata_out = wdata_in66;
	   68: wdata_out = wdata_in67;	
	   69: wdata_out = wdata_in68;
	   70: wdata_out = wdata_in69;	
	   71: wdata_out = wdata_in70;
	   72: wdata_out = wdata_in71;	
	   73: wdata_out = wdata_in72;
	   74: wdata_out = wdata_in73;	
	   75: wdata_out = wdata_in74;
	   76: wdata_out = wdata_in75;	
	   77: wdata_out = wdata_in76;
	   78: wdata_out = wdata_in77;	
	   79: wdata_out = wdata_in78;	
	   80: wdata_out = wdata_in79;	
	   81: wdata_out = wdata_in80;
	   82: wdata_out = wdata_in81;	
	   83: wdata_out = wdata_in82;
	   84: wdata_out = wdata_in83;	
	   85: wdata_out = wdata_in84;
	   86: wdata_out = wdata_in85;	
	   87: wdata_out = wdata_in86;
	   88: wdata_out = wdata_in87;	
	   89: wdata_out = wdata_in88;	
	   90: wdata_out = wdata_in89;	
	   91: wdata_out = wdata_in90;
	   92: wdata_out = wdata_in91;	
	   93: wdata_out = wdata_in92;
	   94: wdata_out = wdata_in93;	
	   95: wdata_out = wdata_in94;
	   96: wdata_out = wdata_in95;	
	   97: wdata_out = wdata_in96;
	   98: wdata_out = wdata_in97;	
	   99: wdata_out = wdata_in98;
	   100: wdata_out = wdata_in99;	
	   101: wdata_out = wdata_in100;
	   102: wdata_out = wdata_in101;	
	   103: wdata_out = wdata_in102;
	   104: wdata_out = wdata_in103;	
	   105: wdata_out = wdata_in104;
	   106: wdata_out = wdata_in105;	
	   107: wdata_out = wdata_in106;
	   108: wdata_out = wdata_in107;	
	   109: wdata_out = wdata_in108;	
	   110: wdata_out = wdata_in109;	
	   111: wdata_out = wdata_in110;
	   112: wdata_out = wdata_in111;	
	   113: wdata_out = wdata_in112;
	   114: wdata_out = wdata_in113;	
	   115: wdata_out = wdata_in114;
	   116: wdata_out = wdata_in115;	
	   117: wdata_out = wdata_in116;
	   118: wdata_out = wdata_in117;	
	   119: wdata_out = wdata_in118;
	   120: wdata_out = wdata_in119;	
	   121: wdata_out = wdata_in120;
	   122: wdata_out = wdata_in121;	
	   123: wdata_out = wdata_in122;
	   124: wdata_out = wdata_in123;	
	   125: wdata_out = wdata_in124;
	   126: wdata_out = wdata_in125;	
	   127: wdata_out = wdata_in126;
	   128: wdata_out = wdata_in127;		
	   129: wdata_out = wdata_in128;
	   130: wdata_out = wdata_in129;	
	   131: wdata_out = wdata_in130;
	   132: wdata_out = wdata_in131;	
	   133: wdata_out = wdata_in132;
	   134: wdata_out = wdata_in133;	
	   135: wdata_out = wdata_in134;
	   136: wdata_out = wdata_in135;	
	   137: wdata_out = wdata_in136;
	   138: wdata_out = wdata_in137;		
	   139: wdata_out = wdata_in138;
	   140: wdata_out = wdata_in139;	
	   141: wdata_out = wdata_in140;
	   142: wdata_out = wdata_in141;	
	   143: wdata_out = wdata_in142;
	   144: wdata_out = wdata_in143;	
	   145: wdata_out = wdata_in144;
	   146: wdata_out = wdata_in145;	
	   147: wdata_out = wdata_in146;
	   148: wdata_out = wdata_in147;		
	   149: wdata_out = wdata_in148;
	   150: wdata_out = wdata_in149;	
	   151: wdata_out = wdata_in150;
	   152: wdata_out = wdata_in151;	
	   153: wdata_out = wdata_in152;
	   154: wdata_out = wdata_in153;	
	   155: wdata_out = wdata_in154;
	   156: wdata_out = wdata_in155;	
	   157: wdata_out = wdata_in156;
	   158: wdata_out = wdata_in157;		
	   159: wdata_out = wdata_in158;
	   160: wdata_out = wdata_in159;	
	   161: wdata_out = wdata_in160;
	   162: wdata_out = wdata_in161;	
	   163: wdata_out = wdata_in162;
	   164: wdata_out = wdata_in163;	
	   165: wdata_out = wdata_in164;
	   166: wdata_out = wdata_in165;	
	   167: wdata_out = wdata_in166;
	   168: wdata_out = wdata_in167;		
	   169: wdata_out = wdata_in168;
	   170: wdata_out = wdata_in169;	
	   171: wdata_out = wdata_in170;
	   172: wdata_out = wdata_in171;	
	   173: wdata_out = wdata_in172;
	   174: wdata_out = wdata_in173;	
	   175: wdata_out = wdata_in174;
	   176: wdata_out = wdata_in175;	
	   177: wdata_out = wdata_in176;
	   178: wdata_out = wdata_in177;		
	   179: wdata_out = wdata_in178;
	   180: wdata_out = wdata_in179;	
	   181: wdata_out = wdata_in180;
	   182: wdata_out = wdata_in181;	
	   183: wdata_out = wdata_in182;
	   184: wdata_out = wdata_in183;	
	   185: wdata_out = wdata_in184;
	   186: wdata_out = wdata_in185;	
	   187: wdata_out = wdata_in186;
	   188: wdata_out = wdata_in187;		
	   189: wdata_out = wdata_in188;
	   190: wdata_out = wdata_in189;	
	   191: wdata_out = wdata_in190;
	   192: wdata_out = wdata_in191;	
	   193: wdata_out = wdata_in192;
	   194: wdata_out = wdata_in193;	
	   195: wdata_out = wdata_in194;
	   196: wdata_out = wdata_in195;	
	   197: wdata_out = wdata_in196;
	   198: wdata_out = wdata_in197;		
	   199: wdata_out = wdata_in198;
	   200: wdata_out = wdata_in199;	
	   201: wdata_out = wdata_in200;
	   202: wdata_out = wdata_in201;	
	   203: wdata_out = wdata_in202;
	   204: wdata_out = wdata_in203;	
	   205: wdata_out = wdata_in204;
	   206: wdata_out = wdata_in205;	
	   207: wdata_out = wdata_in206;
	   208: wdata_out = wdata_in207;		
	   209: wdata_out = wdata_in208;
	   210: wdata_out = wdata_in209;	
	   211: wdata_out = wdata_in210;
	   212: wdata_out = wdata_in211;	
	   213: wdata_out = wdata_in212;
	   214: wdata_out = wdata_in213;	
	   215: wdata_out = wdata_in214;
	   216: wdata_out = wdata_in215;	
	   217: wdata_out = wdata_in216;
	   218: wdata_out = wdata_in217;		
	   219: wdata_out = wdata_in218;
	   220: wdata_out = wdata_in219;	
	   221: wdata_out = wdata_in220;
	   222: wdata_out = wdata_in221;	
	   223: wdata_out = wdata_in222;
	   224: wdata_out = wdata_in223;	
	   225: wdata_out = wdata_in224;
	   226: wdata_out = wdata_in225;	
	   227: wdata_out = wdata_in226;
	   228: wdata_out = wdata_in227;		
	   229: wdata_out = wdata_in228;
	   230: wdata_out = wdata_in229;	
	   231: wdata_out = wdata_in230;
	   232: wdata_out = wdata_in231;	
	   233: wdata_out = wdata_in232;
	   234: wdata_out = wdata_in233;	
	   235: wdata_out = wdata_in234;
	   236: wdata_out = wdata_in235;	
	   237: wdata_out = wdata_in236;
	   238: wdata_out = wdata_in237;		
	   239: wdata_out = wdata_in238;
	   240: wdata_out = wdata_in239;	
	   241: wdata_out = wdata_in240;
	   242: wdata_out = wdata_in241;	
	   243: wdata_out = wdata_in242;
	   244: wdata_out = wdata_in243;	
	   245: wdata_out = wdata_in244;
	   246: wdata_out = wdata_in245;	
	   247: wdata_out = wdata_in246;
	   248: wdata_out = wdata_in247;		
	   249: wdata_out = wdata_in248;
	   250: wdata_out = wdata_in249;	
	   251: wdata_out = wdata_in250;
	   252: wdata_out = wdata_in251;	
	   253: wdata_out = wdata_in252;
	   254: wdata_out = wdata_in253;	
	   255: wdata_out = wdata_in254;
	   256: wdata_out = wdata_in255;			
	   default: wdata_out = 0;
    endcase
  end	 
		
endmodule