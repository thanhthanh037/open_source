module m_leading_zero_cal1 (mult_result, zero1_no, zero2_no, zero3_no, zero4_no, zero_no);
  
  input [1:0] mult_result;//just the first 2 bits of the real mult_result
  input [4:0] zero1_no, zero2_no, zero3_no, zero4_no;
  output reg [6:0] zero_no;
  
  always@ (*)
  begin
    if (mult_result[1])
      zero_no = 0;
    else if (mult_result[0])
      zero_no = 1;  
    else if (!zero1_no[4])//13
      zero_no = zero1_no + 2;
    else if (!zero2_no[4])
      zero_no = zero2_no + 7'b0001111;//15 
    else if (!zero3_no[4])
      zero_no = zero3_no + 7'b0011100;//28
    else if (!zero4_no[4])
      zero_no = zero4_no + 7'b0101001;//41
    else
      zero_no = 7'b1000000;////redundant msb bit but necessary for optimizing speed
  end

endmodule