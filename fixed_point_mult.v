//////////////////////////////////////////////////////////////////////////////////
// Company: 	UoA
// Engineer: 	Thanh Bui 
// 
// Create Date:    15:40:27 02/24/2017 
// Design Name: 
// Module Name:    fixed_point_mult 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 16-bit fixed-point multiplier
//
//////////////////////////////////////////////////////////////////////////////////
module fixed_point_mult(clk, a, b, result);

  input clk;
  input [15:0] a, b;
  output reg [15:0] result;
  
  reg sign;
  reg [29:0] mult_result;
  wire overflow;
  wire [14:0] temp;

  always@ (posedge clk)
  begin
	 sign <= a[15] ^ b [15];
	 mult_result <= a[14:0] * b[14:0]; 
  end
  
  assign overflow = mult_result[29] || mult_result[28] || mult_result[27];
  assign temp = mult_result[26:12];
  
  always@ (*)
    if (temp == 0)
	   result = 16'h0000;
	 else if (overflow)
      begin
        if (sign)
	       result = 16'hffff;
        else
	       result = 16'h7fff;
	   end
    else
      begin
        if (sign)				
	       result = {1'b1, temp};
        else
	       result = {1'b0, temp};
	   end  
  
endmodule