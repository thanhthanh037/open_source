module a_PL_reg2 (clk, exp_comp1, exp_dif, exp_nor, sign_a, sign_b, man_a, man_b, exp_comp2, exp_dif1, exp_nor1, sign_a1, sign_b1, man_a1, man_b1);

  input clk;
  input exp_comp1, sign_a, sign_b;
  input [6:0] exp_dif;//exp_dif here is just the last 7 bit of the real exp_dif
  input [10:0] exp_nor;
  input [52:0] man_a, man_b;
  output reg exp_comp2, sign_a1, sign_b1;
  output reg [6:0] exp_dif1;
  output reg [10:0] exp_nor1;
  output reg [52:0] man_a1, man_b1;

  always@ (posedge clk)
  begin
     exp_comp2 <= exp_comp1;
	   sign_a1 <= sign_a;
	   sign_b1 <= sign_b;
	   exp_dif1 <= exp_dif;
	   exp_nor1 <= exp_nor;
	   man_a1 <= man_a;
	   man_b1 <= man_b;
  end

endmodule
	
