`include "parameter.v"
module crossbar (iflit1, iflit2, iflit3, iflit4, sel, oflit);

  input [flit_data_width-1:0] iflit1, iflit2, iflit3, iflit4;
  input [1:0] sel;
  output reg [flit_data_width-1:0] oflit;
  
  always@ (*)
  begin
    case (sel)
      2'b00: oflit = iflit1;
      2'b01: oflit = iflit2;
      2'b10: oflit = iflit3;
      2'b11: oflit = iflit4;
    endcase
  end
  
endmodule