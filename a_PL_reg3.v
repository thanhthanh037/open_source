module a_PL_reg3 (clk, exp_comp1, exp_dif, exp_nor1, sign_a1, sign_b1, man_a_nor, man_b_nor, exp_comp2, exp_dif1, exp_nor2, sign_a2, sign_b2, man_a_nor1, man_b_nor1);

  input clk;
  input exp_comp1, sign_a1, sign_b1;
  input [2:0] exp_dif;// exp_dif here is just three bit 6, 5, 4 of real exp_dif
  input [10:0] exp_nor1;
  input [52:0] man_a_nor, man_b_nor;
  output reg exp_comp2, sign_a2, sign_b2;
  output reg [2:0] exp_dif1;
  output reg [10:0] exp_nor2;
  output reg [52:0] man_a_nor1, man_b_nor1;

  always@ (posedge clk)
  begin
	   exp_comp2 <= exp_comp1;
		 exp_dif1 <= exp_dif;
	   sign_a2 <= sign_a1;
	   sign_b2 <= sign_b1;
	   exp_nor2 <= exp_nor1;
	   man_a_nor1 <= man_a_nor;
	   man_b_nor1 <= man_b_nor;
  end

endmodule