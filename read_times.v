`include "parameters.v"
module read_times(dir, read_times);

  input [4:0] dir;
  output reg [1:0] read_times;
 
  always@ (*)
  begin
    case (dir)
	   NP: read_times = 1;
	   SP: read_times = 1;
	   EP: read_times = 1;
	   WP: read_times = 1; 
	   NS: read_times = 1;
	   NE: read_times = 1;
	   NW: read_times = 1;
	   SE: read_times = 1; 
	   SW: read_times = 1;  
	   EW: read_times = 1; 
	   NEP: read_times = 2;
	   NWP: read_times = 2;
	   SEP: read_times = 2;
	   SWP: read_times = 2;
	   NSE: read_times = 2;
	   NSW: read_times = 2;
	   NEW: read_times = 2;  
	   SEW: read_times = 2;  
	   NSEW: read_times = 3;
	   default: read_times = 0;	
    endcase		
  end
  
endmodule

