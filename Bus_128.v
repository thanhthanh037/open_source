`include "parameters.v"
module Bus_128 (clk, rst, wrq,				  
                waddr0, waddr,
                wdata_in0, wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, 
                wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, wdata_in17, wdata_in18, wdata_in19,
				wdata_in20, wdata_in21, wdata_in22, wdata_in23, wdata_in24, wdata_in25, wdata_in26, wdata_in27, wdata_in28, wdata_in29, 
				wdata_in30, wdata_in31, wdata_in32, wdata_in33, wdata_in34, wdata_in35, wdata_in36, wdata_in37, wdata_in38, wdata_in39, 
				wdata_in40, wdata_in41, wdata_in42, wdata_in43, wdata_in44, wdata_in45, wdata_in46, wdata_in47, wdata_in48, wdata_in49,
				wdata_in50, wdata_in51, wdata_in52, wdata_in53, wdata_in54, wdata_in55, wdata_in56, wdata_in57, wdata_in58, wdata_in59, 
                wdata_in60, wdata_in61, wdata_in62, wdata_in63, wdata_in64, wdata_in65, wdata_in66, wdata_in67, wdata_in68, wdata_in69,
				wdata_in70, wdata_in71, wdata_in72, wdata_in73, wdata_in74, wdata_in75, wdata_in76, wdata_in77, wdata_in78, wdata_in79, 
				wdata_in80, wdata_in81, wdata_in82, wdata_in83, wdata_in84, wdata_in85, wdata_in86, wdata_in87, wdata_in88, wdata_in89, 
				wdata_in90, wdata_in91, wdata_in92, wdata_in93, wdata_in94, wdata_in95, wdata_in96, wdata_in97, wdata_in98, wdata_in99,
				wdata_in100, wdata_in101, wdata_in102, wdata_in103, wdata_in104, wdata_in105, wdata_in106, wdata_in107, wdata_in108, wdata_in109, 
                wdata_in110, wdata_in111, wdata_in112, wdata_in113, wdata_in114, wdata_in115, wdata_in116, wdata_in117, wdata_in118, wdata_in119,
				wdata_in120, wdata_in121, wdata_in122, wdata_in123, wdata_in124, wdata_in125, wdata_in126, wdata_in127,				  
                wgr,				  
				we,
				broadcast,
				source_node,
                wdata_out);
  
  input clk, rst;
  input [127:0] wrq;	
  input [6:0] waddr0;
  input [126:0] waddr;	
  input [data_width-1:0] wdata_in0, wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9; 
  input [data_width-1:0] wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, wdata_in17, wdata_in18, wdata_in19;
  input [data_width-1:0] wdata_in20, wdata_in21, wdata_in22, wdata_in23, wdata_in24, wdata_in25, wdata_in26, wdata_in27, wdata_in28, wdata_in29; 
  input [data_width-1:0] wdata_in30, wdata_in31, wdata_in32, wdata_in33, wdata_in34, wdata_in35, wdata_in36, wdata_in37, wdata_in38, wdata_in39; 
  input [data_width-1:0] wdata_in40, wdata_in41, wdata_in42, wdata_in43, wdata_in44, wdata_in45, wdata_in46, wdata_in47, wdata_in48, wdata_in49;
  input [data_width-1:0] wdata_in50, wdata_in51, wdata_in52, wdata_in53, wdata_in54, wdata_in55, wdata_in56, wdata_in57, wdata_in58, wdata_in59; 
  input [data_width-1:0] wdata_in60, wdata_in61, wdata_in62, wdata_in63, wdata_in64, wdata_in65, wdata_in66, wdata_in67, wdata_in68, wdata_in69;
  input [data_width-1:0] wdata_in70, wdata_in71, wdata_in72, wdata_in73, wdata_in74, wdata_in75, wdata_in76, wdata_in77, wdata_in78, wdata_in79; 
  input [data_width-1:0] wdata_in80, wdata_in81, wdata_in82, wdata_in83, wdata_in84, wdata_in85, wdata_in86, wdata_in87, wdata_in88, wdata_in89; 
  input [data_width-1:0] wdata_in90, wdata_in91, wdata_in92, wdata_in93, wdata_in94, wdata_in95, wdata_in96, wdata_in97, wdata_in98, wdata_in99;
  input [data_width-1:0] wdata_in100, wdata_in101, wdata_in102, wdata_in103, wdata_in104, wdata_in105, wdata_in106, wdata_in107, wdata_in108, wdata_in109; 
  input [data_width-1:0] wdata_in110, wdata_in111, wdata_in112, wdata_in113, wdata_in114, wdata_in115, wdata_in116, wdata_in117, wdata_in118, wdata_in119;
  input [data_width-1:0] wdata_in120, wdata_in121, wdata_in122, wdata_in123, wdata_in124, wdata_in125, wdata_in126, wdata_in127;	
				  
  output reg [127:0] wgr;	
  
  output reg [127:0] we;	 
  output reg broadcast;
  output reg [6:0] source_node;
  output reg [data_width-1:0] wdata_out;
  
  reg [7:0] state;
  reg count;
 
  always@ (posedge clk)
  if (rst) 
    state <= 0;
  else
    case (state)
	   0: 
		  begin
		    wgr <= 0; 
		    we <= 0;		    			 
			count <= 0;
            broadcast <= 0;			 
			  if (wrq[0])
			    begin
				   wgr[0] <= 1;
				   state <= 1;
				 end
			  else if (wrq[1])
			    begin
				   wgr[1] <= 1;
				   state <= 2;
				 end				 
			  else if (wrq[2])
			    begin
				   wgr[2] <= 1;
				   state <= 3;
				 end
			  else if (wrq[3])
			    begin
				   wgr[3] <= 1;
				   state <= 4;
				 end
			  else if (wrq[4])
			    begin
				   wgr[4] <= 1;
				   state <= 5;
				 end
			  else if (wrq[5])
			    begin
				   wgr[5] <= 1;
				   state <= 6;
				 end
			  else if (wrq[6])
			    begin
				   wgr[6] <= 1;
				   state <= 7;
				 end
			  else if (wrq[7])
			    begin
				   wgr[7] <= 1;
				   state <= 8;
				 end
			  else if (wrq[8])
			    begin
				   wgr[8] <= 1;
				   state <= 9;
				 end
			  else if (wrq[9])
			    begin
				   wgr[9] <= 1;
				   state <= 10;
				 end
			  else if (wrq[10])
			    begin
				   wgr[10] <= 1;
				   state <= 11;
				 end
			  else if (wrq[11])
			    begin
				   wgr[11] <= 1;
				   state <= 12;
				 end
			  else if (wrq[12])
			    begin
				   wgr[12] <= 1;
				   state <= 13;
				 end
			  else if (wrq[13])
			    begin
				   wgr[13] <= 1;
				   state <= 14;
				 end
			  else if (wrq[14])
			    begin
				   wgr[14] <= 1;
				   state <= 15;
				 end
			  else if (wrq[15])
			    begin
				   wgr[15] <= 1;
				   state <= 16;
				 end
			  else if (wrq[16])
			    begin
				   wgr[16] <= 1;
				   state <= 17;
				 end
			  else if (wrq[17])
			    begin
				   wgr[17] <= 1;
				   state <= 18;
				 end
			  else if (wrq[18])
			    begin
				   wgr[18] <= 1;
				   state <= 19;
				 end
			  else if (wrq[19])
			    begin
				   wgr[19] <= 1;
				   state <= 20;
				 end
			  else if (wrq[20])
			    begin
				   wgr[20] <= 1;
				   state <= 21;
				 end
			  else if (wrq[21])
			    begin
				   wgr[21] <= 1;
				   state <= 22;
				 end				 
			  else if (wrq[22])
			    begin
				   wgr[22] <= 1;
				   state <= 23;
				 end
			  else if (wrq[23])
			    begin
				   wgr[23] <= 1;
				   state <= 24;
				 end
			  else if (wrq[24])
			    begin
				   wgr[24] <= 1;
				   state <= 25;
				 end
			  else if (wrq[25])
			    begin
				   wgr[25] <= 1;
				   state <= 26;
				 end
			  else if (wrq[26])
			    begin
				   wgr[26] <= 1;
				   state <=27;
				 end
			  else if (wrq[27])
			    begin
				   wgr[27] <= 1;
				   state <= 28;
				 end
			  else if (wrq[28])
			    begin
				   wgr[28] <= 1;
				   state <= 29;
				 end
			  else if (wrq[29])
			    begin
				   wgr[29] <= 1;
				   state <= 30;
				 end	
			  else if (wrq[30])
			    begin
				   wgr[30] <= 1;
				   state <= 31;
				 end
			  else if (wrq[31])
			    begin
				   wgr[31] <= 1;
				   state <= 32;
				 end				 
			  else if (wrq[32])
			    begin
				   wgr[32] <= 1;
				   state <= 33;
				 end
			  else if (wrq[33])
			    begin
				   wgr[33] <= 1;
				   state <= 34;
				 end
			  else if (wrq[34])
			    begin
				   wgr[34] <= 1;
				   state <= 35;
				 end
			  else if (wrq[35])
			    begin
				   wgr[35] <= 1;
				   state <= 36;
				 end
			  else if (wrq[36])
			    begin
				   wgr[36] <= 1;
				   state <= 37;
				 end
			  else if (wrq[37])
			    begin
				   wgr[37] <= 1;
				   state <= 38;
				 end
			  else if (wrq[38])
			    begin
				   wgr[38] <= 1;
				   state <= 39;
				 end
			  else if (wrq[39])
			    begin
				   wgr[39] <= 1;
				   state <= 40;
				 end	
			  else if (wrq[40])
			    begin
				   wgr[40] <= 1;
				   state <= 41;
				 end
			  else if (wrq[41])
			    begin
				   wgr[41] <= 1;
				   state <= 42;
				 end				 
			  else if (wrq[42])
			    begin
				   wgr[42] <= 1;
				   state <= 43;
				 end
			  else if (wrq[43])
			    begin
				   wgr[43] <= 1;
				   state <= 44;
				 end
			  else if (wrq[44])
			    begin
				   wgr[44] <= 1;
				   state <= 45;
				 end
			  else if (wrq[45])
			    begin
				   wgr[45] <= 1;
				   state <= 46;
				 end
			  else if (wrq[46])
			    begin
				   wgr[46] <= 1;
				   state <= 47;
				 end
			  else if (wrq[47])
			    begin
				   wgr[47] <= 1;
				   state <= 48;
				 end
			  else if (wrq[48])
			    begin
				   wgr[48] <= 1;
				   state <= 49;
				 end
			  else if (wrq[49])
			    begin
				   wgr[49] <= 1;
				   state <= 50;
				 end	
			  else if (wrq[50])
			    begin
				   wgr[50] <= 1;
				   state <= 51;
				 end
			  else if (wrq[51])
			    begin
				   wgr[51] <= 1;
				   state <= 52;
				 end				 
			  else if (wrq[52])
			    begin
				   wgr[52] <= 1;
				   state <= 53;
				 end
			  else if (wrq[53])
			    begin
				   wgr[53] <= 1;
				   state <= 54;
				 end
			  else if (wrq[54])
			    begin
				   wgr[54] <= 1;
				   state <= 55;
				 end
			  else if (wrq[55])
			    begin
				   wgr[55] <= 1;
				   state <= 56;
				 end
			  else if (wrq[56])
			    begin
				   wgr[56] <= 1;
				   state <= 57;
				 end
			  else if (wrq[57])
			    begin
				   wgr[57] <= 1;
				   state <= 58;
				 end
			  else if (wrq[58])
			    begin
				   wgr[58] <= 1;
				   state <= 59;
				 end
			  else if (wrq[59])
			    begin
				   wgr[59] <= 1;
				   state <= 60;
				 end	
			  else if (wrq[60])
			    begin
				   wgr[60] <= 1;
				   state <= 61;
				 end
			  else if (wrq[61])
			    begin
				   wgr[61] <= 1;
				   state <= 62;
				 end				 
			  else if (wrq[62])
			    begin
				   wgr[62] <= 1;
				   state <= 63;
				 end
			  else if (wrq[63])
			    begin
				   wgr[63] <= 1;
				   state <= 64;
				 end
			  else if (wrq[64])
			    begin
				   wgr[64] <= 1;
				   state <= 65;
				 end
			  else if (wrq[65])
			    begin
				   wgr[65] <= 1;
				   state <= 66;
				 end
			  else if (wrq[66])
			    begin
				   wgr[66] <= 1;
				   state <= 67;
				 end
			  else if (wrq[67])
			    begin
				   wgr[67] <= 1;
				   state <= 68;
				 end
			  else if (wrq[68])
			    begin
				   wgr[68] <= 1;
				   state <= 69;
				 end
			  else if (wrq[69])
			    begin
				   wgr[69] <= 1;
				   state <= 70;
				 end
			  else if (wrq[70])
			    begin
				   wgr[70] <= 1;
				   state <= 71;
				 end
			  else if (wrq[71])
			    begin
				   wgr[71] <= 1;
				   state <= 72;
				 end				 
			  else if (wrq[72])
			    begin
				   wgr[72] <= 1;
				   state <= 73;
				 end
			  else if (wrq[73])
			    begin
				   wgr[73] <= 1;
				   state <= 74;
				 end
			  else if (wrq[74])
			    begin
				   wgr[74] <= 1;
				   state <= 75;
				 end
			  else if (wrq[75])
			    begin
				   wgr[75] <= 1;
				   state <= 76;
				 end
			  else if (wrq[76])
			    begin
				   wgr[76] <= 1;
				   state <= 77;
				 end
			  else if (wrq[77])
			    begin
				   wgr[77] <= 1;
				   state <= 78;
				 end
			  else if (wrq[78])
			    begin
				   wgr[78] <= 1;
				   state <= 79;
				 end
			  else if (wrq[79])
			    begin
				   wgr[79] <= 1;
				   state <= 80;
				 end
			  else if (wrq[80])
			    begin
				   wgr[80] <= 1;
				   state <= 81;
				 end
			  else if (wrq[81])
			    begin
				   wgr[81] <= 1;
				   state <= 82;
				 end				 
			  else if (wrq[82])
			    begin
				   wgr[82] <= 1;
				   state <= 83;
				 end
			  else if (wrq[83])
			    begin
				   wgr[83] <= 1;
				   state <= 84;
				 end
			  else if (wrq[84])
			    begin
				   wgr[84] <= 1;
				   state <= 85;
				 end
			  else if (wrq[85])
			    begin
				   wgr[85] <= 1;
				   state <= 86;
				 end
			  else if (wrq[86])
			    begin
				   wgr[86] <= 1;
				   state <= 87;
				 end
			  else if (wrq[87])
			    begin
				   wgr[87] <= 1;
				   state <= 88;
				 end
			  else if (wrq[88])
			    begin
				   wgr[88] <= 1;
				   state <= 89;
				 end
			  else if (wrq[89])
			    begin
				   wgr[89] <= 1;
				   state <= 90;
				 end
			  else if (wrq[90])
			    begin
				   wgr[90] <= 1;
				   state <= 91;
				 end
			  else if (wrq[91])
			    begin
				   wgr[91] <= 1;
				   state <= 92;
				 end				 
			  else if (wrq[92])
			    begin
				   wgr[92] <= 1;
				   state <= 93;
				 end
			  else if (wrq[93])
			    begin
				   wgr[93] <= 1;
				   state <= 94;
				 end
			  else if (wrq[94])
			    begin
				   wgr[94] <= 1;
				   state <= 95;
				 end
			  else if (wrq[95])
			    begin
				   wgr[95] <= 1;
				   state <= 96;
				 end
			  else if (wrq[96])
			    begin
				   wgr[96] <= 1;
				   state <= 97;
				 end
			  else if (wrq[97])
			    begin
				   wgr[97] <= 1;
				   state <= 98;
				 end
			  else if (wrq[98])
			    begin
				   wgr[98] <= 1;
				   state <= 99;
				 end
			  else if (wrq[99])
			    begin
				   wgr[99] <= 1;
				   state <= 100;
				 end
			  else if (wrq[100])
			    begin
				   wgr[100] <= 1;
				   state <= 101;
				 end
			  else if (wrq[101])
			    begin
				   wgr[101] <= 1;
				   state <= 102;
				 end				 
			  else if (wrq[102])
			    begin
				   wgr[102] <= 1;
				   state <= 103;
				 end
			  else if (wrq[103])
			    begin
				   wgr[103] <= 1;
				   state <= 104;
				 end
			  else if (wrq[104])
			    begin
				   wgr[104] <= 1;
				   state <= 105;
				 end
			  else if (wrq[105])
			    begin
				   wgr[105] <= 1;
				   state <= 106;
				 end
			  else if (wrq[106])
			    begin
				   wgr[106] <= 1;
				   state <= 107;
				 end
			  else if (wrq[107])
			    begin
				   wgr[107] <= 1;
				   state <= 108;
				 end
			  else if (wrq[108])
			    begin
				   wgr[108] <= 1;
				   state <= 109;
				 end
			  else if (wrq[109])
			    begin
				   wgr[109] <= 1;
				   state <= 110;
				 end
			  else if (wrq[110])
			    begin
				   wgr[110] <= 1;
				   state <= 111;
				 end
			  else if (wrq[111])
			    begin
				   wgr[111] <= 1;
				   state <= 112;
				 end				 
			  else if (wrq[112])
			    begin
				   wgr[112] <= 1;
				   state <= 113;
				 end
			  else if (wrq[113])
			    begin
				   wgr[113] <= 1;
				   state <= 114;
				 end
			  else if (wrq[114])
			    begin
				   wgr[114] <= 1;
				   state <= 115;
				 end
			  else if (wrq[115])
			    begin
				   wgr[115] <= 1;
				   state <= 116;
				 end
			  else if (wrq[116])
			    begin
				   wgr[116] <= 1;
				   state <= 117;
				 end
			  else if (wrq[117])
			    begin
				   wgr[117] <= 1;
				   state <= 118;
				 end
			  else if (wrq[118])
			    begin
				   wgr[118] <= 1;
				   state <= 119;
				 end
			  else if (wrq[119])
			    begin
				   wgr[119] <= 1;
				   state <= 120;
				 end
			  else if (wrq[120])
			    begin
				   wgr[120] <= 1;
				   state <= 121;
				 end
			  else if (wrq[121])
			    begin
				   wgr[121] <= 1;
				   state <= 122;
				 end				 
			  else if (wrq[122])
			    begin
				   wgr[122] <= 1;
				   state <= 123;
				 end
			  else if (wrq[123])
			    begin
				   wgr[123] <= 1;
				   state <= 124;
				 end
			  else if (wrq[124])
			    begin
				   wgr[124] <= 1;
				   state <= 125;
				 end
			  else if (wrq[125])
			    begin
				   wgr[125] <= 1;
				   state <= 126;
				 end
			  else if (wrq[126])
			    begin
				   wgr[126] <= 1;
				   state <= 127;
				 end
			  else if (wrq[127])
			    begin
				   wgr[127] <= 1;
				   state <= 128;
				 end	 
        end			 
      1:
		  begin
			 count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr0)
					0: 
					  begin
						 we <= 128'hfffffffffffffffffffffffffffffffe;
						 broadcast <= 1;					 
					  end
			      default: we[waddr0] <= 1;
				  endcase
				end
			 source_node <= 0;
			 if (!wrq[0])
			   begin
				  we <= 0;
				  state <= 0;
				end
        end	
      default:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr[state-2])
					0: 
					  begin
						 we <= 128'hfffffffffffffffffffffffffffffffe;
						 broadcast <= 1;						 
					  end
					1: we[0] <= 1;
				  endcase
            end				  
			 source_node <= state-1;
			 if (!wrq[state-1])
			   begin
				  we <= 0;
				  state <= 0;
				end
        end
    endcase
 
  always@ (posedge clk)
  begin
    case (state)
	   1: wdata_out = wdata_in0;
	   2: wdata_out = wdata_in1;
	   3: wdata_out = wdata_in2;
	   4: wdata_out = wdata_in3;	
	   5: wdata_out = wdata_in4;
	   6: wdata_out = wdata_in5;	
	   7: wdata_out = wdata_in6;
	   8: wdata_out = wdata_in7;	
	   9: wdata_out = wdata_in8;
	   10: wdata_out = wdata_in9;	
	   11: wdata_out = wdata_in10;
	   12: wdata_out = wdata_in11;	
	   13: wdata_out = wdata_in12;
	   14: wdata_out = wdata_in13;	
	   15: wdata_out = wdata_in14;
	   16: wdata_out = wdata_in15;	
	   17: wdata_out = wdata_in16;
	   18: wdata_out = wdata_in17;	
	   19: wdata_out = wdata_in18;
	   20: wdata_out = wdata_in19;	
	   21: wdata_out = wdata_in20;
	   22: wdata_out = wdata_in21;	
	   23: wdata_out = wdata_in22;
	   24: wdata_out = wdata_in23;	
	   25: wdata_out = wdata_in24;
	   26: wdata_out = wdata_in25;	
	   27: wdata_out = wdata_in26;
	   28: wdata_out = wdata_in27;	
	   29: wdata_out = wdata_in28;
	   30: wdata_out = wdata_in29;
	   31: wdata_out = wdata_in30;
	   32: wdata_out = wdata_in31;	
	   33: wdata_out = wdata_in32;
	   34: wdata_out = wdata_in33;	
	   35: wdata_out = wdata_in34;
	   36: wdata_out = wdata_in35;	
	   37: wdata_out = wdata_in36;
	   38: wdata_out = wdata_in37;	
	   39: wdata_out = wdata_in38;
	   40: wdata_out = wdata_in39;		
	   41: wdata_out = wdata_in40;
	   42: wdata_out = wdata_in41;	
	   43: wdata_out = wdata_in42;
	   44: wdata_out = wdata_in43;	
	   45: wdata_out = wdata_in44;
	   46: wdata_out = wdata_in45;	
	   47: wdata_out = wdata_in46;
	   48: wdata_out = wdata_in47;	
	   49: wdata_out = wdata_in48;
	   50: wdata_out = wdata_in49;	
	   50: wdata_out = wdata_in49;	
	   51: wdata_out = wdata_in50;
	   52: wdata_out = wdata_in51;	
	   53: wdata_out = wdata_in52;
	   54: wdata_out = wdata_in53;	
	   55: wdata_out = wdata_in54;
	   56: wdata_out = wdata_in55;	
	   57: wdata_out = wdata_in56;
	   58: wdata_out = wdata_in57;	
	   59: wdata_out = wdata_in58;
	   60: wdata_out = wdata_in59;	
	   61: wdata_out = wdata_in60;
	   62: wdata_out = wdata_in61;	
	   63: wdata_out = wdata_in62;
	   64: wdata_out = wdata_in63;	
	   65: wdata_out = wdata_in64;
	   66: wdata_out = wdata_in65;	
	   67: wdata_out = wdata_in66;
	   68: wdata_out = wdata_in67;	
	   69: wdata_out = wdata_in68;
	   70: wdata_out = wdata_in69;	
	   71: wdata_out = wdata_in70;
	   72: wdata_out = wdata_in71;	
	   73: wdata_out = wdata_in72;
	   74: wdata_out = wdata_in73;	
	   75: wdata_out = wdata_in74;
	   76: wdata_out = wdata_in75;	
	   77: wdata_out = wdata_in76;
	   78: wdata_out = wdata_in77;	
	   79: wdata_out = wdata_in78;	
	   80: wdata_out = wdata_in79;	
	   81: wdata_out = wdata_in80;
	   82: wdata_out = wdata_in81;	
	   83: wdata_out = wdata_in82;
	   84: wdata_out = wdata_in83;	
	   85: wdata_out = wdata_in84;
	   86: wdata_out = wdata_in85;	
	   87: wdata_out = wdata_in86;
	   88: wdata_out = wdata_in87;	
	   89: wdata_out = wdata_in88;	
	   90: wdata_out = wdata_in89;	
	   91: wdata_out = wdata_in90;
	   92: wdata_out = wdata_in91;	
	   93: wdata_out = wdata_in92;
	   94: wdata_out = wdata_in93;	
	   95: wdata_out = wdata_in94;
	   96: wdata_out = wdata_in95;	
	   97: wdata_out = wdata_in96;
	   98: wdata_out = wdata_in97;	
	   99: wdata_out = wdata_in98;
	   100: wdata_out = wdata_in99;	
	   101: wdata_out = wdata_in100;
	   102: wdata_out = wdata_in101;	
	   103: wdata_out = wdata_in102;
	   104: wdata_out = wdata_in103;	
	   105: wdata_out = wdata_in104;
	   106: wdata_out = wdata_in105;	
	   107: wdata_out = wdata_in106;
	   108: wdata_out = wdata_in107;	
	   109: wdata_out = wdata_in108;	
	   110: wdata_out = wdata_in109;	
	   111: wdata_out = wdata_in110;
	   112: wdata_out = wdata_in111;	
	   113: wdata_out = wdata_in112;
	   114: wdata_out = wdata_in113;	
	   115: wdata_out = wdata_in114;
	   116: wdata_out = wdata_in115;	
	   117: wdata_out = wdata_in116;
	   118: wdata_out = wdata_in117;	
	   119: wdata_out = wdata_in118;
	   120: wdata_out = wdata_in119;	
	   121: wdata_out = wdata_in120;
	   122: wdata_out = wdata_in121;	
	   123: wdata_out = wdata_in122;
	   124: wdata_out = wdata_in123;	
	   125: wdata_out = wdata_in124;
	   126: wdata_out = wdata_in125;	
	   127: wdata_out = wdata_in126;
	   128: wdata_out = wdata_in127;		
	   default: wdata_out = 0;
    endcase
  end	 
		
endmodule