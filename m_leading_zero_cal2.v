module m_leading_zero_cal2 (zero5_no, zero6_no, zero7_no, zero8_no, zero_no1, zero_no2);
  
  input [4:0] zero5_no, zero6_no, zero7_no, zero8_no;
  input [6:0] zero_no1;
  output reg [6:0] zero_no2;
  
  always@ (*)
  begin
    if (!zero_no1[6])
      zero_no2 = zero_no1;
    else if (!zero5_no[4])
      zero_no2 = zero5_no + 7'b0110110;//54
    else if (!zero6_no[4])
      zero_no2 = zero6_no + 7'b1000011;//67
    else if (!zero7_no[4])
      zero_no2 = zero7_no + 7'b1010000;//80
    else if (!zero8_no[4])
      zero_no2 = zero8_no + 7'b1011101;//93
    else
      zero_no2 = 7'b1101010;//106
  end

endmodule
  
