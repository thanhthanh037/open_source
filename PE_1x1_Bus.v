`include "parameter.v"
module PE_1x1(clk, rst, source_node, wgr, broadcast, we, idata, wrq, waddr, odata);

  input clk, rst, wgr, broadcast, we;
  input [5:0] source_node;
  input [data_width-1:0] idata;
  output wrq;
  output waddr;
  output [data_width-1:0] odata;

  wire w_start, re_i, we_i, re_w, we_w, re_o8, we_o, rst_acc, done_hidden, done_output;
  wire [5:0] waddr_i;
  wire [data_width-1:0] wdata;
  wire [data_width-1:0] input_values;
  wire [data_width-1:0] weights;  
  wire [11:0] result, odata_1;  
			 
  Bus_interface_1x1 Bus_interface (clk, rst, source_node,
								   wgr, broadcast, we, idata, 
								   done_hidden, done_output, odata_1,		    
								   w_start, we_i, we_w, 
								   re_o, waddr_i, wdata,
								   wrq, waddr, odata);
											  
  control_1 control (clk, rst, we_i, re_i, re_w, we_o, rst_acc, done_hidden, done_output);
  
  RAM_i RAM_i (clk, rst, re_i, we_i, w_start, waddr, idata, input_values);
  
  RAM_w RAM_w (clk, rst, re_w, we_w, idata, weights);
  
  RAM_o RAM_o (clk, rst, re_o, we_o, result, odata_1);
  
  neuron_1x neuron (clk, rst, rst_acc, input_values, weights, result);

  
endmodule