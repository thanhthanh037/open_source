module RAM_w_8 (clk, rst, rd_en, wr_en, idata, odata);
    
  parameter [7:0] data_width = 128;
  parameter [4:0] address_width = 16;
  parameter [16:0] RAM_size = 260;
  
  input clk, rst;
  input rd_en, wr_en;
  input [data_width-1:0] idata;
  output reg [data_width-1:0] odata;
  
  reg [data_width-1:0] RAM[RAM_size-1:0];
  reg [address_width-1:0] rd_ptr, wr_ptr;

   //write pointer
  always@ (posedge clk)
  begin
    if (rst)
      wr_ptr <= 0;      
    else if (wr_en) 
      begin
        if (wr_ptr == RAM_size-1)
          wr_ptr <= 0;
        else
          wr_ptr <= wr_ptr + 1'b1;
      end
  end
  
   //write operation
  always@ (posedge clk)
  begin
    if (wr_en)
      RAM[wr_ptr] <= idata;
  end
  
  // read pointer  
  always@ (posedge clk)
  begin
    if (rst)
      rd_ptr <= 0;       
    else if (rd_en)
      begin
        if (rd_ptr == RAM_size-1)
          rd_ptr <= 0;		  
        else    
          rd_ptr <= rd_ptr + 1'b1;
      end
  end
 
  //read operation
  always@ (posedge clk)
  begin
    odata <= RAM[rd_ptr];  
  end

endmodule