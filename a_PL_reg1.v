module a_PL_reg1 (clk, a0, b0, exp_comp, a1, b1, exp_comp1);

input clk;
input [63:0] a0, b0;
input exp_comp;
output reg [63:0] a1, b1;
output reg exp_comp1;

always@ (posedge clk)
begin
	a1 <= a0;
	b1 <= b0;
	exp_comp1 <= exp_comp;
end

endmodule
