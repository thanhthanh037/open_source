//////////////////////////////////////////////////////////////////////////////////
// Company: 	UoA
// Engineer: 	Thanh Bui
// 
// Create Date:    16:21:32 02/27/2017 
// Design Name: 
// Module Name:    neuron_1x 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module neuron_1x(clk, rst, rst_acc, idata, weights, result);

  input clk, rst, rst_acc;  
  input [15:0] idata, weights;
  output [11:0] result;
  

  wire [15:0] mult_result, acc_result;
  reg [15:0] mult_result_2c, acc_result_sm;
  wire [14:0] mult_temp, acc_temp;
  
  
  fixed_point_mult mult (clk, idata, weights, mult_result);
  fixed_point_acc acc (clk, rst, rst_acc, mult_result_2c, acc_result);
  sigmoid sigmoid (clk, acc_result_sm[15:8], result);
  
  assign mult_temp = ~mult_result[14:0] + 1'b1;
  assign acc_temp = ~acc_result[14:0] + 1'b1;
 
  always@ (posedge clk)
  begin
    mult_result_2c = mult_result[15] ? {1'b1,mult_temp} : mult_result;
	acc_result_sm = (acc_result == 16'h8000) ? 16'hffff : acc_result[15] ? {1'b1,acc_temp} : acc_result;
  end
  
endmodule