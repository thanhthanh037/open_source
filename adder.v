//------------------------------------------------------------------------------------------------------
// File         : adder.v
// Author       : Thanh Bui
// Organization : The University of Adelaide
// Created      : March 2015
// Platform     : VHDL 1993
// Simulators   : Modelsim
// Synthesizers : ISE 14.7
// Targets      : Virtex-7 VC707
// Description  : IEEE 754 compliant double-precision floating-point adder with 10 pipeline stages
//------------------------------------------------------------------------------------------------------
module adder (clk, a, b, result);

  input clk;  
  input [63:0] a, b;
  output [63:0] result;

  wire [63:0] a1, b1;
  wire exp_comp, exp_comp1, exp_comp2, exp_comp3;
  wire man_comp, man_comp1;
  wire sign_a1, sign_a2, sign_a3, sign_a4, sign_b1, sign_b2, sign_b3, sign_b4;
  wire [6:0] exp_dif_part;
  wire [2:0] exp_dif_part1;
  wire [10:0] exp_dif, exp_nor, exp_nor1, exp_nor2, exp_nor3, exp_nor4, exp_nor5, exp_nor6, exp_nor7, exp_nor10, exp_nor11;
  wire [11:0] exp_nor8, exp_nor9;
  wire [52:0] man_a, man_a1,  man_a_nor, man_a_nor1, man_a_nor2, man_a_nor3, man_a_nor4;
  wire [52:0] man_b, man_b1, man_b_nor, man_b_nor1, man_b_nor2, man_b_nor3, man_b_nor4;
  wire [53:0] man_sum, man_sum1, man_sum2, man_sum3;
  wire [52:0] man_sum_nor, man_sum_nor1, man_sum_nor2, man_sum_nor3;
  wire [51:0] man_sum_nor4;
  wire sign, sign1, sign2, sign3, sign4, sign5;
  wire [3:0] zero1_no, zero2_no, zero3_no, zero4_no, zero1_no1, zero2_no1, zero3_no1, zero4_no1;
  wire [5:0] zero_no, zero_no1;
  wire [1:0] zero_no_part;

  assign exp_comp = (a[62:52] > b[62:52]) ? 1'b1 : 1'b0;
  //
  a_PL_reg1 a_PL_reg1 (clk, a, b, exp_comp, a1, b1, exp_comp1);
  //
  assign exp_dif = exp_comp1 ? (a1[62:52] - b1[62:52]) : (b1[62:52] - a1[62:52]);
  assign exp_nor = exp_comp1 ? a1[62:52] : b1[62:52];
  assign man_a = a1[62:0] ? {1'b1, a1[51:0]} : {1'b0, a1[51:0]};
  assign man_b = b1[62:0] ? {1'b1, b1[51:0]} : {1'b0, b1[51:0]};
  //
  a_PL_reg2 a_PL_reg2 (clk, exp_comp1, exp_dif[6:0], exp_nor, a1[63], b1[63], man_a, man_b, exp_comp2, exp_dif_part, exp_nor1, sign_a1, sign_b1, man_a1, man_b1);
  //
  assign man_a_nor = exp_comp2 ? man_a1 : (man_a1 >> exp_dif_part[3:0]);
  assign man_b_nor = exp_comp2 ? (man_b1 >> exp_dif_part[3:0]) : man_b1;
  //
  a_PL_reg3 a_PL_reg3 (clk, exp_comp2, exp_dif_part[6:4], exp_nor1, sign_a1, sign_b1, man_a_nor, man_b_nor, exp_comp3, exp_dif_part1, exp_nor2, sign_a2, sign_b2, man_a_nor1, man_b_nor1);
  //
  a_mantissa_shift a_mantissa_shift (exp_comp3, exp_dif_part1, man_a_nor1, man_b_nor1, man_a_nor2, man_b_nor2);
  //
  a_PL_reg4 a_PL_reg4 (clk, exp_nor2, sign_a2, sign_b2, man_a_nor2, man_b_nor2, exp_nor3, sign_a3, sign_b3, man_a_nor3, man_b_nor3);
  //
  assign man_comp = (man_a_nor3 > man_b_nor3) ? 1'b1 : 1'b0;
  //
  a_PL_reg5 a_PL_reg5 (clk, man_comp, exp_nor3, sign_a3, sign_b3, man_a_nor3, man_b_nor3, man_comp1, exp_nor4, sign_a4, sign_b4, man_a_nor4, man_b_nor4);
  //
  a_adding a_adding (man_comp1, sign_a4, man_a_nor4, sign_b4, man_b_nor4, man_sum);
  assign sign = man_comp1 ? sign_a4 : sign_b4;
  //
  a_PL_reg6 a_PL_reg6 (clk, exp_nor4, sign, man_sum, exp_nor5, sign1, man_sum1);
  //
  a_leading_zero a_leading_zero1 (man_sum1[51:39], zero1_no);
  a_leading_zero a_leading_zero2 (man_sum1[38:26], zero2_no);
  a_leading_zero a_leading_zero3 (man_sum1[25:13], zero3_no);
  a_leading_zero a_leading_zero4 (man_sum1[12:0], zero4_no);
  //
  a_PL_reg7 a_PL_reg7 (clk, sign1, exp_nor5, man_sum1, zero1_no, zero2_no, zero3_no, zero4_no, sign2, exp_nor6, man_sum2, zero1_no1, zero2_no1, zero3_no1, zero4_no1);
  //
  a_leading_zero_cal a_leading_zero_cal (man_sum2[53:52], zero1_no1, zero2_no1, zero3_no1, zero4_no1, zero_no);
  //
  a_PL_reg8 a_PL_reg8 (clk, sign2, exp_nor6, man_sum2, zero_no, sign3, exp_nor7, man_sum3, zero_no1);
  //
  assign man_sum_nor = man_sum3[52:0] << zero_no1[3:0];
  assign exp_nor8 = (man_sum3 == 0) ? 12'b0 : (exp_nor7 - zero_no1 + 1'b1);
  //
  a_PL_reg9 a_PL_reg9 (clk, sign3, exp_nor8, man_sum_nor, zero_no1[5:4], sign4, exp_nor9, man_sum_nor1, zero_no_part);
  //
  a_normalise a_normalise (zero_no_part, man_sum_nor1, man_sum_nor2);
  assign exp_nor10 = exp_nor9[11] ? 11'h7ff : exp_nor9[10:0];
  //
  a_PL_reg10 a_PL_reg10 (clk, sign4, exp_nor10, man_sum_nor2, sign5, exp_nor11, man_sum_nor3);
  //
  assign man_sum_nor4 = man_sum_nor3[0] ? {man_sum_nor3[52:2], 1'b1} : man_sum_nor3[52:1];
  assign  result = {sign5, exp_nor11, man_sum_nor4};
 
endmodule