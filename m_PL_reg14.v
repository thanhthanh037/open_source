module m_PL_reg14 (clk, sign13, exp_nor, normalise1_result, zero_no_msb, sign14, exp_nor1, normalise1_result1, zero_no_msb1);

  input clk;
  input sign13;  
  input [11:0] exp_nor;
  input [104:0] normalise1_result;
  input zero_no_msb;
  output reg sign14;  
  output reg [11:0] exp_nor1;
  output reg [104:0] normalise1_result1;
  output reg zero_no_msb1;
  
  always@ (posedge clk)
    begin
      sign14 <= sign13;
      exp_nor1 <= exp_nor;
      normalise1_result1 <= normalise1_result;
		  zero_no_msb1 <= zero_no_msb;
    end

endmodule  