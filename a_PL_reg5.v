module a_PL_reg5 (clk, man_comp, exp_nor3, sign_a3, sign_b3, man_a_nor3, man_b_nor3, man_comp1, exp_nor4, sign_a4, sign_b4, man_a_nor4, man_b_nor4);

  input clk;
  input man_comp, sign_a3, sign_b3;
  input [10:0] exp_nor3;
  input [52:0] man_a_nor3, man_b_nor3;
  output reg man_comp1, sign_a4, sign_b4;
  output reg [10:0] exp_nor4;
  output reg [52:0] man_a_nor4, man_b_nor4;

  always@ (posedge clk)
  begin
    man_comp1 <= man_comp;
    sign_a4 <= sign_a3;
    sign_b4 <= sign_b3;
    exp_nor4 <= exp_nor3;
    man_a_nor4 <= man_a_nor3;
    man_b_nor4 <= man_b_nor3;
  end

endmodule
