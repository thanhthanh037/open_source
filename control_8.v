//////////////////////////////////////////////////////////////////////////////////
// Company: 	UoA
// Engineer: 	Thanh Bui
// 
// Create Date:    16:09:51 03/23/2017 
// Design Name: 
// Module Name:    control_8 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "parameter.v"
module control_8(clk, rst, we_i, output_start, re_i, re_w, re_co, we_o, we_oi, rst_acc, done_hidden, done_output);
 
  input clk, rst, we_i, output_start;
  output reg re_i, re_w, re_co, we_o, we_oi, rst_acc, done_hidden, done_output;
  
  reg [2:0] state;
  reg [9:0] count, neuron;
  reg [3:0] result_no;

  always@ (posedge clk)
  begin
    if (rst)
	  begin
        re_i <= 0;
        re_w <= 0;
	    re_co <= 0;
        we_o <= 0;
		we_oi <= 0;
        rst_acc <= 1;
		done_hidden <= 0;
        done_output <= 0;
        result_no <= 0;		  
	    state <= 0;
	  end
	else
	   case (state)
		  0:
		    begin
			    re_i <= 0;
				re_w <= 0;
				re_co <= 0;
				we_o <= 0;
				we_oi <= 0;
				rst_acc <= 1;
				done_output <= 0;
				result_no <= 0;
				if (we_i)
				  begin
				    count <= 0;
					neuron <= 0;
				    re_i <= 1;
				    re_w <= 1;					 
				    state <= 1;
				  end
			end
		  1:
		    begin	
				we_o <= 0;				
			    count <= count + 1'b1;
				if (count == 4)
				  rst_acc <= 0;
                if (count == 97)
				  begin 
				    re_i <= 0;
				    re_w <= 0;
                  end				
				if (count == 103)
				  begin
				    rst_acc <= 1;
				    we_o <= 1;
				    neuron <= neuron + 1'b1;
				    count <= 0;
					 if (neuron == op_per_neuron_hidden - 1'b1)
					   begin 
				        re_i <= 0;
				        re_w <= 0;
                        done_hidden <= 1;						  
					    state <= 2;
						end
					 else
					   begin
				        re_i <= 1;
				        re_w <= 1;
                       end						  
				  end
			 end
        2:
		    begin
			   we_o <= 0;
			   done_hidden <= 0;
               if (output_start)
				  begin
				    re_co <= 1;
					state <= 3;
				  end
			end
		3:
		    begin
			   we_oi <= 1;
			   count <= count + 1;
				if (count == op_per_neuron_hidden - 1'b1)				
				  begin
				    re_co <= 0;
					count <= 0;
				    re_i <= 1;
				    re_w <= 1;					 
				    state <= 4;
				  end	
			end
        4:
          begin		
            we_oi <= 0;				
			   we_o <= 0;
				count <= count + 1'b1;
				if (count == 4)
				  rst_acc <= 0;				
				if (count == 63)
				  begin 
				    re_i <= 0;
				    re_w <= 0;
                  end	
				if (count == 69)				  
				  begin
				    rst_acc <= 1;
				    we_o <= 1;
					result_no <= result_no + 1'b1;
					count <= 0;
					if (result_no == op_per_neuron_output - 1'b1)
					   begin
						  done_output <= 1;
                          state <= 0;
					   end
					else 
				       begin 
				        re_i <= 1;
				        re_w <= 1;
                       end						 
				  end
		  end
		default: state <= 0;
      endcase
  end				
		  
endmodule
