module m_PL_reg10 (clk, sign9, exp9, mult_result, sign10, exp10, mult_result1);
  
  input   clk;
  input   sign9;
  input   [11:0] exp9;
  input   [105:0] mult_result;
  output reg sign10;
  output reg [11:0] exp10;
  output reg [105:0] mult_result1;

  always@ (posedge clk)
    begin
      sign10 <= sign9;
      exp10 <= exp9;
      mult_result1 <= mult_result;
    end

endmodule


