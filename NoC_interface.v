`include "parameter.v"
module NoC_interface (clk, rst, isend, iaccept, iflit, send_pe, send_pc,
                      dest, a_or_b, sum_real, sum_img, dif_real, dif_img,
                      ar_rdata, ai_rdata, br_rdata, bi_rdata, 
                      osend, oaccept, oflit, op_per_pe, no_of_stage, no_of_pe,
                      start, we_a, we_b, we_w, re_a, re_b,
                      ar_wdata, ai_wdata, br_wdata, bi_wdata, wr_wdata, wi_wdata);                       

  input clk, rst, isend, iaccept;  
  input [flit_data_width-1:0] iflit; 
  input send_pe, send_pc, a_or_b;
  input [63:0] sum_real, sum_img, dif_real, dif_img, ar_rdata, ai_rdata, br_rdata, bi_rdata;
  input [1:0] dest; 
  output reg osend, oaccept;
  output reg [flit_data_width-1:0] oflit;
  output reg [11:0] op_per_pe;
  output reg [4:0] no_of_stage;
  output reg [2:0] no_of_pe;
  output reg start, we_a, we_b, we_w, re_a, re_b;  
  output reg [flit_data_width-67:0] ar_wdata, ai_wdata, br_wdata, bi_wdata, wr_wdata, wi_wdata;
 	
  reg [2:0] r_state;
  reg [2:0] t_state;
  reg [11:0] count_r, count_t;
  reg [5:0] route;

  //receiver
  always@ (posedge clk)
  begin
    if (rst)
      r_state <= 0;
    else
      case (r_state)
        0:
          begin
            oaccept <= 0;
            start <= 0; 
            count_r <= 0;
            we_a = 0;
            we_b = 0;
            we_w = 0;
            if (isend)
              begin
                oaccept <= 1;
                r_state <= 1;
              end
          end       
        1:
          begin
            //oaccept <= 0;
            if (iflit[flit_data_width-1: flit_data_width-2] == 2'b11)
              begin
                op_per_pe <= iflit[flit_data_width-3:flit_data_width-14];
                no_of_stage <= iflit[flit_data_width-15:flit_data_width-19];
                no_of_pe <= iflit[flit_data_width-20:flit_data_width-22]; 
                r_state <= 0;
              end
            else
              case (iflit[flit_data_width-3:flit_data_width-4])
                2'b00: r_state <= 2;
                2'b01: r_state <= 3;
                2'b10: r_state <= 4;
                default: r_state <= 0;
              endcase
          end
        2:
          if (!iflit[flit_data_width-1])
            begin
              we_a = 1;
              ar_wdata = iflit[flit_data_width-3:flit_data_width-66];
              ai_wdata = iflit[flit_data_width-67:0];        
              r_state <= 5; 
            end
        5:
          begin
            ar_wdata = iflit[flit_data_width-3:flit_data_width-66];
            ai_wdata = iflit[flit_data_width-67:0];
            if (iflit[flit_data_width-2])
              r_state <= 0;
          end 
        3:
          if (!iflit[flit_data_width-1])
            begin
              we_b = 1;
              br_wdata = iflit[flit_data_width-3:flit_data_width-66];
              bi_wdata = iflit[flit_data_width-67:0];  
              count_r <= count_r + 1'b1;         
              if (count_r == op_per_pe)
                begin
                  start <= 1;
                  r_state <= 0;
                end
              else
                r_state <= 6;
            end
        6:
          begin
            br_wdata = iflit[flit_data_width-3:flit_data_width-66];
            bi_wdata = iflit[flit_data_width-67:0];
            count_r <= count_r + 1'b1;//count the iflit b data 
            if (iflit[flit_data_width-2])
              begin
                if (count_r == op_per_pe)
                  start <= 1;
                r_state <= 0;
              end
          end 
        4:
          begin
            if (!iflit[flit_data_width-1])
              begin
                we_w = 1;
                wr_wdata = iflit[flit_data_width-3:flit_data_width-66];
                wi_wdata = iflit[flit_data_width-67:0];              
                r_state <= 7; 
              end
          end
        7:
          begin
            wr_wdata = iflit[flit_data_width-3:flit_data_width-66];
            wi_wdata = iflit[flit_data_width-67:0];
            if (iflit[flit_data_width-2])
              r_state <= 0;
          end 
        default: r_state <= 0;
      endcase
  end
  
  //transmiter
  always@ (*)
  begin
    case (dest)
      0: route = route1;
      1: route = route2;
      2: route = route3;
      3: route = route4;
      default: route = 6'b0;
    endcase
  end

  always@ (posedge clk)
  begin
    if (rst)
      t_state <= 0;
    else
      case (t_state)
        0:
          begin
            re_a <= 0;
            re_b <= 0;
            if (send_pe)
              begin
                osend <= 1;
                oflit <= {3'b100, a_or_b, 120'b0, route};
                t_state <= 1;
              end
            else if (send_pc)
              begin
                osend <= 1;
                oflit <= {2'b10, source_pe_no, 2'b11, 117'b0, 4'b1011};
                t_state <= 2;
              end
          end
        1:
          begin
            osend <= 0;
            if (iaccept && !send_pe)
              begin
                count_t <= 0;
                t_state <= 3;
              end
          end
        2:
          begin
            osend <= 0;
            if (iaccept && !send_pc)
              begin
                count_t <= 0;
                re_a <= 1;
                t_state <= 4;
              end
          end
        3:
          begin
            count_t <= count_t + 1'b1;
            if (count_t == op_per_pe)
              begin
                oflit <= {2'b01, dif_real, dif_img};
                t_state <= 0;
              end
            else
              oflit <= {2'b00, dif_real, dif_img};
          end
        4:
          begin
            count_t <= count_t + 1'b1;
            oflit <= {2'b00, ar_rdata, ai_rdata};
            if (count_t == op_per_pe)
              begin
                count_t <= 0;
                re_a <= 0;
                re_b <= 1;
                t_state <= 5;
              end             
          end  
        5:
          begin
            count_t <= count_t + 1'b1;
            if (count_t == 0)
              oflit <= {2'b00, ar_rdata, ai_rdata};
            else
              oflit <= {2'b00, br_rdata, bi_rdata};            
            if (count_t == op_per_pe)
              begin                
                count_t <= 0;
                re_b <= 0;
                t_state <= 6;
              end
          end 
        6:
          begin
            oflit <= {2'b01, br_rdata, bi_rdata};   
            t_state <= 0;
          end    
        default: t_state <= 0;
      endcase
  end  
         
endmodule