module RAM (clk, rst, rd_en, wr_en, wr2_en, idata1, idata2, odata);
    
  parameter [6:0] data_width = 64;
  parameter [3:0] address_width = 12;
  parameter [12:0] RAM_size = 4096;
  
  input clk, rst;
  input rd_en, wr_en, wr2_en;
  input [data_width-1:0] idata1, idata2;
  output reg [data_width-1:0] odata;
  
  reg [data_width-1:0] RAM[RAM_size-1:0];
  reg [address_width-1:0] rd_ptr, wr_ptr;

  //write pointer
  always@ (posedge clk)
  begin
    if (rst)
      wr_ptr <= 0;          
    else if (wr_en) 
      begin
        if (wr_ptr == RAM_size-1)
          wr_ptr <= 0;
        else
          wr_ptr <= wr_ptr + 1'b1;
      end
    else if (wr2_en) 
      begin
        if (wr_ptr == RAM_size-2)
          wr_ptr <= 0;
        else
          wr_ptr <= wr_ptr + 2'b10;
      end
  end
  
  // read pointer  
  always@ (posedge clk)
  begin
    if (rst)
      rd_ptr <= 0;          
    else if (rd_en)
      begin
        if (rd_ptr == RAM_size-1)
          rd_ptr <= 0;
        else        
          rd_ptr <= rd_ptr + 1'b1;
      end
  end
  
  //write operation
  always@ (posedge clk)
  begin
    if (wr_en)
      RAM[wr_ptr] <= idata1;
    else if (wr2_en)
      begin
        RAM[wr_ptr] <= idata1;
        RAM[wr_ptr + 1'b1] <= idata2;
      end
  end
  
  //read operation
  always@ (posedge clk)
  begin
    //if (rd_en)    
      odata <= RAM[rd_ptr];
  end

 endmodule