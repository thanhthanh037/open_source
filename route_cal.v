module route_cal (clk, rst, router_type, x_router, y_router, new_packet, broadcast, x_source, y_source, x_target, y_target, dir, new_dir);
  
  input clk, rst, new_packet, broadcast;
  input [3:0] router_type;
  input [2:0] x_router, y_router, x_source, y_source, x_target, y_target;
  output reg [4:0] dir; 
  output reg new_dir;
  
  reg [4:0] dir_temp; 
  wire [2:0] temp1, temp2, temp3, temp4;
  wire [3:0] temp5, temp6, temp7, temp8;
  
  assign temp1 = x_router - x_source;
  assign temp2 = x_source - x_router; 
  assign temp3 = y_router - y_source;
  assign temp4 = y_source - y_router;   
  assign temp5 = temp1 + temp3;
  assign temp6 = temp1 + temp4;
  assign temp7 = temp2 + temp3;
  assign temp8 = temp2 + temp4;
  
  always@ (posedge clk) 
  begin
    if (rst) 
      dir_temp <= NA;
	 else if (new_packet)
	 begin
	   new_dir <= 1;
	   if (broadcast)
	   begin
		  if (x_router == x_source && y_router == y_source)
          dir_temp <= NSEW;			 
		  else if (x_router > x_source)
		    begin
		      if (y_router > y_source)
		        begin
			       if (temp5[0])
				      dir_temp <= NP;
				    else
				      dir_temp <= EP;
				  end
				else if (y_router < y_source)
				  begin
				    if (temp6[0])
					   dir_temp <= EP;
					 else
					   dir_temp <= SP;
				  end
				else if (y_router == y_source)
				  begin
				    if (temp1[0])
				      dir_temp <= NEP;
				    else
				      dir_temp <= SEP;
				  end
			 end
			 
		  else if (x_router < x_source)
		    begin
		      if (y_router > y_source)
		        begin
			       if (temp7[0])
				      dir_temp <= WP;
				    else
				      dir_temp <= NP;
				  end
				else if (y_router < y_source)
				  begin
				    if (temp8[0])
					   dir_temp <= SP;
					 else
					   dir_temp <= WP;
				  end
				else if (y_router == y_source)
				  begin
				    if (temp2[0])
				      dir_temp <= SWP;
				    else
				      dir_temp <= NWP;
				  end
			 end	
		  
		  else if (x_router == x_source)
		    begin
		      if (y_router > y_source)
		        begin
			       if (temp3[0])
				      dir_temp <= NWP;
				    else
				      dir_temp <= NEP;
				  end
				else if (y_router < y_source)
				  begin
				    if (temp4[0])
					   dir_temp <= SEP;
					 else
					   dir_temp <= SWP;
				  end
			 end
		end		  
		  
    else if (x_target < x_router)
      dir_temp <= W;
    else if (x_target > x_router)
      dir_temp <= E;
    else if (y_target < y_router)
      dir_temp <= S;
    else if (y_target > y_router)  
      dir_temp <= N;
    else if (y_target == y_router)  
      dir_temp <= P;
	 end
	 
    else
	 new_dir <= 0;  
  end
  
  always@(*)
  begin
    case (router_type)
		EN: 
		  case (dir_temp)
			 S: dir = NA;  
			 W: dir = NA;  
			 SP: dir = P;  
			 WP: dir = P;   
			 NWP: dir = NP;  
			 SEP: dir = EP;  
			 SWP: dir = P;  
			 NSEW: dir = NE;  
			 default: dir = dir_temp;
          endcase			  
		ES:
		  case (dir_temp)
			 N: dir = NA;  
			 W: dir = NA;  
			 NP: dir = P;  
			 WP: dir = P;   
			 NEP: dir = EP;  
			 NWP: dir = P;  
			 SWP: dir = SP;  
			 NSEW: dir = SE;  
			 default: dir = dir_temp;
          endcase
		WN: 
		  case (dir_temp)
			 S: dir = NA;  
			 E: dir = NA;  
			 SP: dir = P;  
			 EP: dir = P;  
			 NEP: dir = NP;  
			 SEP: dir = P;  
			 SWP: dir = WP;  
			 NSEW: dir = NW;  
			 default: dir = dir_temp;
          endcase
		WS: 
		  case (dir_temp)
			 N: dir = NA;  
			 E: dir = NA;  
			 NP: dir = P;  
			 EP: dir = P;  
			 NEP: dir = P;  
			 NWP: dir = WP;  
			 SEP: dir = SP;  
			 NSEW: dir = SW;  
			 default: dir = dir_temp;
        endcase
		NES: 
		  case (dir_temp)
			 W: dir = NA;  
			 WP: dir = P;   
			 NWP: dir = NP;  
			 SWP: dir = SP;  
			 NSEW: dir = NSE;  
			 default: dir = dir_temp;
          endcase
		NWS: 
		  case (dir_temp)
			 E: dir = NA;  
			 EP: dir = P;  
			 NEP: dir = NP;  
			 SEP: dir = SP;  
			 NSEW: dir = NSW;  
			 default: dir = dir_temp;
          endcase
		NWE: 
		  case (dir_temp)
			 S: dir = NA;  
			 SP: dir = P;  
			 SEP: dir = EP;  
			 SWP: dir = WP;  
			 NSEW: dir = NEW;  
			 default: dir = dir_temp;
          endcase
		SWE: 
		  case (dir_temp)
			 N: dir = NA;  
			 NP: dir = P;  
			 NEP: dir = EP;  
			 NWP: dir = WP;  
			 NSEW: dir = SEW;  
			 default: dir = dir_temp;
          endcase
		default: dir = dir_temp;
	 endcase
  end
  
endmodule  