module m_PL_reg2 (clk, sign1, exp1, a1, b1, sum1, sign2, exp2, a2, b2, sum1_1); 
  
  input   clk;
  input   sign1;
  input   [11:0] exp1;
  input   [52:0] a1, b1;
  input   [104:0] sum1;
  output reg  sign2;
  output reg [11:0] exp2;
  output reg [52:0] a2, b2;
  output reg [104:0] sum1_1;

  always@ (posedge clk)
    begin
      sign2 <= sign1;
      exp2 <= exp1;
      a2 <= a1;
      b2 <= b1;
      sum1_1 <= sum1;
    end

endmodule

