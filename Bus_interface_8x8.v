`include "parameter.v"
module Bus_interface_8x8 (clk, rst, source_node,
						  wgr, broadcast, we, idata, 
                          done_hidden, done_output, rdata_1, rdata_2, rdata_3, rdata_4, rdata_5, rdata_6, rdata_7, rdata_8,		    
						  w_start, we_i, we_w_1, we_w_2, we_w_3, we_w_4, we_w_5, we_w_6, we_w_7, we_w_8,
						  re_o, waddr_i, wdata,
						  wrq, waddr_o, odata);   

  input clk, rst;  
  input [5:0] source_node; 
  input wgr, broadcast, we;
  input [data_width-1:0] idata;
  input done_hidden, done_output;
  input [11:0] rdata_1, rdata_2, rdata_3, rdata_4, rdata_5, rdata_6, rdata_7, rdata_8; 
  output reg [data_width-1:0] wdata;
  output reg w_start, we_i, we_w_1, we_w_2, we_w_3, we_w_4, we_w_5, we_w_6, we_w_7, we_w_8, re_o, wrq;  
  output reg [5:0] waddr_i;
  output reg waddr_o;
  output reg [data_width-1:0] odata; 
  
  reg  r_state;
  reg [2:0] t_state;
  reg [6:0] count_t;
  reg [2:0] count_r;
  
  //receiver
  always@ (posedge clk)
  begin
    if (rst)
      r_state <= 0;
    else
      case (r_state)
        0:
          begin
            we_i <= 0;
            we_w_1 <= 0;
		    we_w_2 <= 0;
		    we_w_3 <= 0;
			we_w_4 <= 0;
            we_w_5 <= 0;
			we_w_6 <= 0;
			we_w_7 <= 0;
			we_w_8 <= 0;
			w_start <= 1;
			count_r <= 0;
            if (we)
              begin
                if (broadcast)
				  begin
					w_start <= 0;
					we_i <= 1;
				  end
				else 
				  begin
					case(count_r)
					  0: we_w_1 <= 1;
					  1: we_w_2 <= 1;
					  2: we_w_3 <= 1;
					  3: we_w_4 <= 1;
					  4: we_w_5 <= 1;
					  5: we_w_6 <= 1;
					  6: we_w_7 <= 1;
					  7: we_w_8 <= 1;					 
					endcase
					if (count_r == 7)
					  count_r <= 0;
					else
					  count_r <= count_r + 1'b1; 
				  end
				r_state <= 1;
              end
          end       
        1:
		  begin			   
            if (!we)
			  begin
                we_i <= 0;
				we_w_1 <= 0;
				we_w_2 <= 0;
				we_w_3 <= 0;
				we_w_4 <= 0;
				we_w_5 <= 0;
				we_w_6 <= 0;
				we_w_7 <= 0;
				we_w_8 <= 0;
  				r_state <= 0;              
  			  end
			end
        default: r_state <= 0;
      endcase
	wdata <= idata;
    if (source_node == 0)
	  waddr_i <= 0;
	else
	  waddr_i <= source_node - 1'b1;
		 
  end
  
  //transmiter
  always@ (posedge clk)
  begin
    if (rst)
      t_state <= 0;
    else
      case (t_state)
        0:
          begin
            re_o <= 0;
            if (done_hidden)
              begin
                wrq <= 1;
                waddr_o <= 0;
                t_state <= 1;
              end
			else if (done_output)
              begin
                wrq <= 1;
                waddr_o <= 1;
                t_state <= 2;
              end				  
          end
        1:
          begin            
            if (wgr)
			  begin         
                count_t <= 0;
				re_o <= 1;
				t_state <= 3;
              end
          end
        3:
          begin
			odata = {4'b0, rdata_1, 4'b0, rdata_2, 4'b0, rdata_3, 4'b0, rdata_4, 4'b0, rdata_5, 4'b0, rdata_6, 4'b0, rdata_7, 4'b0, rdata_8};
            count_t <= count_t + 1'b1;
			if (count_t == op_per_neuron_hidden -1'b1)
			  begin
				re_o <= 0;
				wrq <= 0;
                t_state <= 0; 
              end					 
          end			 
        2:
          begin            
            if (wgr)
              begin
                count_t <= 0;
				re_o <= 1;
				t_state <= 4;
              end
          end	 
        4:
          begin
			odata = {4'b0, rdata_1, 4'b0, rdata_2, 4'b0, rdata_3, 4'b0, rdata_4, 4'b0, rdata_5, 4'b0, rdata_6, 4'b0, rdata_7, 4'b0, rdata_8};
            count_t <= count_t + 1'b1;
			if (count_t == op_per_neuron_output -1'b1)
			  begin
			    re_o <= 0;
                wrq <= 0;
				t_state <= 0;
              end        
          end
			 
        default: t_state <= 0;
      endcase
    	
  end  
  	    
  
endmodule