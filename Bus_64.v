`include "parameters.v"
module Bus_64 (clk, rst, wrq0, wrq1, wrq2, wrq3, wrq4, wrq5, wrq6, wrq7, wrq8, wrq9, 
               wrq10, wrq11, wrq12, wrq13, wrq14, wrq15, wrq16, wrq17, wrq18, wrq19,
			   wrq20, wrq21, wrq22, wrq23, wrq24, wrq25, wrq26, wrq27, wrq28, wrq29, 
			   wrq30, wrq31, wrq32, wrq33, wrq34, wrq35, wrq36, wrq37, wrq38, wrq39, 
			   wrq40, wrq41, wrq42, wrq43, wrq44, wrq45, wrq46, wrq47, wrq48, wrq49,
			   wrq50, wrq51, wrq52, wrq53, wrq54, wrq55, wrq56, wrq57, wrq58, wrq59, 
			   wrq60, wrq61, wrq62, wrq63, 				  
               waddr0, waddr1, waddr2, waddr3, waddr4, waddr5, waddr6, waddr7, waddr8, waddr9, 
               waddr10, waddr11, waddr12, waddr13, waddr14, waddr15, waddr16, waddr17, waddr18, waddr19,
			   waddr20, waddr21, waddr22, waddr23, waddr24, waddr25, waddr26, waddr27, waddr28, waddr29, 
			   waddr30, waddr31, waddr32, waddr33, waddr34, waddr35, waddr36, waddr37, waddr38, waddr39, 
			   waddr40, waddr41, waddr42, waddr43, waddr44, waddr45, waddr46, waddr47, waddr48, waddr49, 
			   waddr50, waddr51, waddr52, waddr53, waddr54, waddr55, waddr56, waddr57, waddr58, waddr59, 
			   waddr60, waddr61, waddr62, waddr63, 
               wdata_in0, wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, 
               wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, wdata_in17, wdata_in18, wdata_in19,
			   wdata_in20, wdata_in21, wdata_in22, wdata_in23, wdata_in24, wdata_in25, wdata_in26, wdata_in27, wdata_in28, wdata_in29, 
			   wdata_in30, wdata_in31, wdata_in32, wdata_in33, wdata_in34, wdata_in35, wdata_in36, wdata_in37, wdata_in38, wdata_in39, 
			   wdata_in40, wdata_in41, wdata_in42, wdata_in43, wdata_in44, wdata_in45, wdata_in46, wdata_in47, wdata_in48, wdata_in49, 
			   wdata_in50, wdata_in51, wdata_in52, wdata_in53, wdata_in54, wdata_in55, wdata_in56, wdata_in57, wdata_in58, wdata_in59, 
			   wdata_in60, wdata_in61, wdata_in62, wdata_in63,				  
               wgr0, wgr1, wgr2, wgr3, wgr4, wgr5, wgr6, wgr7, wgr8, wgr9, 
               wgr10, wgr11, wgr12, wgr13, wgr14, wgr15, wgr16, wgr17, wgr18, wgr19,
			   wgr20, wgr21, wgr22, wgr23, wgr24, wgr25, wgr26, wgr27, wgr28, wgr29, 
			   wgr30, wgr31, wgr32, wgr33, wgr34, wgr35, wgr36, wgr37, wgr38, wgr39, 
			   wgr40, wgr41, wgr42, wgr43, wgr44, wgr45, wgr46, wgr47, wgr48, wgr49, 
			   wgr50, wgr51, wgr52, wgr53, wgr54, wgr55, wgr56, wgr57, wgr58, wgr59, 
			   wgr60, wgr61, wgr62, wgr63,				  
			   we0, we1, we2, we3, we4, we5, we6, we7, we8, we9, 
               we10, we11, we12, we13, we14, we15, we16, we17, we18, we19,
			   we20, we21, we22, we23, we24, we25, we26, we27, we28, we29, 
			   we30, we31, we32, we33, we34, we35, we36, we37, we38, we39, 
			   we40, we41, we42, we43, we44, we45, we46, we47, we48, we49,
			   we50, we51, we52, we53, we54, we55, we56, we57, we58, we59, 
			   we60, we61, we62, we63,
			   broadcast,
			   source_node,
               wdata_out);
    
  input clk, rst;
  input wrq0, wrq1, wrq2, wrq3, wrq4, wrq5, wrq6, wrq7, wrq8, wrq9;
  input wrq10, wrq11, wrq12, wrq13, wrq14, wrq15, wrq16, wrq17, wrq18, wrq19;
  input wrq20, wrq21, wrq22, wrq23, wrq24, wrq25, wrq26, wrq27, wrq28, wrq29; 
  input wrq30, wrq31, wrq32, wrq33, wrq34, wrq35, wrq36, wrq37, wrq38, wrq39; 
  input wrq40, wrq41, wrq42, wrq43, wrq44, wrq45, wrq46, wrq47, wrq48, wrq49; 
  input wrq50, wrq51, wrq52, wrq53, wrq54, wrq55, wrq56, wrq57, wrq58, wrq59; 
  input wrq60, wrq61, wrq62, wrq63;  
  input [5:0] waddr0;
  input waddr1, waddr2, waddr3, waddr4, waddr5, waddr6, waddr7, waddr8, waddr9;
  input waddr10, waddr11, waddr12, waddr13, waddr14, waddr15, waddr16, waddr17, waddr18, waddr19;
  input waddr20, waddr21, waddr22, waddr23, waddr24, waddr25, waddr26, waddr27, waddr28, waddr29; 
  input waddr30, waddr31, waddr32, waddr33, waddr34, waddr35, waddr36, waddr37, waddr38, waddr39; 
  input waddr40, waddr41, waddr42, waddr43, waddr44, waddr45, waddr46, waddr47, waddr48, waddr49; 
  input waddr50, waddr51, waddr52, waddr53, waddr54, waddr55, waddr56, waddr57, waddr58, waddr59; 
  input waddr60, waddr61, waddr62, waddr63;
  input [data_width-1:0] wdata_in0;
  input [data_width-1:0] wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9;
  input [data_width-1:0] wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, wdata_in17, wdata_in18, wdata_in19;
  input [data_width-1:0] wdata_in20, wdata_in21, wdata_in22, wdata_in23, wdata_in24, wdata_in25, wdata_in26, wdata_in27, wdata_in28, wdata_in29; 
  input [data_width-1:0] wdata_in30, wdata_in31, wdata_in32, wdata_in33, wdata_in34, wdata_in35, wdata_in36, wdata_in37, wdata_in38, wdata_in39; 
  input [data_width-1:0] wdata_in40, wdata_in41, wdata_in42, wdata_in43, wdata_in44, wdata_in45, wdata_in46, wdata_in47, wdata_in48, wdata_in49; 
  input [data_width-1:0] wdata_in50, wdata_in51, wdata_in52, wdata_in53, wdata_in54, wdata_in55, wdata_in56, wdata_in57, wdata_in58, wdata_in59; 
  input [data_width-1:0] wdata_in60, wdata_in61, wdata_in62, wdata_in63; 
				  
  output reg wgr0, wgr1, wgr2, wgr3, wgr4, wgr5, wgr6, wgr7, wgr8, wgr9;
  output reg wgr10, wgr11, wgr12, wgr13, wgr14, wgr15, wgr16, wgr17, wgr18, wgr19;
  output reg wgr20, wgr21, wgr22, wgr23, wgr24, wgr25, wgr26, wgr27, wgr28, wgr29; 
  output reg wgr30, wgr31, wgr32, wgr33, wgr34, wgr35, wgr36, wgr37, wgr38, wgr39; 
  output reg wgr40, wgr41, wgr42, wgr43, wgr44, wgr45, wgr46, wgr47, wgr48, wgr49; 
  output reg wgr50, wgr51, wgr52, wgr53, wgr54, wgr55, wgr56, wgr57, wgr58, wgr59; 
  output reg wgr60, wgr61, wgr62, wgr63;
  output reg we0, we1, we2, we3, we4, we5, we6, we7, we8, we9;
  output reg we10, we11, we12, we13, we14, we15, we16, we17, we18, we19;
  output reg we20, we21, we22, we23, we24, we25, we26, we27, we28, we29; 
  output reg we30, we31, we32, we33, we34, we35, we36, we37, we38, we39; 
  output reg we40, we41, we42, we43, we44, we45, we46, we47, we48, we49;
  output reg we50, we51, we52, we53, we54, we55, we56, we57, we58, we59; 
  output reg we60, we61, we62, we63;  
  output reg broadcast;
  output reg [5:0] source_node;
  output reg [data_width-1:0] wdata_out;
  
  reg [6:0] state;
  reg count;
  
  always@ (posedge clk)
  if (rst) 
    state <= 0;
  else
    case (state)
	   0: 
		  begin
		    wgr0 <= 0;
		    wgr1 <= 0;
		    wgr2 <= 0;		
		    wgr3 <= 0;
		    wgr4 <= 0;	
		    wgr5 <= 0;
		    wgr6 <= 0;		
		    wgr7 <= 0;
		    wgr8 <= 0;	
		    wgr9 <= 0;
		    wgr10 <= 0;		
		    wgr11 <= 0;
		    wgr12 <= 0;	
		    wgr13 <= 0;
		    wgr14 <= 0;		
		    wgr15 <= 0;
			wgr16 <= 0;
		    wgr17 <= 0;
		    wgr18 <= 0;	
		    wgr19 <= 0;	
		    wgr20 <= 0;
		    wgr21 <= 0;
		    wgr22 <= 0;		
		    wgr23 <= 0;
		    wgr24 <= 0;	
		    wgr25 <= 0;
		    wgr26 <= 0;		
		    wgr27 <= 0;
		    wgr28 <= 0;	
		    wgr29 <= 0;
		    wgr30 <= 0;
		    wgr31 <= 0;
		    wgr32 <= 0;		
		    wgr33 <= 0;
		    wgr34 <= 0;	
		    wgr35 <= 0;
		    wgr36 <= 0;		
		    wgr37 <= 0;
		    wgr38 <= 0;	
		    wgr39 <= 0;
		    wgr40 <= 0;
		    wgr41 <= 0;
		    wgr42 <= 0;		
		    wgr43 <= 0;
		    wgr44 <= 0;	
		    wgr45 <= 0;
		    wgr46 <= 0;		
		    wgr47 <= 0;
		    wgr48 <= 0;	
		    wgr49 <= 0;
		    wgr50 <= 0;
		    wgr51 <= 0;
		    wgr52 <= 0;		
		    wgr53 <= 0;
		    wgr54 <= 0;	
		    wgr55 <= 0;
		    wgr56 <= 0;		
		    wgr57 <= 0;
		    wgr58 <= 0;	
		    wgr59 <= 0;
		    wgr60 <= 0;
		    wgr61 <= 0;
		    wgr62 <= 0;		
		    wgr63 <= 0;
		    			 
		    we0 <= 0;
		    we1 <= 0;
		    we2 <= 0;		
		    we3 <= 0;
		    we4 <= 0;	
		    we5 <= 0;
		    we6 <= 0;		
		    we7 <= 0;
		    we8 <= 0;	
		    we9 <= 0;
		    we10 <= 0;		
		    we11 <= 0;
		    we12 <= 0;	
		    we13 <= 0;
		    we14 <= 0;		
		    we15 <= 0;
			 we16 <= 0;
		    we17 <= 0;
		    we18 <= 0;	
		    we19 <= 0;	
		    we20 <= 0;
		    we21 <= 0;
		    we22 <= 0;		
		    we23 <= 0;
		    we24 <= 0;	
		    we25 <= 0;
		    we26 <= 0;		
		    we27 <= 0;
		    we28 <= 0;	
		    we29 <= 0;
		    we30 <= 0;
		    we31 <= 0;
		    we32 <= 0;		
		    we33 <= 0;
		    we34 <= 0;	
		    we35 <= 0;
		    we36 <= 0;		
		    we37 <= 0;
		    we38 <= 0;	
		    we39 <= 0;
		    we40 <= 0;
		    we41 <= 0;
		    we42 <= 0;		
		    we43 <= 0;
		    we44 <= 0;	
		    we45 <= 0;
		    we46 <= 0;		
		    we47 <= 0;
		    we48 <= 0;	
		    we49 <= 0;
		    we50 <= 0;
		    we51 <= 0;
		    we52 <= 0;		
		    we53 <= 0;
		    we54 <= 0;	
		    we55 <= 0;
		    we56 <= 0;		
		    we57 <= 0;
		    we58 <= 0;	
		    we59 <= 0;
		    we60 <= 0;
		    we61 <= 0;
		    we62 <= 0;		
		    we63 <= 0;
		    			 
			count <= 0;
            broadcast <= 0;			 
			  if (wrq0)
			    begin
				   wgr0 <= 1;
				   state <= 1;
				 end
			  else if (wrq1)
			    begin
				   wgr1 <= 1;
				   state <= 2;
				 end				 
			  else if (wrq2)
			    begin
				   wgr2 <= 1;
				   state <= 3;
				 end
			  else if (wrq3)
			    begin
				   wgr3 <= 1;
				   state <= 4;
				 end
			  else if (wrq4)
			    begin
				   wgr4 <= 1;
				   state <= 5;
				 end
			  else if (wrq5)
			    begin
				   wgr5 <= 1;
				   state <= 6;
				 end
			  else if (wrq6)
			    begin
				   wgr6 <= 1;
				   state <= 7;
				 end
			  else if (wrq7)
			    begin
				   wgr7 <= 1;
				   state <= 8;
				 end
			  else if (wrq8)
			    begin
				   wgr8 <= 1;
				   state <= 9;
				 end
			  else if (wrq9)
			    begin
				   wgr9 <= 1;
				   state <= 10;
				 end
			  else if (wrq10)
			    begin
				   wgr10 <= 1;
				   state <= 11;
				 end
			  else if (wrq11)
			    begin
				   wgr11 <= 1;
				   state <= 12;
				 end
			  else if (wrq12)
			    begin
				   wgr12 <= 1;
				   state <= 13;
				 end
			  else if (wrq13)
			    begin
				   wgr13 <= 1;
				   state <= 14;
				 end
			  else if (wrq14)
			    begin
				   wgr14 <= 1;
				   state <= 15;
				 end
			  else if (wrq15)
			    begin
				   wgr15 <= 1;
				   state <= 16;
				 end
			  else if (wrq16)
			    begin
				   wgr16 <= 1;
				   state <= 17;
				 end
			  else if (wrq17)
			    begin
				   wgr17 <= 1;
				   state <= 18;
				 end
			  else if (wrq18)
			    begin
				   wgr18 <= 1;
				   state <= 19;
				 end
			  else if (wrq19)
			    begin
				   wgr19 <= 1;
				   state <= 20;
				 end
			  else if (wrq20)
			    begin
				   wgr20 <= 1;
				   state <= 21;
				 end
			  else if (wrq21)
			    begin
				   wgr21 <= 1;
				   state <= 22;
				 end				 
			  else if (wrq22)
			    begin
				   wgr22 <= 1;
				   state <= 23;
				 end
			  else if (wrq23)
			    begin
				   wgr23 <= 1;
				   state <= 24;
				 end
			  else if (wrq24)
			    begin
				   wgr24 <= 1;
				   state <= 25;
				 end
			  else if (wrq25)
			    begin
				   wgr25 <= 1;
				   state <= 26;
				 end
			  else if (wrq26)
			    begin
				   wgr26 <= 1;
				   state <=27;
				 end
			  else if (wrq27)
			    begin
				   wgr27 <= 1;
				   state <= 28;
				 end
			  else if (wrq28)
			    begin
				   wgr28 <= 1;
				   state <= 29;
				 end
			  else if (wrq29)
			    begin
				   wgr29 <= 1;
				   state <= 30;
				 end	
			  else if (wrq30)
			    begin
				   wgr30 <= 1;
				   state <= 31;
				 end
			  else if (wrq31)
			    begin
				   wgr31 <= 1;
				   state <= 32;
				 end				 
			  else if (wrq32)
			    begin
				   wgr32 <= 1;
				   state <= 33;
				 end
			  else if (wrq33)
			    begin
				   wgr33 <= 1;
				   state <= 34;
				 end
			  else if (wrq34)
			    begin
				   wgr34 <= 1;
				   state <= 35;
				 end
			  else if (wrq35)
			    begin
				   wgr35 <= 1;
				   state <= 36;
				 end
			  else if (wrq36)
			    begin
				   wgr36 <= 1;
				   state <= 37;
				 end
			  else if (wrq37)
			    begin
				   wgr37 <= 1;
				   state <= 38;
				 end
			  else if (wrq38)
			    begin
				   wgr38 <= 1;
				   state <= 39;
				 end
			  else if (wrq39)
			    begin
				   wgr39 <= 1;
				   state <= 40;
				 end	
			  else if (wrq40)
			    begin
				   wgr40 <= 1;
				   state <= 41;
				 end
			  else if (wrq41)
			    begin
				   wgr41 <= 1;
				   state <= 42;
				 end				 
			  else if (wrq42)
			    begin
				   wgr42 <= 1;
				   state <= 43;
				 end
			  else if (wrq43)
			    begin
				   wgr43 <= 1;
				   state <= 44;
				 end
			  else if (wrq44)
			    begin
				   wgr44 <= 1;
				   state <= 45;
				 end
			  else if (wrq45)
			    begin
				   wgr45 <= 1;
				   state <= 46;
				 end
			  else if (wrq46)
			    begin
				   wgr46 <= 1;
				   state <= 47;
				 end
			  else if (wrq47)
			    begin
				   wgr47 <= 1;
				   state <= 48;
				 end
			  else if (wrq48)
			    begin
				   wgr48 <= 1;
				   state <= 49;
				 end
			  else if (wrq49)
			    begin
				   wgr49 <= 1;
				   state <= 50;
				 end	
			  else if (wrq50)
			    begin
				   wgr50 <= 1;
				   state <= 51;
				 end
			  else if (wrq51)
			    begin
				   wgr51 <= 1;
				   state <= 52;
				 end				 
			  else if (wrq52)
			    begin
				   wgr52 <= 1;
				   state <= 53;
				 end
			  else if (wrq53)
			    begin
				   wgr53 <= 1;
				   state <= 54;
				 end
			  else if (wrq54)
			    begin
				   wgr54 <= 1;
				   state <= 55;
				 end
			  else if (wrq55)
			    begin
				   wgr55 <= 1;
				   state <= 56;
				 end
			  else if (wrq56)
			    begin
				   wgr56 <= 1;
				   state <= 57;
				 end
			  else if (wrq57)
			    begin
				   wgr57 <= 1;
				   state <= 58;
				 end
			  else if (wrq58)
			    begin
				   wgr58 <= 1;
				   state <= 59;
				 end
			  else if (wrq59)
			    begin
				   wgr59 <= 1;
				   state <= 60;
				 end	
			  else if (wrq60)
			    begin
				   wgr60 <= 1;
				   state <= 61;
				 end
			  else if (wrq61)
			    begin
				   wgr61 <= 1;
				   state <= 62;
				 end				 
			  else if (wrq62)
			    begin
				   wgr62 <= 1;
				   state <= 63;
				 end
			  else if (wrq63)
			    begin
				   wgr63 <= 1;
				   state <= 64;
				 end	 
        end			 
      1:
		  begin
			 count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr0)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;							 
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;					 
					  end
					1: we1 <= 1;
					2: we2 <= 1;	
					3: we3 <= 1;
					4: we4 <= 1;	
					5: we5 <= 1;
					6: we6 <= 1;	
					7: we7 <= 1;
					8: we8 <= 1;	
					9: we9 <= 1;
					10: we10 <= 1;	
					11: we11 <= 1;
					12: we12 <= 1;	
					13: we13 <= 1;
					14: we14 <= 1;	
					15: we15 <= 1;
					16: we16 <= 1;	
					17: we17 <= 1;
					18: we18 <= 1;	
					19: we19 <= 1;
					20: we20 <= 1;	
					21: we21 <= 1;
					22: we22 <= 1;	
					23: we23 <= 1;
					24: we24 <= 1;	
					25: we25 <= 1;
					26: we26 <= 1;	
					27: we27 <= 1;
					28: we28 <= 1;	
					29: we29 <= 1;
					30: we30 <= 1;	
					31: we31 <= 1;
					32: we32 <= 1;	
					33: we33 <= 1;
					34: we34 <= 1;	
					35: we35 <= 1;
					36: we36 <= 1;	
					37: we37 <= 1;
					38: we38 <= 1;	
					39: we39 <= 1;	
					40: we40 <= 1;						
					41: we41 <= 1;
					42: we42 <= 1;	
					43: we43 <= 1;
					44: we44 <= 1;	
					45: we45 <= 1;
					46: we46 <= 1;	
					47: we47 <= 1;
					48: we48 <= 1;	
					49: we49 <= 1;	
					50: we50 <= 1;	
					51: we51 <= 1;
					52: we52 <= 1;	
					53: we53 <= 1;
					54: we54 <= 1;	
					55: we55 <= 1;
					56: we56 <= 1;	
					57: we57 <= 1;
					58: we58 <= 1;	
					59: we59 <= 1;	
					60: we60 <= 1;						
					61: we61 <= 1;
					62: we62 <= 1;	
					63: we63 <= 1;					
				  endcase
				end
			 source_node <= 0;
			 if (!wrq0)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				  
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end	
      2:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr1)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 1;
			 if (!wrq1)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      3:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr2)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 2;
			 if (!wrq2)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      4:
        begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr3)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 3;
			 if (!wrq3)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      5:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr4)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 4;
			 if (!wrq4)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      6:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr5)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 5;
			 if (!wrq5)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      7:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr6)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 6;
			 if (!wrq6)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      8:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr7)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 7;
			 if (!wrq7)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      9:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr8)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 8;
			 if (!wrq8)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      10:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr9)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 9;
			 if (!wrq9)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      11:
		  begin
			 count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr10)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;							 
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;					 
					  end
					1: we0 <= 1;		
				  endcase
				end
			 source_node <= 10;
			 if (!wrq10)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				  
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end	
      12:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr11)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 11;
			 if (!wrq11)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      13:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr12)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 12;
			 if (!wrq12)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      14:
        begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr13)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 13;
			 if (!wrq13)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      15:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr14)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 14;
			 if (!wrq14)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      16:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr15)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 15;
			 if (!wrq15)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      17:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr16)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 16;
			 if (!wrq16)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      18:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr17)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 17;
			 if (!wrq17)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      19:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr18)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 18;
			 if (!wrq18)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      20:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr19)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 19;
			 if (!wrq19)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end	
      21:
		  begin
			 count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr20)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;							 
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;					 
					  end
					1: we0 <= 1;
				  endcase
				end
			 source_node <= 20;
			 if (!wrq20)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				  
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end	
      22:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr21)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 21;
			 if (!wrq21)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      23:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr22)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 22;
			 if (!wrq22)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      24:
        begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr23)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 23;
			 if (!wrq23)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      25:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr24)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 24;
			 if (!wrq24)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      26:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr25)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 25;
			 if (!wrq25)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      27:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr26)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 26;
			 if (!wrq26)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      28:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr27)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 27;
			 if (!wrq27)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      29:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr28)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 28;
			 if (!wrq28)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      30:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr29)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 29;
			 if (!wrq29)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end	
      31:
		  begin
			 count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr30)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;							 
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;					 
					  end
					1: we0 <= 1;
				  endcase
				end
			 source_node <= 30;
			 if (!wrq30)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				  
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end	
      32:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr31)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 31;
			 if (!wrq31)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      33:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr32)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 32;
			 if (!wrq32)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      34:
        begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr33)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 33;
			 if (!wrq33)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      35:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr34)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 34;
			 if (!wrq34)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      36:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr35)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 35;
			 if (!wrq35)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      37:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr36)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 36;
			 if (!wrq36)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      38:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr37)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 37;
			 if (!wrq37)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      39:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr38)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 38;
			 if (!wrq38)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      40:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr39)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 39;
			 if (!wrq39)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end	
      41:
		  begin
			 count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr40)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;							 
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;					 
					  end
					1: we0 <= 1;
				  endcase
				end
			 source_node <= 40;
			 if (!wrq40)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				  
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end	
      42:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr41)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 41;
			 if (!wrq41)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      43:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr42)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 42;
			 if (!wrq42)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      44:
        begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr43)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 43;
			 if (!wrq43)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      45:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr44)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 44;
			 if (!wrq44)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      46:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr45)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 45;
			 if (!wrq45)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      47:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr46)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 46;
			 if (!wrq46)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      48:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr47)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 47;
			 if (!wrq47)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      49:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr48)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 48;
			 if (!wrq48)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      50:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr49)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 49;
			 if (!wrq49)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      51:
		  begin
			 count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr50)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;							 
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;					 
					  end
					1: we0 <= 1;
				  endcase
				end
			 source_node <= 50;
			 if (!wrq50)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				  
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end	
      52:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr51)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 51;
			 if (!wrq51)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      53:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr52)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 52;
			 if (!wrq52)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      54:
        begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr53)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 53;
			 if (!wrq53)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      55:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr54)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 54;
			 if (!wrq54)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      56:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr55)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 55;
			 if (!wrq55)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      57:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr56)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 56;
			 if (!wrq56)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      58:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr57)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 57;
			 if (!wrq57)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      59:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr58)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 58;
			 if (!wrq58)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      60:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr59)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 59;
			 if (!wrq59)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end	
      61:
		  begin
			 count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr60)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;							 
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;					 
					  end
					1: we0 <= 1;
				  endcase
				end
			 source_node <= 60;
			 if (!wrq40)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				  
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end	
      62:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr61)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 61;
			 if (!wrq61)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      63:
		  begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr62)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 62;
			 if (!wrq62)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
      64:
        begin
		    count <= count + 1'b1;
			 if (count == 1)
			   begin
				  count <= 0;
				  case(waddr63)
					0: 
					  begin
						 we0 <= 0;
						 we1 <= 1;
						 we2 <= 1;		
						 we3 <= 1;
						 we4 <= 1;	
						 we5 <= 1;
						 we6 <= 1;		
						 we7 <= 1;
						 we8 <= 1;	
						 we9 <= 1;
						 we10 <= 1;
						 we11 <= 1;		
						 we11 <= 1;
						 we12 <= 1;	
						 we13 <= 1;
						 we14 <= 1;		
						 we15 <= 1;
						 we16 <= 1;	
						 we17 <= 1;
						 we18 <= 1;	
						 we19 <= 1;	
						 we20 <= 1;
						 we21 <= 1;		
						 we21 <= 1;
						 we22 <= 1;	
						 we23 <= 1;
						 we24 <= 1;		
						 we25 <= 1;
						 we26 <= 1;	
						 we27 <= 1;
						 we28 <= 1;	
						 we29 <= 1;	
						 we30 <= 1;
						 we31 <= 1;		
						 we31 <= 1;
						 we32 <= 1;	
						 we33 <= 1;
						 we34 <= 1;		
						 we35 <= 1;
						 we36 <= 1;	
						 we37 <= 1;
						 we38 <= 1;	
						 we39 <= 1;	
						 we40 <= 1;
						 we41 <= 1;		
						 we41 <= 1;
						 we42 <= 1;	
						 we43 <= 1;
						 we44 <= 1;		
						 we45 <= 1;
						 we46 <= 1;	
						 we47 <= 1;
						 we48 <= 1;	
						 we49 <= 1;
						 we50 <= 1;
						 we51 <= 1;
						 we52 <= 1;		
						 we53 <= 1;
						 we54 <= 1;	
						 we55 <= 1;
						 we56 <= 1;		
						 we57 <= 1;
						 we58 <= 1;	
						 we59 <= 1;
						 we60 <= 1;
						 we61 <= 1;
						 we62 <= 1;		
						 we63 <= 1;
						 	
						 broadcast <= 1;						 
					  end
					1: we0 <= 1;
				  endcase
            end				  
			 source_node <= 63;
			 if (!wrq63)
			   begin
				  we0 <= 0;
				  we1 <= 0;
				  we2 <= 0;		
				  we3 <= 0;
				  we4 <= 0;	
				  we5 <= 0;
				  we6 <= 0;		
				  we7 <= 0;
				  we8 <= 0;	
				  we9 <= 0;
				  we10 <= 0;		
				  we11 <= 0;
				  we12 <= 0;	
				  we13 <= 0;
				  we14 <= 0;		
				  we15 <= 0;
				  we16 <= 0;
				  we16 <= 0;		
				  we17 <= 0;
				  we18 <= 0;	
				  we19 <= 0;	
				  we20 <= 0;
				  we21 <= 0;
				  we22 <= 0;		
				  we23 <= 0;
				  we24 <= 0;	
				  we25 <= 0;
				  we26 <= 0;		
				  we27 <= 0;
				  we28 <= 0;	
				  we29 <= 0;
				  we30 <= 0;
				  we31 <= 0;
				  we32 <= 0;		
				  we33 <= 0;
				  we34 <= 0;	
				  we35 <= 0;
				  we36 <= 0;		
				  we37 <= 0;
				  we38 <= 0;	
				  we39 <= 0;
				  we40 <= 0;
				  we41 <= 0;
				  we42 <= 0;		
				  we43 <= 0;
				  we44 <= 0;	
				  we45 <= 0;
				  we46 <= 0;		
				  we47 <= 0;
				  we48 <= 0;	
				  we49 <= 0;				
  	           we50 <= 0;
				  we51 <= 0;
				  we52 <= 0;		
				  we53 <= 0;
				  we54 <= 0;	
				  we55 <= 0;
				  we56 <= 0;		
				  we57 <= 0;
				  we58 <= 0;	
				  we59 <= 0;
				  we60 <= 0;
				  we61 <= 0;
				  we62 <= 0;		
				  we63 <= 0;
				  
				  state <= 0;
				end
        end
    endcase
 
  always@ (posedge clk)
  begin
    case (state)
	   1: wdata_out = wdata_in0;
	   2: wdata_out = wdata_in1;
	   3: wdata_out = wdata_in2;
	   4: wdata_out = wdata_in3;	
	   5: wdata_out = wdata_in4;
	   6: wdata_out = wdata_in5;	
	   7: wdata_out = wdata_in6;
	   8: wdata_out = wdata_in7;	
	   9: wdata_out = wdata_in8;
	   10: wdata_out = wdata_in9;	
	   11: wdata_out = wdata_in10;
	   12: wdata_out = wdata_in11;	
	   13: wdata_out = wdata_in12;
	   14: wdata_out = wdata_in13;	
	   15: wdata_out = wdata_in14;
	   16: wdata_out = wdata_in15;	
	   17: wdata_out = wdata_in16;
	   18: wdata_out = wdata_in17;	
	   19: wdata_out = wdata_in18;
	   20: wdata_out = wdata_in19;	
	   21: wdata_out = wdata_in20;
	   22: wdata_out = wdata_in21;	
	   23: wdata_out = wdata_in22;
	   24: wdata_out = wdata_in23;	
	   25: wdata_out = wdata_in24;
	   26: wdata_out = wdata_in25;	
	   27: wdata_out = wdata_in26;
	   28: wdata_out = wdata_in27;	
	   29: wdata_out = wdata_in28;
	   30: wdata_out = wdata_in29;
	   31: wdata_out = wdata_in30;
	   32: wdata_out = wdata_in31;	
	   33: wdata_out = wdata_in32;
	   34: wdata_out = wdata_in33;	
	   35: wdata_out = wdata_in34;
	   36: wdata_out = wdata_in35;	
	   37: wdata_out = wdata_in36;
	   38: wdata_out = wdata_in37;	
	   39: wdata_out = wdata_in38;
	   40: wdata_out = wdata_in39;		
	   41: wdata_out = wdata_in40;
	   42: wdata_out = wdata_in41;	
	   43: wdata_out = wdata_in42;
	   44: wdata_out = wdata_in43;	
	   45: wdata_out = wdata_in44;
	   46: wdata_out = wdata_in45;	
	   47: wdata_out = wdata_in46;
	   48: wdata_out = wdata_in47;	
	   49: wdata_out = wdata_in48;
	   50: wdata_out = wdata_in49;	
	   50: wdata_out = wdata_in49;	
	   51: wdata_out = wdata_in50;
	   52: wdata_out = wdata_in51;	
	   53: wdata_out = wdata_in52;
	   54: wdata_out = wdata_in53;	
	   55: wdata_out = wdata_in54;
	   56: wdata_out = wdata_in55;	
	   57: wdata_out = wdata_in56;
	   58: wdata_out = wdata_in57;	
	   59: wdata_out = wdata_in58;
	   60: wdata_out = wdata_in59;	
	   61: wdata_out = wdata_in60;
	   62: wdata_out = wdata_in61;	
	   63: wdata_out = wdata_in62;
	   64: wdata_out = wdata_in63;		
	   default: wdata_out = 0;
    endcase
  end	 
		
endmodule