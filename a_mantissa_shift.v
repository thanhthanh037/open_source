module a_mantissa_shift (exp_comp, exp_dif, man_a, man_b, man_a_nor, man_b_nor);// exp_dif here is just three bit 6,5,4
  
  input exp_comp;
  input [2:0] exp_dif;
  input [52:0] man_a, man_b;
  output reg [52:0] man_a_nor, man_b_nor;
  
  always@ (*)
  begin
    if (exp_comp)
      begin
        man_a_nor = man_a;
        if(exp_dif[2]) 
          man_b_nor = man_b >> 64; 
        else       
          case(exp_dif[1:0]) 
            2'b00: man_b_nor = man_b;
            2'b01: man_b_nor = man_b >> 16;
            2'b10: man_b_nor = man_b >> 32;  
            2'b11: man_b_nor = man_b >> 48;
            default: man_b_nor = man_b;
          endcase
        end
    else
      begin
        man_b_nor = man_b;
        if(exp_dif[2]) 
          man_a_nor = man_a >> 64; 
        else       
          case(exp_dif[1:0]) 
            2'b00: man_a_nor = man_a; 
            2'b01: man_a_nor = man_a >> 16;
            2'b10: man_a_nor = man_a >> 32;  
            2'b11: man_a_nor = man_a >> 48;
            default: man_a_nor = man_a;
          endcase
        end 
  end 
  
endmodule      
          
           
           
             
