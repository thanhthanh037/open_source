//----------------------------------------------------------------------------------------------------------------------
// File         : Bus_16_Channels.v
// Author       : Thanh Bui
// Organization : The University of Adelaide
// Created      : May 2015
// Platform     : VHDL 1993
// Simulators   : Modelsim
// Synthesizers : ISE 14.7
// Targets      : Virtex-7 VC707
// Description  : 16-channel bus customized for an FFT system of 16 PEs
//----------------------------------------------------------------------------------------------------------------------
`include "parameter.v"
module Bus_16_Channels (clk, rst, wrq1, wrq2, wrq3, wrq4, wrq5, wrq6, wrq7, wrq8, wrq9, wrq10, wrq11, wrq12, wrq13, wrq14, wrq15, wrq16,
                        waddr1, waddr2, waddr3, waddr4, waddr5, waddr6, waddr7, waddr8, waddr9, waddr10, waddr11, waddr12, waddr13, waddr14, waddr15, waddr16,
                        wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16,				  
                        wgr1, wgr2, wgr3, wgr4, wgr5, wgr6, wgr7, wgr8, wgr9, wgr10, wgr11, wgr12, wgr13, wgr14, wgr15, wgr16,
				        we1, we2, we3, we4, we5, we6, we7, we8, we9, we10, we11, we12, we13, we14, we15, we16,
                        wdata_out1, wdata_out2, wdata_out3, wdata_out4, wdata_out5, wdata_out6, wdata_out7, wdata_out8, wdata_out9, wdata_out10, wdata_out11, wdata_out12, wdata_out13, wdata_out14, wdata_out15, wdata_out16);
 
  input clk, rst, wrq1, wrq2, wrq3, wrq4, wrq5, wrq6, wrq7, wrq8, wrq9, wrq10, wrq11, wrq12, wrq13, wrq14, wrq15, wrq16;
  input [3:0] waddr1, waddr2, waddr3, waddr4, waddr5, waddr6, waddr7, waddr8, waddr9, waddr10, waddr11, waddr12, waddr13, waddr14, waddr15, waddr16;
  input [data_width-1:0] wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16;
  output wgr1, wgr2, wgr3, wgr4, wgr5, wgr6, wgr7, wgr8, wgr9, wgr10, wgr11, wgr12, wgr13, wgr14, wgr15, wgr16;
  output we1, we2, we3, we4, we5, we6, we7, we8, we9, we10, we11, we12, we13, we14, we15, we16;
  output [data_width-1:0] wdata_out1, wdata_out2, wdata_out3, wdata_out4, wdata_out5, wdata_out6, wdata_out7, wdata_out8, wdata_out9, wdata_out10, wdata_out11, wdata_out12, wdata_out13, wdata_out14, wdata_out15, wdata_out16;  

  reg wrq0102, wrq0103, wrq0104, wrq0105, wrq0106, wrq0107, wrq0108, wrq0109, wrq0110, wrq0111, wrq0112, wrq0113, wrq0114, wrq0115, wrq0116;
  reg wrq0201, wrq0203, wrq0204, wrq0205, wrq0206, wrq0207, wrq0208, wrq0209, wrq0210, wrq0211, wrq0212, wrq0213, wrq0214, wrq0215, wrq0216;
  reg wrq0301, wrq0302, wrq0304, wrq0305, wrq0306, wrq0307, wrq0308, wrq0309, wrq0310, wrq0311, wrq0312, wrq0313, wrq0314, wrq0315, wrq0316;
  reg wrq0401, wrq0402, wrq0403, wrq0405, wrq0406, wrq0407, wrq0408, wrq0409, wrq0410, wrq0411, wrq0412, wrq0413, wrq0414, wrq0415, wrq0416;
  reg wrq0501, wrq0502, wrq0503, wrq0504, wrq0506, wrq0507, wrq0508, wrq0509, wrq0510, wrq0511, wrq0512, wrq0513, wrq0514, wrq0515, wrq0516;
  reg wrq0601, wrq0602, wrq0603, wrq0604, wrq0605, wrq0607, wrq0608, wrq0609, wrq0610, wrq0611, wrq0612, wrq0613, wrq0614, wrq0615, wrq0616;
  reg wrq0701, wrq0702, wrq0703, wrq0704, wrq0705, wrq0706, wrq0708, wrq0709, wrq0710, wrq0711, wrq0712, wrq0713, wrq0714, wrq0715, wrq0716;
  reg wrq0801, wrq0802, wrq0803, wrq0804, wrq0805, wrq0806, wrq0807, wrq0809, wrq0810, wrq0811, wrq0812, wrq0813, wrq0814, wrq0815, wrq0816;
  reg wrq0901, wrq0902, wrq0903, wrq0904, wrq0905, wrq0906, wrq0907, wrq0908, wrq0910, wrq0911, wrq0912, wrq0913, wrq0914, wrq0915, wrq0916;
  reg wrq1001, wrq1002, wrq1003, wrq1004, wrq1005, wrq1006, wrq1007, wrq1008, wrq1009, wrq1011, wrq1012, wrq1013, wrq1014, wrq1015, wrq1016;
  reg wrq1101, wrq1102, wrq1103, wrq1104, wrq1105, wrq1106, wrq1107, wrq1108, wrq1109, wrq1110, wrq1112, wrq1113, wrq1114, wrq1115, wrq1116;
  reg wrq1201, wrq1202, wrq1203, wrq1204, wrq1205, wrq1206, wrq1207, wrq1208, wrq1209, wrq1210, wrq1211, wrq1213, wrq1214, wrq1215, wrq1216;
  reg wrq1301, wrq1302, wrq1303, wrq1304, wrq1305, wrq1306, wrq1307, wrq1308, wrq1309, wrq1310, wrq1311, wrq1312, wrq1314, wrq1315, wrq1316;
  reg wrq1401, wrq1402, wrq1403, wrq1404, wrq1405, wrq1406, wrq1407, wrq1408, wrq1409, wrq1410, wrq1411, wrq1412, wrq1413, wrq1415, wrq1416;
  reg wrq1501, wrq1502, wrq1503, wrq1504, wrq1505, wrq1506, wrq1507, wrq1508, wrq1509, wrq1510, wrq1511, wrq1512, wrq1513, wrq1514, wrq1516;
  reg wrq1601, wrq1602, wrq1603, wrq1604, wrq1605, wrq1606, wrq1607, wrq1608, wrq1609, wrq1610, wrq1611, wrq1612, wrq1613, wrq1614, wrq1615;
  
  wire wgr0102, wgr0103, wgr0104, wgr0105, wgr0106, wgr0107, wgr0108, wgr0109, wgr0110, wgr0111, wgr0112, wgr0113, wgr0114, wgr0115, wgr0116;
  wire wgr0201, wgr0203, wgr0204, wgr0205, wgr0206, wgr0207, wgr0208, wgr0209, wgr0210, wgr0211, wgr0212, wgr0213, wgr0214, wgr0215, wgr0216;
  wire wgr0301, wgr0302, wgr0304, wgr0305, wgr0306, wgr0307, wgr0308, wgr0309, wgr0310, wgr0311, wgr0312, wgr0313, wgr0314, wgr0315, wgr0316;
  wire wgr0401, wgr0402, wgr0403, wgr0405, wgr0406, wgr0407, wgr0408, wgr0409, wgr4010, wgr4011, wgr4012, wgr4013, wgr4014, wgr4015, wgr4016;
  wire wgr0501, wgr0502, wgr0503, wgr0504, wgr0506, wgr0507, wgr0508, wgr0509, wgr0510, wgr0511, wgr0512, wgr0513, wgr0514, wgr0515, wgr0516;
  wire wgr0601, wgr0602, wgr0603, wgr0604, wgr0605, wgr0607, wgr0608, wgr0609, wgr0610, wgr0611, wgr0612, wgr0613, wgr0614, wgr0615, wgr0616;
  wire wgr0701, wgr0702, wgr0703, wgr0704, wgr0705, wgr0706, wgr0708, wgr0709, wgr0710, wgr0711, wgr0712, wgr0713, wgr0714, wgr0715, wgr0716;
  wire wgr0801, wgr0802, wgr0803, wgr0804, wgr0805, wgr0806, wgr0807, wgr0809, wgr0810, wgr0811, wgr0812, wgr0813, wgr0814, wgr0815, wgr0816;
  wire wgr0901, wgr0902, wgr0903, wgr0904, wgr0905, wgr0906, wgr0907, wgr0908, wgr0910, wgr0911, wgr0912, wgr0913, wgr0914, wgr0915, wgr0916;
  wire wgr1001, wgr1002, wgr1003, wgr1004, wgr1005, wgr1006, wgr1007, wgr1008, wgr1009, wgr1011, wgr1012, wgr1013, wgr1014, wgr1015, wgr1016;
  wire wgr1101, wgr1102, wgr1103, wgr1104, wgr1105, wgr1106, wgr1107, wgr1108, wgr1109, wgr1110, wgr1112, wgr1113, wgr1114, wgr1115, wgr1116;
  wire wgr1201, wgr1202, wgr1203, wgr1204, wgr1205, wgr1206, wgr1207, wgr1208, wgr1209, wgr1210, wgr1211, wgr1213, wgr1214, wgr1215, wgr1216;
  wire wgr1301, wgr1302, wgr1303, wgr1304, wgr1305, wgr1306, wgr1307, wgr1308, wgr1309, wgr1310, wgr1311, wgr1312, wgr1314, wgr1315, wgr1316;
  wire wgr1401, wgr1402, wgr1403, wgr1404, wgr1405, wgr1406, wgr1407, wgr1408, wgr1409, wgr1410, wgr1411, wgr1412, wgr1413, wgr1415, wgr1416;
  wire wgr1501, wgr1502, wgr1503, wgr1504, wgr1505, wgr1506, wgr1507, wgr1508, wgr1509, wgr1510, wgr1511, wgr1512, wgr1513, wgr1514, wgr1516;
  wire wgr1601, wgr1602, wgr1603, wgr1604, wgr1605, wgr1606, wgr1607, wgr1608, wgr1609, wgr1610, wgr1611, wgr1612, wgr1613, wgr1614, wgr1615;   

  always@ (posedge clk)
  if (wrq1)
    case (waddr1)	   
	   4'b0001: wrq0102 <= 1;
	   4'b0010: wrq0103 <= 1;		
	   4'b0011: wrq0104 <= 1;
	   4'b0100: wrq0105 <= 1;
	   4'b0101: wrq0106 <= 1;
	   4'b0110: wrq0107 <= 1;		
	   4'b0111: wrq0108 <= 1;
	   4'b1000: wrq0109 <= 1;
	   4'b1001: wrq0110 <= 1;
	   4'b1010: wrq0111 <= 1;		
	   4'b1011: wrq0112 <= 1;
	   4'b1100: wrq0113 <= 1;
	   4'b1101: wrq0114 <= 1;
	   4'b1110: wrq0115 <= 1;		
	   4'b1111: wrq0116 <= 1;
    endcase
  else
	begin
	   wrq0102 <= 0;
	   wrq0103 <= 0;		
	   wrq0104 <= 0;
	   wrq0105 <= 0;
	   wrq0106 <= 0;
	   wrq0107 <= 0;		
	   wrq0108 <= 0;
	   wrq0109 <= 0;
	   wrq0110 <= 0;
	   wrq0111 <= 0;		
	   wrq0112 <= 0;
	   wrq0113 <= 0;
	   wrq0114 <= 0;
	   wrq0115 <= 0;		
	   wrq0116 <= 0;	 
	end

  always@ (posedge clk)
  if (wrq2)
    case (waddr2)	   
	   4'b0000: wrq0201 <= 1;
	   4'b0010: wrq0203 <= 1;		
	   4'b0011: wrq0204 <= 1;
	   4'b0100: wrq0205 <= 1;
	   4'b0101: wrq0206 <= 1;
	   4'b0110: wrq0207 <= 1;		
	   4'b0111: wrq0208 <= 1;
	   4'b1000: wrq0209 <= 1;
	   4'b1001: wrq0210 <= 1;
	   4'b1010: wrq0211 <= 1;		
	   4'b1011: wrq0212 <= 1;
	   4'b1100: wrq0213 <= 1;
	   4'b1101: wrq0214 <= 1;
	   4'b1110: wrq0215 <= 1;		
	   4'b1111: wrq0216 <= 1;
    endcase
  else
	begin
	   wrq0201 <= 0;
	   wrq0203 <= 0;		
	   wrq0204 <= 0;
	   wrq0205 <= 0;
	   wrq0206 <= 0;
	   wrq0207 <= 0;		
	   wrq0208 <= 0;
	   wrq0209 <= 0;
	   wrq0210 <= 0;
	   wrq0211 <= 0;		
	   wrq0212 <= 0;
	   wrq0213 <= 0;
	   wrq0214 <= 0;
	   wrq0215 <= 0;		
	   wrq0216 <= 0;	 
	end

  always@ (posedge clk)
  if (wrq3)
    case (waddr3)	   
	   4'b0000: wrq0301 <= 1;
	   4'b0001: wrq0302 <= 1;		
	   4'b0011: wrq0304 <= 1;
	   4'b0100: wrq0305 <= 1;
	   4'b0101: wrq0306 <= 1;
	   4'b0110: wrq0307 <= 1;		
	   4'b0111: wrq0308 <= 1;
	   4'b1000: wrq0309 <= 1;
	   4'b1001: wrq0310 <= 1;
	   4'b1010: wrq0311 <= 1;		
	   4'b1011: wrq0312 <= 1;
	   4'b1100: wrq0313 <= 1;
	   4'b1101: wrq0314 <= 1;
	   4'b1110: wrq0315 <= 1;		
	   4'b1111: wrq0316 <= 1;
    endcase
  else
	begin
	   wrq0301 <= 0;
	   wrq0302 <= 0;		
	   wrq0304 <= 0;
	   wrq0305 <= 0;
	   wrq0306 <= 0;
	   wrq0307 <= 0;		
	   wrq0308 <= 0;
	   wrq0309 <= 0;
	   wrq0310 <= 0;
	   wrq0311 <= 0;		
	   wrq0312 <= 0;
	   wrq0313 <= 0;
	   wrq0314 <= 0;
	   wrq0315 <= 0;		
	   wrq0316 <= 0;	 
	end

  always@ (posedge clk)
  if (wrq4)
    case (waddr4)	   
	   4'b0000: wrq0401 <= 1;
	   4'b0001: wrq0402 <= 1;		
	   4'b0010: wrq0403 <= 1;
	   4'b0100: wrq0405 <= 1;
	   4'b0101: wrq0406 <= 1;
	   4'b0110: wrq0407 <= 1;		
	   4'b0111: wrq0408 <= 1;
	   4'b1000: wrq0409 <= 1;
	   4'b1001: wrq0410 <= 1;
	   4'b1010: wrq0411 <= 1;		
	   4'b1011: wrq0412 <= 1;
	   4'b1100: wrq0413 <= 1;
	   4'b1101: wrq0414 <= 1;
	   4'b1110: wrq0415 <= 1;		
	   4'b1111: wrq0416 <= 1;
    endcase
  else
	begin
	   wrq0401 <= 0;
	   wrq0402 <= 0;		
	   wrq0403 <= 0;
	   wrq0405 <= 0;
	   wrq0406 <= 0;
	   wrq0406 <= 0;		
	   wrq0408 <= 0;
	   wrq0409 <= 0;
	   wrq0410 <= 0;
	   wrq0411 <= 0;		
	   wrq0412 <= 0;
	   wrq0413 <= 0;
	   wrq0414 <= 0;
	   wrq0415 <= 0;		
	   wrq0416 <= 0;	 
	end
	 
  always@ (posedge clk)
  if (wrq5)
    case (waddr5)	   
	   4'b0000: wrq0501 <= 1;
	   4'b0001: wrq0502 <= 1;		
	   4'b0010: wrq0503 <= 1;
	   4'b0011: wrq0504 <= 1;
	   4'b0101: wrq0506 <= 1;
	   4'b0110: wrq0507 <= 1;		
	   4'b0111: wrq0508 <= 1;
	   4'b1000: wrq0509 <= 1;
	   4'b1001: wrq0510 <= 1;
	   4'b1010: wrq0511 <= 1;		
	   4'b1011: wrq0512 <= 1;
	   4'b1100: wrq0513 <= 1;
	   4'b1101: wrq0514 <= 1;
	   4'b1110: wrq0515 <= 1;		
	   4'b1111: wrq0516 <= 1;
    endcase
  else
	begin
	   wrq0501 <= 0;
	   wrq0502 <= 0;		
	   wrq0503 <= 0;
	   wrq0504 <= 0;
	   wrq0506 <= 0;
	   wrq0507 <= 0;		
	   wrq0508 <= 0;
	   wrq0509 <= 0;
	   wrq0510 <= 0;
	   wrq0511 <= 0;		
	   wrq0512 <= 0;
	   wrq0513 <= 0;
	   wrq0514 <= 0;
	   wrq0515 <= 0;		
	   wrq0516 <= 0;	 
	end

  always@ (posedge clk)
  if (wrq6)
    case (waddr6)	   
	   4'b0000: wrq0601 <= 1;
	   4'b0001: wrq0602 <= 1;		
	   4'b0010: wrq0603 <= 1;
	   4'b0011: wrq0604 <= 1;
	   4'b0100: wrq0605 <= 1;
	   4'b0110: wrq0607 <= 1;		
	   4'b0111: wrq0608 <= 1;
	   4'b1000: wrq0609 <= 1;
	   4'b1001: wrq0610 <= 1;
	   4'b1010: wrq0611 <= 1;		
	   4'b1011: wrq0612 <= 1;
	   4'b1100: wrq0613 <= 1;
	   4'b1101: wrq0614 <= 1;
	   4'b1110: wrq0615 <= 1;		
	   4'b1111: wrq0616 <= 1;
    endcase
  else
	begin
	   wrq0601 <= 0;
	   wrq0602 <= 0;		
	   wrq0603 <= 0;
	   wrq0604 <= 0;
	   wrq0605 <= 0;
	   wrq0607 <= 0;		
	   wrq0608 <= 0;
	   wrq0609 <= 0;
	   wrq0610 <= 0;
	   wrq0611 <= 0;		
	   wrq0612 <= 0;
	   wrq0613 <= 0;
	   wrq0614 <= 0;
	   wrq0615 <= 0;		
	   wrq0616 <= 0;	 
	end

  always@ (posedge clk)
  if (wrq7)
    case (waddr7)	   
	   4'b0000: wrq0701 <= 1;
	   4'b0001: wrq0702 <= 1;		
	   4'b0010: wrq0703 <= 1;
	   4'b0011: wrq0704 <= 1;
	   4'b0100: wrq0705 <= 1;
	   4'b0101: wrq0706 <= 1;		
	   4'b0111: wrq0708 <= 1;
	   4'b1000: wrq0709 <= 1;
	   4'b1001: wrq0710 <= 1;
	   4'b1010: wrq0711 <= 1;		
	   4'b1011: wrq0712 <= 1;
	   4'b1100: wrq0713 <= 1;
	   4'b1101: wrq0714 <= 1;
	   4'b1110: wrq0715 <= 1;		
	   4'b1111: wrq0716 <= 1;
    endcase
  else
	begin
	   wrq0701 <= 0;
	   wrq0702 <= 0;		
	   wrq0703 <= 0;
	   wrq0704 <= 0;
	   wrq0705 <= 0;
	   wrq0706 <= 0;		
	   wrq0708 <= 0;
	   wrq0709 <= 0;
	   wrq0710 <= 0;
	   wrq0711 <= 0;		
	   wrq0712 <= 0;
	   wrq0713 <= 0;
	   wrq0714 <= 0;
	   wrq0715 <= 0;		
	   wrq0716 <= 0;	 
	end

  always@ (posedge clk)
  if (wrq8)
    case (waddr8)	   
	   4'b0000: wrq0801 <= 1;
	   4'b0001: wrq0802 <= 1;		
	   4'b0010: wrq0803 <= 1;
	   4'b0011: wrq0804 <= 1;
	   4'b0100: wrq0805 <= 1;
	   4'b0101: wrq0806 <= 1;		
	   4'b0110: wrq0807 <= 1;
	   4'b1000: wrq0809 <= 1;
	   4'b1001: wrq0810 <= 1;
	   4'b1010: wrq0811 <= 1;		
	   4'b1011: wrq0812 <= 1;
	   4'b1100: wrq0813 <= 1;
	   4'b1101: wrq0814 <= 1;
	   4'b1110: wrq0815 <= 1;		
	   4'b1111: wrq0816 <= 1;
    endcase
  else
	begin
	   wrq0801 <= 0;
	   wrq0802 <= 0;		
	   wrq0803 <= 0;
	   wrq0804 <= 0;
	   wrq0805 <= 0;
	   wrq0806 <= 0;		
	   wrq0807 <= 0;
	   wrq0809 <= 0;
	   wrq0810 <= 0;
	   wrq0811 <= 0;		
	   wrq0812 <= 0;
	   wrq0813 <= 0;
	   wrq0814 <= 0;
	   wrq0815 <= 0;		
	   wrq0816 <= 0;	 
	end

  always@ (posedge clk)
  if (wrq9)
    case (waddr9)	   
	   4'b0000: wrq0901 <= 1;
	   4'b0001: wrq0902 <= 1;		
	   4'b0010: wrq0903 <= 1;
	   4'b0011: wrq0904 <= 1;
	   4'b0100: wrq0905 <= 1;
	   4'b0101: wrq0906 <= 1;		
	   4'b0110: wrq0907 <= 1;
	   4'b0111: wrq0908 <= 1;
	   4'b1001: wrq0910 <= 1;
	   4'b1010: wrq0911 <= 1;		
	   4'b1011: wrq0912 <= 1;
	   4'b1100: wrq0913 <= 1;
	   4'b1101: wrq0914 <= 1;
	   4'b1110: wrq0915 <= 1;		
	   4'b1111: wrq0916 <= 1;
    endcase
  else
	begin
	   wrq0901 <= 0;
	   wrq0902 <= 0;		
	   wrq0903 <= 0;
	   wrq0904 <= 0;
	   wrq0905 <= 0;
	   wrq0906 <= 0;		
	   wrq0907 <= 0;
	   wrq0908 <= 0;
	   wrq0910 <= 0;
	   wrq0911 <= 0;		
	   wrq0912 <= 0;
	   wrq0913 <= 0;
	   wrq0914 <= 0;
	   wrq0915 <= 0;		
	   wrq0916 <= 0;	 
	end

  always@ (posedge clk)
  if (wrq10)
    case (waddr10)	   
	   4'b0000: wrq1001 <= 1;
	   4'b0001: wrq1002 <= 1;		
	   4'b0010: wrq1003 <= 1;
	   4'b0011: wrq1004 <= 1;
	   4'b0100: wrq1005 <= 1;
	   4'b0101: wrq1006 <= 1;		
	   4'b0110: wrq1007 <= 1;
	   4'b0111: wrq1008 <= 1;
	   4'b1000: wrq1009 <= 1;
	   4'b1010: wrq1011 <= 1;		
	   4'b1011: wrq1012 <= 1;
	   4'b1100: wrq1013 <= 1;
	   4'b1101: wrq1014 <= 1;
	   4'b1110: wrq1015 <= 1;		
	   4'b1111: wrq1016 <= 1;
    endcase
  else
	begin
	   wrq1001 <= 0;
	   wrq1002 <= 0;		
	   wrq1003 <= 0;
	   wrq1004 <= 0;
	   wrq1005 <= 0;
	   wrq1006 <= 0;		
	   wrq1007 <= 0;
	   wrq1008 <= 0;
	   wrq1009 <= 0;
	   wrq1011 <= 0;		
	   wrq1012 <= 0;
	   wrq1013 <= 0;
	   wrq1014 <= 0;
	   wrq1015 <= 0;		
	   wrq1016 <= 0;	 
	end

  always@ (posedge clk)
  if (wrq11)
    case (waddr11)	   
	   4'b0000: wrq1101 <= 1;
	   4'b0001: wrq1102 <= 1;		
	   4'b0010: wrq1103 <= 1;
	   4'b0011: wrq1104 <= 1;
	   4'b0100: wrq1105 <= 1;
	   4'b0101: wrq1106 <= 1;		
	   4'b0110: wrq1107 <= 1;
	   4'b0111: wrq1108 <= 1;
	   4'b1000: wrq1109 <= 1;
	   4'b1001: wrq1110 <= 1;		
	   4'b1011: wrq1112 <= 1;
	   4'b1100: wrq1113 <= 1;
	   4'b1101: wrq1114 <= 1;
	   4'b1110: wrq1115 <= 1;		
	   4'b1111: wrq1116 <= 1;
    endcase
  else
	begin
	   wrq1101 <= 0;
	   wrq1102 <= 0;		
	   wrq1103 <= 0;
	   wrq1104 <= 0;
	   wrq1105 <= 0;
	   wrq1106 <= 0;		
	   wrq1107 <= 0;
	   wrq1108 <= 0;
	   wrq1109 <= 0;
	   wrq1110 <= 0;		
	   wrq1112 <= 0;
	   wrq1113 <= 0;
	   wrq1114 <= 0;
	   wrq1115 <= 0;		
	   wrq1116 <= 0;	 
	end

  always@ (posedge clk)
  if (wrq12)
    case (waddr12)	   
	   4'b0000: wrq1201 <= 1;
	   4'b0001: wrq1202 <= 1;		
	   4'b0010: wrq1203 <= 1;
	   4'b0011: wrq1204 <= 1;
	   4'b0100: wrq1205 <= 1;
	   4'b0101: wrq1206 <= 1;		
	   4'b0110: wrq1207 <= 1;
	   4'b0111: wrq1208 <= 1;
	   4'b1000: wrq1209 <= 1;
	   4'b1001: wrq1210 <= 1;		
	   4'b1010: wrq1211 <= 1;
	   4'b1100: wrq1213 <= 1;
	   4'b1101: wrq1214 <= 1;
	   4'b1110: wrq1215 <= 1;		
	   4'b1111: wrq1216 <= 1;
    endcase
  else
	begin
	   wrq1201 <= 0;
	   wrq1202 <= 0;		
	   wrq1203 <= 0;
	   wrq1204 <= 0;
	   wrq1205 <= 0;
	   wrq1206 <= 0;		
	   wrq1207 <= 0;
	   wrq1208 <= 0;
	   wrq1209 <= 0;
	   wrq1210 <= 0;		
	   wrq1211 <= 0;
	   wrq1213 <= 0;
	   wrq1214 <= 0;
	   wrq1215 <= 0;		
	   wrq1216 <= 0;	 
	end

  always@ (posedge clk)
  if (wrq13)
    case (waddr13)	   
	   4'b0000: wrq1301 <= 1;
	   4'b0001: wrq1302 <= 1;		
	   4'b0010: wrq1303 <= 1;
	   4'b0011: wrq1304 <= 1;
	   4'b0100: wrq1305 <= 1;
	   4'b0101: wrq1306 <= 1;		
	   4'b0110: wrq1307 <= 1;
	   4'b0111: wrq1308 <= 1;
	   4'b1000: wrq1309 <= 1;
	   4'b1001: wrq1310 <= 1;		
	   4'b1010: wrq1311 <= 1;
	   4'b1011: wrq1312 <= 1;
	   4'b1101: wrq1314 <= 1;
	   4'b1110: wrq1315 <= 1;		
	   4'b1111: wrq1316 <= 1;
    endcase
  else
	begin
	   wrq1301 <= 0;
	   wrq1302 <= 0;		
	   wrq1303 <= 0;
	   wrq1304 <= 0;
	   wrq1305 <= 0;
	   wrq1306 <= 0;		
	   wrq1307 <= 0;
	   wrq1308 <= 0;
	   wrq1309 <= 0;
	   wrq1310 <= 0;		
	   wrq1311 <= 0;
	   wrq1312 <= 0;
	   wrq1314 <= 0;
	   wrq1315 <= 0;		
	   wrq1316 <= 0;	 
	end

  always@ (posedge clk)
  if (wrq14)
    case (waddr14)	   
	   4'b0000: wrq1401 <= 1;
	   4'b0001: wrq1402 <= 1;		
	   4'b0010: wrq1403 <= 1;
	   4'b0011: wrq1404 <= 1;
	   4'b0100: wrq1405 <= 1;
	   4'b0101: wrq1406 <= 1;		
	   4'b0110: wrq1407 <= 1;
	   4'b0111: wrq1408 <= 1;
	   4'b1000: wrq1409 <= 1;
	   4'b1001: wrq1410 <= 1;		
	   4'b1010: wrq1411 <= 1;
	   4'b1011: wrq1412 <= 1;
	   4'b1100: wrq1413 <= 1;
	   4'b1110: wrq1415 <= 1;		
	   4'b1111: wrq1416 <= 1;
    endcase
  else
	begin
	   wrq1401 <= 0;
	   wrq1402 <= 0;		
	   wrq1403 <= 0;
	   wrq1404 <= 0;
	   wrq1405 <= 0;
	   wrq1406 <= 0;		
	   wrq1407 <= 0;
	   wrq1408 <= 0;
	   wrq1409 <= 0;
	   wrq1410 <= 0;		
	   wrq1411 <= 0;
	   wrq1412 <= 0;
	   wrq1413 <= 0;
	   wrq1415 <= 0;		
	   wrq1416 <= 0;	 
	end

  always@ (posedge clk)
  if (wrq15)
    case (waddr15)	   
	   4'b0000: wrq1501 <= 1;
	   4'b0001: wrq1502 <= 1;		
	   4'b0010: wrq1503 <= 1;
	   4'b0011: wrq1504 <= 1;
	   4'b0100: wrq1505 <= 1;
	   4'b0101: wrq1506 <= 1;		
	   4'b0110: wrq1507 <= 1;
	   4'b0111: wrq1508 <= 1;
	   4'b1000: wrq1509 <= 1;
	   4'b1001: wrq1510 <= 1;		
	   4'b1010: wrq1511 <= 1;
	   4'b1011: wrq1512 <= 1;
	   4'b1100: wrq1513 <= 1;
	   4'b1101: wrq1514 <= 1;		
	   4'b1111: wrq1516 <= 1;
    endcase
  else
	begin
	   wrq1501 <= 0;
	   wrq1502 <= 0;		
	   wrq1503 <= 0;
	   wrq1504 <= 0;
	   wrq1505 <= 0;
	   wrq1506 <= 0;		
	   wrq1507 <= 0;
	   wrq1508 <= 0;
	   wrq1509 <= 0;
	   wrq1510 <= 0;		
	   wrq1511 <= 0;
	   wrq1512 <= 0;
	   wrq1513 <= 0;
	   wrq1514 <= 0;		
	   wrq1516 <= 0;	 
	end

  always@ (posedge clk)
  if (wrq16)
    case (waddr16)	   
	   4'b0000: wrq1601 <= 1;
	   4'b0001: wrq1602 <= 1;		
	   4'b0010: wrq1603 <= 1;
	   4'b0011: wrq1604 <= 1;
	   4'b0100: wrq1605 <= 1;
	   4'b0101: wrq1606 <= 1;		
	   4'b0110: wrq1607 <= 1;
	   4'b0111: wrq1608 <= 1;
	   4'b1000: wrq1609 <= 1;
	   4'b1001: wrq1610 <= 1;		
	   4'b1010: wrq1611 <= 1;
	   4'b1011: wrq1612 <= 1;
	   4'b1100: wrq1613 <= 1;
	   4'b1101: wrq1614 <= 1;		
	   4'b1110: wrq1615 <= 1;
    endcase
  else
	begin
	   wrq1601 <= 0;
	   wrq1602 <= 0;		
	   wrq1603 <= 0;
	   wrq1604 <= 0;
	   wrq1605 <= 0;
	   wrq1606 <= 0;		
	   wrq1607 <= 0;
	   wrq1608 <= 0;
	   wrq1609 <= 0;
	   wrq1610 <= 0;		
	   wrq1611 <= 0;
	   wrq1612 <= 0;
	   wrq1613 <= 0;
	   wrq1614 <= 0;		
	   wrq1615 <= 0;	 
	end
  
  state_machine st1 (clk, rst, wrq0201, wrq0301, wrq0401, wrq0501, wrq0601, wrq0701, wrq0801, wrq0901, wrq1001, wrq1101, wrq1201, wrq1301, wrq1401, wrq1501, wrq1601,  
                     wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, 
					 wgr0102, wgr0103, wgr0104, wgr0105, wgr0106, wgr0107, wgr0108, wgr0109, wgr0110, wgr0111, wgr0112, wgr0113, wgr0114, wgr0115, wgr0116, we1, wdata_out1);

  state_machine st2 (clk, rst, wrq0102, wrq0302, wrq0402, wrq0502, wrq0602, wrq0702, wrq0802, wrq0902, wrq1002, wrq1102, wrq1202, wrq1302, wrq1402, wrq1502, wrq1602,  
                     wdata_in1, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, 
					 wgr0201, wgr0203, wgr0204, wgr0205, wgr0206, wgr0207, wgr0208, wgr0209, wgr0210, wgr0211, wgr0212, wgr0213, wgr0214, wgr0215, wgr0216, we2, wdata_out2);

  state_machine st3 (clk, rst, wrq0103, wrq0203, wrq0403, wrq0503, wrq0603, wrq0703, wrq0803, wrq0903, wrq1003, wrq1103, wrq1203, wrq1303, wrq1403, wrq1503, wrq1603,  
                     wdata_in1, wdata_in2, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, 
					 wgr0301, wgr0302, wgr0304, wgr0305, wgr0306, wgr0307, wgr0308, wgr0309, wgr0310, wgr0311, wgr0312, wgr0313, wgr0314, wgr0315, wgr0316, we3, wdata_out3);

  state_machine st4 (clk, rst, wrq0104, wrq0204, wrq0304, wrq0504, wrq0604, wrq0704, wrq0804, wrq0904, wrq1004, wrq1104, wrq1204, wrq1304, wrq1404, wrq1504, wrq1604,  
                     wdata_in1, wdata_in2, wdata_in3, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, 
					 wgr0401, wgr0402, wgr0403, wgr0405, wgr0406, wgr0407, wgr0408, wgr0409, wgr0410, wgr0411, wgr0412, wgr0413, wgr0414, wgr0415, wgr0416, we4, wdata_out4);							 

  state_machine st5 (clk, rst, wrq0105, wrq0205, wrq0305, wrq0405, wrq0605, wrq0705, wrq0805, wrq0905, wrq1005, wrq1105, wrq1205, wrq1305, wrq1405, wrq1505, wrq1605,  
                     wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in6, wdata_in7, wdata_in8, wdata_in9, wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, 
					 wgr0501, wgr0502, wgr0503, wgr0504, wgr0506, wgr0507, wgr0508, wgr0509, wgr0510, wgr0511, wgr0512, wgr0513, wgr0514, wgr0515, wgr0516, we5, wdata_out5);

  state_machine st6 (clk, rst, wrq0106, wrq0206, wrq0306, wrq0406, wrq0506, wrq0706, wrq0806, wrq0906, wrq1006, wrq1106, wrq1206, wrq1306, wrq1406, wrq1506, wrq1606,  
                     wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in7, wdata_in8, wdata_in9, wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, 
					 wgr0601, wgr0602, wgr0603, wgr0604, wgr0605, wgr0607, wgr0608, wgr0609, wgr0610, wgr0611, wgr0612, wgr0613, wgr0614, wgr0615, wgr0616, we6, wdata_out6);

  state_machine st7 (clk, rst, wrq0107, wrq0207, wrq0307, wrq0407, wrq0507, wrq0607, wrq0807, wrq0907, wrq1007, wrq1107, wrq1207, wrq1307, wrq1407, wrq1507, wrq1607,  
                     wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in8, wdata_in9, wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, 
					 wgr0701, wgr0702, wgr0703, wgr0704, wgr0705, wgr0706, wgr0708, wgr0709, wgr0710, wgr0711, wgr0712, wgr0713, wgr0714, wgr0715, wgr0716, we7, wdata_out7);

  state_machine st8 (clk, rst, wrq0108, wrq0208, wrq0308, wrq0408, wrq0508, wrq0608, wrq0708, wrq0908, wrq1008, wrq1108, wrq1208, wrq1308, wrq1408, wrq1508, wrq1608,  
                     wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in9, wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, 
					 wgr0801, wgr0802, wgr0803, wgr0804, wgr0805, wgr0806, wgr0807, wgr0809, wgr0810, wgr0811, wgr0812, wgr0813, wgr0814, wgr0815, wgr0816, we8, wdata_out8);

  state_machine st9 (clk, rst, wrq0109, wrq0209, wrq0309, wrq0409, wrq0509, wrq0609, wrq0709, wrq0809, wrq1009, wrq1109, wrq1209, wrq1309, wrq1409, wrq1509, wrq1609,  
                     wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, 
					 wgr0901, wgr0902, wgr0903, wgr0904, wgr0905, wgr0906, wgr0907, wgr0908, wgr0910, wgr0911, wgr0912, wgr0913, wgr0914, wgr0915, wgr0916, we9, wdata_out9);

  state_machine st10 (clk, rst, wrq0110, wrq0210, wrq0310, wrq0410, wrq0510, wrq0610, wrq0710, wrq0810, wrq0910, wrq1110, wrq1210, wrq1310, wrq1410, wrq1510, wrq1610,  
                      wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, 
					  wgr1001, wgr1002, wgr1003, wgr1004, wgr1005, wgr1006, wgr1007, wgr1008, wgr1009, wgr1011, wgr1012, wgr1013, wgr1014, wgr1015, wgr1016, we10, wdata_out10);

  state_machine st11 (clk, rst, wrq0111, wrq0211, wrq0311, wrq0411, wrq0511, wrq0611, wrq0711, wrq0811, wrq0911, wrq1011, wrq1211, wrq1311, wrq1411, wrq1511, wrq1611,  
                      wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, wdata_in10, wdata_in12, wdata_in13, wdata_in14, wdata_in15, wdata_in16, 
					  wgr1101, wgr1102, wgr1103, wgr1104, wgr1105, wgr1106, wgr1107, wgr1108, wgr1109, wgr1110, wgr1112, wgr1113, wgr1114, wgr1115, wgr1116, we11, wdata_out11);

  state_machine st12 (clk, rst, wrq0112, wrq0212, wrq0312, wrq0412, wrq0512, wrq0612, wrq0712, wrq0812, wrq0912, wrq1012, wrq1112, wrq1312, wrq1412, wrq1512, wrq1612,  
                      wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, wdata_in10, wdata_in11, wdata_in13, wdata_in14, wdata_in15, wdata_in16, 
					  wgr1201, wgr1202, wgr1203, wgr1204, wgr1205, wgr1206, wgr1207, wgr1208, wgr1209, wgr1210, wgr1211, wgr1213, wgr1214, wgr1215, wgr1216, we12, wdata_out12);

  state_machine st13 (clk, rst, wrq0113, wrq0213, wrq0313, wrq0413, wrq0513, wrq0613, wrq0713, wrq0813, wrq0913, wrq1013, wrq1113, wrq1213, wrq1413, wrq1513, wrq1613,  
                      wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, wdata_in10, wdata_in11, wdata_in12, wdata_in14, wdata_in15, wdata_in16, 
					  wgr1301, wgr1302, wgr1303, wgr1304, wgr1305, wgr1306, wgr1307, wgr1308, wgr1309, wgr1310, wgr1311, wgr1312, wgr1314, wgr1315, wgr1316, we13, wdata_out13);

  state_machine st14 (clk, rst, wrq0114, wrq0214, wrq0314, wrq0414, wrq0514, wrq0614, wrq0714, wrq0814, wrq0914, wrq1014, wrq1114, wrq1214, wrq1314, wrq1514, wrq1614,  
                      wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in15, wdata_in16, 
					  wgr1401, wgr1402, wgr1403, wgr1404, wgr1405, wgr1406, wgr1407, wgr1408, wgr1409, wgr1410, wgr1411, wgr1412, wgr1413, wgr1415, wgr1416, we14, wdata_out14);

  state_machine st15 (clk, rst, wrq0115, wrq0215, wrq0315, wrq0415, wrq0515, wrq0615, wrq0715, wrq0815, wrq0915, wrq1015, wrq1115, wrq1215, wrq1315, wrq1415, wrq1615,  
                      wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in16, 
					  wgr1501, wgr1502, wgr1503, wgr1504, wgr1505, wgr1506, wgr1507, wgr1508, wgr1509, wgr1510, wgr1511, wgr1512, wgr1513, wgr1514, wgr1516, we15, wdata_out15);

  state_machine st16 (clk, rst, wrq0116, wrq0216, wrq0316, wrq0416, wrq0516, wrq0616, wrq0716, wrq0816, wrq0916, wrq1016, wrq1116, wrq1216, wrq1316, wrq1416, wrq1516,  
                      wdata_in1, wdata_in2, wdata_in3, wdata_in4, wdata_in5, wdata_in6, wdata_in7, wdata_in8, wdata_in9, wdata_in10, wdata_in11, wdata_in12, wdata_in13, wdata_in14, wdata_in15, 
					  wgr1601, wgr1602, wgr1603, wgr1604, wgr1605, wgr1606, wgr1607, wgr1608, wgr1609, wgr1610, wgr1611, wgr1612, wgr1613, wgr1614, wgr1615, we16, wdata_out16);
							
  assign wgr1 = wgr0201 || wgr0301 || wgr0401 || wgr0501 || wgr0601 || wgr0701 || wgr0801 || wgr0901 || wgr1001 || wgr1101 || wgr1201 || wgr1301 || wgr1401 || wgr1501 || wgr1601; 
  assign wgr2 = wgr0102 || wgr0302 || wgr0402 || wgr0502 || wgr0602 || wgr0702 || wgr0802 || wgr0902 || wgr1002 || wgr1102 || wgr1202 || wgr1302 || wgr1402 || wgr1502 || wgr1602;  
  assign wgr3 = wgr0103 || wgr0203 || wgr0403 || wgr0503 || wgr0603 || wgr0703 || wgr0803 || wgr0903 || wgr1003 || wgr1103 || wgr1203 || wgr1303 || wgr1403 || wgr1503 || wgr1603;
  assign wgr4 = wgr0104 || wgr0204 || wgr0304 || wgr0504 || wgr0604 || wgr0704 || wgr0804 || wgr0904 || wgr1004 || wgr1104 || wgr1204 || wgr1304 || wgr1404 || wgr1504 || wgr1604;
  assign wgr5 = wgr0105 || wgr0205 || wgr0305 || wgr0405 || wgr0605 || wgr0705 || wgr0805 || wgr0905 || wgr1005 || wgr1105 || wgr1205 || wgr1305 || wgr1405 || wgr1505 || wgr1605;
  assign wgr6 = wgr0106 || wgr0206 || wgr0306 || wgr0406 || wgr0506 || wgr0706 || wgr0806 || wgr0906 || wgr1006 || wgr1106 || wgr1206 || wgr1306 || wgr1406 || wgr1506 || wgr1606;
  assign wgr7 = wgr0107 || wgr0207 || wgr0307 || wgr0407 || wgr0507 || wgr0607 || wgr0807 || wgr0907 || wgr1007 || wgr1107 || wgr1207 || wgr1307 || wgr1407 || wgr1507 || wgr1607; 
  assign wgr8 = wgr0108 || wgr0208 || wgr0308 || wgr0408 || wgr0508 || wgr0608 || wgr0708 || wgr0908 || wgr1008 || wgr1108 || wgr1208 || wgr1308 || wgr1408 || wgr1508 || wgr1608;  
  assign wgr9 = wgr0109 || wgr0209 || wgr0309 || wgr0409 || wgr0509 || wgr0609 || wgr0709 || wgr0809 || wgr1009 || wgr1109 || wgr1209 || wgr1309 || wgr1409 || wgr1509 || wgr1609;
  assign wgr10 = wgr0110 || wgr0210 || wgr0310 || wgr0410 || wgr0510 || wgr0610 || wgr0710 || wgr0810 || wgr0910 || wgr1110 || wgr1210 || wgr1310 || wgr1410 || wgr1510 || wgr1610;
  assign wgr11 = wgr0111 || wgr0211 || wgr0311 || wgr0411 || wgr0511 || wgr0611 || wgr0711 || wgr0811 || wgr0911 || wgr1011 || wgr1211 || wgr1311 || wgr1411 || wgr1511 || wgr1611;
  assign wgr12 = wgr0112 || wgr0212 || wgr0312 || wgr0412 || wgr0512 || wgr0612 || wgr0712 || wgr0812 || wgr0912 || wgr1012 || wgr1112 || wgr1312 || wgr1412 || wgr1512 || wgr1612;
  assign wgr13 = wgr0113 || wgr0213 || wgr0313 || wgr0413 || wgr0513 || wgr0613 || wgr0713 || wgr0813 || wgr0913 || wgr1013 || wgr1113 || wgr1213 || wgr1413 || wgr1513 || wgr1613;  
  assign wgr14 = wgr0114 || wgr0214 || wgr0314 || wgr0414 || wgr0514 || wgr0614 || wgr0714 || wgr0814 || wgr0914 || wgr1014 || wgr1114 || wgr1214 || wgr1314 || wgr1514 || wgr1614;
  assign wgr15 = wgr0115 || wgr0215 || wgr0315 || wgr0415 || wgr0515 || wgr0615 || wgr0715 || wgr0815 || wgr0915 || wgr1015 || wgr1115 || wgr1215 || wgr1315 || wgr1415 || wgr1615;
  assign wgr16 = wgr0116 || wgr0216 || wgr0316 || wgr0416 || wgr0516 || wgr0616 || wgr0716 || wgr0816 || wgr0916 || wgr1016 || wgr1116 || wgr1216 || wgr1316 || wgr1416 || wgr1516;

endmodule