`include "parameter.v"
module PE_8x1(clk, rst, x_router, y_router, isend, iaccept, iflit, osend, oaccept, oflit);

  input clk, rst;  
  input [2:0] x_router, y_router;   
  input isend, iaccept;
  input [flit_data_width-1:0] iflit;
  output osend, oaccept;
  output [flit_data_width-1:0] oflit;
  
  wire w_start, re_i, re_w, we_w_1, we_w_2, we_w_3, we_w_4, we_w_5, we_w_6, we_w_7, we_w_8, re_o_1, re_o_2, re_o_3, re_o_4, re_o_5, re_o_6, re_o_7, re_o_8, we_o, rst_acc, output_start;
  wire [5:0] waddr;
  wire [15:0] idata, input_values, weights_1, weights_2, weights_3, weights_4, weights_5, weights_6, weights_7, weights_8;  
  wire [11:0] result_1, result_2, result_3, result_4, result_5, result_6, result_7, result_8, odata_1, odata_2, odata_3, odata_4, odata_5, odata_6, odata_7, odata_8; 
  
  NoC_interface_8x1 NoC_interface (clk, rst, x_router, y_router,
								   isend, iaccept, iflit, 
								   done_hidden, done_output, odata_1, odata_2, odata_3, odata_4, odata_5, odata_6, odata_7, odata_8,
								   osend, oaccept, oflit, 
								   w_start, we_i, we_w_1, we_w_2, we_w_3, we_w_4, we_w_5, we_w_6, we_w_7, we_w_8, 
								   re_o_1, re_o_2, re_o_3, re_o_4, re_o_5, re_o_6, re_o_7, re_o_8, waddr, idata);
								   
  control_1 control (clk, rst, we_i, re_i, re_w, we_o, rst_acc, done_hidden, done_output);
  
  RAM_i RAM_i (clk, rst, re_i, we_i, w_start, waddr, idata, output_start, input_values);
  
  RAM_w RAM_w_1 (clk, rst, re_w, we_w_1, idata, weights_1);
  RAM_w RAM_w_2 (clk, rst, re_w, we_w_2, idata, weights_2);
  RAM_w RAM_w_3 (clk, rst, re_w, we_w_3, idata, weights_3);
  RAM_w RAM_w_4 (clk, rst, re_w, we_w_4, idata, weights_4);   
  RAM_w RAM_w_5 (clk, rst, re_w, we_w_5, idata, weights_5);
  RAM_w RAM_w_6 (clk, rst, re_w, we_w_6, idata, weights_6);
  RAM_w RAM_w_7 (clk, rst, re_w, we_w_7, idata, weights_7);
  RAM_w RAM_w_8 (clk, rst, re_w, we_w_8, idata, weights_8);
  
  RAM_o RAM_o_1 (clk, rst, re_o_1, we_o, result_1, odata_1);
  RAM_o RAM_o_2 (clk, rst, re_o_2, we_o, result_2, odata_2);
  RAM_o RAM_o_3 (clk, rst, re_o_3, we_o, result_3, odata_3);
  RAM_o RAM_o_4 (clk, rst, re_o_4, we_o, result_4, odata_4);  
  RAM_o RAM_o_5 (clk, rst, re_o_5, we_o, result_5, odata_5);
  RAM_o RAM_o_6 (clk, rst, re_o_6, we_o, result_6, odata_6);
  RAM_o RAM_o_7 (clk, rst, re_o_7, we_o, result_7, odata_7);
  RAM_o RAM_o_8 (clk, rst, re_o_8, we_o, result_8, odata_8); 
  
  neuron_1x neuron_1 (clk, rst, rst_acc, input_values, weights_1, result_1);
  neuron_1x neuron_2 (clk, rst, rst_acc, input_values, weights_2, result_2);
  neuron_1x neuron_3 (clk, rst, rst_acc, input_values, weights_3, result_3);
  neuron_1x neuron_4 (clk, rst, rst_acc, input_values, weights_4, result_4);
  neuron_1x neuron_5 (clk, rst, rst_acc, input_values, weights_5, result_5);
  neuron_1x neuron_6 (clk, rst, rst_acc, input_values, weights_6, result_6);
  neuron_1x neuron_7 (clk, rst, rst_acc, input_values, weights_7, result_7);
  neuron_1x neuron_8 (clk, rst, rst_acc, input_values, weights_8, result_8);  
  
endmodule