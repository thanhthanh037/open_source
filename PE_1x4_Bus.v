`include "parameter.v"
module PE_1x4(clk, rst, source_node, wgr, broadcast, we, idata, wrq, waddr, odata);

  input clk, rst, wgr, broadcast, we;
  input [5:0] source_node;
  input [data_width-1:0] idata;
  output wrq;
  output waddr;
  output [data_width-1:0] odata;

  wire w_start, re_i, we_i, re_w, we_w, re_o, we_o, rst_acc, done_hidden, done_output;
  wire [5:0] waddr_i;
  wire [data_width-1:0] wdata;
  wire [data_width-1:0] input_values;
  wire [data_width-1:0] weights;  
  wire [11:0] result, odata_1;
			 
  Bus_interface_1x4 Bus_interface (clk, rst, source_node,
								   wgr, broadcast, we, idata, 
								   done_hidden, done_output, odata_1,		    
							       w_start, we_i, we_w, 
								   re_o, waddr_i, wdata,
								   wrq, waddr, odata);
											  
  control_4 control (clk, rst, we_i, re_i, re_w, we_o, rst_acc, done_hidden, done_output);
  
  RAM_i_4 RAM_i (clk, rst, re_i, we_i, w_start, waddr, idata, input_values);
  
  RAM_w_4 RAM_w (clk, rst, re_w, we_w, idata, weights);   
  
  RAM_o RAM_o (clk, rst, re_o, we_o, result, odata_1); 
  
  neuron_4x neuron_1 (clk, rst, rst_acc, input_values[15:0], input_values[31:16], input_values[47:32], input_values[63:48], weights[15:0], weights[31:16], weights[47:32], weights[63:48], result);

  
endmodule