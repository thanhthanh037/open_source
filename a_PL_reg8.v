module a_PL_reg8 (clk, sign2, exp_nor6, man_sum2, zero_no, sign3, exp_nor7, man_sum3, zero_no1);

  input clk, sign2;
  input [10:0] exp_nor6;
  input [53:0] man_sum2;
  input [5:0] zero_no;
  output reg sign3;
  output reg [10:0] exp_nor7;
  output reg [53:0] man_sum3;
  output reg [5:0] zero_no1;
  
  always@ (posedge clk)
  begin
    sign3 <= sign2;
    exp_nor7 <= exp_nor6;
    man_sum3 <= man_sum2;
    zero_no1 <= zero_no;
  end
  
endmodule