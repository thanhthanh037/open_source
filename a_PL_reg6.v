module a_PL_reg6 (clk, exp_nor2, sign, man_sum, exp_nor3, sign1, man_sum1);

  input clk;
  input sign;
  input [10:0] exp_nor2;
  input [53:0] man_sum;
  output reg sign1;
  output reg [10:0] exp_nor3;
  output reg [53:0] man_sum1;

  always@ (posedge clk)
  begin
	   sign1 <= sign;
	   exp_nor3 <= exp_nor2;
	   man_sum1 <= man_sum;
  end

endmodule